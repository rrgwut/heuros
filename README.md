# README #

THIS REPO IS DEPRECATED. THE NEW VERSION OF HEUROS CAN BE FOUND [HERE](https://bitbucket.org/rrgwut/heuros2)

Heuros is a natual-inspired system of 3D visual object recognition. It is intended to run on Ubuntu Linux using ROS and PCL. It also requires a GPU with CUDA technology. For information on set up and usage refer to [the wiki page](https://bitbucket.org/rrgwut/heuros/wiki).

### Authors ###

* [Bogdan Harasymowicz-Boggio](http://repo.bg.pw.edu.pl/index.php/en/r#/info/author/WUT392715/Bogdan%252CHarasymowicz-Boggio)
* Łukasz Chechliński
* Daniel Koguciuk

Warsaw University of Technology