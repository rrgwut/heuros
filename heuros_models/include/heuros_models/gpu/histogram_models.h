#ifndef HISTOGRAMS_GPU_H
#define HISTOGRAMS_GPU_H

#include <iostream>
#include <heuros_core/metacloud_gpu.h>
#include <heuros_core/backprojection_models.h>
#include <heuros_core/timers_gpu.h>

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif

using namespace std;
using namespace pcl::gpu;

class HistogramsModels
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    HistogramsModels();

    /** \brief Calculate all the possible 2D feature histograms. */
    void calcAllHistograms2D( MetacloudGPUConstPtr metacloud_gpu,
                              IndicesConstPtr h_indices,
                              HistogramsPtr &d_histograms,
                              HistogramsPtr &d_lut );

    /** \brief Detect important features and create the model. */
    void addClass( vector<HistogramsPtr> views,
                   BackprojectionModelsPtr models,
                   int points_sum,
                   int class_idx );

    /** \brief Create the 2D histogram masks for backprojection. */
    void addViewMask( HistogramsPtr unsorted_histograms,
                      BackprojectionModelsPtr models,
                      float mask_threshold,
                      int class_idx );

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

protected:

    /** \brief GPU timer events. */
    cudaEvent_t start_all, stop_a, stop_b, stop_c, stop_d;
};

#endif
