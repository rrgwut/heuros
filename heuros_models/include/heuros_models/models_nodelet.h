#ifndef MODELS_NODELET_H
#define MODELS_NODELET_H

#include <iostream>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PointStamped.h>

#include <heuros_core/StampedAddress.h>
#include <heuros_core/GetStringList.h>
#include <heuros_core/SimpleInstruction.h>
#include <models_manager.h>

using namespace std;

namespace heuros_models
{

class ModelsNodelet : public nodelet::Nodelet
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief On nodelet initialization. */
    virtual void onInit();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Features manager object. */
    ModelsManager models_manager;

    /** \brief Metacloud shared pointer. */
    MetacloudConstPtr metacloud;

    /** \brief Segmentation result indices. */
    IndicesConstPtr indices;

    /** \brief Metacloud subscriber. */
    ros::Subscriber sub_metacloud;

    /** \brief Cluster subscriber. */
    ros::Subscriber sub_cluster;

    /** \brief Models publisher. */
    ros::Publisher pub_models;

    /** \brief Class names publisher. */
    ros::Publisher pub_class_names;

    /** \brief Processing done publisher. */
    ros::Publisher pub_done;

    /** \brief Simple instruction service. */
    ros::ServiceServer srv_instruction;

    /** \brief Get string list service. */
    ros::ServiceServer srv_get_string_list;

    /** \brief Processing done message. */
    std_msgs::String::Ptr done_msg;

    /** \brief Variables that control whether to accept new scenes and clusters. */
    bool accept_scene, accept_cluster;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Metacloud received callback. */
    void metacloudCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Cluster indices received callback. */
    void clusterCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Simple instructions service function. */
    bool instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp );

    /** \brief Get file list service function. */
    bool getStringListSrv( heuros_core::GetStringListRequest &req, heuros_core::GetStringListResponse &resp );
};

}

#endif
