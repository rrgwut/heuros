#ifndef MODELS_MANAGER_H
#define MODELS_MANAGER_H

#include <iostream>
#include <fstream>
#include <ros/ros.h>
#include <ros/package.h>

#include <heuros_core/metacloud.h>
#include <heuros_core/metacloud_gpu.h>
#include <heuros_core/StampedAddress.h>
#include <histogram_models.h>

using namespace std;

class ModelsManager
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief All the input views. */
    vector<vector<HistogramsPtr> > input_views;

    /** \brief Histogram-feature lookup table. */
    HistogramsPtr histograms_lut;

    /** \brief Vector of class names. */
    vector<string> class_names;

    /** \brief Model objects. */
    BackprojectionModelsPtr models;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

     /** \brief Constructor. */
    ModelsManager();

    /** \brief Initialize the views and object models. */
    void initializeModels();

    /** \brief Add a new object view for building the model. */
    void addView( MetacloudConstPtr metacloud, IndicesConstPtr indices );

    /** \brief Create the object models from gathered data. */
    void createModels();

    /** \brief Save the object models to file. */
    void saveModels( string file_name );

    /** \brief Load the object models from file. */
    void loadModels( string file_name );

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief GPU Histograms object. */
    HistogramsModels histogram_models;

    /** \brief Accumulated number of points in the views of each class. */
    vector<int> points_sums;

    /** \brief Number of histograms. */
    int n_hists;
};

#endif
