#include <models_manager.h>

ModelsManager::ModelsManager()
{
    initializeModels();
}

void ModelsManager::initializeModels()
{
    input_views.clear();
    class_names.clear();
    points_sums.clear();
}

void ModelsManager::addView( MetacloudConstPtr metacloud, IndicesConstPtr indices )
{
    // Read prefix
    ros::NodeHandle n;
    string prefix;
    n.param( "heuros/prefix", prefix, string("") );

    int class_idx = -1;
    for( int i=0; i<input_views.size(); i++ ){
        if( prefix == class_names[i] )
            class_idx = i;
    }

    if( class_idx < 0 ){
        class_idx = input_views.size();
        class_names.push_back( prefix );
        input_views.push_back( vector<HistogramsPtr>() );
        points_sums.push_back(0);
    }

    MetacloudGPUConstPtr metacloud_gpu = ADDRESS_2_METACLOUD_GPU(metacloud->metacloud_gpu);
    HistogramsPtr view_histograms( new FeaturesGPU<int> );
    histogram_models.calcAllHistograms2D( metacloud_gpu, indices, view_histograms, histograms_lut );
    input_views[class_idx].push_back( view_histograms );

    // Keep record of the points ammounts for each class
    points_sums[class_idx] += indices->size();

    // Calculate the number of histograms
    int n_ftrs = metacloud_gpu->numFeatures();
    n_hists = n_ftrs*(n_ftrs-1)/2;

    cout << "Added new model view" << endl;
}

void ModelsManager::createModels()
{
    if( input_views.size() == 0 ) return;

    models = BackprojectionModelsPtr( new BackprojectionModels( n_hists, input_views.size() ) );
    models->histograms_lut = histograms_lut;
    models->class_names = class_names;

    // Get the relevant features for each object and create the object test masks
    for( int i=0; i<input_views.size(); i++ ){
        histogram_models.addClass( input_views[i], models, points_sums[i], i );
    }
}

void ModelsManager::saveModels( string file_name )
{

    if( file_name == "" )
        file_name = "default.bpm";
    string path = ros::package::getPath("heuros_core") + "/../data/models/" + file_name;
    ofstream file( path.c_str() );

    // General
    file << "num_histograms:" << endl;
    file << models->numHistograms() << endl;
    file << "num_objects:" << endl;
    file << models->numObjects() << endl;

    // Class names
    file << endl << "class_names:" << endl;
    for( int i=0; i<models->class_names.size(); i++ )
        file << models->class_names[i] << endl;

    // Lut
    vector<int> h_lut;
    models->histograms_lut->array->download( h_lut );
    file << endl << "histogram-feature_LUT:" << endl;
    for( int i=0; i<models->numHistograms(); i++ )
        file << h_lut[2*i] << " " << h_lut[2*i+1] << endl;

    // Relevance
    vector<unsigned int> h_relevance;
    models->histograms_relevance->download( h_relevance );
    file << endl << "histograms_relevance:" << endl;
    for( int i=0; i<models->numHistograms(); i++ ){
        for( int j=0; j<models->numNinjaSets(); j++ ){
            file << h_relevance[i*models->numNinjaSets() + j] << " ";
        }
        file << endl;
    }

    // Tests
    vector<unsigned int> h_tests;
    models->objects_test->download( h_tests );
    file << endl << "histogram_tests:" << endl;
    for( int i=0; i<models->numHistograms(); i++ ){
        for( int j=0; j<HIST_STEP*models->numNinjaSets(); j++ ){
            file << h_tests[i*HIST_STEP*models->numNinjaSets() + j] << " ";
        }
        file << endl;
    }

    // Close the files
    file.close();
}

void ModelsManager::loadModels( string file_name )
{
    if( file_name == "" )
        file_name = "default.bpm";
    string path = ros::package::getPath("heuros_core") + "/../data/models/" + file_name;

    ifstream file( path.c_str() );
    if( !file.good() ){
        cout << "Error: File not found: " << file_name << endl;
        return;
    }

    // General
    string line;
    int n_objects;
    file >> line;
    file >> n_hists;
    file >> line;
    file >> n_objects;

    // Create models object
    models = BackprojectionModelsPtr( new BackprojectionModels( n_hists, n_objects ) );

    // Class names
    file >> line;
    models->class_names.resize( n_objects );
    for( int i=0; i<n_objects; i++ )
        file >> models->class_names[i];

    // Lut
    file >> line;
    vector<int> h_lut( n_hists*2 );
    for( int i=0; i<models->numHistograms(); i++ )
        file >> h_lut[2*i] >> h_lut[2*i+1];
    models->histograms_lut->array->upload( h_lut );

    // Relevance
    file >> line;
    vector<unsigned int> h_relevance( n_hists*models->numNinjaSets() );
    for( int i=0; i<models->numHistograms(); i++ )
        for( int j=0; j<models->numNinjaSets(); j++ )
            file >> h_relevance[i*models->numNinjaSets() + j];
    models->histograms_relevance->upload( h_relevance );

    // Tests
    file >> line;
    vector<unsigned int> h_tests( n_hists*HIST_STEP*models->numNinjaSets() );
    for( int i=0; i<models->numHistograms(); i++ )
        for( int j=0; j<HIST_STEP*models->numNinjaSets(); j++ )
            file >> h_tests[i*HIST_STEP*models->numNinjaSets() + j];
    models->objects_test->upload( h_tests );

    // Close the file
    file.close();

    // Cleanup just in case
    initializeModels();
}

