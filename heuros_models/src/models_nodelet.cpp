#include <models_nodelet.h>
// this should really be in the implementation (.cpp file)
#include <pluginlib/class_list_macros.h>

namespace heuros_models
{

void ModelsNodelet::onInit()
{
    cout << "Starting Heuros Models..." << endl;

    accept_scene = false;
    accept_cluster = false;
    ros::NodeHandle n;

    // Set up subscribers
    sub_metacloud = n.subscribe( "/heuros/seg_metacloud", 1, &ModelsNodelet::metacloudCb , this );
    sub_cluster = n.subscribe( "/heuros/cluster", 1, &ModelsNodelet::clusterCb , this );

    // Set up publishers
    pub_models = n.advertise<heuros_core::StampedAddress>( "/heuros/backprojection_models", 1, true );
    pub_done = n.advertise<std_msgs::String>( "/heuros/done", 1 );
    done_msg = std_msgs::String::Ptr( new std_msgs::String );
    done_msg->data = "models";

    // Set up services
    srv_instruction = n.advertiseService( "/heuros/mod_instruction", &ModelsNodelet::instructionSrv, this );
    srv_get_string_list = n.advertiseService("/heuros/get_class_names", &ModelsNodelet::getStringListSrv, this);
}

void ModelsNodelet::metacloudCb( const heuros_core::StampedAddressConstPtr &msg )
{
    // Accept only if instructed
    if( !accept_scene ) return;
    accept_scene = false;

    cout << "Models: Metacloud received" << endl;
    metacloud = ADDRESS_2_METACLOUD(msg->address);
}

void ModelsNodelet::clusterCb( const heuros_core::StampedAddressConstPtr &msg )
{
    // Accept only if instructed
    if( !accept_cluster ) return;
    accept_cluster = false;

    cout << "Models: Cluster indices received" << endl;
    indices = ADDRESS_2_INDICES(msg->address);

    // Models initialized on each cluster for test purposes
    models_manager.addView( metacloud, indices );

    // Publish done message
    pub_done.publish( done_msg );
}

bool ModelsNodelet::getStringListSrv( heuros_core::GetStringListRequest &req, heuros_core::GetStringListResponse &resp )
{
    cout << "Models: Received get class names request" << endl;

    if( req.query == "props" ){
        if( !models_manager.models )
            return false;
        resp.string_list = models_manager.models->class_names;
    }

    return true;
}

// ------------------------------
// ----- INSTRUCTION SERVER -----
// ------------------------------

bool ModelsNodelet::instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp )
{
    // ================== ADD CLUSTER ==================

    if( req.instruction == "add_cluster" )
    {
        cout << "Models: Received add cluster instruction" << endl;
        accept_scene = true;
        accept_cluster = true;
    }

    // ================== CREATE MODELS ==================

    if( req.instruction == "create_models" )
    {
        cout << "Models: Received create models instruction" << endl;
        models_manager.createModels();
        models_manager.saveModels( req.parameter );

        heuros_core::StampedAddressPtr models_msg( new heuros_core::StampedAddress );
        models_msg->address = PTR_2_ADDRESS( models_manager.models );
        pub_models.publish( models_msg );
    }

    // ================== LOAD MODELS ==================

    if( req.instruction == "load_models" )
    {
        cout << "Models: Received load models instruction" << endl;
        models_manager.loadModels( req.parameter );

        heuros_core::StampedAddressPtr models_msg( new heuros_core::StampedAddress );
        models_msg->address = PTR_2_ADDRESS( models_manager.models );
        pub_models.publish( models_msg );
    }

    // ================== RESET MODELS ==================

    if( req.instruction == "reset_models" )
    {
        cout << "Models: Received reset models instruction" << endl;
        models_manager.initializeModels();
    }

    // ==================

    return true;
}

}

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(heuros_models::ModelsNodelet, nodelet::Nodelet)
