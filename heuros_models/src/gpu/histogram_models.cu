#include <histogram_models.h>
#include <stdio.h>

// MAIN ADJUSTABLE PARAMETERS
#define SINGLE_VIEW_THRESHOLD 0.2
#define SUM_VIEW_THRESHOLD 0.6
#define RELEVANCE_THRESHOLD 0.4

// -------------------------
// NOTE: Another good configuration is 0.3, 0.5, 0.4
// -------------------------

__device__ int pow( int x, int y )
{
    int result = 1;
    for( int i=0; i<y; i++ )
        result *= x;
    return result;
}

__global__ void extractFeaturesCluster( const float *features_src,
                                        float *features_tgt,
                                        const int *indices,
                                        int src_step,
                                        int tgt_step,
                                        int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        // Feature index
        int ftr_idx = idx / tgt_step;
        // Index of the source point (read from indices)
        int src_point = *(indices + idx%tgt_step);
        // Index of the source feature value
        int src_idx = ftr_idx*src_step + src_point;
        // tgt_val = src_val
        *(features_tgt + idx) = *(features_src + src_idx);
    }
}

__global__ void calcualteHistograms( const float *features,
                                     int *histograms,
                                     const int *histograms_lut,
                                     int ftrs_step,
                                     int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        // Calculate the histogram and point index
        // H1:p1p2p3 H2:p1p2p3 H3:p1p2p3 ...
        int hist_idx = idx / ftrs_step;
        int point_idx = idx % ftrs_step;

        // Read the two concerning feature indices (LUT could be in global memory!)
        int ftr1_idx = *(histograms_lut+hist_idx*2);
        int ftr2_idx = *(histograms_lut+hist_idx*2+1);

        // Read the two concering feature values
        float ftr1_val = *(features + ftr1_idx*ftrs_step + point_idx);
        float ftr2_val = *(features + ftr2_idx*ftrs_step + point_idx);

        // Count only non-nans
        if( ftr1_val == ftr1_val && ftr2_val == ftr2_val ){
            // Calculate the bins
            int ftr1_bin = 0.5*(ftr1_val+0.99) * HIST_BINS;
            int ftr2_bin = 0.5*(ftr2_val+0.99) * HIST_BINS;

            // AtomicAdd
            int *bin_ptr = histograms + hist_idx*HIST_STEP + ftr1_bin*HIST_BINS + ftr2_bin;
            atomicAdd( bin_ptr, 1 );
        }
    }
}

__global__ void sumArrays( const int *src,
                           int *tgt,
                           int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        *(tgt+idx) += *(src+idx);
    }
}

#define RADIX_BITS 4
#define POW_2_RB 16 // 2^RADIX_BITS

__global__ void radixSort( const int *histograms,
                           int *sorted_histograms,
                           int *sorted_addresses,
                           int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){

        // Copy the histograms to shared memory
        __shared__ int keys[HIST_STEP];
        *(keys+threadIdx.x) = *(histograms+idx);
        //printf("Thread %d; Key %u \n", threadIdx.x, *(keys+threadIdx.x));

        // Create the values (histogram indexes) in shared memory
        __shared__ int values[HIST_STEP];
        *(values+threadIdx.x) = threadIdx.x;

        // Create the scan arrays in shared memory
        __shared__ unsigned short int bin_scan_arrays[POW_2_RB * HIST_STEP];

        __syncthreads(); // Where do we need this?

        for( int i=0; i<sizeof(int)*8/RADIX_BITS; i++ ){

            // Get the key
            int key = *(keys+threadIdx.x);

            // Get the bin
            int radix_bin_idx = (key>>i) & 0x000F;

            // Initialize the bin_scan_arrays
            for( int j=0; j<POW_2_RB; j++ ){
                if( radix_bin_idx == j )
                    bin_scan_arrays[ threadIdx.x*POW_2_RB + j ] = 1;
                else
                    bin_scan_arrays[ threadIdx.x*POW_2_RB + j ] = 0;
            }

            // Synchronize
            __syncthreads();

            // Scan (400 vals so 2^9=512)
            for( int j=0; j<9; j++ ){
                int jump = pow(2,j);
                for( int k=0; k<POW_2_RB; k++ ){
                    // Read
                    unsigned short int neighbor;
                    if( threadIdx.x<jump )
                        neighbor = 0; // NOT COALESCED
                    else
                        neighbor = bin_scan_arrays[ (threadIdx.x-jump)*POW_2_RB + k ];

                    // Synchronize
                    __syncthreads();

                    // Write
                    bin_scan_arrays[ threadIdx.x*POW_2_RB + k ] += neighbor;

                    // Synchronize
                    __syncthreads();
                }
            }

            // Calculate target address
            unsigned short int target_address = 0;

            // Sum every final bin (last scan element) before ours
            for( int j=0; j<radix_bin_idx; j++ )
                target_address += bin_scan_arrays[ (HIST_STEP-1)*POW_2_RB + j ];

            // Add our local scan result - 1
            target_address += bin_scan_arrays[ threadIdx.x*POW_2_RB + radix_bin_idx ] - 1;

            // Update the keys and values in the sorted arrays

            // Read values
            int tmp_key = *(keys+threadIdx.x);
            int tmp_value = *(values+threadIdx.x);

            // Synchronize
            __syncthreads();

            // Write
            *(keys+target_address) = tmp_key;
            *(values+target_address) = tmp_value;

            // Synchronize
            __syncthreads();
        }

        // Copy to global arrays
        *(sorted_histograms+idx) = *(keys+threadIdx.x);
        *(sorted_addresses+idx) = *(values+threadIdx.x);
    }
}

__global__ void thresholdHistograms( const int *sorted_histograms,
                                     const int *sorted_addresses,
                                     unsigned int *ninja_test,
                                     int num_ninja_sets,
                                     float mask_threshold,
                                     int class_idx,
                                     int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if( idx<N ){

        // Get histogram index
        int hist_idx = idx / HIST_STEP;

        // Create scan array in shared memory
        __shared__ int values_scan[HIST_STEP];
        *(values_scan+threadIdx.x) = *(sorted_histograms+idx);

        // Synchronize
        __syncthreads();

        // Scan (400 vals so 2^9=512)
        for( int i=0; i<9; i++ ){
            int jump = pow(2,i);

            // Read
            int neighbor;
            if( threadIdx.x<jump )
                neighbor = 0;
            else
                neighbor = *(values_scan+threadIdx.x - jump);

            // Synchronize
            __syncthreads();

            // Write
            *(values_scan+threadIdx.x) += neighbor;

            // Synchronize
            __syncthreads();
        }

        // Set the corresponidng bit to 1 if the scan value < histogram sum * THRESHOLD
        if( *(values_scan+threadIdx.x) < *(values_scan+HIST_STEP-1) * mask_threshold ){
            // Get ninja address
            int tgt_idx = hist_idx*HIST_STEP + *(sorted_addresses+idx);
            int ninja_idx = tgt_idx * num_ninja_sets + class_idx/32;
            // Bitwise and (set to 0 the corresponding bin)
            *(ninja_test + ninja_idx) &= ~(1<<class_idx);
        }else{
            // leave the value unchanged
        }
    }
}

__global__ void sumPositives( const int *histograms,
                              const unsigned int *ninja_test,
                              int *histogram_positives,
                              int num_ninja_sets,
                              int class_idx,
                              int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if( idx<N ){

        // Get histogram index
        int hist_idx = idx / HIST_STEP;

        // Get histogram value
        int hist_val = *(histograms+idx);

        // Get the object test value
        int ninja_idx = idx * num_ninja_sets + class_idx/32;
        int obj_test = *(ninja_test+ninja_idx) & (1<<class_idx) ? 1 : 0;

        // Create scan array in shared memory
        __shared__ int values_scan[HIST_STEP];
        *(values_scan+threadIdx.x) = obj_test * hist_val;

        // Synchronize
        __syncthreads();

        // Scan (400 vals so 2^9=512)
        for( int i=0; i<9; i++ ){
            int jump = pow(2,i);

            // Read
            int neighbor;
            if( threadIdx.x<jump )
                neighbor = 0;
            else
                neighbor = *(values_scan+threadIdx.x - jump);

            // Synchronize
            __syncthreads();

            // Write
            *(values_scan+threadIdx.x) += neighbor;

            // Synchronize
            __syncthreads();
        }

        // If this is the last thread, copy the value to results
        if( threadIdx.x == HIST_STEP-1 )
            *(histogram_positives+hist_idx) = *(values_scan+threadIdx.x);
    }
}

HistogramsModels::HistogramsModels()
{
    cudaEventCreate( &start_all );
    cudaEventCreate( &stop_a );
    cudaEventCreate( &stop_b );
    cudaEventCreate( &stop_c );
    cudaEventCreate( &stop_d );
}

void HistogramsModels::calcAllHistograms2D( MetacloudGPUConstPtr metacloud_gpu,
                                         IndicesConstPtr h_indices,
                                         HistogramsPtr &d_histograms,
                                         HistogramsPtr &d_lut )
{
    cout << "Calculating object histograms..." << endl;

    // ================== TRANSFER THE MODEL TO GPU ==================

    // Prepare data structures
    FeaturesGPU<float> d_object_features( metacloud_gpu->numFeatures(), h_indices->size() );
    DeviceArray<int> d_indices( h_indices->size() );
    d_indices.upload( h_indices->data(), h_indices->size() );

    // Call extractFeaturesCluster
    int N = d_indices.size() * metacloud_gpu->numFeatures();
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Kernel (each thread copies only a single float - one feature value at one point)
    extractFeaturesCluster <<< nBlocks, blockSize >>> ( metacloud_gpu->features->array->ptr(),
                                                        d_object_features.array->ptr(),
                                                        d_indices.ptr(),
                                                        metacloud_gpu->size(),
                                                        d_indices.size(),
                                                        N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // ================== CREATE THE HISTOGRAM STRUCTURES ==================

    // Initialize structures
    int n_ftrs = metacloud_gpu->numFeatures();
    int n_hists = n_ftrs*(n_ftrs-1)/2;
    d_histograms = HistogramsPtr( new FeaturesGPU<int> );
    d_histograms->create( n_hists, HIST_STEP );
    cudaMemset( d_histograms->data(), 0, d_histograms->array->size()*sizeof(int) );

    // Create lookup tables
    vector<int> h_lut;
    for( int f1=0; f1<n_ftrs-1; f1++ ){
        for( int f2=f1+1; f2<n_ftrs; f2++ ){
            h_lut.push_back(f1);
            h_lut.push_back(f2);
        }
    }
    d_lut = HistogramsPtr( new FeaturesGPU<int> );
    d_lut->create( n_hists, 2 );
    d_lut->array->upload( h_lut.data(), h_lut.size() );

    // ================== CALCULATE HISTOGRAMS ==================

    N = d_indices.size() * n_hists;
    blockSize = 512;
    nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Kernel
    calcualteHistograms <<< nBlocks, blockSize >>> ( d_object_features.data(),
                                                     d_histograms->data(),
                                                     d_lut->data(),
                                                     d_object_features.step,
                                                     N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
}

void HistogramsModels::addClass( vector<HistogramsPtr> views,
                              BackprojectionModelsPtr models,
                              int points_sum,
                              int class_idx )
{
    // ================== CREATE THE COMBINED CLASS MASK ==================

    // Create histogram sum
    HistogramsPtr sum_view( new FeaturesGPU<int> );
    sum_view->create( views[0]->num_features, HIST_STEP );
    cudaMemset( sum_view->data(), 0, sum_view->array->sizeBytes() );

    for( int i=0; i<views.size(); i++ ){
        // Reject bins without enough occurrences in every instance
        addViewMask( views[i], models, SINGLE_VIEW_THRESHOLD, class_idx );

        // Sum histograms
        int N = views[i]->array->size();
        int blockSize = 512;
        int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

        // Kernel
        sumArrays <<< nBlocks, blockSize >>> ( views[i]->data(),
                                               sum_view->data(),
                                               N );
        cudaSafeCall(cudaGetLastError());
        cudaSafeCall(cudaDeviceSynchronize());
    }

    // Create the final model by masking with the summed view
    addViewMask( sum_view, models, SUM_VIEW_THRESHOLD, class_idx );

    // ================== GET THE RELEVANT FEATURES ==================

    // Sum positive tests for each histogram
    FeaturesGPU<int> d_positives( sum_view->num_features, 1 );

    int N = sum_view->array->size();
    int blockSize = HIST_STEP;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Kernel
    sumPositives <<< nBlocks, blockSize >>> ( sum_view->data(),
                                              models->objects_test->ptr(),
                                              d_positives.data(),
                                              models->numNinjaSets(),
                                              class_idx,
                                              N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Download ninja relevance
    vector<unsigned int> h_ninja_mask;
    models->histograms_relevance->download( h_ninja_mask );

    // Calculate relative ammount of points included in the mask
    vector<int> h_positives;
    d_positives.array->download( h_positives );
    for( int i=0; i<sum_view->num_features; i++ ){
        float relative_positive = float(h_positives[i]) / points_sum;
        if( relative_positive > RELEVANCE_THRESHOLD )
            h_ninja_mask[ i*models->numNinjaSets() + class_idx/32 ] |= 1 << class_idx;
    }

    // Upload ninja relevance
    models->histograms_relevance->upload( h_ninja_mask );
}

void HistogramsModels::addViewMask( HistogramsPtr unsorted_histograms,
                                 BackprojectionModelsPtr models,
                                 float mask_threshold,
                                 int class_idx )
{
    // ================== RADIX SORT ==================

    FeaturesGPU<int> sorted_histograms( unsorted_histograms->num_features, unsorted_histograms->step );

    FeaturesGPU<int> sorted_addresses( unsorted_histograms->num_features, unsorted_histograms->step );

    int N = unsorted_histograms->array->size();
    int blockSize = HIST_STEP;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Kernel
    radixSort <<< nBlocks, blockSize >>> ( unsorted_histograms->data(),
                                           sorted_histograms.data(),
                                           sorted_addresses.data(),
                                           N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // ================== CREATE THE MASKS ==================

    N = unsorted_histograms->array->size();
    blockSize = HIST_STEP;
    nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Kernel
    thresholdHistograms <<< nBlocks, blockSize >>> ( sorted_histograms.data(),
                                                     sorted_addresses.data(),
                                                     models->objects_test->ptr(),
                                                     models->numNinjaSets(),
                                                     mask_threshold,
                                                     class_idx,
                                                     N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
}


