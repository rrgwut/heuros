#include <segmentation_manager.h>

SegmentationManager::SegmentationManager()
{
    cudaEventCreate( &start_all );
    cudaEventCreate( &stop_all );

    flat_segmentation.appendNames( features_names );
    nonflat_segmentation.appendNames( features_names );
}

SegmentationManager::~SegmentationManager(){
    cudaEventDestroy(start_all);
    cudaEventDestroy(stop_all);
}

void SegmentationManager::process( MetacloudPtr &_metacloud )
{
    // Initialize
    timerStart( start_all );
    metacloud = _metacloud;
    metacloud_gpu = ADDRESS_2_METACLOUD_GPU(metacloud->metacloud_gpu);
    assert( metacloud_gpu->cloud );
    const int num_segment_sets = 2;
    metacloud_gpu->segment_indices->create( num_segment_sets, metacloud->size());

    // Flat areas segmentation
    flat_segmentation.calculate( metacloud_gpu );
    flatDimensionsCPU();

    // Non-flat clusters
    nonflat_segmentation.calculate( metacloud_gpu );

    // Download indices to host
    downloadFeatures();

    // Copy segment sets names
    metacloud_gpu->segment_sets_names = features_names;
    metacloud->segment_sets_names = metacloud_gpu->segment_sets_names;

    cout << "Total segmentation time: " << fixed << timerStop( start_all, stop_all ) << " ms." << endl;
}

void SegmentationManager::downloadFeatures()
{
    for( int i=0; i<metacloud_gpu->numSegmentSets(); i++ ){
        IndicesPtr h_indices( new vector<int> );
        metacloud_gpu->segment_indices->downloadFeature( h_indices, i );
        metacloud->segment_indices.push_back( h_indices );
    }
    for(int i=0; i<metacloud_gpu->segmented_points->size(); i++){
        int list_size = metacloud_gpu->segmented_points->at(i)->size();
        IndicesPtr h_indices( new vector<int>(list_size) );
        cudaMemcpy(h_indices->data(), metacloud_gpu->segmented_points->at(i)->ptr(),
                   list_size*sizeof(int), cudaMemcpyDeviceToHost);
        cudaDeviceSynchronize();
        metacloud->segmented_points.push_back( h_indices );
    }
}

void SegmentationManager::flatDimensionsCPU()
{
    // Cleare the results vector
    MetacloudGPU::SegmentedFeaturePtr seg_side_a( new MetacloudGPU::SegmentedFeature );
    MetacloudGPU::SegmentedFeaturePtr seg_side_b( new MetacloudGPU::SegmentedFeature );
    seg_side_a->name = "Side A";
    seg_side_b->name = "Side B";

    // Loop ver each contour
    for( int i=0; i<flat_segmentation.num_clusters; i++ ){

        // Create the compacted contour vector
        vector<cv::Point2f> contour;
        for( int j=0; j<CTR_RESOLUTION; j++ ){
            int point_index = flat_segmentation.h_ctr_indices[ i*CTR_RESOLUTION + j ];
            //cout << "I: " << i << " :: J: " << j << " :: x: " << h_flat_points[ point_index ].x << endl;
            if( point_index > 0 ){
                float2 _point = flat_segmentation.h_flat_points[ point_index ];
                cv::Point2f point( _point.x, _point.y );
                contour.push_back( point );
            }
        }

        // Measure sides of the min area enclosing rect
        float side_a, side_b;
        if( contour.size() < 3 ){
            side_a = F_NAN; side_b = F_NAN;
        }else{
            cv::RotatedRect rect;
            rect = cv::minAreaRect( contour );
            side_a = max( rect.size.width, rect.size.height );
            side_b = min( rect.size.width, rect.size.height );
        }
        seg_side_a->values.push_back( side_a );
        seg_side_b->values.push_back( side_b );
    }

    metacloud_gpu->segmented_features.back().push_back(seg_side_a);
    metacloud_gpu->segmented_features.back().push_back(seg_side_b);
}

void SegmentationManager::getCluster( int seed_idx, int seg_set_idx )
{
    // Create the cluster
    selected_cluster = IndicesPtr( new vector<int>() );

    IndicesConstPtr point_labels = metacloud->segment_indices[seg_set_idx];

    // Get the selected point's label
    int selected_label = point_labels->at(seed_idx);
    if( selected_label == -1 ){
        cout << "ERROR: The picked point does not belong to a valid segment" << endl;
        return;
    }

    for( int i=0; i<point_labels->size(); i++ ){
        if( point_labels->at(i) == selected_label )
            selected_cluster->push_back(i);
    }

    cout << "Cluster extraction complete" << endl;
}
