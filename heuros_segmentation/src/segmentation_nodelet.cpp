#include <segmentation_nodelet.h>
// this should really be in the implementation (.cpp file)
#include <pluginlib/class_list_macros.h>

namespace heuros_segmentation
{

void SegmentationNodelet::onInit()
{
    cout << "Starting Heuros Segmentation..." << endl;

    active_segmentation = "nonflat";

    ros::NodeHandle n;

    // Set up subscribers
    sub_metacloud = n.subscribe( "/heuros/ftr_metacloud", 1, &SegmentationNodelet::metacloudCb , this );
    sub_pick = n.subscribe( "/heuros/picked_point", 1, &SegmentationNodelet::pickCb , this );

    // Set up publishers
    pub_cluster = n.advertise<heuros_core::StampedAddress>( "/heuros/cluster", 1, true );
    pub_metacloud = n.advertise<heuros_core::StampedAddress>( "/heuros/seg_metacloud", 1, true );

    // Set up services
    srv_instruction = n.advertiseService( "/heuros/seg_instruction", &SegmentationNodelet::instructionSrv, this );
    srv_get_string_list = n.advertiseService("/heuros/get_segmentation_names", &SegmentationNodelet::getStringListSrv, this);

    // Service clients
    //cli_vis_instruction = n.serviceClient<heuros_core::SimpleInstruction>( "/heuros/vis_instruction" );
}

void SegmentationNodelet::metacloudCb( const heuros_core::StampedAddressConstPtr &msg )
{
    cout << "Segmentation: Metacloud received" << endl;

    // Run all the segmentation manager's calculations
    segmentation_manager.process( ADDRESS_2_METACLOUD(msg->address) );

    // Publish results
    cout << "Publishing segmentation..." << endl;
    heuros_core::StampedAddressPtr pub_msg( new heuros_core::StampedAddress );
    pub_msg->address = PTR_2_ADDRESS( segmentation_manager.metacloud );
    pub_metacloud.publish( pub_msg );
}

void SegmentationNodelet::pickCb( const heuros_core::PickedPointConstPtr &msg )
{
    cout << "Segmentation: Picked point received. Performing segmentation..." << endl;

    if( active_segmentation == "nonflat" )
        segmentation_manager.getCluster( msg->index, 1 );
    else if( active_segmentation == "flat" )
        segmentation_manager.getCluster( msg->index, 0 );

    //heuros_core::SimpleInstruction srv;
    //srv.request.instruction = "accept_cluster";
    //cli_vis_instruction.call( srv ); // THIS HANGS HEUROS, EVEN MULTITHREADED. WHY??

    heuros_core::StampedAddressPtr cluster_msg( new heuros_core::StampedAddress );
    cluster_msg->address = PTR_2_ADDRESS(segmentation_manager.selected_cluster);
    pub_cluster.publish( cluster_msg );
}

bool SegmentationNodelet::getStringListSrv( heuros_core::GetStringListRequest &req, heuros_core::GetStringListResponse &resp )
{
    cout << "Features: Received get features names request" << endl;

    if( req.query == "features" ){
        resp.string_list = segmentation_manager.features_names;
    }

    return true;
}

// ------------------------------
// ----- INSTRUCTION SERVER -----
// ------------------------------

bool SegmentationNodelet::instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp )
{
    // ================== SEGMENT FROM PLANE ==================

    if( req.instruction == "segment_nonflat" )
    {
        cout << "Segmentation: Received segment planar instruction" << endl;
        active_segmentation = "nonflat";
    }

    // ================== SEGMENT SMOOTH ==================

    if( req.instruction == "segment_flat" )
    {
        cout << "Segmentation: Received segment smooth instruction" << endl;
        active_segmentation = "flat";
    }

    // ================== RESET SEGMENTATION ==================

    if( req.instruction == "reset" )
    {
        cout << "Segmentation: Received reset instruction" << endl;
        //accept_scene = true;
    }

    // ==================

    return true;
}

}

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(heuros_segmentation::SegmentationNodelet, nodelet::Nodelet)
