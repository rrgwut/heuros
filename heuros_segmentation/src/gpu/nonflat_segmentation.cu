#include <nonflat_segmentation.h>
#include <floodfill.h>
#include <stdio.h>

// FLOODFILL PARAMETERS
#define NUM_NONFLAT_FLOODS 10
#define MIN_NONFLAT_AREA 0.0004 // square meters

// THRESHOLDS FOR PLANE CLASSIFICATION
#define THRESH_SIDE_A 0.6 // meters
#define THRESH_RECT_AREA 0.2 // square meters

//---------------------------------------------------------

__device__ bool nonflatTest( int idx, int *flat_labels, float *segments_side_a, float *segments_side_b )
{
    //bool nonflat = false;
    int flat_label = flat_labels[idx];
    // If doesn't belong to flat segment, mark as nonflat
    if( flat_label < 0 )
        return true;
    // Else check if it is nonflat enough
    else{
        //printf("HAPPENED! flat_label = %d/n", flat_label );
        float side_a = segments_side_a[ flat_label ];
        float side_b = segments_side_b[ flat_label ];
        float rect_area = side_a * side_b;
        if( side_a < THRESH_SIDE_A || rect_area < THRESH_RECT_AREA )
            return true;
    }
    // Else return false
    return false;
}

__global__ void floodInitialize( int *flat_labels,
                                 float *segments_side_a,
                                 float *segments_side_b,
                                 int *point_labels,
                                 int *neighbors,
                                 int *neighbors_sizes,
                                 int *connections,
                                 int *connections_sizes,
                                 int max_neighbors,
                                 int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){

        const int neighbors_size = neighbors_sizes[idx];
        const int *point_neighbors = neighbors + idx*max_neighbors;

        // If point belongs to a great flat area, break
        if( !nonflatTest( idx, flat_labels, segments_side_a, segments_side_b ) ){
            point_labels[idx] = -1;
            connections_sizes[idx] = 0;
            return;
        }

        // Loop over neighbors
        int num_connections = 0;
        for( int i=0; i<neighbors_size; i++ ){
            // Get neighbor index
            int nbr_idx = point_neighbors[i];
            if( nbr_idx == idx ) continue; // no auto-connection
            // If nonflat, create connection
            if( nonflatTest( nbr_idx, flat_labels, segments_side_a, segments_side_b ) ){
                connections[ idx*max_neighbors + num_connections ] = nbr_idx;
                num_connections++;
            }
        }
        point_labels[idx] = idx;
        connections_sizes[idx] = num_connections;
    }
}

__global__ void calculateIndicesLists(int* points_labels, int N,
                                      int** lists, int* curr_idx){
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        int label = points_labels[idx];
        if(label>=0){
            int idx_on_list = atomicAdd(curr_idx+label, 1);
            lists[label][idx_on_list] = idx;
        }
    }
}


NonflatSegmentation::NonflatSegmentation()
{
    cudaEventCreate( &start );
    cudaEventCreate( &stop );
}

vector<int> NonflatSegmentation::allocFeatures( int num_features )
{
    vector<int> ftr_indices;
    for( int i=0; i<num_features; i++ ){
        ftr_indices.push_back( metacloud_gpu->segment_indices->countUp() );
    }
    return ftr_indices;
}

void NonflatSegmentation::appendNames( vector<string> &names )
{
   names.push_back("Nonflat segments");
}

void NonflatSegmentation::calculate( MetacloudGPUPtr _metacloud_gpu )
{
    // Initialization and allocation
    timerStart( start );
    metacloud_gpu = _metacloud_gpu;
    vector<int> ftr_indices = allocFeatures(1);

    // Set up kernel params
    int N = metacloud_gpu->cloud->size();
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Prepare to flood-fill

    // Initialize data structures
    DeviceArrayIntPtr point_labels = DeviceArrayIntPtr( new DeviceArray<int>(metacloud_gpu->size()) );
    DeviceArray<int> connections( metacloud_gpu->neighbors_indices.data.size() );
    DeviceArray<int> connections_sizes( metacloud_gpu->size() );
    int max_neighbors = metacloud_gpu->neighbors_indices.max_elems;

    DeviceArray<float> d_side_a, d_side_b;
    d_side_a.upload( metacloud_gpu->segmented_features[0][1]->values );
    d_side_b.upload( metacloud_gpu->segmented_features[0][2]->values );

    // Set seeds and connections
    floodInitialize <<< nBlocks, blockSize >>>( metacloud_gpu->segment_indices->getFeaturePtr(0),
                                                d_side_a.ptr(),
                                                d_side_b.ptr(),
                                                point_labels->ptr(),
                                                metacloud_gpu->neighbors_indices.data.ptr(),
                                                metacloud_gpu->neighbors_indices.sizes.ptr(),
                                                connections.ptr(),
                                                connections_sizes.ptr(),
                                                max_neighbors,
                                                N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Create array for the floodfill results
    DeviceArrayIntPtr label_sources;
    DeviceArrayIntPtr cluster_counts;

    // Run the floodfill segmentation algorithm
    FloodFill::ffSegmentation( connections,
                               connections_sizes,
                               point_labels,
                               label_sources,
                               cluster_counts,
                               num_clusters,
                               NUM_NONFLAT_FLOODS,
                               MIN_NONFLAT_AREA*(1.0/NBR_R_2)*(1.0/NBR_R_2)*metacloud_gpu->avg_neighbors_2/F_PI );

    // Save the point labels
    cudaMemcpy( metacloud_gpu->segment_indices->getFeaturePtr(ftr_indices[0]),
                point_labels->ptr(),
                point_labels->sizeBytes(),
                cudaMemcpyDeviceToDevice );    

    // Append vector of segmented features for the segment set
    vector<MetacloudGPU::SegmentedFeaturePtr> new_seg_set;
    metacloud_gpu->segmented_features.push_back( new_seg_set );

    // Save the cluster counts
    vector<int> h_int_counts;
    cluster_counts->download( h_int_counts );
    MetacloudGPU::SegmentedFeaturePtr seg_ftr( new MetacloudGPU::SegmentedFeature );
    seg_ftr->name = "Nonflat cluster counts";
    seg_ftr->values.resize( cluster_counts->size() );
    for( int i=0; i<cluster_counts->size(); i++ )
        seg_ftr->values[i] = h_int_counts[i];
    metacloud_gpu->segmented_features.back().push_back( seg_ftr );

    //Calculate list of indices in each cluster
    metacloud_gpu->segmented_points->resize(cluster_counts->size());
    int** d_lists;
    int** h_lists = new int*[cluster_counts->size()];
    cudaMalloc((void**)&(d_lists), cluster_counts->size()*sizeof(int**));
    int* d_curr_idx;
    cudaMalloc((void**)&d_curr_idx, cluster_counts->size()*sizeof(int));
    cudaMemset(d_curr_idx, 0, cluster_counts->size()*sizeof(int));

    for(size_t i=0; i<cluster_counts->size(); i++){
        DeviceArrayIntPtr tmp_list = DeviceArrayIntPtr(new DeviceArray<int>(h_int_counts.at(i)) );
        cudaMemset(tmp_list->ptr(), -1, tmp_list->sizeBytes());
        metacloud_gpu->segmented_points->at(i) = tmp_list;
        h_lists[i] = tmp_list->ptr();
    }
    cudaMemcpy(d_lists, h_lists, cluster_counts->size()*sizeof(int*), cudaMemcpyHostToDevice);

    calculateIndicesLists<<< nBlocks, blockSize >>>(point_labels->ptr(), N,
                                                    d_lists, d_curr_idx);

    delete[] h_lists;
    cudaFree(d_lists);
    cudaFree(d_curr_idx);

    //calcIndicesLists <<< nBlocks, blockSize >>>(point_labels, indices_in_clusters);

    cout << "Nonflat segmentation performed in " << fixed << timerStop( start, stop ) << " ms." << endl;
    cout << "Found " << num_clusters << " non-flat clusters" << endl;
}
