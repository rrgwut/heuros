#include <floodfill.h>
#include <stdio.h>

// FLOODFILL HEURISTICS
#define RAND_STEP 3
#define NUM_NESTED_FLOODS 7
#define FULL_FLOOD_THRESH 20

//---------------------------------------------------------

__global__ void setIntsTo( int *data,
                           int value,
                           int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        data[idx] = value;
    }
}

__global__ void flood( int *point_labels,
                       const int *sparse_label_sources,
                       int *sparse_cluster_counts,
                       const int *connections,
                       const int *connections_sizes,
                       const int max_connections,
                       int random_offset,
                       int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){

        // Convenience variables in local memory
        int local_connections[MAX_NEIGHBORS];

        int num_connections;
        int full_num_connections = *(connections_sizes + idx);
        // Full connections
        if( full_num_connections < FULL_FLOOD_THRESH )
        {
            num_connections = full_num_connections;
            for( int i=0; i<num_connections; i++ )
                local_connections[i] = connections[ idx*max_connections+i ];
        }
        // Pseudorandom connections
        else
        {
            num_connections = full_num_connections / RAND_STEP;
            for( int i=0; i<num_connections; i++ )
                local_connections[i] = connections[ idx*max_connections + RAND_STEP*i + random_offset ];
        }

        // Initialize label from current value
        int initial_label = *(point_labels+idx);
        int curr_label = initial_label;
        bool label_changed = false;

        // Flood fill kernel loops
        for( int r=0; r<NUM_NESTED_FLOODS; r++ ){
            // Go over the connections
            int max_nbr_label = curr_label;
            for( int i=0; i<num_connections; i++ ){

                // Pull neighbor current label from global memory
                int nbr_label = point_labels[ local_connections[i] ];

                // Compare with the max neighbor label
                if( nbr_label > max_nbr_label )
                    max_nbr_label = nbr_label;
            }
            // Compare the max label with the current label
            if( max_nbr_label > curr_label ){
                // Set new label
                curr_label = max_nbr_label;
                label_changed = true;
                // Write to global memory
                *(point_labels+idx) = max_nbr_label;
            }
        }

        // Heuristics

        // Read most recent source's label
        int up_initial_label = -1;
        if( initial_label >=0 )
            up_initial_label = *(point_labels+sparse_label_sources[initial_label]);

        // If source's label is greater, update curr_label
        if( up_initial_label > curr_label )
        {
            curr_label = up_initial_label;
            label_changed = true;
            // Write to global memory
            *(point_labels+idx) = curr_label;
        }
        // If curr_label is greater, update label at source
        else if( curr_label > up_initial_label ){
            atomicCAS( point_labels+sparse_label_sources[up_initial_label], up_initial_label, curr_label );
        }

        // Increment the final cluster count (global)
        if( label_changed ){
            atomicAdd( sparse_cluster_counts+curr_label, 1 );

            ///////////////////////////////////////
            //NEED TO TRY ATOMIC ADD -1
            ///////////////////////////////////////

            // Zero the previous cluster ???
            *(sparse_cluster_counts+initial_label) = 0;
        }
    }
}

__global__ void pickNewLabels( int *tmp_new_labels,
                               const int *sparse_cluster_counts,
                               int *num_clusters,
                               int min_points,
                               int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        //printf( "IDX: %d :: counts: %d\n", idx, sparse_cluster_counts[idx] );
        if( sparse_cluster_counts[idx] >= min_points)
        {
            int new_label = atomicAdd( num_clusters, 1 );
            tmp_new_labels[idx] = new_label;
        }
    }
}

__global__ void moveClusterInfo( int *tmp_new_labels,
                                   const int *sparse_label_sources,
                                   const int *sparse_cluster_counts,
                                   int *label_sources,
                                   int *cluster_counts,
                                   int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        int new_label = tmp_new_labels[idx];
        cluster_counts[new_label] = sparse_cluster_counts[idx];
        label_sources[new_label] = sparse_label_sources[idx];
    }
}

__global__ void relabelAll( int *point_labels,
                            const int *tmp_new_labels,
                            int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        // Read label
        int point_label = point_labels[idx];
        if( point_label >= 0 ){
            int new_label = tmp_new_labels[point_label];
            point_labels[idx] = new_label;
        }
    }
}

void FloodFill::cudaSetIntsTo( DeviceArray<int> &array, int value )
{
    // Set up kernel params
    int N = array.size();
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Call kernel
    setIntsTo <<< nBlocks, blockSize >>>( array.ptr(), value, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
}

void FloodFill::ffSegmentation( const DeviceArray<int> &connections,
                                const DeviceArray<int> &connections_sizes,
                                DeviceArrayIntPtr &point_labels,
                                DeviceArrayIntPtr &label_sources,
                                DeviceArrayIntPtr &cluster_counts,
                                int &num_clusters,
                                int num_floods,
                                int min_count )
{
    // Set up kernel params
    int N = point_labels->size();
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Set initial counts
    DeviceArrayIntPtr sparse_label_sources;
    DeviceArrayIntPtr sparse_cluster_counts;
    if( !cluster_counts ){
        sparse_label_sources = DeviceArrayIntPtr( new DeviceArray<int>(point_labels->size()) );
        cudaMemcpy( sparse_label_sources->ptr(), point_labels->ptr(), point_labels->sizeBytes(), cudaMemcpyDeviceToDevice );
        sparse_cluster_counts = DeviceArrayIntPtr( new DeviceArray<int>(point_labels->size()) );
        cudaSetIntsTo( *sparse_cluster_counts, 1 );
    }else{
        sparse_label_sources = label_sources;
        sparse_cluster_counts = cluster_counts;
    }

    // Floodfill iterations
    for( int i=0; i<num_floods; i++ ){
        flood <<< nBlocks, blockSize >>>( point_labels->ptr(),
                                          sparse_label_sources->ptr(),
                                          sparse_cluster_counts->ptr(),
                                          connections.ptr(),
                                          connections_sizes.ptr(),
                                          MAX_NEIGHBORS,
                                          rand()%RAND_STEP,
                                          N );
        cudaSafeCall(cudaGetLastError());
        cudaSafeCall(cudaDeviceSynchronize());
    }

    // Number of clusters
    DeviceArray<int> d_num_clusters(1);
    cudaMemset( d_num_clusters.ptr(), 0, sizeof(int) );
    DeviceArray<int> tmp_new_labels( point_labels->size() );
    cudaSetIntsTo( tmp_new_labels, -1 );

    // Set up kernel params
    N = sparse_cluster_counts->size();
    blockSize = 512;
    nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    //CLUSTER COUNTS ARE PROBABLY THE PROBLEM!!!!

    // Threshold, count clusters and pick new labels
    pickNewLabels <<< nBlocks, blockSize >>>( tmp_new_labels.ptr(),
                                              sparse_cluster_counts->ptr(),
                                              d_num_clusters.ptr(),
                                              min_count,
                                              N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Create dense cluster counts array
    d_num_clusters.download( &num_clusters );
    label_sources = DeviceArrayIntPtr( new DeviceArray<int>(num_clusters) );
    cluster_counts = DeviceArrayIntPtr( new DeviceArray<int>(num_clusters) );

    // Move cluster counts
    moveClusterInfo <<< nBlocks, blockSize >>>( tmp_new_labels.ptr(),
                                                sparse_label_sources->ptr(),
                                                sparse_cluster_counts->ptr(),
                                                label_sources->ptr(),
                                                cluster_counts->ptr(),
                                                N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Set up kernel params
    N = point_labels->size();
    blockSize = 512;
    nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Relabel all points
    relabelAll <<< nBlocks, blockSize >>>( point_labels->ptr(),
                                           tmp_new_labels.ptr(),
                                           N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
}
