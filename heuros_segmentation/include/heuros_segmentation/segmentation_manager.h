#ifndef SEGMENTATION_MANAGER_H
#define SEGMENTATION_MANAGER_H

#include <cv.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/gpu/octree/octree.hpp>

#include <heuros_core/metacloud.h>
#include <heuros_core/timers_gpu.h>
#include <flat_segmentation.h>
#include <nonflat_segmentation.h>

class SegmentationManager
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Metacloud shared pointer. */
    MetacloudPtr metacloud;

    /** \brief GPU metacloud */
    MetacloudGPUPtr metacloud_gpu;

    /** \brief Flat segmentation GPU object. */
    FlatSegmentation flat_segmentation;

    /** \brief Non-flat segmentation GPU object. */
    NonflatSegmentation nonflat_segmentation;

    /** \brief GPU timer events. */
    cudaEvent_t start_all, stop_all;

    /** \brief Selected segment indices. */
    IndicesPtr selected_cluster;

    /** \brief Vector of feature names. */
    vector<string> features_names;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    SegmentationManager();

    /** \brief Destructor. */
    ~SegmentationManager();

    /** \brief Set input point cloud and trigger calculations. */
    void process( MetacloudPtr &_metacloud );

    /** \brief Download features to host. */
    void downloadFeatures();

    /** Calculate flat features using OpenCV on CPU */
    void flatDimensionsCPU();

    /** Get seeded cluster from previous segmentation */
    void getCluster( int seed_idx , int seg_set_idx );
};

#endif
