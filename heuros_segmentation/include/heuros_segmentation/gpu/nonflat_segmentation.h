#ifndef NONFLAT_SEGMENTATION_H
#define NONFLAT_SEGMENTATION_H

#include <heuros_core/metacloud_gpu.h>
#include <heuros_core/timers_gpu.h>

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif

using namespace std;

class NonflatSegmentation
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Number of detected "flat" clusters */
    int num_clusters;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor */
    NonflatSegmentation();

    /** \brief Main method to calculate the feature values */
    void calculate( MetacloudGPUPtr _metacloud_gpu );

    /** Append the features names */
    void appendNames( vector<string> &names );

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief GPU metacloud */
    MetacloudGPUPtr metacloud_gpu;

    /** \brief Labels linking points to "flat" clusters */
    DeviceArrayIntPtr point_labels;

    /** \brief Number of points in each cluster */
    DeviceArrayIntPtr cluster_counts;

    /** \brief GPU timer events. */
    cudaEvent_t start, stop;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Allocate the features on GPU metacloud and return corresponding indices. */
    vector<int> allocFeatures( int num_features );

};

#endif
