#ifndef FLAT_SEGMENTATION_H
#define FLAT_SEGMENTATION_H

#include <heuros_core/metacloud_gpu.h>
#include <heuros_core/timers_gpu.h>

using namespace std;

// GEOMETRIC PARAMETERS
#define CTR_RESOLUTION 100

class FlatSegmentation
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Number of detected "flat" clusters */
    int num_clusters;

    /** \brief Indices to contour points (num_clusters*CTR_RESOLUTION) */
    vector<int> h_ctr_indices;

    /** \brief Flat coordinates of all the cloud points */
    vector<float2> h_flat_points;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor */
    FlatSegmentation();

    /** \brief Main method to calculate the feature values */
    void calculate( MetacloudGPUPtr _metacloud_gpu );

    /** Append the features names */
    void appendNames( vector<string> &names );

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief GPU metacloud */
    MetacloudGPUPtr metacloud_gpu;

    /** \brief GPU timer events. */
    cudaEvent_t start, stop;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Allocate the features on GPU metacloud and return corresponding indices. */
    vector<int> allocFeatures( int num_features );
};

#endif
