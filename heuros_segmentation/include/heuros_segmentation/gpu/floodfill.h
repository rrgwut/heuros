#ifndef FLOODFILL_H
#define FLOODFILL_H

#include <heuros_core/metacloud_gpu.h>

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif

using namespace std;
using namespace pcl::gpu;

class FloodFill
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Set DeviceArray<int> values to value. */
    static void cudaSetIntsTo( DeviceArray<int> &array, int value );
        
    /** \brief Call GPU floodfill calculations */
    static void ffSegmentation( const DeviceArray<int> &connections,
                                const DeviceArray<int> &connections_sizes,
                                DeviceArrayIntPtr &point_labels,
                                DeviceArrayIntPtr &label_sources,
                                DeviceArrayIntPtr &cluster_counts,
                                int &num_clusters,
                                int num_floods,
                                int min_count );
};

#endif
