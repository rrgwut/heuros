#ifndef SEGMENTATION_NODELET_H
#define SEGMENTATION_NODELET_H

#include <iostream>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>

#include <heuros_core/StampedAddress.h>
#include <heuros_core/GetStringList.h>
#include <heuros_core/PickedPoint.h>
#include <heuros_core/SimpleInstruction.h>
#include <segmentation_manager.h>

using namespace std;

namespace heuros_segmentation
{

class SegmentationNodelet : public nodelet::Nodelet
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief On nodelet initialization. */
    virtual void onInit();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Segmentation manager object. */
    SegmentationManager segmentation_manager;

    /** \brief Currently active segmentation algotithm (planar or smooth) */
    string active_segmentation;

    /** \brief Metacloud subscriber. */
    ros::Subscriber sub_metacloud;

    /** \brief Picked point subscriber. */
    ros::Subscriber sub_pick;

    /** \brief Metacloud publisher. */
    ros::Publisher pub_metacloud;

    /** \brief Cluster publisher. */
    ros::Publisher pub_cluster;

    /** \brief Simple instruction service. */
    ros::ServiceServer srv_instruction;

    /** \brief Get string list service. */
    ros::ServiceServer srv_get_string_list;

    /** \brief Simple instruction client for the visualization node. */
    //ros::ServiceClient cli_vis_instruction;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Metacloud received callback. */
    void metacloudCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Picked point received callback. */
    void pickCb( const heuros_core::PickedPointConstPtr &msg );

    /** \brief Get features list service function. */
    bool getStringListSrv( heuros_core::GetStringListRequest &req, heuros_core::GetStringListResponse &resp );

    /** \brief Simple instructions service function. */
    bool instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp );
};

}

#endif
