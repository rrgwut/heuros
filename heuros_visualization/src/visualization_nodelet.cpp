#include <visualization_nodelet.h>
// this should really be in the implementation (.cpp file)
#include <pluginlib/class_list_macros.h>

namespace heuros_visualization
{

void VisualizationNodelet::onInit()
{
    cout << "Starting Heuros Visualization..." << endl;

    ros::NodeHandle nh;

    // Get number of viewports parameter
    int num_viewports = 0;

    nh.param( "heuros/num_viewports", num_viewports, int(0) );
    accept_scene = true;
    accept_cluster = true;

    if( num_viewports != 1 && num_viewports != 2 && num_viewports != 4 ){
        ROS_ERROR( "Number of viewports either wrong or not set");
        return;
    }

    // Initialize visualization window
    vis_ptr = VisWindowPtr( new VisWindow(num_viewports) );

    // Set up publishers
    pub_pick = nh.advertise<heuros_core::PickedPoint>( "/heuros/picked_point", 1, true );
    pub_done = nh.advertise<std_msgs::String>( "/heuros/done", 1 );
    done_msg = std_msgs::String::Ptr( new std_msgs::String );
    done_msg->data = "visualization";

    // Set up subscribers
    sub_metacloud = nh.subscribe( "/heuros/hse_metacloud", 1, &VisualizationNodelet::metacloudCb , this );
    sub_reg_metacloud = nh.subscribe( "/heuros/reg_metacloud", 1, &VisualizationNodelet::regMetacloudCb , this );
    sub_det_backprojection = nh.subscribe( "/heuros/det_backprojection", 1, &VisualizationNodelet::detBackprojectionCb , this );
    sub_det_detections = nh.subscribe( "/heuros/det_detections", 1, &VisualizationNodelet::detDetectionsCb , this );
    sub_cluster = nh.subscribe( "/heuros/cluster", 1, &VisualizationNodelet::clusterCb , this );
    sub_kinfu_view = nh.subscribe( "/heuros/kin_view", 1, &VisualizationNodelet::kinfuCb , this );

    // Set up services
    srv_instruction = nh.advertiseService("/heuros/vis_instruction", &VisualizationNodelet::instructionSrv, this);

    // Connect user interaction callbacks
    vis_ptr->viewer->registerPointPickingCallback( &VisualizationNodelet::pickCb, *this, 0 );
    vis_ptr->viewer->registerKeyboardCallback( &VisualizationNodelet::keyCb, *this, 0 );

    timer = nh.createTimer(ros::Duration(0.1), &VisualizationNodelet::timerCb, this );
    timer.start();
}

void VisualizationNodelet::metacloudCb( const heuros_core::StampedAddressConstPtr &msg )
{
    // Accept only if instructed
    if( !accept_scene ){
        accept_scene = true;
        return;
    }

    cout << "Visualization: Metacloud received" << endl;
    vis_ptr->metacloud = ADDRESS_2_METACLOUD(msg->address);
    vis_ptr->metacloud_gpu = ADDRESS_2_METACLOUD_GPU( vis_ptr->metacloud->metacloud_gpu);
    vis_ptr->setCloudAllViewports( vis_ptr->metacloud->cloud_xyzrgbn );

    // Publish done message
    pub_done.publish(done_msg);
}

void VisualizationNodelet::regMetacloudCb( const heuros_core::StampedAddressConstPtr &msg )
{
    cout << "Visualization: Registration metacloud received" << endl;
    vis_ptr->reg_metacloud = ADDRESS_2_METACLOUD(msg->address);
    vis_ptr->displayRegistration( vis_ptr->selected_window );
}

void VisualizationNodelet::detBackprojectionCb( const heuros_core::StampedAddressConstPtr &msg )
{
    cout << "Visualization: Backprojection received" << endl;
    vis_ptr->backprojection = ADDRESS_2_DETECTIONS(msg->address);

    // Display if we got both - backprojection and detections
    if( vis_ptr->detections )
        if( vis_ptr->detections->file_name == vis_ptr->backprojection->file_name )
            displayDetections();
}

void VisualizationNodelet::detDetectionsCb( const heuros_core::StampedAddressConstPtr &msg )
{
    cout << "Visualization: Detections from backprojection received" << endl;
    vis_ptr->detections = ADDRESS_2_DETECTIONS(msg->address);

    // Display if we got both - backprojection and detections
    if( vis_ptr->backprojection )
        if( vis_ptr->detections->file_name == vis_ptr->backprojection->file_name )
            displayDetections();
}

void VisualizationNodelet::clusterCb( const heuros_core::StampedAddressConstPtr &msg )
{
    // Accept only if instructed
    if( !accept_cluster ){
        accept_cluster = true;
        accept_scene = true;
        return;
    }

    cout << "Visualization: Cluster indices received" << endl;
    vis_ptr->cluster = ADDRESS_2_INDICES(msg->address);
    vis_ptr->displayCluster( vis_ptr->selected_window );
}

void VisualizationNodelet::pickCb( const pcl::visualization::PointPickingEvent &event, void* args )
{
    pcl::PointXYZ point_xyz;
    event.getPoint( point_xyz.x, point_xyz.y, point_xyz.z );

    geometry_msgs::Point point_ros;
    point_ros.x = point_xyz.x; point_ros.y = point_xyz.y; point_ros.z = point_xyz.z;

    ros::NodeHandle n;
    accept_cluster = true;

    heuros_core::PickedPointPtr msg( new heuros_core::PickedPoint );
    msg->point = point_ros;
    msg->index = event.getPointIndex();
    pub_pick.publish( msg );
}

void VisualizationNodelet::kinfuCb( const ImagePtr &msg )
{
    // If kinfu is running display on windo 0 by default
    static bool first_callback = true;
    if(first_callback){
        vis_ptr->attachFeatureToViewport(0,-1);
        first_callback = false;
    }

    vis_ptr->kinfu_view = msg;
    vis_ptr->displayKinfu();
}

void VisualizationNodelet::keyCb( const pcl::visualization::KeyboardEvent &event, void* args )
{
    //...
}

void VisualizationNodelet::timerCb( const ros::TimerEvent& )
{
    vis_ptr->viewer->spinOnce (100);
}

void VisualizationNodelet::displayDetections()
{
    if( vis_ptr->det_display_mode == "none" ){
        // Do nothing
    }else if( vis_ptr->det_display_mode == "backprojection" ){
        vis_ptr->displayBackprojection( vis_ptr->selected_window, vis_ptr->selected_object );
    }else if( vis_ptr->det_display_mode == "grid" ){
        vis_ptr->setRepresentation( vis_ptr->selected_window, 0 );
        vis_ptr->attachFeatureToViewport( vis_ptr->selected_window, 0 );
        vis_ptr->displayDetections( vis_ptr->selected_window, vis_ptr->selected_object );
    }else if( vis_ptr->det_display_mode == "backprojection_grid" ){
        vis_ptr->displayBackprojection( vis_ptr->selected_window, vis_ptr->selected_object );
        vis_ptr->displayDetections( vis_ptr->selected_window, vis_ptr->selected_object );
    }
}

// ------------------------------
// ----- INSTRUCTION SERVER -----
// ------------------------------

bool VisualizationNodelet::instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp )
{
    // ================== ACCEPT SCENE ==================

    if( req.instruction == "reject_next_scene" )
    {
        cout << "Visualization: Received reject next scene instruction" << endl;
        accept_scene = false;
    }

    // ================== ACCEPT CLUSTER ==================

    else if( req.instruction == "reject_next_cluster" )
    {
        cout << "Visualization: Received reject next cluster instruction" << endl;
        accept_cluster = false;
    }

    // ================== SET REPRESENTATION ==================

    else if( req.instruction == "set_representation" )
    {
        cout << "Visualization: Received set representation instruction" << endl;
        int repr = atoi( req.parameter.c_str() );
        vis_ptr->setRepresentation( vis_ptr->selected_window, repr );
    }

    // ================== SET VIEW PORT ==================

    else if( req.instruction == "set_viewport" )
    {
        cout << "Visualization: Received set viewport instruction" << endl;
        int viewport = atoi( req.parameter.c_str() );
        vis_ptr->selected_window = viewport;
        vis_ptr->setBackgrounds();
    }

    // ================== SET NORMALS ==================

    else if( req.instruction == "set_normals" )
    {
        cout << "Visualization: Received set normals instruction" << endl;
        int state = atoi( req.parameter.c_str() );
        vis_ptr->setNormals( vis_ptr->selected_window, state );
    }

    // ================== SET POINT SIZE ==================

    else if( req.instruction == "set_point_size" )
    {
        cout << "Visualization: Received set point size instruction" << endl;
        int size = atoi( req.parameter.c_str() );
        vis_ptr->setPointSize(size);
    }

    // ================== DISPLAY FEATURE ==================

    else if( req.instruction == "display_feature" )
    {
        cout << "Visualization: Received display feature instruction" << endl;
        int ftr_idx = atoi( req.parameter.c_str() );
        vis_ptr->attachFeatureToViewport( vis_ptr->selected_window, ftr_idx );
    }

    // ================== SET DETECTIONS DISPLAY MODE ==================

    else if( req.instruction == "set_det_display_mode" )
    {
        cout << "Visualization: Received set detection display mode instruction" << endl;
        vis_ptr->det_display_mode = req.parameter;
        displayDetections();
    }

    // ================== SET OBJECT ==================

    else if( req.instruction == "set_object" )
    {
        cout << "Visualization: Received set object instruction" << endl;
        vis_ptr->selected_object = atoi( req.parameter.c_str() );
        displayDetections();
    }

    // =============== DISPLAY PROPS ==================

    else if(req.instruction == "display_props"){
        cout << "Visualization: received display props instruction" << endl;
        vis_ptr->togglePropsDisplay(vis_ptr->selected_window);
    }

    // ==================

    return true;
}

}

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(heuros_visualization::VisualizationNodelet, nodelet::Nodelet)
