#include <vis_window.h>
#include <heuros_core/private_thief.hpp>

#include <boost/filesystem.hpp>
#include <pcl/io/file_io.h>
#include <vtkImageData.h>
#include <vtkImageReslice.h>
#include <vtkRenderWindow.h>

// ---------------------
// -----CONSTRUCTOR-----
// ---------------------

VisWindow::VisWindow( int _num_viewports ) : max_viewports(9)
{
    num_viewports = _num_viewports;
    if( num_viewports > max_viewports ){
        cout << "Too many viewports. Setting max: " << max_viewports << endl;
        num_viewports = max_viewports;
    }

    axes_on = false;
    selected_window = 0;
    selected_object = 0;
    point_size = 1;
    det_display_mode = "none";
    // Theta set to default - it has nice shades (although it's an angle!!!)
    default_feature = 0;

    viewport_vector.resize( num_viewports );

    // !!!
    // Careful now! viewport_vector elements' id is -1
    // !!!

    setNumViewports( _num_viewports );

    float x_step = 1.0;
    if( num_viewports > 1 ) x_step = 0.5;
    float y_step = 1.0;
    if( num_viewports > 2 ) y_step = 0.5;

    for( int i=0; i<num_viewports; i++ ){

        int x = i%2;
        int y = int(i/2);
        if( num_viewports > 2 ) y = 1-y;

        ViewportWindow &vp_win = viewport_vector[i];
        //vp_win.ftr_repr = 0;
        // Here the id is assigned
        viewer->createViewPort( x*x_step+0.001, y*y_step+0.001, (x+1)*x_step-0.001, (y+1)*y_step-0.001, vp_win.id );
        vp_win.setStr();
        vp_win.active_ftr_idx = default_feature;
        //viewer->addCoordinateSystem( 1.0, vp_win.id );
    }
    setBackgrounds();

    loadPropsMeshes();
}

// -----------------------------------
// -----SET VISUALIZATION TARGETS-----
// -----------------------------------

void VisWindow::setCloudAllViewports( const pcl::PointCloud<pcl::PointXYZRGBNormal>::ConstPtr &_cloud )
{
    cloud = _cloud;

    output_cloud = cloud->makeShared();
    output_cloud->sensor_orientation_ = Eigen::Quaternionf(1,0,0,0);

    for( int i=0; i<num_viewports; i++ ){

        ViewportWindow &vp_win = viewport_vector[i];

        //displayColors( i ); // UGLY, UGLY FIX FOR VTK CONSOLE SPAM - IT WOULD KILL ONLINE PERFORMANCE
        attachFeatureToViewport( i, vp_win.active_ftr_idx );

        // Prevent red warnings
        if( vp_win.active_ftr_idx == -1 ) continue;

        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_IMMEDIATE_RENDERING, 1.0, vp_win.id_str + "_cloud", vp_win.id );
    }

    setBackgrounds();
}

// ------------------------
// -----OTHER SETTINGS-----
// ------------------------

void VisWindow::setBackgrounds()
{
    for( int i=0; i<num_viewports; i++ ){
        ViewportWindow &vp_win = viewport_vector[i];
        if( i == selected_window ){
            viewer->setBackgroundColor( 0.1, 0.1, 0.1, vp_win.id ); // +1 needed in prevous versions of PCL
        }else{
            viewer->setBackgroundColor( 0.0, 0.0, 0.0, vp_win.id ); // +1 needed in prevous versions of PCL
        }
    }
    displayKinfu();
}

void VisWindow::setPointSize( int size )
{
    point_size = size;
    for( int i=0; i<num_viewports; i++ ){
        ViewportWindow &vp_win = viewport_vector[i];
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    }
}

void VisWindow::setCallbacks()
{
    viewer->registerKeyboardCallback( &VisWindow::onKeyboard, *this, 0 );
    viewer->registerMouseCallback( &VisWindow::onMouse, *this, 0 );
}

void VisWindow::setNumViewports( int n )
{
    num_viewports = n;
    viewer = boost::shared_ptr<pcl::visualization::PCLVisualizer>(new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->initCameraParameters();

    viewer->setCameraPosition( 1.0, 0.6,-1.0,
                               1.0, 1.0, 1.0,
                               0.0,-1.0, 0.0 );
    setCallbacks();
}

void VisWindow::attachFeatureToViewport( int window_pos, int ftr_idx, string title )
{
    ViewportWindow &vp_win = viewport_vector[window_pos];
    assert( vp_win.id > 0 );
    vp_win.active_ftr_idx = ftr_idx;
    vp_win.title = title;

    if( !metacloud || ftr_idx == -1 )
        return;

    int n_point_ftrs = metacloud->numFeatures();
    int n_segment_sets = metacloud->numSegmentSets();

    if( ftr_idx < n_point_ftrs ){ // Point feature
        if( title == "" ) vp_win.title = metacloud->features_names[ftr_idx];
        displayFeature( window_pos );
    }else if( ftr_idx < n_point_ftrs + n_segment_sets ){ // Segment set
        int set_idx = ftr_idx - n_point_ftrs;
        if( title == "" ) vp_win.title = metacloud->segment_sets_names[set_idx];
        displaySegmentSet( window_pos, set_idx );
    }else if( ftr_idx == n_point_ftrs + n_segment_sets ){ // Colors
        displayColors( window_pos );
    }else if( ftr_idx == n_point_ftrs + n_segment_sets + 1 ){ // Colors
        vp_win.active_ftr_idx = -1;
    }
}

// ---------------------------
// -----DISPLAY FUNCTIONS-----
// ---------------------------

void VisWindow::displayColors( int window_pos )
{
    if( !output_cloud ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    clearWindow( window_pos );
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBNormal>rgb(cloud);
    viewer->addPointCloud<pcl::PointXYZRGBNormal>( output_cloud, rgb, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    viewer->addText( "RGB colors", 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );
}

void VisWindow::displayCloudOnly( int window_pos )
{
    if( !output_cloud ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    clearWindow( window_pos );

    for( int i=0; i<output_cloud->size(); i++ ){
        unsigned int rgb_uint = 100 << 16 | 100 << 8 | 0;
        output_cloud->at(i).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBNormal> custom( output_cloud );
    viewer->addPointCloud<pcl::PointXYZRGBNormal>( output_cloud, custom, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    viewer->addText( "No feature", 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );
}

void VisWindow::displayFeature( int window_pos )
{
    if( !output_cloud ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    if( vp_win.active_ftr_idx < 0 ){
        //displayColors( window_pos );
        displayCloudOnly( window_pos );
        return;
    }

    clearWindow( window_pos );

    for( int i=0; i<output_cloud->size(); i++ ){

        float ftr_val = metacloud->features[ vp_win.active_ftr_idx ]->at(i);
        Vec3b rgb = representations.f2givenRepresentation( ftr_val, vp_win.ftr_repr );

        unsigned int rgb_uint = (unsigned int)rgb[0] << 16 | (unsigned int)rgb[1] << 8 | (unsigned int)rgb[2];
        output_cloud->at(i).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBNormal> custom( output_cloud );
    viewer->addPointCloud<pcl::PointXYZRGBNormal>( output_cloud, custom, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    viewer->addText( vp_win.title, 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );

    //Display props
    displayProps(window_pos);
}

void VisWindow::displaySegmentSet( int window_pos, int set_idx )
{
    if( !output_cloud ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    clearWindow( window_pos );

    // Pick segment colors
    vector<Vec3b> colors( metacloud_gpu->segmented_features[set_idx][0]->values.size() );
    for( int i=0; i<colors.size(); i++ ){
        // Get segment's number of points
        //int num_pts = metacloud_gpu->segmented_features[set_idx][0]->values[i];
        //colors[i] = Representations::f2rainbow( (num_pts%10000)/5000.0-1.0 );

        // Pick the color
        if( vp_win.ftr_repr == 0 ){
            Vec3b color = Vec3b( rand()%256, rand()%256, rand()%256 );
            color[rand()%3] = 255;
            colors[i] = color;
        }else{
            Vec3b color = Vec3b( 255, 255, 255 );
            colors[i] = color;
        }
    }

    // Get the segment indices
    IndicesConstPtr seg_indices = metacloud->segment_indices[set_idx];

    for( int i=0; i<output_cloud->size(); i++ ){
        Vec3b rgb;
        // Read segment index
        int seg_idx = seg_indices->at(i);
        if( seg_idx < 0 ){
            float ftr_val = -0.5; 
            if( vp_win.ftr_repr == 0 )
                ftr_val = 0.5*metacloud->features[ default_feature ]->at(i);

            rgb = representations.f2givenRepresentation( ftr_val, 1 );
        }else
            rgb = colors[seg_idx];

        unsigned int rgb_uint = (unsigned int)rgb[0] << 16 | (unsigned int)rgb[1] << 8 | (unsigned int)rgb[2];
        output_cloud->at(i).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBNormal> custom( output_cloud );
    viewer->addPointCloud<pcl::PointXYZRGBNormal>( output_cloud, custom, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    viewer->addText( vp_win.title, 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );

    //Display props
    displayProps(window_pos);
}

void VisWindow::colorSegment(int window_pos, int segment_index)
{
    if( !output_cloud ) return;
    if( segment_index >= metacloud->segmented_points.size() ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    clearWindow( window_pos );

    for( int i=0; i<metacloud->segmented_points[segment_index]->size(); i++ ){
        unsigned int rgb_uint = 255 << 16 | 0 << 8 | 0;
        output_cloud->at( metacloud->segmented_points[segment_index]->at(i) ).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBNormal> custom( output_cloud );
    viewer->addPointCloud<pcl::PointXYZRGBNormal>( output_cloud, custom, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties( pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
}

void VisWindow::displayProps(int window_pos)
{
    if( !output_cloud ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];
    if( !vp_win.show_props ) return;

    clearWindow( window_pos );

    for(size_t i=0; i<metacloud->props.size(); i++){
        Prop pr = metacloud->props[i];
        pcl::PointXYZ center = pr.center;

        // Pick unique name
        string name = pr.object_class;
        stringstream ss;
        ss << "prop_" << i << "_" << vp_win.id_str;

        // Display name
        pcl::PointXYZ text_pos = pr.center;
        text_pos.y -= 0.1;
        text_pos.x -= 0.1;
        viewer->addText3D( name, text_pos, 0.05, 0.0, 1.0, 0.0, ss.str()+"_name", vp_win.id+1 );

        // Display marker
        viewer->addSphere(center, 0.02, 0, 1, 0, ss.str(), vp_win.id );

        int seg_idx = pr.segment_index;
        for( int i=0; i<metacloud->segmented_points[seg_idx]->size(); i++ ){
            unsigned int rgb_uint = 255 << 16 | 0 << 8 | 0;
            output_cloud->at( metacloud->segmented_points[seg_idx]->at(i) ).rgb = *reinterpret_cast<float*>(&rgb_uint);
        }
    }

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBNormal> custom( output_cloud );
    viewer->addPointCloud<pcl::PointXYZRGBNormal>( output_cloud, custom, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties( pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    viewer->addText( "Recognized objects", 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );
}

void VisWindow::displayCluster( int window_pos )
{
    if( !output_cloud ) return;
    if( !cluster ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    clearWindow( window_pos );

    for( int i=0; i<output_cloud->size(); i++ ){
        float ftr_val = metacloud->features[ default_feature ]->at(i);
        Vec3b rgb = representations.f2givenRepresentation( ftr_val, 0 );
        unsigned int rgb_uint = rgb_uint = (unsigned int)rgb[0] << 16 | (unsigned int)rgb[1] << 8 | (unsigned int)rgb[2];
        output_cloud->at(i).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }

    for( int i=0; i<cluster->size(); i++ ){
        unsigned int rgb_uint = 255 << 16 | 0 << 8 | 0;
        output_cloud->at( cluster->at(i) ).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBNormal> custom( output_cloud );
    viewer->addPointCloud<pcl::PointXYZRGBNormal>( output_cloud, custom, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties( pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );
    viewer->addText( "Cluster", 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );
}

void VisWindow::displayRegistration( int window_pos )
{
    if( !output_cloud ) return;
    if( !cluster ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    attachFeatureToViewport( window_pos, default_feature );

    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr reg_cloud = reg_metacloud->cloud_xyzrgbn;

    for( int i=0; i<reg_cloud->size(); i++ ){
        unsigned int rgb_uint = 255 << 16 | 0 << 8 | 0;
        reg_cloud->at(i).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBNormal> custom( reg_cloud );
    viewer->addPointCloud<pcl::PointXYZRGBNormal>( reg_cloud, custom, vp_win.id_str + "_registration", vp_win.id );
    viewer->setPointCloudRenderingProperties( pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_registration", vp_win.id );
    viewer->removeAllShapes( vp_win.id );
    //viewer->removeShape( vp_win.id_str + "_title", vp_win.id );
    viewer->addText( "Registration", 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );
}

void VisWindow::setNormals( int window_pos, bool on_off )
{
    if( !output_cloud ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    viewer->removePointCloud( vp_win.id_str + "_normals", vp_win.id );

    if( on_off == true ){
        vp_win.show_normals = true;
        viewer->addPointCloudNormals<pcl::PointXYZRGBNormal, pcl::PointXYZRGBNormal>( output_cloud, output_cloud, 10, 0.02, vp_win.id_str + "_normals", vp_win.id );
    }else if( on_off == false ){
        vp_win.show_normals = false;
    }
}

void VisWindow::setRepresentation( int window_pos, int repr )
{
    if( repr >= representations.num_representations ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    vp_win.ftr_repr = repr;
    displayFeature( window_pos );
}

void VisWindow::displayBackprojection( int window_pos, int object_idx )
{
    if( !backprojection ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    clearWindow( window_pos );

    // Download ninja array
    vector<unsigned int> ninja_cpu;
    backprojection->bit_objects->download( ninja_cpu );

    for( int i=0; i<output_cloud->size(); i++ ){
        // Get object test value
        unsigned int ninja_set = ninja_cpu[ i*backprojection->numNinjaSets() + object_idx/32 ];
        bool test = ninja_set & 1<<(object_idx%32);
        unsigned int pass = test? 255 : 0;
        unsigned int rgb_uint = pass << 16 | pass << 8 | pass;
        output_cloud->at(i).rgb = *reinterpret_cast<float*>(&rgb_uint);
    }

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBNormal> custom( output_cloud );
    viewer->addPointCloud<pcl::PointXYZRGBNormal>( output_cloud, custom, vp_win.id_str + "_cloud", vp_win.id );
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, vp_win.id_str + "_cloud", vp_win.id );

    // Set title
    viewer->addText( "Backprojection of: " + detections->class_names[object_idx], 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );
}

void VisWindow::displayDetections( int window_pos, int object_idx )
{
    if( !detections ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    // Remove shapes, clouds and title
    viewer->removeAllShapes(vp_win.id );
    //viewer->removeShape( vp_win.id_str + "_title", vp_win.id );

    // Download point cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr detections_cloud;
    detections_cloud = pcl::PointCloud<pcl::PointXYZ>::Ptr( new pcl::PointCloud<pcl::PointXYZ> );
    detections_cloud->resize( detections->size() );
    detections->cloud->download( (DPoint*)detections_cloud->points.data() );

    // Download ninja array
    vector<unsigned int> ninja_cpu;
    detections->bit_objects->download( ninja_cpu );

    // Draw shapes
    for( int i=0; i<detections_cloud->size(); i++ ){
        // Get object test value
        unsigned int ninja_set = ninja_cpu[ i*detections->numNinjaSets() + object_idx/32 ];
        bool test = ninja_set & 1<<(object_idx%32);
        stringstream ss;
        ss << "Sphere " << i;
        if( test )
            viewer->addSphere( detections_cloud->at(i), 0.01, 1, 0, 0, ss.str(), vp_win.id );
        else
            viewer->addSphere( detections_cloud->at(i), 0.003, 0, 0, 0, ss.str(), vp_win.id );
    }

    // Set title
    viewer->addText( "Detections of: " + detections->class_names[object_idx], 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );
}

//===================================
//========== PRIVATE THIEF ==========
//===================================

struct AddActorToRender { typedef void(pcl::visualization::PCLVisualizer::*type)(const vtkSmartPointer<vtkProp>&, int); };
template class rob<AddActorToRender, &pcl::visualization::PCLVisualizer::addActorToRenderer>;

//===================================
//===================================
//===================================

void VisWindow::displayKinfu()
{
    int kinfu_idx = -1;
    // Display kinfu in non-selected windows
    for( int i=0; i<num_viewports; i++ ){
        if( viewport_vector[i].active_ftr_idx == kinfu_idx ){
            displayKinfu(i);
        }
    }
}

void VisWindow::displayKinfu( int window_pos )
{
    if( !kinfu_view ) return;
    ViewportWindow &vp_win = viewport_vector[window_pos];

    clearWindow( window_pos );

    void* data = const_cast<void*> (reinterpret_cast<const void*> (kinfu_view->data.data()));

    // Gray window if selected
    vector<unsigned char> grayed_data;
    if( window_pos == selected_window ){
        grayed_data.resize(kinfu_view->step*kinfu_view->height);
        for( int i=0; i<kinfu_view->step*kinfu_view->height; i++ ){
            if( kinfu_view->data[i] == 0 ){
                grayed_data[i] = 25;
            }else{
                grayed_data[i] = kinfu_view->data[i];
            }
        }
        data = const_cast<void*> (reinterpret_cast<const void*> (grayed_data.data()));
    }

    int data_width = kinfu_view->width, data_height = kinfu_view->height;
    vtkSmartPointer<vtkImageData> image = vtkSmartPointer<vtkImageData>::New();
    image->SetExtent (0, data_width - 1, 0, data_height - 1, 0, 0);
#if VTK_MAJOR_VERSION < 6
    image->SetScalarTypeToUnsignedChar ();
    image->SetNumberOfScalarComponents (3);
    image->AllocateScalars ();
#else
    image->AllocateScalars (VTK_UNSIGNED_CHAR, 3);
#endif
    image->GetPointData ()->GetScalars ()->SetVoidArray (data, 3 * data_width * data_width, 1);

    // Resize image
    float scale = 1;
    float x_div = 1, y_div = 1;
    if( num_viewports > 1 )
        x_div = 2;
    else if( num_viewports > 2 )
        y_div = 2;
    vtkSmartPointer<vtkRenderWindow> render_window = viewer->getRenderWindow();
    int *size = render_window->GetSize();
    float size_x = size[0] / x_div;
    float size_y = size[1] / y_div;
    if( size_x/size_y > data_width/data_height )
        scale = size_y / data_height;
    else
        scale = size_x / data_width;
    vtkSmartPointer<vtkImageReslice> reslice = vtkSmartPointer<vtkImageReslice>::New();
    // Gray window if selected
    if( window_pos == selected_window )
        reslice->SetBackgroundColor(25, 25, 25, 1);
    reslice->SetOutputExtent(0, int(size_x)-1, 0, int(size_y)-1, 0, 0);
    reslice->SetResliceAxesDirectionCosines(1,0,0, 0,-1,0, 0,0,-1);
    reslice->SetOutputSpacing(1/scale,1/scale,1);
#if VTK_MAJOR_VERSION <= 5
    reslice->SetInputConnection(image->GetProducerPort());
#else
    reslice->SetInputData(image);
#endif
    reslice->Update();

    // Create mapper
    kinfu_mapper = vtkSmartPointer<vtkImageMapper>::New ();
#if VTK_MAJOR_VERSION <= 5
    kinfu_mapper->SetInputConnection(reslice->GetOutput()->GetProducerPort());
#else
    kinfu_mapper->SetInputData(reslice->GetOutput());
#endif
    kinfu_mapper->SetColorWindow(255);
    kinfu_mapper->SetColorLevel(127.5);

    // Add actor
    kinfu_actor = vtkSmartPointer<vtkActor2D>::New ();
    kinfu_actor->SetMapper(kinfu_mapper);
    kinfu_actor->SetPosition(0, 0);

    // Add to renderer (HACK)
    ((*viewer).*result<AddActorToRender>::ptr)( kinfu_actor, vp_win.id );
    // Save the pointer/ID pair to the global actor map
    (*(viewer->getShapeActorMap()))[vp_win.id_str + "_kinfu"] = kinfu_actor;

    // Set title
    viewer->addText( "Kinect Fusion", 10, 10, 20, 0.0, 1.0, 0.0, vp_win.id_str + "_title", vp_win.id );
}

void VisWindow::clearWindow( int window_pos )
{
    ViewportWindow &vp_win = viewport_vector[window_pos];
    viewer->removeAllPointClouds( vp_win.id );
    viewer->removeAllShapes( vp_win.id );
}

void VisWindow::togglePropsDisplay(int window_pos){
    ViewportWindow &vp_win = viewport_vector[window_pos];
    vp_win.show_props = !vp_win.show_props;
    attachFeatureToViewport( window_pos, vp_win.active_ftr_idx );
}

void VisWindow::loadPropsMeshes(){
    using namespace boost::filesystem;

    loaded_props_meshes.clear();
    loaded_props_names.clear();

    string directory = ros::package::getPath("heuros_visualization") + "/../data/props_meshes/";
    vector<string> file_list;
    if( exists( directory ) ){
        directory_iterator end ;
        for( directory_iterator iter(directory) ; iter != end ; ++iter )
            if ( iter->path().extension() == ".obj"){
                file_list.push_back( iter->path().filename().string() );
            }
    }

    for(size_t i=0; i<file_list.size(); i++){
        string object_class;
        string file_name = file_list[i];
        object_class.resize(file_name.size()-4);
        for(size_t j=0; j<object_class.size(); j++){
            object_class[j] = file_name[j];
        }
        string filepath = directory + file_name;
        pcl::PolygonMesh mesh;
        pcl::io::load(filepath.c_str(),mesh);

        loaded_props_meshes.push_back(mesh);
        loaded_props_names.push_back(object_class);

        cout<<"Visualization: Loaded mesh of the "<<object_class<<endl;
    }
}

// --------------------------------
// -----HANDLE KEYBOARD EVENTS-----
// --------------------------------

void VisWindow::onKeyboard( const pcl::visualization::KeyboardEvent &event, void* )
{
    ViewportWindow &vp_win = viewport_vector[ selected_window ];

    // ================== All viewports options ==================

    // Increase point size
    if( event.getKeyCode() == '+' && event.keyDown () )
    {
        if( point_size < 7 )
            setPointSize( point_size+1 );
    }

    // Decrease point size
    if( event.getKeyCode() == '-' && event.keyDown () )
    {
        if( point_size > 1 )
            setPointSize( point_size-1 );
    }

    // Show / hide coordinate system
    if( event.getKeyCode() == 'a' && event.keyDown () )
    {
        if( axes_on ){
            for( int i=0; i<num_viewports; i++ )
                viewer->removeCoordinateSystem( viewport_vector[i].id );
            axes_on = false;
        }else{
            for( int i=0; i<num_viewports; i++ )
                viewer->addCoordinateSystem( 1.0, viewport_vector[i].id );
            axes_on = true;
        }
    }

    // ================== Viewport navigation ==================

    if (event.getKeySym () == "Right" && event.keyDown ())
    {
        if( selected_window < num_viewports-1 )
            selected_window++;
        else
            selected_window = 0;
        setBackgrounds();
    }

    if (event.getKeySym () == "Left" && event.keyDown ())
    {
        if( selected_window >= 0 ) // allow -1 to hide the selection
            selected_window--;
        else
            selected_window = num_viewports-1;
        setBackgrounds();
    }

    if (event.getKeySym () == "Down" && event.keyDown ())
    {
        if( selected_window < 2 && num_viewports > 2 )
            selected_window += 2;
        else
            selected_window = num_viewports-1;
        setBackgrounds();
    }

    if (event.getKeySym () == "Up" && event.keyDown ())
    {
        if( selected_window > 1 && num_viewports > 2 )
            selected_window -= 2;
        else
            selected_window = 0;
        setBackgrounds();
    }

    // ================== Current viewport options ==================

    if( selected_window < 0 ){
        cout << "WARNING: No viewport selected." << endl;
        return;
    }

    // Display colors
    if( event.getKeySym() == "c" && event.keyDown () )
    {
        displayColors( selected_window );
    }

    // Display segmented cluster
    if( event.getKeySym() == "s" && event.keyDown () )
    {
        displayCluster( selected_window );
    }

    // Display registration
    if( event.getKeySym() == "r" && event.keyDown () )
    {
        displayRegistration( selected_window );
    }

    // Toggle backprojection metacloud
    /*if( event.getKeySym() == "b" && event.keyDown () )
    {
        if( active_metacloud == "features" )
            setBackprojection( selected_window, true );
        else 
            setBackprojection( selected_window, false );
    }*/

    // Toggle detections
    if( event.getKeySym() == "d" && event.keyDown () )
    {
        displayDetections( selected_window, 0 );
    }

    // Change detection object
    if( event.getKeySym() == "k" && event.keyDown () )
    {
        if( selected_object < detections->numObjects()-1 )
            selected_object++;
        else
            selected_object = 0;

        displayDetections( selected_window, selected_object );
    }

    // Toggle normals
    if( event.getKeySym() == "n" && event.keyDown () )
    {
        if( vp_win.show_normals )
            setNormals( selected_window, false );
        else
            setNormals( selected_window, true );
    }

    // Change feature representation (cyclic)
    if( event.getKeyCode() == '`' && event.keyDown () )
    {
        int n_point_ftrs = metacloud->numFeatures();
        int n_segment_sets = metacloud->numSegmentSets();

        if( vp_win.active_ftr_idx < n_point_ftrs ){
            if( vp_win.ftr_repr < representations.num_representations-1 )
                vp_win.ftr_repr++;
            else
                vp_win.ftr_repr = 0;
        }else if( vp_win.active_ftr_idx < n_point_ftrs + n_segment_sets ){
            if( vp_win.ftr_repr == 0 ) 
                vp_win.ftr_repr = 1; 
            else
                vp_win.ftr_repr = 0;
        }
        attachFeatureToViewport( selected_window, vp_win.active_ftr_idx );
    }

    // Hide features
    if( event.getKeyCode() == '0' )
    {
        displayCloudOnly( selected_window );
    }

    // Display the selected feature
    char code = event.getKeyCode();
    int feature_idx = atoi( &code ) - 1;
    if( feature_idx >= 0 )
    {
        attachFeatureToViewport( selected_window, feature_idx );
    }
}

// -----------------------------
// -----HANDLE MOUSE EVENTS-----
// -----------------------------

void VisWindow::onMouse( const pcl::visualization::MouseEvent &event, void* )
{
    if( event.getType () == pcl::visualization::MouseEvent::MouseButtonRelease )
    {
        unsigned int mouse_x = event.getX();
        unsigned int mouse_y = event.getY();
        //viewer->getViewerPose()
    }
}

