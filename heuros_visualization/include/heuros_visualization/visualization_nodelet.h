#ifndef VISUALIZATION_NODELET_H
#define VISUALIZATION_NODELET_H

#include <iostream>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PointStamped.h>

#include <heuros_core/StampedAddress.h>
#include <heuros_core/PickedPoint.h>
#include <heuros_core/SimpleInstruction.h>
#include <heuros_core/metacloud.h>
#include <vis_window.h>

using namespace std;

namespace heuros_visualization
{

class VisualizationNodelet : public nodelet::Nodelet
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief On nodelet initialization. */
    virtual void onInit();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Visualization window pointer. */
    VisWindowPtr vis_ptr;

    /** \brief Metacloud subscriber. */
    ros::Subscriber sub_metacloud;

    /** \brief Backprojection subscriber. */
    ros::Subscriber sub_det_backprojection;

    /** \brief Metacloud backprojection detections subscriber. */
    ros::Subscriber sub_det_detections;

    /** \brief Registration metacloud subscriber. */
    ros::Subscriber sub_reg_metacloud;

    /** \brief Cluster subscriber. */
    ros::Subscriber sub_cluster;

    /** \brief Kinfu view subscriber. */
    ros::Subscriber sub_kinfu_view;

    /** \brief Picked point subscriber. */
    ros::Publisher pub_pick;

    /** \brief Processing done publisher. */
    ros::Publisher pub_done;

    /** \brief Simple instruction service. */
    ros::ServiceServer srv_instruction;

    /** \brief Processing done message. */
    std_msgs::String::Ptr done_msg;

    /** \brief Cyclic timer (used for visualization). */
    ros::Timer timer;

    /** \brief Variables that control whether to display new scenes and clusters. */
    bool accept_scene, accept_cluster;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Metacloud received callback. */
    void metacloudCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Registration metacloud received callback. */
    void regMetacloudCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Backprojection metacloud received callback. */
    void detBackprojectionCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Backprojection detections received callback. */
    void detDetectionsCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Cluster indices received callback. */
    void clusterCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Kinfu view received callback. */
    void kinfuCb( const ImagePtr &msg );

    /** \brief Picked point callback. */
    void pickCb( const pcl::visualization::PointPickingEvent &event, void* args );

    /** \brief Key pressed callback. */
    void keyCb( const pcl::visualization::KeyboardEvent &event, void* args );

    /** \brief Simple instructions service function. */
    bool instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp );

    /** \brief Timer callback. */
    void timerCb(const ros::TimerEvent&);

    /** \brief Mode-wise method to display detection. */
    void displayDetections();
};

}

#endif
