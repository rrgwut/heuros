#ifndef VIS_WINDOW_H
#define VIS_WINDOW_H

#include <iostream>
#include <ros/ros.h>
#include <ros/package.h>
#include <cv.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <vtkImageMapper.h>
#include <sensor_msgs/Image.h>

#include <heuros_core/metacloud.h>
#include <heuros_core/metacloud_gpu.h>
#include <heuros_core/detections.h>
#include <representations.h>

using namespace std;
using namespace Eigen;
using namespace cv;

typedef boost::shared_ptr<sensor_msgs::Image> ImagePtr;

class VisWindow
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief PCLVisualizer object. */
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;

    /** \brief Number of viewports (display windows). */
    int num_viewports;

    /** \brief Metacloud shared pointer. */
    MetacloudConstPtr metacloud;

    /** \brief GPU metacloud */
    MetacloudGPUPtr metacloud_gpu;

    /** \brief Registration metacloud shared pointer. */
    MetacloudConstPtr reg_metacloud;

    /** \brief Backprojection detections shared pointer (GPU structure). */
    DetectionsConstPtr backprojection;

    /** \brief Voxel detections shared pointer (GPU structure). */
    DetectionsConstPtr detections;

    /** \brief Cluster indices shared pointer. */
    IndicesConstPtr cluster;

    /** \brief Kinfu view shared pointer. */
    ImagePtr kinfu_view;

    /** \brief Index of the selected window. */
    int selected_window;

    int selected_object;

    string det_display_mode;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. Requires number of viewports. */
    VisWindow( int _num_viewports );

    /** \brief Set input pointcloud. */
    void setCloudAllViewports( const pcl::PointCloud<pcl::PointXYZRGBNormal>::ConstPtr &_cloud );

    /** \brief Attach a feature (vector of floats) to a view port. */
    void attachFeatureToViewport( int window_pos, int ftr_idx, string title="" );

    /**\brief Toggle props display for a viewport */
    void togglePropsDisplay( int window_pos );

    /** \brief Display cloud with rgb colors. */
    void displayColors( int window_pos );

    /** \brief Display cloud without color. */
    void displayCloudOnly( int window_pos );

    /** \brief Display cloud mapping the current feature into RGB. */
    void displayFeature( int window_pos );

    /** \brief Display the selected segment set. */
    void displaySegmentSet( int window_pos, int set_idx );

    /** \brief Display cluster. */
    void displayCluster( int window_pos );

    /** \brief Display props */
    void displayProps( int window_pos );

    /** \brief Color segment. */
    void colorSegment( int window_pos, int segment_index );

    /** \brief Display registration. */
    void displayRegistration( int window_pos );

    /** \brief Turn normals on (1), off (0) */
    void setNormals( int window_pos, bool on_off );

    /** \brief Set the color representation of the features. */
    void setRepresentation( int window_pos, int repr );

    /** \brief Display backprojection of a selected object class. */
    void displayBackprojection( int window_pos, int object_idx );

    /** \brief Display the detections of a selected object class. */
    void displayDetections( int window_pos, int object_idx );

    /** \brief Display the kinfu "2D" visualization. */
    void displayKinfu();

    /** \brief Display the kinfu "2D" visualization. */
    void displayKinfu( int window_pos );

    /** \brief Set backgrond color for all viewport windows (i.e. to visualize selection). */
    void setBackgrounds();

    /** \brief Set point size for all viewports. */
    void setPointSize( int size );

protected:

    //=====================================================
    //================= PROTECTED STRUCTURES ==============
    //=====================================================

    /** \brief Structure holding settings for the display windows. */
    struct ViewportWindow
    {
        int id; // Not the same as position on the viewport_vector

        volatile int active_ftr_idx; // Index of the active feature

        string id_str; // Id number as string for pointcloud names

        string title; // Displayed title

        bool show_colors, show_normals, show_cluster, show_props;

        int ftr_repr; // Feature representation style

        ViewportWindow()
        {
            id = -1;
            active_ftr_idx = -1;
            show_normals = false;
            show_props = false;
            ftr_repr = COLD_HOT;
        }

        void setStr()
        {
            stringstream ss;
            ss << id;
            id_str = ss.str();
        }
    };

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Whether currently displaying the axes. */
    bool axes_on;

    /** \brief Maximum number of viewports handled by the visualizer. */
    const int max_viewports;

    /** \brief Point size for all viewports. */
    int point_size;

    /** \brief Feature displayed by default. */
    int default_feature;

    /** \brief Pointer to the input pointcloud. */
    pcl::PointCloud<pcl::PointXYZRGBNormal>::ConstPtr cloud;

    /** \brief Pointer to the output pointcloud. */
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr output_cloud;

    /** \brief Vector of viewport windows. */
    vector<ViewportWindow> viewport_vector;

    /** \brief Features representation object. */
    Representations representations;

    /** \brief Loaded meshes of props and their names */
    vector<pcl::PolygonMesh> loaded_props_meshes;
    vector<string> loaded_props_names;

    /** \brief Kinfu visualization objects */
    vtkSmartPointer<vtkImageMapper>kinfu_mapper;
    vtkSmartPointer<vtkActor2D>kinfu_actor;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Set number of view ports (visualization windows). */
    void setNumViewports( int n );

    /** \brief Set the callback functions to events. */
    void setCallbacks();

    /** \brief Handle keyboard events. */
    void onKeyboard( const pcl::visualization::KeyboardEvent &event, void* );

    /** \brief Handle mouse events. */
    void onMouse( const pcl::visualization::MouseEvent &event, void* );

    /** \brief Load props meshes */
    void loadPropsMeshes();

    /** \brief Remove everything from the selected viewport */
    void clearWindow( int window_pos );
};

/** \brief Class shared pointer. */
// @{
typedef boost::shared_ptr<VisWindow> VisWindowPtr;
typedef boost::shared_ptr<const VisWindow> VisWindowConstPtr;
// @}

#endif
