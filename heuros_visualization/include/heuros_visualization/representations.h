#ifndef REPRESENTATIONS_H
#define REPRESENTATIONS_H

#include <iostream>
#include <vector>
#include <cv.h>

using namespace std;
using namespace cv;

// Available feature representation types.
//
#define F_NAN 0.0f/0.0f
#define NAN_CHECK(a) a==a?false:true

#define COLD_HOT 0
#define GRAYSCALE 1
#define BLUE_YELLOW 2
#define ABNORMAL 3

class Representations
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Vector of pointers to available representation functions. */
    vector<Vec3b (*)( const float )> representation_functions;

    /** \brief Number of available representations. */
    int num_representations;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    Representations();

    /** \brief Convert float value into a given RGB color representation. */
    Vec3b f2givenRepresentation( const float x, const int representation );

    /** \brief Convert float value into cold-hot RGB color representation. */
    static Vec3b f2coldHot( const float x );

    /** \brief Convert float value into grayscale representation. */
    static Vec3b f2grayscale( const float x );

    /** \brief Convert float value into blue-yellow RGB color representation. */
    static Vec3b f2blueYellow( const float x );

    /** \brief Convert float value into abnormal detector RGB color representation. */
    static Vec3b f2abnormal( const float x );

    /** \brief Convert HSV to RGB */
    static Vec3b hsv2rgb( Vec3f hsv );

    /** \brief Convert float value into rainbow color representation. */
    static Vec3b f2rainbow( float x );
};

#endif
