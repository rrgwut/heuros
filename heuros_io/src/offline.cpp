#include <offline.h>

Offline::Offline()
{
    cudaEventCreate( &start_all );
    cudaEventCreate( &stop_read );
    cudaEventCreate( &stop_copy );
}

void Offline::savePointcloud(string file_name, const MetacloudPtr &metacloud )
{
    string path = ros::package::getPath("heuros_io") + "/../data/scenes/" + file_name;
    pcl::io::savePCDFile( path.c_str(), *metacloud->cloud_xyzrgbn, true );
}

bool Offline::loadPointcloud( string file_name, MetacloudPtr &metacloud )
{
    cout << "Loading point cloud: " << file_name << endl;

    metacloud = MetacloudPtr( new Metacloud );

    timerStart( start_all );

    metacloud->file_name = file_name;
    string path = ros::package::getPath("heuros_io") + "/../data/scenes/" + file_name;

    if( pcl::io::loadPCDFile( path.c_str(), *metacloud->cloud_xyzrgbn ) == -1 ){
        cout << "Error: File not found: " << file_name << endl;
        return false;
    }

    cout << "Cloud read in : " << fixed << timerStop( start_all, stop_read ) << " ms. It contains " << metacloud->size() << " points." << endl;

    // MEMORY LOW-LEVEL COPYING

    size_t cloud_sz = metacloud->size();
    metacloud->cloud_xyz->resize( cloud_sz );
    metacloud->vec_rgb.resize( cloud_sz );
    metacloud->vec_normals.resize( cloud_sz );

    void* xyzrgbn_ptr = (void*)( metacloud->cloud_xyzrgbn->points.data() );
    void* xyz_ptr = (void*)( metacloud->cloud_xyz->points.data() );
    void* vrgb_ptr = (void*)( metacloud->vec_rgb.data() );
    void* vn_ptr = (void*)( metacloud->vec_normals.data() );

    size_t xyz_step = sizeof(pcl::PointXYZ);
    size_t xyzrgbn_step = sizeof(pcl::PointXYZRGBNormal);

    for( int i=0; i<cloud_sz; i++ ){

        // xyz to xyz
        memcpy( xyz_ptr + i*xyz_step,
                xyzrgbn_ptr + i*xyzrgbn_step,
                xyz_step );

        // rgb to vec rgb
        memcpy( vrgb_ptr + i*sizeof(float),
                xyzrgbn_ptr + i*xyzrgbn_step + 2*xyz_step,
                sizeof(float) );

        // normal to vec normal
        memcpy( vn_ptr + i*xyz_step,
                xyzrgbn_ptr + i*xyzrgbn_step + xyz_step,
                xyz_step );
    }
    cout << "Arrays copied in : " << fixed << timerStop( stop_read, stop_copy ) << " ms." << endl;
    return true;
}

void Offline::saveCluster( string cloud_name, const IndicesConstPtr &indices )
{
    ros::NodeHandle n;
    string prefix;
    n.param( "heuros/prefix", prefix, string("") );

    string file_name;

    int file_no = 1;
    do{
        stringstream ss; ss << prefix << "_" << file_no++ << ".txt";
        file_name = ros::package::getPath("heuros_io") + "/../data/clusters/" + ss.str();
    }while( fileExists(file_name) );

    cout << "Saving segmented cluster indices to: " << file_name << endl;

    ofstream file( file_name.c_str() );

    file << prefix << endl;
    file << cloud_name << endl;

    for( int i=0; i<indices->size(); i++ )
        file << indices->at(i) << " ";

    file.close();
}

bool Offline::loadCluster( string cluster_name, IndicesPtr &indices, string &prefix, string &cloud_name )
{
    string path = ros::package::getPath("heuros_io") + "/../data/clusters/" + cluster_name;

    ifstream infile( path.c_str() );

    if( !infile.good() ){
        cout << "Error: File not found: " << cluster_name << endl;
        return false;
    }

    // Get train / test from the cluster name
    vector<string> elems;
    boost::split( elems, cluster_name, boost::is_any_of("/") );
    string t_t_comp = elems[0];

    // Read prefix
    infile >> prefix;

    // Get the cloud name from the path string
    string cloud_path;
    infile >> cloud_path;
    elems.clear(); // needed?
    boost::split( elems, cloud_path, boost::is_any_of("/") );
    cloud_name = elems[elems.size()-1];

    // Add the train / test path component
    cloud_name = t_t_comp + "/" + cloud_name;

    // Read indices data
    indices = IndicesPtr( new vector<int> );
    int idx;
    infile >> idx;
    while( !infile.eof() ){
        indices->push_back(idx);
        infile >> idx;
    }

    infile.close();
    return true;
}

bool Offline::fileExists( string file_name_abs )
{
    ifstream infile( file_name_abs.c_str() );
    return infile.good();
}

void Offline::timerStart( cudaEvent_t _start )
{
    cudaEventRecord( _start, 0 );
}

float Offline::timerStop( cudaEvent_t _start, cudaEvent_t _stop )
{
    float time;
    cudaEventRecord( _stop, 0 );
    cudaEventSynchronize( _stop );
    cudaEventElapsedTime( &time, _start, _stop );
    return time;
}
