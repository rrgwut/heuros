#include <io_nodelet.h>
// this should really be in the implementation (.cpp file)
#include <pluginlib/class_list_macros.h>

namespace heuros_io
{

void IONodelet::onInit()
{
    cout << "Starting Heuros IO..." << endl;

    auto_publish_cluster = false;

    ros::NodeHandle n;

    // Set up publishers
    pub_metacloud = n.advertise<heuros_core::StampedAddress>( "/heuros/input_metacloud", 1, false );
    pub_cluster = n.advertise<heuros_core::StampedAddress>( "/heuros/cluster", 1, false );
    pub_objects = n.advertise<heuros_msgs::Props>( "/heuros_objects", 1, false );

    // Set up subscribers
    sub_kinfu = n.subscribe( "/heuros/kin_cloud", 1, &IONodelet::kinfuCb, this ); // This will have to be expanded and include TSDF
    sub_metacloud = n.subscribe( "/heuros/hse_metacloud", 1, &IONodelet::metacloudCb, this );
    sub_cluster = n.subscribe( "/heuros/cluster", 1, &IONodelet::clusterCb, this );

    // Set up services
    srv_instruction = n.advertiseService("/heuros/io_instruction", &IONodelet::instructionSrv, this);
    srv_get_string_list = n.advertiseService("/heuros/get_string_list", &IONodelet::getStringListSrv, this);
}

void IONodelet::kinfuCb( const heuros_core::StampedAddressConstPtr &msg )
{
    cout << "IO: Kinfu cloud received" << endl;
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud = ADDRESS_2_POINTCLOUD(msg->address);
    online.receivePointcloud( cloud, metacloud );
    heuros_core::StampedAddressPtr pub_msg( new heuros_core::StampedAddress );
    pub_msg->address = PTR_2_ADDRESS(metacloud);
    pub_metacloud.publish( pub_msg );
}

void IONodelet::clusterCb( const heuros_core::StampedAddressConstPtr &msg )
{
    cout << "IO: Cluster indices received" << endl;
    cluster = ADDRESS_2_INDICES(msg->address);
}

void IONodelet::metacloudCb( const heuros_core::StampedAddressConstPtr &msg )
{
    cout << "IO: Metacloud received" << endl;

    // If cluster loaded from file, now it's the time to publish it
    if( auto_publish_cluster ){
        heuros_core::StampedAddressPtr cluster_msg( new heuros_core::StampedAddress );
        cluster_msg->address = PTR_2_ADDRESS(cluster);
        pub_cluster.publish( cluster_msg );
    }

    // Publish props
    heuros_msgs::Props objects_msg;

    MetacloudConstPtr output_metacloud_ptr = ADDRESS_2_METACLOUD( msg->address );
    for( int i=0; i<output_metacloud_ptr->props.size(); i++ ){
        heuros_msgs::Prop object;
        object.class_name = output_metacloud_ptr->props[i].object_class;
        object.pose.position.x = output_metacloud_ptr->props[i].center.x;
        object.pose.position.y = output_metacloud_ptr->props[i].center.y;
        object.pose.position.z = output_metacloud_ptr->props[i].center.z;
        objects_msg.props.push_back( object );
    }
    pub_objects.publish( objects_msg );
}

bool IONodelet::getStringListSrv( heuros_core::GetStringListRequest &req, heuros_core::GetStringListResponse &resp )
{
    cout << "IO: Received get string list request" << endl;

    string directory = ros::package::getPath("heuros_io") + "/../data/" + req.query;
    vector<string> file_list;
    if( exists( directory ) ){
        directory_iterator end ;
        for( directory_iterator iter(directory) ; iter != end ; ++iter )
            if ( iter->path().extension() == ".txt" || iter->path().extension() == ".pcd"
                 || iter->path().extension() == ".hse"){
                file_list.push_back( iter->path().filename().string() );
            }
    }
    sort( file_list.begin(), file_list.end() );
    resp.string_list = file_list;

    return true;
}

// ------------------------------
// ----- INSTRUCTION SERVER -----
// ------------------------------

bool IONodelet::instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp )
{
    // ================== LOAD SCENE ==================

    if( req.instruction == "load_scene" )
    {
        cout << "IO: Received load scene instruction" << endl;

        if( !offline.loadPointcloud( req.parameter, metacloud ) ) return false;
        auto_publish_cluster = false;
        heuros_core::StampedAddressPtr pub_msg( new heuros_core::StampedAddress );
        pub_msg->address = PTR_2_ADDRESS(metacloud);
        pub_metacloud.publish( pub_msg );
    }

    // ================== LOAD CLUSTER ==================

    else if( req.instruction == "load_cluster" )
    {
        cout << "IO: Received load cluster instruction" << endl;

        ros::NodeHandle n;

        // Load cluster; read prefix and cloud name
        string prefix;
        string cloud_name;
        if( !offline.loadCluster( req.parameter, cluster, prefix, cloud_name ) ) return false;

        // Return cloud name
        resp.feedback = cloud_name;

        // Set prefix
        cout << "Setting prefix: " << prefix << endl;
        n.setParam( "heuros/prefix", prefix );

        // Load scene
        bool load_cloud = false;
        if( !metacloud )
            load_cloud = true;
        else if( metacloud->file_name != cloud_name )
            load_cloud = true;

        if( load_cloud ){
            if( !offline.loadPointcloud( cloud_name, metacloud ) ) return false;
            auto_publish_cluster = true;
            heuros_core::StampedAddressPtr pub_msg( new heuros_core::StampedAddress );
            pub_msg->address = PTR_2_ADDRESS(metacloud);
            pub_metacloud.publish( pub_msg );
        }else{
            heuros_core::StampedAddressPtr cluster_msg( new heuros_core::StampedAddress );
            cluster_msg->address = PTR_2_ADDRESS(cluster);
            pub_cluster.publish( cluster_msg );
        }
    }

    // ================== SAVE CLUSTER ==================

    else if( req.instruction == "save_cluster" )
    {
        if( !cluster ){
            cout << "IO: There are no clusters to save" << endl;
            return false;
        }

        offline.saveCluster( metacloud->file_name, cluster );
    }

    return true;
}

}

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(heuros_io::IONodelet, nodelet::Nodelet)
