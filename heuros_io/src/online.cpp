#include <online.h>
#include <heuros_core/timers_gpu.h>

Online::Online()
{
    cudaEventCreate( &start_all );
    cudaEventCreate( &stop_copy );
}

bool Online::receivePointcloud( const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr &point_cloud, MetacloudPtr &metacloud )
{
    cout << "Receiving point cloud..." << endl;

    metacloud = MetacloudPtr( new Metacloud );

    timerStart( start_all );

    metacloud->file_name = "kinfu";
    string path = "kinfu";
    metacloud->cloud_xyzrgbn = point_cloud;

    // MEMORY LOW-LEVEL COPYING

    size_t cloud_sz = metacloud->size();
    metacloud->cloud_xyz->resize( cloud_sz );
    metacloud->vec_rgb.resize( cloud_sz );
    metacloud->vec_normals.resize( cloud_sz );

    void* xyzrgbn_ptr = (void*)( metacloud->cloud_xyzrgbn->points.data() );
    void* xyz_ptr = (void*)( metacloud->cloud_xyz->points.data() );
    void* vrgb_ptr = (void*)( metacloud->vec_rgb.data() );
    void* vn_ptr = (void*)( metacloud->vec_normals.data() );

    size_t xyz_step = sizeof(pcl::PointXYZ);
    size_t xyzrgbn_step = sizeof(pcl::PointXYZRGBNormal);

    for( int i=0; i<cloud_sz; i++ ){

        // xyz to xyz
        memcpy( xyz_ptr + i*xyz_step,
                xyzrgbn_ptr + i*xyzrgbn_step,
                xyz_step );

        // rgb to vec rgb
        memcpy( vrgb_ptr + i*sizeof(float),
                xyzrgbn_ptr + i*xyzrgbn_step + 2*xyz_step,
                sizeof(float) );

        // normal to vec normal
        memcpy( vn_ptr + i*xyz_step,
                xyzrgbn_ptr + i*xyzrgbn_step + xyz_step,
                xyz_step );
    }
    cout << "Arrays copied in : " << fixed << timerStop( start_all, stop_copy ) << " ms." << endl;
    return true;
};
