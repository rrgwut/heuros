#ifndef HISTOGRAMS_NODELET_H
#define HISTOGRAMS_NODELET_H

#include <iostream>
#include <boost/filesystem.hpp>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <std_msgs/String.h>

#include <heuros_msgs/Props.h>
#include <heuros_core/metacloud.h>
#include <heuros_core/StampedAddress.h>
#include <heuros_core/SimpleInstruction.h>
#include <heuros_core/GetStringList.h>
#include <offline.h>
#include <online.h>

using namespace std;
using namespace boost::filesystem;

namespace heuros_io
{

class IONodelet : public nodelet::Nodelet
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief On nodelet initialization. */
    virtual void onInit();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Flag to automatically publish the cluster after getting metacloud. */
    bool auto_publish_cluster;

    /** \brief Metacloud shared pointer. */
    MetacloudPtr metacloud;

    /** \brief cluster result indices. */
    IndicesPtr cluster;

    /** \brief Kinfu cloud subscriber. */
    ros::Subscriber sub_kinfu;

    /** \brief Metacloud subscriber. */
    ros::Subscriber sub_metacloud;

    /** \brief Cluster subscriber. */
    ros::Subscriber sub_cluster;

    /** \brief Metacloud publisher. */
    ros::Publisher pub_metacloud;

    /** \brief Cluster indices publisher. */
    ros::Publisher pub_cluster;

    /** \brief Output - objects publisher. */
    ros::Publisher pub_objects;

    /** \brief Simple instruction service. */
    ros::ServiceServer srv_instruction;

    /** \brief Get string list service. */
    ros::ServiceServer srv_get_string_list;

    /** \brief Offline IO. */
    Offline offline;

    /** \brief Online IO. */
    Online online;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Kinfu cloud received callback. */
    void kinfuCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Metacloud received callback. */
    void metacloudCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Cluster indices received callback. */
    void clusterCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Simple instructions service function. */
    bool instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp );

    /** \brief Get file list service function. */
    bool getStringListSrv( heuros_core::GetStringListRequest &req, heuros_core::GetStringListResponse &resp );
};

}

#endif
