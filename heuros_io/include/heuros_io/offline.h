#ifndef FEATURES_IO_H
#define FEATURES_IO_H

#include <iostream>
#include <boost/algorithm/string.hpp>
#include <cuda_runtime.h>
#include <ros/ros.h>
#include <ros/package.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>

#include <heuros_core/metacloud.h>

using namespace std;

class Offline
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    Offline();

    /** \brief Save point cloud data to file. */
    void savePointcloud( string file_name, const MetacloudPtr &metacloud );

    /** \brief Load point cloud data from a file. */
    bool loadPointcloud( string file_name, MetacloudPtr &metacloud );

    /** \brief Save cluster indices to file. */
    void saveCluster( string cloud_name , const IndicesConstPtr &indices );

    /** \brief Load cluster, prefix and cloud name data from a file. */
    bool loadCluster( string cluster_name, IndicesPtr &indices , string &prefix, string &cloud_name );

    /** \brief Check whether file exists. */
    static bool fileExists(string file_name_abs );

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief GPU timer events. */
    cudaEvent_t start_all, stop_read, stop_copy;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Start GPU timer. */
    void timerStart( cudaEvent_t _start );

    /** \brief Stop GPU timer and return elapsed time. */
    float timerStop( cudaEvent_t _start, cudaEvent_t _stop );

};

#endif
