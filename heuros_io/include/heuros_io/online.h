#ifndef ONLINE_H
#define ONLINE_H

#include <iostream>
#include <boost/algorithm/string.hpp>
#include <cuda_runtime.h>
#include <ros/ros.h>
#include <ros/package.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>

#include <heuros_core/metacloud.h>

using namespace std;

class Online
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    Online();

    /** \brief Load point cloud data from a file. */
    bool receivePointcloud( const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr &point_cloud, MetacloudPtr &metacloud );

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief GPU timer events. */
    cudaEvent_t start_all, stop_copy;

};

#endif

