#! /usr/bin/octave -qf 
###############################################################
# This script will work both as an executable or through Octave
1; # Inform Octave this is not a function file
###############################################################

################################################################
####################### FILL DATA ARRAYS #######################
################################################################

DIR_NAME = "data/registration/oil_both"; # No '/' at the end!

# Includes
pkg load io;

# Load file names list
files = glob( ["../" DIR_NAME "/*.mat"] );

total_hyp = [];
tested_hyp = [];

for i = 1:length(files)
  # Load data
  file_name = files(i){:};
  load( file_name );
  
  if( alignment_success == 0 )
    continue;
  else
    total_hyp = [total_hyp num_hypotheses];
    tested_hyp = [tested_hyp num_tested];
  endif
endfor

num_success = length(total_hyp)
mean_total = mean(total_hyp)
mean_tested = mean(tested_hyp)
mean_total_std = std(total_hyp) / sqrt( length(total_hyp) )
mean_tested_std = std(tested_hyp) / sqrt( length(tested_hyp) )
