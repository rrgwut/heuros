#! /usr/bin/octave -qf 
###############################################################
# This script will work both as an executable or through Octave
1; # Inform Octave this is not a function file
###############################################################

################################################################
####################### FILL DATA ARRAYS #######################
################################################################

RESOLUTION = 40;

# Includes
pkg load io;

# Load file names list
files = glob( ["../data/histograms/" int2str(RESOLUTION) "/*.mat"] );

prev_prefix = "null";

for i = 1:length(files)
  # Load prefix and histogram data
  file_name = files(i){:};
  load( file_name );
  
  if( strcmp(prefix, prev_prefix) )
    # Append histogram to the current subarray
    histograms{length(histograms)} = { histograms{length(histograms)}{:} histogram };

  elseif( strcmp(prev_prefix, "null") )
	# Create histograms cell array
    histograms = {{histogram}};
    # Create object classes array
    classes = {prefix};
    # Set previous prefix
    prev_prefix = prefix;
  else
    # Append a histogram subarray with the current histogram
    histograms = { histograms{:} {histogram} };
    # Add a new class
	classes = { classes{:} prefix };
	# Set previous prefix
    prev_prefix = prefix;
  endif
  
endfor

# Get the number of features
nclasses = length( classes );
nfeatures = rows( histograms{1}{1} );
nbins = columns( histograms{1}{1} );

######################################################################
####################### CALCULATE CORRELATIONS #######################
######################################################################

selected_corr = zeros(nfeatures,1);

for f = 1:nfeatures
  selected_corr(f) = corr( histograms{4}{2}(f,:), histograms{5}{1}(f,:) );
endfor

xlswrite( "../data/statistics/selected.xls", selected_corr );
