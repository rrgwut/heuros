#! /usr/bin/octave -qf 
###############################################################
# This script will work both as an executable or through Octave
1; # Inform Octave this is not a function file
###############################################################

################################################################
####################### FILL DATA ARRAYS #######################
################################################################

RESOLUTION = 15;

# Includes
pkg load io;

# Load file names list
files = glob( ["../data/histograms/" int2str(RESOLUTION) "/*.mat"] );

prev_prefix = "null";

for i = 1:length(files)
  # Load prefix and histogram data
  file_name = files(i){:};
  load( file_name );
  
  if( strcmp(prefix, prev_prefix) )
    # Append histogram to the current subarray
    histograms{length(histograms)} = { histograms{length(histograms)}{:} histogram };

  elseif( strcmp(prev_prefix, "null") )
	# Create histograms cell array
    histograms = {{histogram}};
    # Create object classes array
    classes = {prefix};
    # Set previous prefix
    prev_prefix = prefix;
  else
    # Append a histogram subarray with the current histogram
    histograms = { histograms{:} {histogram} };
    # Add a new class
	classes = { classes{:} prefix };
	# Set previous prefix
    prev_prefix = prefix;
  endif
  
endfor

# Get the number of features
nclasses = length( classes );
nfeatures = rows( histograms{1}{1} );
nbins = columns( histograms{1}{1} );

######################################################################
####################### CALCULATE CORRELATIONS #######################
######################################################################

avg_correlations = cell(1,nfeatures);

printf ("Processed 0%%.\n");
for f = 1:nfeatures
  # We are processing feature f
  avg_correlations{f} = zeros( nclasses, nclasses );
  for y = 1:nclasses
    for x = 1:nclasses
      # We are on cell (y,x) of the avg_correlations matrix
      if( x < y )# Skip repeated calculations
        continue;
      endif
      corr_sum = 0;
      for yi = 1:length(histograms{y})
        for xi = 1:length(histograms{x})
          # We are comparing hist yi from class y to xi from class x
          if( y == x && xi == yi ) # Skip self-correlation
            continue;
          endif
          # Do the actual calculation
          corr_sum += corr( histograms{y}{yi}(f,:), histograms{x}{xi}(f,:) );
        endfor
      endfor
      # Calculate how many histograms were compared
      if( y == x )
        count = length(histograms{y}) * length(histograms{y}) - length(histograms{y});
      else
        count = length(histograms{y}) * length(histograms{x});
      endif
      # Calculate the average correlation at (y, x)
      avg_correlations{f}(y,x) = corr_sum / count;
    endfor
  endfor
  avg_correlations{f} = avg_correlations{f} + transpose(avg_correlations{f}) - eye(length(avg_correlations{f})) .* avg_correlations{f};
  printf ("Processed %d%%.\n", 100*f/nfeatures);
  fflush(stdout);
endfor

#########################################################################
####################### CALCULATE SIGNAL TO NOISE #######################
#########################################################################

# self / avg of others
#{
signal_to_noise = zeros( nfeatures, nclasses );

for f = 1:nfeatures
  for c = 1:nclasses
    self_corr = avg_correlations{f}(c,c);
    others_corr = 0;
    for d = 1:nclasses
      if( c == d ) 
        continue;
      endif
      others_corr += avg_correlations{f}(c,d);
    endfor
    signal_to_noise(f,c) = (nclasses-1) * self_corr / others_corr;
  endfor
endfor
#}

# self / max of others

signal_to_noise = zeros( nfeatures, nclasses );

for f = 1:nfeatures
  sorted_corr = sort( avg_correlations{f}, 2, "descend" );
  for c = 1:nclasses
    self_corr = avg_correlations{f}(c,c);
    # signal to noise = self correlation / greatest correlation other than self-
    if( self_corr == sorted_corr(c,1) ) 
      stn = self_corr / sorted_corr(c,2);
    else
      stn = self_corr / sorted_corr(c,1);
    endif
    signal_to_noise(f,c) = stn;
  endfor
endfor
#}

#############################################################
####################### EXPORT TO XLS #######################
#############################################################

# Write correlations to vertical output matrix
output_mat = avg_correlations{1};
for f = 2:nfeatures
  output_mat = [ output_mat; avg_correlations{f} ];
endfor

# Write correlations to XLS (must manually remove file first)
xlswrite( ["../data/statistics/correlations_" int2str(RESOLUTION) ".xls"], output_mat );

# Write signal to noise to XLS (must manually remove file first)
xlswrite( ["../data/statistics/signal_to_noise_" int2str(RESOLUTION) ".xls"], signal_to_noise );

###############################################################
####################### PLOT HISTOGRAMS #######################
###############################################################

#PLOTTING TURNED OFF
###{
# Create array of feature names
feature_names = { "Convexity" "Anisotropy" "Inclinatoin" "Hue" "Saturation" };
subplot_names = { "a   " "b   " "c   " "d   " "e   " };

# Create bins matrix for plotting
bin_step = 2 / nbins;
bins = [ -1+bin_step/2 : bin_step : 1-bin_step/2 ];

WIDTH = 3;
COLOR = [.3 .3 .3]

for i = 1:nclasses  
  for j = 1:length(histograms{i})
    # Create the subplots
    
    ## Uncomment for subplot names
    #set (gcf, "papersize", [9,WIDTH+0.2]);
    #set (gcf, "paperposition", [-0.5,0.25,10,WIDTH]);
    
    set (gcf, "papersize", [9,WIDTH]);
    set (gcf, "paperposition", [-0.5,0,10,WIDTH]);
    for k = 1:nfeatures
      subplot( nfeatures, 1, k );
      output = bar( bins, histograms{i}{j}(k,:), 1, "facecolor", COLOR, "edgecolor", COLOR );
      #output = stairs( bins, histograms{i}{j}(k,:), "k", "linewidth", 5 );      
      axis("ticx");
      set (gca, "fontsize", 14);
      
      ## Uncomment for subplot names
      #ylabel( subplot_names{k}, "rotation", 0, "fontsize", 20 );
    endfor
    # save the plot file
    filename = [ "../data/images/histograms/" int2str(RESOLUTION) "/" classes{i} "_" int2str(j) ];
    saveas( output, filename, "pdf" );
  endfor
endfor
#}

###############################################################
###############################################################
###############################################################
printf ("All done.\n");
###############################################################
###############################################################
###############################################################
