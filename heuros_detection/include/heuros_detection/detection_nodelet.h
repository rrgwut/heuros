#ifndef DETECTION_NODELET_H
#define DETECTION_NODELET_H

#include <iostream>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PointStamped.h>

#include <heuros_core/StampedAddress.h>
#include <heuros_core/SimpleInstruction.h>
#include <backprojection.h>

using namespace std;

namespace heuros_detection
{

class DetectionNodelet : public nodelet::Nodelet
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief On nodelet initialization. */
    virtual void onInit();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Backprojection utility object. */
    Backprojection backprojection;

    /** \brief Metacloud subscriber. */
    ros::Subscriber sub_metacloud;

    /** \brief Model object subscriber. */
    ros::Subscriber sub_models;

    /** \brief Backprojection publisher (for visualization). */
    ros::Publisher pub_backprojection;

    /** \brief Detections publisher. */
    ros::Publisher pub_detections;

    /** \brief Processing done publisher. */
    ros::Publisher pub_done;

    /** \brief Simple instruction service. */
    ros::ServiceServer srv_instruction;

    /** \brief Processing done message. */
    std_msgs::String::Ptr done_msg;

    /** \brief Variable to control whether to accept new scenes. */
    bool accept_scene;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Metacloud received callback. */
    void metacloudCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Model object ceived callback. */
    void modelsCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Simple instructions service function. */
    bool instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp );
};

}

#endif
