#ifndef BACKPROJECTION_GPU_H
#define BACKPROJECTION_GPU_H

#include <iostream>
#include <heuros_core/metacloud_gpu.h>
#include <heuros_core/backprojection_models.h>
#include <heuros_core/detections.h>
#include <heuros_core/timers_gpu.h>

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif

using namespace std;
using namespace pcl::gpu;

class BackprojectionGPU
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    BackprojectionGPU();

    /** \brief Run backprojection and detection for the scene using the model. */
    void projectAllHistograms2D( MetacloudGPUConstPtr metacloud_gpu,
                                 DetectionsPtr detections,
                                 DetectionsPtr backprojection,
                                 BackprojectionModelsPtr models );

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief GPU timer events. */
    cudaEvent_t start_all, stop_a, stop_b, stop_c;
};

#endif
