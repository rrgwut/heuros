#ifndef BACKPROJECTION_H
#define BACKPROJECTION_H

#include <iostream>

#include <heuros_core/metacloud.h>
#include <heuros_core/backprojection_models.h>
#include <backprojection_gpu.h>

using namespace std;

class Backprojection
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Metacloud shared pointer. */
    MetacloudConstPtr metacloud;

    /** \brief Detections after grid filtering. */
    DetectionsPtr detections;

    /** \brief Backprojection results. */
    DetectionsPtr backprojection;

    /** \brief Gpu metacloud shared pointer. */
    MetacloudGPUConstPtr metacloud_gpu;

    /** \brief Backprojection metacloud on GPU. */
    MetacloudGPUPtr back_metacloud_gpu;

    /** \brief Candidate detections cloud on GPU. */
    DPointCloudPtr detections_gpu;

    /** \brief List of model objects. */
    BackprojectionModelsPtr models;

    /** \brief GPU backprojectio object. */
    BackprojectionGPU backprojection_gpu;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Run backprojection and detection for the scene using <<THE FIRST MODE FOR NOWL>>. */
    void projectAll();
};

#endif
