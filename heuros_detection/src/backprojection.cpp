#include <backprojection.h>

void Backprojection::projectAll()
{
    // Get the input metacloud
    metacloud_gpu = ADDRESS_2_METACLOUD_GPU(metacloud->metacloud_gpu);

    // Create the output GPU structures
    detections = DetectionsPtr( new Detections(models->numObjects()) );
    detections->class_names = models->class_names;
    detections->file_name = metacloud->file_name;

    backprojection = DetectionsPtr( new Detections(models->numObjects()) );
    backprojection->class_names = models->class_names;
    backprojection->file_name = metacloud->file_name;

    // Call the detection GPU method
    backprojection_gpu.projectAllHistograms2D( metacloud_gpu,
                                               detections,
                                               backprojection,
                                               models );
}
