#include <backprojection_gpu.h>
#include <stdio.h>

// ADJUSTABLE MAIN PARAMETERS
#define GRID_STEP 0.05f // meters
#define VOXEL_THRESHOLD 0.15 // fraction of full flat patch

//---------------------------------------------------------

__device__ void atomicCASFloat( float* address, float compare, float val )
{
    int* address_as_int = (int*)address;
    atomicCAS( address_as_int, __float_as_int(compare), __float_as_int(val) );
}

__device__ static float atomicMax(float* address, float val)
{
    int* address_as_i = (int*) address;
    int old = *address_as_i, assumed;
    do {
        assumed = old;
        old = ::atomicCAS(address_as_i, assumed,
            __float_as_int(::fmaxf(val, __int_as_float(assumed))));
    } while (assumed != old);
    return __int_as_float(old);
}

__device__ int pow( int x, int y )
{
    int result = 1;
    for( int i=0; i<y; i++ )
        result *= x;
    return result;
}

__global__ void backproject2D( const float *features,
                               unsigned int *backprojection,
                               const unsigned int *objects_test,
                               const unsigned int *histograms_relevance,
                               const int *histograms_lut,
                               int ftrs_step,
                               int num_histograms,
                               int num_ninja_sets,
                               int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){

        // Loop over ninja sets
        for( int ns=0; ns<num_ninja_sets; ns++ ){

            unsigned int val = 0;
            unsigned int ban = 0;

            // Loop over features
            for( int i=0; i<num_histograms; i++ ){

                // Read the feature relevance
                const unsigned int rel = *( histograms_relevance + i*num_ninja_sets + ns ); // Should be in global memory

                // Read the two concerning feature indices (LUT could be in global memory!)
                int ftr1_idx = *(histograms_lut+i*2);
                int ftr2_idx = *(histograms_lut+i*2+1);

                // Read the two concering feature values
                float ftr1_val = *(features + ftr1_idx*ftrs_step + idx);
                float ftr2_val = *(features + ftr2_idx*ftrs_step + idx);

                // Process only non-nans
                if( !NAN_CHECK(ftr1_val) && !NAN_CHECK(ftr2_val) ){
                    // Calculate the bins
                    int ftr1_bin = 0.5*(ftr1_val+0.99) * HIST_BINS;
                    int ftr2_bin = 0.5*(ftr2_val+0.99) * HIST_BINS;

                    // Get the ninja values
                    const unsigned int test = *( objects_test + (i*HIST_STEP + ftr1_bin*HIST_BINS + ftr2_bin)*num_ninja_sets + ns );

                    // Do the bitwise arithmetics
                    val |= test & rel;
                    ban |= ~test & rel;
                }else{
                    ban |= rel;
                }
            }

            // Write the final backprojection result
            unsigned int *target = backprojection + idx*num_ninja_sets + ns;
            *target = val & ~ban;
        }
    }
}

__global__ void getMaxCoord( DPoint *cloud,
                             float *max_coord,
                             int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        DPoint point = cloud[idx];
        float max_xyz = point.x;
        if( point.y > max_xyz ) max_xyz = point.y;
        if( point.z > max_xyz ) max_xyz = point.z;
        atomicMax( max_coord, max_xyz );
    }
}

__global__ void voxelHistogram( DPoint *cloud,
                                const unsigned int *backprojection,
                                int *grid_histogram,
                                int num_objects,
                                int grid_dim,
                                int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){

        DPoint point = *(cloud+idx);

        // Calculate bin coordinates
        int bin_x = point.x / GRID_STEP;
        int bin_y = point.y / GRID_STEP;
        int bin_z = point.z / GRID_STEP;

        // Get grid index
        int voxel_idx = ( bin_z*grid_dim + bin_y ) * grid_dim + bin_x;

        // Calculate number of ninja sets
        int num_ninja_sets = num_objects/32 + 1;

        // Loop over ninja sets
        for( int ns=0; ns<num_ninja_sets; ns++ ){
            // Read ninja set
            unsigned int ninja_vals = *( backprojection + idx*num_ninja_sets + ns );
            int n_max = (ns<num_ninja_sets-1) ? 32 : num_objects%32;
            // Loop over objects inside set
            for( int n_idx=0; n_idx<n_max; n_idx++ ){
                if( ninja_vals & 1 << n_idx ){
                    int grid_idx = voxel_idx*num_objects + ns*32 + n_idx;
                    atomicAdd( grid_histogram+grid_idx, 1 );
                }
            }
        }
    }
}

__global__ void blockThreshAndScan ( int *grid_histogram,
                                     char *grid_mask,
                                     unsigned int *ninja_grid,
                                     int *output_scan,
                                     int *block_sums,
                                     int threshold,
                                     int num_objects,
                                     int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){

        // Calculate number of ninja sets
        int num_ninja_sets = num_objects/32 + 1;

        // Thresholding result
        int thresholded_val = 0;

        // Loop over ninja sets
        for( int ns=0; ns<num_ninja_sets; ns++ ){
            int n_max = (ns<num_ninja_sets-1) ? 32 : num_objects%32;
            // Initialize ninja array for thresholding results
            unsigned int ninja_vals = 0;
            // Loop over objects inside set
            for( int n_idx=0; n_idx<n_max; n_idx++ ){
                int grid_idx = idx*num_objects + ns*32 + n_idx;
                // Threshold voxel value
                if( *(grid_histogram+grid_idx) > threshold ){
                    thresholded_val = 1;
                    ninja_vals |= 1 << n_idx;
                }
            }
            // Write output if needed
            if( ninja_vals )
                *( ninja_grid + idx*num_ninja_sets + ns ) = ninja_vals;
        }
        if( thresholded_val )
            *( grid_mask + idx ) = 1;

        // Create block scan array in shared memory
        __shared__ int values_scan[512];
        *(values_scan+threadIdx.x) = thresholded_val;

        // Synchronize
        __syncthreads();

        // Scan (512 vals so 2^9=512)
        for( int i=0; i<9; i++ ){
            int jump = pow(2,i);

            // Read
            int neighbor;
            if( threadIdx.x<jump )
                neighbor = 0;
            else
                neighbor = *(values_scan+threadIdx.x - jump);

            // Synchronize
            __syncthreads();

            // Write
            *(values_scan+threadIdx.x) += neighbor;

            // Synchronize
            __syncthreads();
        }

        // Copy the results to the output scan
        *(output_scan+idx) = *(values_scan+threadIdx.x);

        // If we are at the last thread of the block, write to block sums
        if( threadIdx.x == blockDim.x-1 || idx == N-1 )
            *(block_sums+blockIdx.x) = *(values_scan+threadIdx.x);
    }
}

__global__ void compactGrid ( char *grid_mask,
                              unsigned int *ninja_grid,
                              DPoint *out_points,
                              unsigned int *ninja_out,
                              int *block_scans,
                              int *block_sums,
                              int num_ninja_sets,
                              int grid_dim,
                              int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        if( *(grid_mask+idx) > 0 ){

            // Calculate address
            int address = 0;
            if( blockIdx.x > 0 )
                address += *(block_sums+blockIdx.x-1);
            address += *(block_scans+idx)-1;

            // Calculate voxel coordinates
            DPoint point;
            point.z = idx / int(grid_dim*grid_dim) * GRID_STEP + 0.5*GRID_STEP;
            point.y = idx % int(grid_dim*grid_dim) / grid_dim * GRID_STEP + 0.5*GRID_STEP;
            point.x = idx % int(grid_dim) * GRID_STEP + 0.5*GRID_STEP;

            // Write point xyz
            *(out_points+address) = point;

            // Copy ninja values
            for( int ns=0; ns<num_ninja_sets; ns++ ){
                *( ninja_out + address*num_ninja_sets + ns ) = *( ninja_grid + idx*num_ninja_sets + ns );
            }
        }
    }
}


BackprojectionGPU::BackprojectionGPU()
{
    cudaEventCreate( &start_all );
    cudaEventCreate( &stop_a );
    cudaEventCreate( &stop_b );
    cudaEventCreate( &stop_c );
}

void BackprojectionGPU::projectAllHistograms2D( MetacloudGPUConstPtr metacloud_gpu,
                                                DetectionsPtr detections,
                                                DetectionsPtr backprojection,
                                                BackprojectionModelsPtr models )
{
    // ================== BACKPROJECTION ==================

    backprojection->cloud = metacloud_gpu->cloud;
    backprojection->bit_objects = NinjaArrayPtr( new NinjaArray );
    backprojection->bit_objects->create( metacloud_gpu->size()*backprojection->numNinjaSets() );

    timerStart( start_all );

    // Call backproject2D kernel
    int N = metacloud_gpu->size();
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Kernel
    backproject2D <<< nBlocks, blockSize >>> ( metacloud_gpu->features->array->ptr(),
                                               backprojection->bit_objects->ptr(),
                                               models->objects_test->ptr(),
                                               models->histograms_relevance->ptr(),
                                               models->histograms_lut->array->ptr(),
                                               metacloud_gpu->features->step,
                                               models->numHistograms(),
                                               models->numNinjaSets(),
                                               N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
    cout << "Backprojection done in: " << fixed << timerStop( start_all, stop_a ) << " ms." << endl;

    // ================== CREATE THE 3D GRID HISTOGRAM ==================

    DeviceArray<float> d_max_coord( 1 );
    cudaMemset( d_max_coord.ptr(), 0, 1*sizeof(float) );

    N = metacloud_gpu->size();
    blockSize = 512;
    nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Kernel
    getMaxCoord <<< nBlocks, blockSize >>> ( backprojection->cloud->ptr(),
                                             d_max_coord.ptr(),
                                             N );

    float max_coord;
    d_max_coord.download(&max_coord);
    int grid_dim = max_coord / GRID_STEP + 1;
    int num_voxels = (grid_dim*grid_dim*grid_dim);

    // Create the grid
    DeviceArray<int> d_grid_histogram( num_voxels * backprojection->numObjects() );
    cudaMemset( d_grid_histogram.ptr(), 0, d_grid_histogram.size()*sizeof(int) );

    // Kernel
    voxelHistogram <<< nBlocks, blockSize >>> ( backprojection->cloud->ptr(),
                                                backprojection->bit_objects->ptr(),
                                                d_grid_histogram.ptr(),
                                                models->numObjects(),
                                                grid_dim,
                                                N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
    cout << "Voxel histogram built in: " << fixed << timerStop( stop_a, stop_b ) << " ms." << endl;

    // ================== THRESHOLD AND SCAN THE HISTOGARM ==================

    // Create the bitwise object masks
    NinjaArray d_ninja_grid( num_voxels * backprojection->numNinjaSets() );
    cudaMemset( d_ninja_grid.ptr(), 0, d_ninja_grid.sizeBytes() );

    // Create the thresholded grid mask
    DeviceArray<char> d_grid_mask( num_voxels );
    cudaMemset( d_grid_mask.ptr(), 0, d_grid_mask.sizeBytes() );

    // Create the scan grid
    DeviceArray<int> d_block_scans( num_voxels );

    N = num_voxels;
    blockSize = 512;
    nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Create the block sums array for global scan
    DeviceArray<int> d_block_sums( nBlocks );

    int pts_per_voxel = metacloud_gpu->avg_neighbors / F_PI * (1.0/NBR_R_1)*(1.0/NBR_R_1) * GRID_STEP * GRID_STEP;

    // Kernel (threshold and local block scan)
    blockThreshAndScan <<< nBlocks, blockSize >>> ( d_grid_histogram.ptr(),
                                                    d_grid_mask.ptr(),
                                                    d_ninja_grid.ptr(),
                                                    d_block_scans.ptr(),
                                                    d_block_sums.ptr(),
                                                    pts_per_voxel*VOXEL_THRESHOLD,
                                                    models->numObjects(),
                                                    N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Download the block sums
    vector<int> h_block_sums;
    d_block_sums.download( h_block_sums );

    // Scan on CPU - WARNING: THIS CAN BE SLOW FOR LARGE GRIDS!!!
    int marching_sum = 0;
    for( int i=0; i<h_block_sums.size(); i++ ){
        marching_sum += h_block_sums[i];
        h_block_sums[i] = marching_sum;
    }

    // Upload scan result to GPU
    d_block_sums.upload( h_block_sums );

    // ================== COMPACT THE GRID ==================

    int num_out_voxels = h_block_sums[h_block_sums.size()-1];
    detections->cloud = DPointCloudPtr( new DPointCloud(num_out_voxels) );
    detections->bit_objects = NinjaArrayPtr( new NinjaArray );
    detections->bit_objects->create( num_out_voxels*detections->numNinjaSets() );

    N = num_voxels;
    blockSize = 512;
    nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Kernel
    compactGrid <<< nBlocks, blockSize >>> ( d_grid_mask.ptr(),
                                             d_ninja_grid.ptr(),
                                             detections->cloud->ptr(),
                                             detections->bit_objects->ptr(),
                                             d_block_scans.ptr(),
                                             d_block_sums.ptr(),
                                             detections->numNinjaSets(),
                                             grid_dim,
                                             N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
    cout << "Grid compacting done in: " << fixed << timerStop( stop_b, stop_c ) << " ms." << endl;

    cout << "Number of output voxels: " << detections->cloud->size() << endl;

    // Sum the points into the voxels and threshold. Voila
}
