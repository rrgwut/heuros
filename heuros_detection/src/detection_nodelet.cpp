#include <detection_nodelet.h>
// this should really be in the implementation (.cpp file)
#include <pluginlib/class_list_macros.h>

namespace heuros_detection
{

void DetectionNodelet::onInit()
{
    cout << "Starting Heuros Detection..." << endl;

    accept_scene = false;
    ros::NodeHandle n;

    // Set up publishers
    pub_backprojection = n.advertise<heuros_core::StampedAddress>( "/heuros/det_backprojection", 1, false );
    pub_detections = n.advertise<heuros_core::StampedAddress>( "/heuros/det_detections", 1, false );
    pub_done = n.advertise<std_msgs::String>( "/heuros/done", 1 );
    done_msg = std_msgs::String::Ptr( new std_msgs::String );
    done_msg->data = "detection";

    // Set up subscribers
    sub_metacloud = n.subscribe( "/heuros/seg_metacloud", 1, &DetectionNodelet::metacloudCb , this );
    sub_models = n.subscribe( "/heuros/backprojection_models", 1, &DetectionNodelet::modelsCb , this );

    // Set up services
    srv_instruction = n.advertiseService( "/heuros/det_instruction", &DetectionNodelet::instructionSrv, this );
}

void DetectionNodelet::metacloudCb( const heuros_core::StampedAddressConstPtr &msg )
{
    // Accept only if instructed
    if( !accept_scene ) return;
    accept_scene = false;

    cout << "Detection: Metacloud received" << endl;
    backprojection.metacloud = ADDRESS_2_METACLOUD(msg->address);
    backprojection.projectAll();

    // Publish backprojection results
    cout << "Publishing backprojection..." << endl;
    heuros_core::StampedAddressPtr projection_msg( new heuros_core::StampedAddress );
    projection_msg->address = PTR_2_ADDRESS( backprojection.backprojection );
    pub_backprojection.publish( projection_msg );

    // Publish detection results
    cout << "Publishing detections..." << endl;
    heuros_core::StampedAddressPtr detections_msg( new heuros_core::StampedAddress );
    detections_msg->address = PTR_2_ADDRESS( backprojection.detections );
    pub_detections.publish( detections_msg );

    // Publish done message
    pub_done.publish( done_msg );
}

void DetectionNodelet::modelsCb( const heuros_core::StampedAddressConstPtr &msg )
{
    cout << "Detection: Model objects received" << endl;
    BackprojectionModelsPtr tmp_models = ADDRESS_2_BACKPROJECTION_MODELS(msg->address);
    backprojection.models = tmp_models;

    // Publish done message
    pub_done.publish( done_msg );
}

// ------------------------------
// ----- INSTRUCTION SERVER -----
// ------------------------------

bool DetectionNodelet::instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp )
{
    // ================== TARGET SCENE ==================

    if( req.instruction == "detect" )
    {
        cout << "Detection: Received detect instruction" << endl;
        accept_scene = true;
    }

    // ==================

    return true;
}

}

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(heuros_detection::DetectionNodelet, nodelet::Nodelet)
