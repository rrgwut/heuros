#include <hse_nodelet.h>
// this should really be in the implementation (.cpp file)
#include <pluginlib/class_list_macros.h>
#include <boost/thread.hpp>

namespace heuros_hse
{

void HSENodelet::onInit()
{
    cout << "Starting Heuros Histogram Search Engine..." << endl;

    ros::NodeHandle n;

    // Set up publishers
    pub_metacloud = n.advertise<heuros_core::StampedAddress>( "/heuros/hse_metacloud", 1, true );

    // Set up subscribers
    sub_metacloud = n.subscribe( "/heuros/seg_metacloud", 1, &HSENodelet::metacloudCb , this );
    sub_pick = n.subscribe( "/heuros/picked_point", 1, &HSENodelet::pickCb , this );

    // Set up services
    srv_instruction = n.advertiseService( "/heuros/hse_instruction", &HSENodelet::instructionSrv, this );
    srv_get_class_names = n.advertiseService("/heuros/hse/get_calss_names",&HSENodelet::getClassNamesSrv, this );

    if(hse_manager.loadBaseFromFile("default.hse")){
        cout << "HSE: Model base loaded from file" << endl;
    }
}

void HSENodelet::metacloudCb( const heuros_core::StampedAddressConstPtr &msg )
{
    cout << "HSE: Metacloud received" << endl;

    hse_manager.calcNaiveHistograms( ADDRESS_2_METACLOUD(msg->address) );

    hse_manager.runSearchEngine();

    hse_manager.metacloud->props.clear();
    for(size_t i=0; i<hse_manager.detections_class.size(); i++){
        int seg = hse_manager.detections_seg[i];
        int cls = hse_manager.detections_class[i];
        float scr = hse_manager.detections_score[i];
        string cls_name = hse_manager.model_names[cls];
        pcl::PointXYZ p = hse_manager.detections_pos[i];

        Prop pr(p, cls_name, seg);
        hse_manager.metacloud->props.push_back(pr);

        cout << "HSE: Segment "<<seg<<" at x: "<<p.x<<" y: "<<p.y<<" z: "<<p.z<<
                " has "<<scr<<" similarity with class "
             <<cls_name<<endl;
    }

    // Publish results
    cout << "HSE: Publishing props..." << endl;
    heuros_core::StampedAddressPtr pub_msg( new heuros_core::StampedAddress );
    pub_msg->address = PTR_2_ADDRESS( hse_manager.metacloud );
    pub_metacloud.publish( pub_msg );

    //boost::thread callThread(&HSENodelet::callService, this);
}

void HSENodelet::pickCb( const heuros_core::PickedPointConstPtr &msg )
{
    cout << "HSE: Picked point received." << endl;
    if(hse_manager.setActiveObject(msg->index)){
        cout << "HSE: Active object set." << endl;
    }
}

// ------------------------------
// ----- INSTRUCTION SERVER -----
// ------------------------------

bool HSENodelet::getClassNamesSrv(heuros_core::GetStringListRequest &req, heuros_core::GetStringListResponse &resp){
    cout << "HSE: Received get class names instruction" << endl;
    if(req.query == "get_all_names"){
        resp.string_list = hse_manager.model_names;
    }
    return true;
}

bool HSENodelet::instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp )
{
    if(req.instruction == "add_model_to_base"){
        string model_name = req.parameter;
        if(hse_manager.addModelToBase(model_name)){
            cout << "HSE: Model added to the base" << endl;
        }
    }
    else if(req.instruction == "save_base_to_file"){
        string base_name = req.parameter;
        if(hse_manager.saveBaseToFile(base_name)){
            cout << "HSE: Model base saved to file" << endl;
        }
    }
    else if(req.instruction == "load_base_from_file"){
        string base_name = req.parameter;
        if(hse_manager.loadBaseFromFile(base_name)){
            cout << "HSE: Model base loaded from file" << endl;
        }
    }
    else if(req.instruction == "new_base"){
        hse_manager.model_histograms.resize(0);
        hse_manager.model_names.resize(0);
        cout << "HSE: New model base" << endl;
    }
    return true;
}

}

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(heuros_hse::HSENodelet, nodelet::Nodelet)
