#include <hse_manager.h>
#include <heuros_histograms/histogram_calculations_cpu.h>

HSE_Manager::HSE_Manager()
{
    cudaEventCreate( &start_all );
    cudaEventCreate( &stop_all );
}

HSE_Manager::~HSE_Manager(){
    cudaEventDestroy(start_all);
    cudaEventDestroy(stop_all);
}

void HSE_Manager::calcNaiveHistograms( MetacloudPtr _metacloud ){

    metacloud = _metacloud;

    int num_segments = metacloud->segmented_points.size();
    naive_histograms.resize(num_segments);
    for(size_t i=0; i<num_segments; i++){
        // Call the calculation method
        vector<HistogramCPU> ftr_hists = HistogramCalculationsCPU::calcAllHistograms( metacloud, metacloud->segmented_points[i]);
        naive_histograms[i] = ftr_hists; 
    }
}

void HSE_Manager::runSearchEngine(){
    const float score_thresh = 0.9f;
    timerStart( start_all );
    //calculate correlations
    vector<vector<vector<Mat> > > all_correlations;
    all_correlations.resize(naive_histograms.size());
    for(size_t i=0; i<all_correlations.size(); i++){
        vector<vector<Mat> > seg_all_correlations;
        vector<HistogramCPU> seg_hist = naive_histograms[i];
        seg_all_correlations.resize(model_histograms.size());
        for(size_t j=0; j<seg_all_correlations.size(); j++){
            vector<Mat> seg_class_correlations;
            vector<vector<HistogramCPU> > class_histograms = model_histograms[j];
            seg_class_correlations.resize(class_histograms.size());
            for(size_t k=0; k<seg_class_correlations.size(); k++){
                vector<HistogramCPU> model_hist = class_histograms[k];
                Mat corr;
                HistogramCalculationsCPU::calcCorrelations(seg_hist, model_hist, corr, corr_modes);
                seg_class_correlations[k] = corr;
            }
            seg_all_correlations[j] = seg_class_correlations;
        }
        all_correlations[i] = seg_all_correlations;
    }
    //find best correlation for each segment
    vector<int> best_classes;
    vector<float> best_scores;
    best_classes.resize(all_correlations.size());
    best_scores.resize(all_correlations.size());
    for(size_t i=0; i<all_correlations.size(); i++){
        vector<vector<Mat> > seg_all_correlations = all_correlations[i];
        int best_class;
        float best_score = -1.0f;
        for(size_t j=0; j<seg_all_correlations.size(); j++){
            vector<Mat> seg_class_correlations = seg_all_correlations[j];
            for(size_t k=0; k<seg_class_correlations.size(); k++){
                Mat corr = seg_class_correlations[k];
                Mat weights = uniqueness.col(j);
                float score = HistogramCalculationsCPU::getObjectCorrelation(corr, weights);
                if(score > best_score){
                    best_score = score;
                    best_class = j;
                }
            }
        }
        best_classes[i] = best_class;
        best_scores[i] = best_score;
    }

    //find matches
    detections_class.resize(0);
    detections_score.resize(0);
    detections_seg.resize(0);
    detections_pos.resize(0);
    for(size_t i=0; i<best_scores.size(); i++){
        float score = best_scores[i];
        if(score >= score_thresh){
            detections_class.push_back(best_classes[i]);
            detections_score.push_back(best_scores[i]);
            detections_seg.push_back(i);
            pcl::PointXYZ p = calculateSegmentCenter(i);
            detections_pos.push_back(p);
        }
    }

    cout << "HSE: Search Engine finished work in: " << fixed <<
            timerStop( start_all, stop_all ) << " ms." << endl;
}

bool HSE_Manager::setActiveObject(int point_idx){
    int nonflat_idx = -1;
    for(size_t i=0; i<metacloud->segment_sets_names.size(); i++){
        if(metacloud->segment_sets_names.at(i) == "Nonflat segments"){
            nonflat_idx = i;
            break;
        }
    }
    if(nonflat_idx == -1){
        cout << "HSE: Unable to find Nonflat segments in metacloud" <<endl;
        return false;
    }
    int seg_idx = metacloud->segment_indices.at(nonflat_idx)->at(point_idx);
    if(seg_idx<0 || seg_idx>=metacloud->segmented_points.size()){
        cout << "HSE: Picked point does not belong to any segment" << endl;
        return false;
    }
    active_object = naive_histograms.at(seg_idx);
    return true;
}

bool HSE_Manager::addModelToBase(string model_name){
    if(active_object.size()==0){
        cout << "HSE: No active object" <<endl;
        return false;
    }
    int class_idx = -1;
    for(size_t i=0; i<model_names.size(); i++){
        if(model_name == model_names[i]){
            class_idx = i;
            break;
        }
    }
    if(class_idx == -1){
        class_idx = model_names.size();
        model_names.push_back(model_name);
        vector<vector<HistogramCPU> > class_vec;
        model_histograms.push_back(class_vec);
    }

    model_histograms[class_idx].push_back(active_object);
    return true;
}

bool HSE_Manager::loadBaseFromFile(string base_name){
    string package_path = ros::package::getPath("heuros_hse");
    string base_path = package_path + "/../data/hse/" + base_name;
    ifstream base(base_path.c_str());
    if(!base.is_open()){
        cout << "HSE: unable to load from" << base_path << endl;
        return false;
    }

    int n;
    float f;
    string str;

    int num_hist_features;
    base>>num_hist_features;
    corr_modes.resize(num_hist_features);
    for(int i=0; i<num_hist_features; i++){
        base>>n;
        corr_modes[i] = n;
    }

    base>>n;
    model_histograms.resize(n);
    model_names.resize(n);
    uniqueness = Mat(num_hist_features, model_histograms.size(), CV_32FC1);
    for(size_t i=0; i<model_histograms.size(); i++){
        base>>str;
        model_names[i] = str;
        for(int j=0; j<num_hist_features; j++){
            base>>f;
            uniqueness.at<float>(j,i) = f;
        }
        base>>n;
        vector<vector<HistogramCPU> > class_histograms;
        class_histograms.resize(n);
        for(size_t j=0; j<class_histograms.size(); j++){
            base>>n;
            vector<HistogramCPU> object_histograms;
            object_histograms.resize(n);
            for(size_t k=0; k<object_histograms.size(); k++){
                HistogramCPU hist;
                base>>n;
                hist.lut.resize(n);
                for(size_t m=0; m<hist.lut.size(); m++){
                    base>>n;
                    hist.lut[m] = n;
                }
                int cols, rows;
                base >> cols;
                base >> rows;
                hist.data = cv::Mat(rows, cols, CV_32FC1);
                for(int y=0; y<hist.data.rows; y++){
                    for(int x=0; x<hist.data.cols; x++){
                        base >> f;
                        hist.data.at<float>(y,x) = f;
                    }
                }
                base>>n;
                hist.instance = n;
                object_histograms[k] = hist;
            }
            class_histograms[j] = object_histograms;
        }
        model_histograms[i] = class_histograms;
    }

    base.close();
    return true;
}

pcl::PointXYZ HSE_Manager::calculateSegmentCenter(int seg_idx){
    double x=0, y=0, z=0;
    IndicesPtr seg = metacloud->segmented_points[seg_idx];
    for(size_t i=0; i<seg->size(); i++){
        int idx = seg->at(i);
        pcl::PointXYZ p = metacloud->cloud_xyz->at(idx);
        x += p.x;
        y += p.y;
        z += p.z;
    }
    int n= seg->size();
    pcl::PointXYZ center;
    center.x = x/n;
    center.y = y/n;
    center.z = z/n;
    return center;
}

bool HSE_Manager::saveBaseToFile(string base_name){
    if(model_histograms.size() == 0){
        cout << "HSE: model base empty, nothing to save" << endl;
        return false;
    }

    //calculate features uniqueness for base
    int num_hist_features = model_histograms[0][0].size();
    corr_modes.resize(num_hist_features);
    vector<Mat> unq_scores(2);
    for(int i=USE_PEARSON_CORR; i<=USE_CUSTOM_CORR; i++){
        for(size_t j=0; j<corr_modes.size(); j++){
            corr_modes[j] = i;
        }
        HistogramsManager::processHistograms(model_histograms,
                                             model_names,
                                             corr_modes,
                                             uniqueness, unq_scores[i-USE_PEARSON_CORR]);
    }
    //find best uniqueness score
    for(size_t i=0; i<corr_modes.size(); i++){
        float best_res = -10;
        float best_mode = -1;
        for(size_t j=0; j<unq_scores.size(); j++){
            Mat unq = unq_scores[j];
            float r = unq.at<float>(i);
            if(r>best_res){
                best_res = r;
                best_mode = j + USE_PEARSON_CORR;
            }
        }
        corr_modes[i] = best_mode;
    }
    HistogramsManager::processHistograms(model_histograms,
                                         model_names,
                                         corr_modes,
                                         uniqueness, uniqueness_score);

    string package_path = ros::package::getPath("heuros_hse");
    string base_path = package_path + "/../data/hse/" + base_name;
    ofstream base(base_path.c_str());
    if(!base.is_open()){
        cout << "HSE: unable to save to " << base_path << endl;
        return false;
    }
    base<<num_hist_features<<endl;
    for(size_t i=0; i<corr_modes.size(); i++){
        base << corr_modes[i] << " ";
    }
    base << endl;

    base<<model_histograms.size()<<endl;
    for(size_t i=0; i<model_histograms.size(); i++){
        base<<model_names[i]<<endl;
        for(size_t j=0; j<num_hist_features; j++){
            float uw = uniqueness.at<float>(j,i);
            float uw_thresh = 2;
            uw = 1;//uw<uw_thresh ? 0 : 1;
            base << uw <<" ";
        }
        base << endl;
        vector<vector<HistogramCPU> > class_histograms = model_histograms[i];
        base<<class_histograms.size()<<endl;
        for(size_t j=0; j<class_histograms.size(); j++){
            vector<HistogramCPU> object_histograms = class_histograms[j];
            base<<object_histograms.size()<<endl;
            for(size_t k=0; k<object_histograms.size(); k++){
                HistogramCPU hist = object_histograms[k];

                base<<hist.lut.size()<<endl;
                for(size_t m=0; m<hist.lut.size(); m++){
                    base << hist.lut[m] << " ";
                }
                base << endl;

                base<<hist.data.cols<<" "<<hist.data.rows<<endl;
                for(int y=0; y<hist.data.rows; y++){
                    for(int x=0; x<hist.data.cols; x++){
                        base << hist.data.at<float>(y,x) << " ";
                    }
                }
                base << endl;

                base <<hist.instance << endl;
            }
        }
    }

    base.close();
    return true;
}
