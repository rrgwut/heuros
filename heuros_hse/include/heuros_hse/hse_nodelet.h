#ifndef HSE_NODELET_H
#define HSE_NODELET_H

#include <nodelet/nodelet.h>
#include <ros/ros.h>

#include <heuros_core/StampedAddress.h>
#include <heuros_core/PickedPoint.h>
#include <heuros_core/SimpleInstruction.h>
#include <heuros_core/GetStringList.h>
#include <hse_manager.h>

using namespace std;

namespace heuros_hse
{

class HSENodelet : public nodelet::Nodelet
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief On nodelet initialization. */
    virtual void onInit();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Segmentation manager object. */
    HSE_Manager hse_manager;

    /** \brief Metacloud publisher */
    ros::Publisher pub_metacloud;

    /** \brief Metacloud subscriber. */
    ros::Subscriber sub_metacloud;

    /** \brief Picked point subscriber. */
    ros::Subscriber sub_pick;

    /** \brief Simple instruction service. */
    ros::ServiceServer srv_instruction;

    /** \brief Get string list of class names service. */
    ros::ServiceServer srv_get_class_names;

    /** \brief Simple instruction client for the visualization node. */
    ros::ServiceClient cli_vis_instruction;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Metacloud received callback. */
    void metacloudCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Picked point received callback. */
    void pickCb( const heuros_core::PickedPointConstPtr &msg );

    /** \brief Simple instructions service function. */
    bool instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp );

    /** \brief Get file list service function. */
    bool getClassNamesSrv( heuros_core::GetStringListRequest &req, heuros_core::GetStringListResponse &resp );

    /** \brief Call service funcion - for multithreading */
    void callService();
};

}

#endif
