#ifndef HSE_MANAGER_H
#define HSE_MANAGER_H

#include <fstream>

#include <heuros_core/metacloud.h>
#include <heuros_core/metacloud_gpu.h>
#include <heuros_core/timers_gpu.h>
#include <heuros_histograms/histograms_manager.h>

class HSE_Manager
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Metacloud shared pointer. */
    MetacloudPtr metacloud;

    /** \brief GPU metacloud. */
    //MetacloudGPUPtr metacloud_gpu;

    /** \brief GPU timer events. */
    cudaEvent_t start_all, stop_all;

    /** \brief Active cluster histograms - for user commands */
    vector<HistogramCPU> active_object;

    /** \brief Models histograms. */
    vector<vector<vector<HistogramCPU> > > model_histograms;

    /** \brief Models classes. */
    vector<string> model_names;

    /** \brief Histograms calculated for naive (nonflat) segments. */
    vector<vector<HistogramCPU> > naive_histograms;

    /** \brief Parameters of the model base */
    Mat uniqueness;
    Mat uniqueness_score;
    vector<int> corr_modes;

    /** \brief Detected objects - segment index, class index and scor. */
    vector<int> detections_seg;
    vector<int> detections_class;
    vector<float> detections_score;
    vector<pcl::PointXYZ> detections_pos;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    HSE_Manager();

    /** \brief Destructor. */
    ~HSE_Manager();

    /** \brief Set active histograms based on point index. */
    bool setActiveObject(int point_idx);

    /** \brief Add model to the base. */
    bool addModelToBase(string model_name);

    /** \brief Save base to file. */
    bool saveBaseToFile(string base_name);

    /** \brief Load base from file. */
    bool loadBaseFromFile(string base_name);

    /** \brief Calculate histograms for the naive (nonflat) segments. */
    void calcNaiveHistograms( MetacloudPtr _metacloud );

    /** \brief Rus Histogram Search Engine for current metacloud. */
    void runSearchEngine();

    /** \brief calculates center of a given segment. */
    pcl::PointXYZ calculateSegmentCenter(int seg_idx);
};

#endif
