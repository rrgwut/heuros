#ifndef HISTOGRAMS_NODELET_H
#define HISTOGRAMS_NODELET_H

#include <iostream>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PointStamped.h>

#include <heuros_core/StampedAddress.h>
#include <heuros_core/PickedPoint.h>
#include <heuros_core/SimpleInstruction.h>
#include <histograms_manager.h>
//#include <histograms_1d.h>

using namespace std;

namespace heuros_histograms
{

class HistogramsNodelet : public nodelet::Nodelet
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief On nodelet initialization. */
    virtual void onInit();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Metacloud shared pointer. */
    MetacloudPtr metacloud;

    /** \brief Segmentation result indices. */
    IndicesConstPtr indices;

    /** \brief Histograms manager object. */
    HistogramsManager histograms_manager;

    /** \brief 1D histograms utility object. */
    //Histograms1D histograms_1d;

    /** \brief Metacloud subscriber. */
    ros::Subscriber sub_metacloud;

    /** \brief Cluster subscriber. */
    ros::Subscriber sub_cluster;

    /** \brief Publisher for metacloud with calculated histograms */
    ros::Publisher pub_metacloud;

    /** \brief Scene processed publisher. */
    ros::Publisher pub_done;

    /** \brief Simple instruction service. */
    ros::ServiceServer srv_instruction;

    /** \brief Processing done message. */
    std_msgs::String::Ptr done_msg;

    /** \brief Variables that control whether to accept new scenes and clusters. */
    bool accept_scene, accept_cluster;

    /** \brief Correlation mode. */
    int correlatiopn_mode;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Metacloud received callback. */
    void metacloudCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Cluster indices received callback. */
    void clusterCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Simple instructions service function. */
    bool instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp );
};

}

#endif
