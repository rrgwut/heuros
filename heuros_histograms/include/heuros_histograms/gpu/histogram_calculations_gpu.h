#ifndef HISTOGRAM_CALCULATIONS_GPU_H
#define HISTOGRAM_CALCULATIONS_GPU_H

#include <heuros_core/metacloud_gpu.h>
#include <heuros_core/timers_gpu.h>

using namespace std;

#define HIST_NORM_L1 1

#define HIST_NORM_L2 2

class HistogramCalculationsGPU
{
public:

    static void extractFeaturesCluster( const FeaturesGPU<float> &d_src, FeaturesGPU<float> &d_dst, IndicesConstPtr &h_indices );

    static void calcUnnormHistograms( const FeaturesGPU<float> &features, HistogramsGPU<int> &histograms );

    static void calcNormHistograms( const FeaturesGPU<float> &features, HistogramsGPU<float> &histograms, int norm );

    static void normalizeHistograms( HistogramsGPU<int> &src_hists, HistogramsGPU<float> &dst_hists, int norm );

    static void standarize( const HistogramsGPU<float> &src_hists, HistogramsGPU<float> &dst_hists );

    static void calcCorrelations( const HistogramsGPU<float> &array_a, const HistogramsGPU<float> &array_b, vector<float> &correlations );
};

#endif
