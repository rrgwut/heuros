#ifndef HISTOGRAMS_MANAGER
#define HISTOGRAMS_MANAGER

#include <iostream>
#include <fstream>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <ros/ros.h>
#include <ros/package.h>
#include <opencv2/opencv.hpp>

#include <heuros_core/metacloud.h>
#include <heuros_core/metacloud_gpu.h>
#include <heuros_core/timers_gpu.h>
#include <heuros_histograms/histogram_calculations_cpu.h>

#ifndef USE_PEARSON_CORR
#define USE_PEARSON_CORR 1
#define USE_CUSTOM_CORR 2
#define USE_BHATTACHARYYA 3
#define USE_L1_DIST 4
#define USE_L2_DIST 5
#endif

#define GROUPING_CLASS_ALL 1
#define GROUPING_CLASS_NOREP 2
#define GROUPING_INSTANCE 3

class HistogramsManager
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================    

    /** \brief Vector of CPU feature histograms. */
    vector<vector<vector<HistogramCPU> > > histograms;

    /** \brief Vector of class names. */
    vector<string> class_names;

    /** \brief Vector of basic feature names. */
    vector<string> base_feature_names;

    /** \brief Vector of histogram feature names. */
    vector<string> hist_feature_names;

    /** \brief GPU timer events. */
    cudaEvent_t start_all, start_gpu, start_cpu, stop_all;

    /** \brief Histogram grouping mode (either class or instance). */
    int grouping_mode;

    /** \brief vector of matrices of avarage correlations for each feature. */
    vector<cv::Mat> mean_corrs;

    /** \brief vector of matrices of all correlations for each feature. */
    vector<cv::Mat> all_corrs;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    HistogramsManager();

    /** \brief Reset the histograms. */
    void clear();

    /** \brief Create and save the histograms for a new cluster. */
    void processCluster( MetacloudPtr metacloud, IndicesConstPtr indices );

    /** \brief Process the all the created histograms. */
    void processAllHistograms( string file_name, int corr_mode );

    /** \brief Process the given histograms vector with choosen correlation
     * mode for each feature */
    static void processHistograms(vector<vector<vector<HistogramCPU> > > &hists,
                                 vector<string> &class_names,
                                 vector<int> &corr_modes,
                                 cv::Mat &uniqueness,
                                 cv::Mat &uniqueness_score);

    /** \brief Calculate uniqueness in pairs for histograms. */
    void calculateUniquenessInPairs( string file_name );

    /** \brief Save the object models to file. */
    void saveHistograms( string file_name );

    /** \brief Save the object models to file. */
    void saveStatistics( string file_name );

    /** \brief Load the object models from file. */
    void loadHistograms( string file_name );

    /** \brief Calculate histograms for segments on the scene */
    void calcHistogramsForSegments( MetacloudPtr metacloud );

protected:

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Get class index. Append class to structures if doesn't exist. */
    void getOrCreateClass( int &class_idx, int &instance );

    /** \brief Setup histogram features names. */
    void setHistFeaturesNames();

    /** \brief Convert instances to object classes. */
    void instancesToClasses( vector<vector<vector<HistogramCPU> > > &new_histograms, vector<string> &new_class_names );
};

#endif
