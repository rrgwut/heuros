#ifndef HISTOGRAM_CALCULATIONS_CPU_H
#define HISTOGRAM_CALCULATIONS_CPU_H

#include<iostream>
#include <cv.h>
#include <highgui.h>

#include<heuros_core/metacloud.h>
#include<heuros_core/histograms_cpu.h>
#include<heuros_histograms/gpu/histogram_calculations_gpu.h>

#ifndef USE_PEARSON_CORR
#define USE_PEARSON_CORR 1
#define USE_CUSTOM_CORR 2
#define USE_BHATTACHARYYA 3
#define USE_L1_DIST 4
#define USE_L2_DIST 5
#endif

using namespace std;
using namespace cv;

class HistogramCalculationsCPU
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Calculate normalized histograms. */
    static void calcNormHistograms( const MetacloudConstPtr &metacloud, const IndicesConstPtr &indices, vector<HistogramCPU> &histograms, int norm );

    /** \brief Calculate normalized D2, D3 and D4 histograms. */
    static void calcNormD2D3D4Histograms( const MetacloudConstPtr &metacloud, const IndicesConstPtr &indices, vector<HistogramCPU> &histograms, int norm );

    /** \brief Calculate correlations of corresponding feature histograms. */
    static void calcCorrelations( vector<HistogramCPU> &array_a, vector<HistogramCPU> &array_b, Mat &correlations, int method = CV_COMP_CORREL );

    /** \brief Calculate correlations of corresponding feature histograms
     * with method given for each feature */
    static void calcCorrelations( vector<HistogramCPU> &array_a, vector<HistogramCPU> &array_b, Mat &correlations, vector<int> methods_for_features);

    /** \brief Calculate correlations of corresponding feature histograms - custom method */
    static void calcCorrelations_2( vector<HistogramCPU> &array_a, vector<HistogramCPU> &array_b, Mat &correlations );

    /** \brief Calculate correlations of corresponding feature histograms - using L2 distance */
    static void calcCorrelations_L1L2( vector<HistogramCPU> &array_a, vector<HistogramCPU> &array_b, Mat &correlations, bool L2_mode );

    /** \brief Get objects correlation from all histograms correlation. */
    static float getObjectCorrelation(Mat &corr, Mat &weights);

    /** \brief Calculate all the histograms of a cluster. */
    static vector<HistogramCPU> calcAllHistograms( const MetacloudConstPtr &metacloud, const IndicesConstPtr &indices, int instance=0 );

protected:

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Calculate histogram of a given feature over the interest indices. */
    static void featureHistogram( const MetacloudConstPtr &metacloud, HistogramCPU &histogram, const Mat &mask, int norm );

    /** \brief Increment bin value and expand histogram, if necessary. */
    static void incrementBin(Mat& data, int idx);

    /** \brief Calculate custom histogram correlation. */
    static double compareHist_2(Mat a, Mat b);

    /** \brief Calculate L2 distance based histogram correlation. */
    static double compareHist_L1L2(Mat a, Mat b, bool L2_mode);
};

#endif
