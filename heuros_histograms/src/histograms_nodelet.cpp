#include <histograms_nodelet.h>
// this should really be in the implementation (.cpp file)
#include <pluginlib/class_list_macros.h>

namespace heuros_histograms
{

void HistogramsNodelet::onInit()
{
    cout << "Starting Heuros Histograms..." << endl;

    ros::NodeHandle n;

    // Set up publishers
    pub_done = n.advertise<std_msgs::String>( "/heuros/done", 1 );
    done_msg = std_msgs::String::Ptr( new std_msgs::String );
    done_msg->data = "histograms";

    pub_metacloud = n.advertise<heuros_core::StampedAddress>( "/heuros/hist_metacloud", 1, true );

    // Set up subscribers
    sub_metacloud = n.subscribe( "/heuros/seg_metacloud", 1, &HistogramsNodelet::metacloudCb , this );
    sub_cluster = n.subscribe( "/heuros/cluster", 1, &HistogramsNodelet::clusterCb , this );

    // Set up services
    srv_instruction = n.advertiseService( "/heuros/his_instruction", &HistogramsNodelet::instructionSrv, this );

    correlatiopn_mode = USE_PEARSON_CORR;
}

void HistogramsNodelet::metacloudCb( const heuros_core::StampedAddressConstPtr &msg )
{
    // Accept only if instructed
    if( !accept_scene ) return;
    accept_scene = false;

    cout << "Histograms: Metacloud received" << endl;
    metacloud = ADDRESS_2_METACLOUD(msg->address);

    /* 
    if(online_mode){
        histograms_manager.calcHistogramsForSegments(metacloud);

        // Publish results
        cout << "Publishing histograms..." << endl;
        heuros_core::StampedAddressPtr pub_msg( new heuros_core::StampedAddress );
        pub_msg->address = PTR_2_ADDRESS( metacloud );
        pub_metacloud.publish( pub_msg );
    }
    */
}

void HistogramsNodelet::clusterCb( const heuros_core::StampedAddressConstPtr &msg )
{
    // Accept only if instructed
    if( !accept_cluster ) return;
    accept_cluster = false;

    cout << "Histograms: Cluster indices received" << endl;
    indices = ADDRESS_2_INDICES(msg->address);

    // Calculate and store the histograms
    histograms_manager.processCluster( metacloud, indices );

    // Publish done message
    pub_done.publish( done_msg );
}

bool HistogramsNodelet::instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp )
{
    /* TODO
     *
     * Check OpenCV nan resistance
     *
    */

    // ================== ADD CLUSTER ==================

    if( req.instruction == "add_histogram" )
    {
        cout << "Histograms: Received add histogram instruction" << endl;
        accept_scene = true;
        accept_cluster = true;
    }

    // ================== SAVE HISTOGRAMS ==================

    if( req.instruction == "save" )
    {
        cout << "Histograms: Received save instruction" << endl;
        histograms_manager.saveHistograms(req.parameter);
        histograms_manager.clear();
        pub_done.publish( done_msg );
    }

    // ================== LOAD HISTOGRAMS ==================

    if( req.instruction == "load" )
    {
        cout << "Histograms: Received load instruction" << endl;
        histograms_manager.loadHistograms(req.parameter);
        pub_done.publish( done_msg );
    }

    // ================ SET CORRELATION MODE =================

    if( req.instruction == "set_corr_mode" )
    {
        cout << "Histograms: Received set correlation mode instruction" << endl;
        if(req.parameter == "pearson")
            correlatiopn_mode = USE_PEARSON_CORR;
        else if(req.parameter == "custom")
            correlatiopn_mode = USE_CUSTOM_CORR;
        else if(req.parameter == "bhattacharyya")
            correlatiopn_mode = USE_BHATTACHARYYA;
        else if(req.parameter == "L2")
            correlatiopn_mode = USE_L2_DIST;
        else if(req.parameter == "L1")
            correlatiopn_mode = USE_L1_DIST;

        pub_done.publish( done_msg );
    }

    // ================ SET GROUPING MODE =================

    if( req.instruction == "set_grouping_mode" )
    {
        cout << "Histograms: Received set grouping mode instruction" << endl;
        if(req.parameter == "class_all")
            histograms_manager.grouping_mode = GROUPING_CLASS_ALL;
        else if(req.parameter == "class_norep")
            histograms_manager.grouping_mode = GROUPING_CLASS_NOREP;
        else if(req.parameter == "instance")
            histograms_manager.grouping_mode = GROUPING_INSTANCE;

        pub_done.publish( done_msg );
    }

    // ================== PROCESS HISTOGRAMS ==================

    if( req.instruction == "process" )
    {
        cout << "Histograms: Received process instruction" << endl;
        histograms_manager.processAllHistograms(req.parameter, correlatiopn_mode);
        pub_done.publish( done_msg );
    }

    // ============= CALCULATE UNIQUENESS IN PAIRS =============

    if( req.instruction == "uniqueness_in_pairs" )
    {
        cout << "Histograms: Received calculate uniqueness in pairs instruction" << endl;
        histograms_manager.calculateUniquenessInPairs(req.parameter);
        pub_done.publish( done_msg );
    }

    // ==================

    return true;
}

}

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(heuros_histograms::HistogramsNodelet, nodelet::Nodelet)
