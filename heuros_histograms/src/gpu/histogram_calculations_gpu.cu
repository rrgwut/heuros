#include <histogram_calculations_gpu.h>
#include <stdio.h>

__global__ void extractFeatures( const float *features_src,
                                 float *features_dst,
                                 const int *indices,
                                 int src_step,
                                 int dst_step,
                                 int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        // Feature index
        int ftr_idx = idx / dst_step;
        // Index of the source point (read from indices)
        int src_point = *(indices + idx%dst_step);
        // Index of the source feature value
        int src_idx = ftr_idx*src_step + src_point;
        // dst_val = src_val
        *(features_dst + idx) = *(features_src + src_idx);
    }
}

__global__ void histograms1D( const float *features,
                              int *histograms,
                              const int *histograms_lut,
                              int ftrs_step,
                              int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        // Calculate the histogram and point index
        int hist_idx = idx / ftrs_step;
        int point_idx = idx % ftrs_step;

        // Read the concerning feature index
        int ftr_idx = *(histograms_lut+hist_idx*1);

        // Read the concering feature value
        float ftr_val = *(features + ftr_idx*ftrs_step + point_idx);

        // Count only non-nans
        if( ftr_val == ftr_val ){
            // Calculate the bin
            int ftr_bin = 0.5*(ftr_val+0.99) * HIST_RES;

            // AtomicAdd
            int *bin_ptr = histograms + hist_idx*HIST_RES + ftr_bin;
            atomicAdd( bin_ptr, 1 );
        }
    }
}

__global__ void histograms2D( const float *features,
                              int *histograms,
                              const int *histograms_lut,
                              int ftrs_step,
                              int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        // Calculate the histogram and point index
        // H1:p1p2p3 H2:p1p2p3 H3:p1p2p3 ...
        int hist_idx = idx / ftrs_step;
        int point_idx = idx % ftrs_step;

        // Read the two concerning feature indices
        int ftr1_idx = *(histograms_lut+hist_idx*2);
        int ftr2_idx = *(histograms_lut+hist_idx*2+1);

        // Read the two concering feature values
        float ftr1_val = *(features + ftr1_idx*ftrs_step + point_idx);
        float ftr2_val = *(features + ftr2_idx*ftrs_step + point_idx);

        // Count only non-nans
        if( ftr1_val == ftr1_val && ftr2_val == ftr2_val ){
            // Calculate the bins
            int ftr1_bin = 0.5*(ftr1_val+0.99) * HIST_RES;
            int ftr2_bin = 0.5*(ftr2_val+0.99) * HIST_RES;

            // AtomicAdd
            int *bin_ptr = histograms + hist_idx*HIST_2D_STEP + ftr1_bin*HIST_RES + ftr2_bin;
            atomicAdd( bin_ptr, 1 );
        }
    }
}

template<class T>
__global__ void calcSums( const T *histograms,
                          T *sums,
                          int hist_step,
                          int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        int hist_idx = idx / hist_step;
        int bin_idx = idx % hist_step;
        T bin_val = histograms[hist_idx*hist_step + bin_idx];
        atomicAdd( sums + hist_idx, bin_val );
    }
}

template<class T>
__global__ void calcSquaresSums( const T *histograms,
                                 T *sums,
                                 int hist_step,
                                 int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        int hist_idx = idx / hist_step;
        int bin_idx = idx % hist_step;
        T bin_val = histograms[hist_idx*hist_step + bin_idx];
        atomicAdd( sums + hist_idx, bin_val*bin_val );
    }
}

__global__ void shiftArrays( const float *src_arrays,
                             float *dst_arrays,
                             float *shifts,
                             int arr_step,
                             int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        int arr_idx = idx / arr_step;
        int pt_idx = idx % arr_step;
        float pt_val = src_arrays[arr_idx*arr_step + pt_idx];
        float shift = shifts[arr_idx];
        dst_arrays[arr_idx*arr_step + pt_idx] = pt_val+shift;
    }
}

template<class T>
__global__ void scaleArrays( const T *src_arrays,
                             float *dst_arrays,
                             float *scales,
                             int arr_step,
                             int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        int arr_idx = idx / arr_step;
        int pt_idx = idx % arr_step;
        T pt_val = src_arrays[arr_idx*arr_step + pt_idx];
        float scale = scales[arr_idx];
        dst_arrays[arr_idx*arr_step + pt_idx] = pt_val*scale;
    }
}

__global__ void dotProducts( const float *arrays_a,
                             const float *arrays_b,
                             float *dot_prods,
                             int arr_step,
                             int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){
        int arr_idx = idx / arr_step;
        int pt_idx = idx % arr_step;
        float a_val = arrays_a[arr_idx*arr_step + pt_idx];
        float b_val = arrays_b[arr_idx*arr_step + pt_idx];
        atomicAdd( dot_prods + arr_idx, a_val*b_val );
    }
}

void HistogramCalculationsGPU::extractFeaturesCluster( const FeaturesGPU<float> &d_src, FeaturesGPU<float> &d_dst, IndicesConstPtr &h_indices )
{
    // Alloc dst features
    d_dst.create( d_src.num_features, h_indices->size() );

    // Upload h_indices
    DeviceArray<int> d_indices( h_indices->size() );
    d_indices.upload( *h_indices );

    // Set up kernel
    int N = d_indices.size() * d_src.num_features;
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Call kernel
    extractFeatures<<< nBlocks, blockSize >>> ( d_src.array->ptr(),
                                                d_dst.array->ptr(),
                                                d_indices.ptr(),
                                                d_src.numPoints(),
                                                d_dst.numPoints(),
                                                N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
}

void HistogramCalculationsGPU::calcUnnormHistograms( const FeaturesGPU<float> &features, HistogramsGPU<int> &histograms )
{
    // Set histograms to 0
    cudaSafeCall( cudaMemset( histograms.data(), 0, histograms.array->sizeBytes() ) );
    cudaSafeCall( cudaDeviceSynchronize() );

    // Setup kernel params
    int N = features.numPoints() * histograms.features_lut->num_features;
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Call either 1D or 2D histograms kernel
    if( histograms.num_dimensions == 1 ){
        histograms1D <<< nBlocks, blockSize >>> ( features.array->ptr(),
                                                  histograms.array->ptr(),
                                                  histograms.features_lut->array->ptr(),
                                                  features.step,
                                                  N );
    }else if( histograms.num_dimensions == 2 ){
        histograms2D <<< nBlocks, blockSize >>> ( features.array->ptr(),
                                                  histograms.array->ptr(),
                                                  histograms.features_lut->array->ptr(),
                                                  features.step,
                                                  N );
    }
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
}

void HistogramCalculationsGPU::calcNormHistograms( const FeaturesGPU<float> &features, HistogramsGPU<float> &histograms, int norm )
{
    HistogramsGPU<int> unnorm_hists( histograms.num_features, histograms.resolution(), histograms.num_dimensions );
    unnorm_hists.features_lut = histograms.features_lut;
    calcUnnormHistograms( features, unnorm_hists );
    normalizeHistograms( unnorm_hists, histograms, norm );
}

void HistogramCalculationsGPU::normalizeHistograms( HistogramsGPU<int> &src_hists, HistogramsGPU<float> &dst_hists, int norm )
{
    // Create dst array
    dst_hists.create( src_hists.num_features, src_hists.resolution(), src_hists.num_dimensions);

    // Either simple or square sums for each hist
    DeviceArray<int> norm_sums(src_hists.num_features);
    cudaSafeCall( cudaMemset( norm_sums, 0, norm_sums.sizeBytes() ) );
    cudaSafeCall( cudaDeviceSynchronize() );

    // Set up kernel
    int N = src_hists.numBins() * src_hists.num_features;
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Call either kernel depending on the normalization method
    if( norm == HIST_NORM_L1 ){
        calcSums<int><<< nBlocks, blockSize >>> ( src_hists.data(), norm_sums.ptr(), src_hists.numBins(), N );
    }else if( norm == HIST_NORM_L2 ){
        calcSquaresSums<int><<< nBlocks, blockSize >>> ( src_hists.data(), norm_sums.ptr(), src_hists.numBins(), N );
    }
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Calculate scales (CPU)
    vector<int> h_norm_sums;
    norm_sums.download( h_norm_sums );
    vector<float> h_scales( h_norm_sums.size() );
    for( int i=0; i<h_scales.size(); i++ ){
        if( norm == HIST_NORM_L1 )
            h_scales[i] = 1.0/h_norm_sums[i];
        else if( norm == HIST_NORM_L2 )
            h_scales[i] = sqrt(1.0/h_norm_sums[i]);
    }
    DeviceArray<float> scales;
    scales.upload( h_scales );

    // Scale the histograms
    scaleArrays<int><<< nBlocks, blockSize >>>( src_hists.data(), dst_hists.data(), scales.ptr(), src_hists.step, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
}

void HistogramCalculationsGPU::standarize( const HistogramsGPU<float> &src_hists, HistogramsGPU<float> &dst_hists )
{
    // Create dst array
    dst_hists.create( src_hists.num_features, src_hists.resolution(), src_hists.num_dimensions);

    // Set up kernel
    int N = src_hists.numBins() * src_hists.num_features;
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Sums for each array
    DeviceArray<float> norm_sums(src_hists.num_features);
    cudaSafeCall( cudaMemset( norm_sums, 0, norm_sums.sizeBytes() ) );
    cudaSafeCall(cudaDeviceSynchronize());

    // Shift the arrays to 0 means
    calcSums<float><<< nBlocks, blockSize >>> ( src_hists.data(), norm_sums.ptr(), src_hists.numBins(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
    vector<float> h_shifts;
    norm_sums.download( h_shifts );
    for( int i=0; i<h_shifts.size(); i++ )
       h_shifts[i] = -1*h_shifts[i]/src_hists.numPoints();
    DeviceArray<float> shifts;
    shifts.upload( h_shifts );
    shiftArrays <<< nBlocks, blockSize >>>( src_hists.data(), dst_hists.data(), shifts.ptr(), src_hists.step, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Calc square deviations
    cudaSafeCall( cudaMemset( norm_sums, 0, norm_sums.sizeBytes() ) );
    cudaSafeCall(cudaDeviceSynchronize());
    calcSquaresSums<float><<< nBlocks, blockSize >>> ( dst_hists.data(), norm_sums.ptr(), dst_hists.numBins(), N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
    vector<float> h_sq_sums;
    norm_sums.download( h_sq_sums );
    vector<float> h_scales( h_sq_sums.size() );
    for( int i=0; i<h_sq_sums.size(); i++ )
       h_scales[i] = sqrt( 1.0 / (h_sq_sums[i]/dst_hists.numPoints()) ); // sqrt( 1/variance )
    DeviceArray<float> scales;
    scales.upload( h_scales );

    // Scale the arrays
    scaleArrays<float><<< nBlocks, blockSize >>>( dst_hists.data(), dst_hists.data(), scales.ptr(), dst_hists.step, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());
}

// WARNING - THIS IS NOT NAN-RESISTANT!
void HistogramCalculationsGPU::calcCorrelations( const HistogramsGPU<float> &array_a, const HistogramsGPU<float> &array_b, vector<float> &correlations )
{
    // Standarize the arrays
    HistogramsGPU<float> std_arr_a, std_arr_b;
    standarize( array_a, std_arr_a );
    standarize( array_b, std_arr_b );

    // Set up kernel
    int N = array_a.numBins() * array_a.num_features;
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Calculate array dot products
    DeviceArray<float> dot_prods( std_arr_a.num_features );
    cudaSafeCall( cudaMemset( dot_prods.ptr(), 0, dot_prods.sizeBytes() ) );
    cudaSafeCall(cudaDeviceSynchronize());
    dotProducts <<< nBlocks, blockSize >>>( std_arr_a.data(), std_arr_b.data(), dot_prods.ptr(), std_arr_a.step, N );
    cudaSafeCall(cudaGetLastError());
    cudaSafeCall(cudaDeviceSynchronize());

    // Divide results by N
    correlations.resize( dot_prods.size() );
    dot_prods.download( correlations );
    for( int i=0; i<correlations.size(); i++ )
       correlations[i] /= array_a.numPoints();
}
