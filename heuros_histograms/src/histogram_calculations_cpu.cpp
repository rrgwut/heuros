#include<histogram_calculations_cpu.h>

void HistogramCalculationsCPU::calcNormHistograms( const MetacloudConstPtr &metacloud, const IndicesConstPtr &indices, vector<HistogramCPU> &histograms, int norm )
{
    // Create indices OpenCV mask to calculate histograms
    Mat mask = Mat( metacloud->size(), 1, CV_8UC1, Scalar(0) );
    for( int i=0; i<indices->size(); i++ ){
        int idx = indices->at(i);
        mask.at<unsigned char>( idx ) = 255;
    }

    // Process lookup table
    for( int i=0; i<histograms.size(); i++ )
        featureHistogram( metacloud, histograms[i], mask, norm );
}

void HistogramCalculationsCPU::calcNormD2D3D4Histograms( const MetacloudConstPtr &metacloud, const IndicesConstPtr &indices, vector<HistogramCPU> &histograms, int norm )
{
    // Histograms resolution
    const double d_d2 = 0.002;
    const double d_d3 = 0.00001;
    const double d_d4 = 0.000001;
    // Number of random point pairs
    const size_t N_d2 = 10*indices->size();

    Mat data_d2(1,1,CV_32FC1);
    data_d2.setTo(0);
    Mat data_d3(1,1,CV_32FC1);
    data_d3.setTo(0);
    Mat data_d4(1,1,CV_32FC1);
    data_d4.setTo(0);
    Mat data_d3_mod(1,1,CV_32FC1);
    data_d3_mod.setTo(0);
    Mat data_d4_mod(1,1,CV_32FC1);
    data_d4_mod.setTo(0);

    for(size_t i=0; i<N_d2; i++){
        size_t idx_1 = indices->at(rand()%indices->size());
        size_t idx_2 = indices->at(rand()%indices->size());
        size_t idx_3 = indices->at(rand()%indices->size());
        size_t idx_4 = indices->at(rand()%indices->size());
        pcl::PointXYZ p1 = metacloud->cloud_xyz->at(idx_1);
        pcl::PointXYZ p2 = metacloud->cloud_xyz->at(idx_2);
        pcl::PointXYZ p3 = metacloud->cloud_xyz->at(idx_3);
        pcl::PointXYZ p4 = metacloud->cloud_xyz->at(idx_4);
        double d2 = sqrt(pow(p1.data[0]-p2.data[0],2)+
                pow(p1.data[1]-p2.data[1],2) + pow(p1.data[2]-p2.data[2],2));
        float ax = p2.data[0] - p1.data[0];
        float ay = p2.data[1] - p1.data[1];
        float az = p2.data[2] - p1.data[2];
        float bx = p3.data[0] - p1.data[0];
        float by = p3.data[1] - p1.data[1];
        float bz = p3.data[2] - p1.data[2];
        float cx = p4.data[0] - p1.data[0];
        float cy = p4.data[1] - p1.data[1];
        float cz = p4.data[2] - p1.data[2];

        float fx = ay*bz - az*by;
        float fy = az*bx - ax*bz;
        float fz = ax*by - ay*bx;

        double d3 = sqrt(pow(fx,2)+pow(fy,2)+pow(fz,2));
        double d4 = abs( fx*cx + fy*cy + fz*cz );

        int d2_idx = d2/d_d2;
        incrementBin(data_d2, d2_idx);
        int d3_idx = d3/d_d3;
        incrementBin(data_d3, d3_idx);
        int d4_idx = d4/d_d4;
        incrementBin(data_d4, d4_idx);

        float da = sqrt(pow(ax,2)+pow(ay,2)+pow(az,2));
        float db = sqrt(pow(bx,2)+pow(by,2)+pow(bz,2));
        float dc = sqrt(pow(cx,2)+pow(cy,2)+pow(cz,2));

        double d3_mod = sqrt(d3);
        int d3_mod_idx = d3_mod/(0.2*d_d2);
        incrementBin(data_d3_mod, d3_mod_idx);
        double d4_mod = pow(d4, 0.3333f);
        int d4_mod_idx = d4_mod/(0.2*d_d2);
        incrementBin(data_d4_mod, d4_mod_idx);
    }
    resize(data_d2, data_d2, Size(20,1),0,0,INTER_AREA);
    resize(data_d3, data_d3, Size(20,1),0,0,INTER_AREA);
    resize(data_d4, data_d4, Size(20,1),0,0,INTER_AREA);
    resize(data_d3_mod, data_d3_mod, Size(20,1),0,0,INTER_AREA);
    resize(data_d4_mod, data_d4_mod, Size(20,1),0,0,INTER_AREA);

    cv::normalize(data_d2, data_d2);
    cv::normalize(data_d3, data_d3);
    cv::normalize(data_d4, data_d4);
    cv::normalize(data_d3_mod, data_d3_mod);
    cv::normalize(data_d4_mod, data_d4_mod);

    histograms[histograms.size()-5].data = data_d2;
    histograms[histograms.size()-4].data = data_d3;
    histograms[histograms.size()-3].data = data_d4;
    histograms[histograms.size()-2].data = data_d3_mod;
    histograms[histograms.size()-1].data = data_d4_mod;
}

void HistogramCalculationsCPU::calcCorrelations(vector<HistogramCPU> &array_a,
                                                vector<HistogramCPU> &array_b,
                                                Mat &correlations,
                                                vector<int> methods_for_features)
{
    // Allocate
    if( correlations.empty() )
        correlations = Mat( 1, array_a.size(), CV_32FC1 );

    for( int i=0; i<array_a.size(); i++ ){
        int method = methods_for_features[i];
        double r;
        switch(method){
        case USE_PEARSON_CORR:
            r = compareHist(array_a[i].data, array_b[i].data, CV_COMP_CORREL);
            break;
        case USE_BHATTACHARYYA:
            r = compareHist(array_a[i].data, array_b[i].data, CV_COMP_BHATTACHARYYA);
            r = 1-r;
            break;
        case USE_CUSTOM_CORR:
            r = compareHist_2(array_a[i].data, array_b[i].data);
            break;
        case USE_L1_DIST:
            r = compareHist_L1L2(array_a[i].data, array_b[i].data, false);
            break;
        case USE_L2_DIST:
            r = compareHist_L1L2(array_a[i].data, array_b[i].data, true);
            break;
        default:
            r = compareHist(array_a[i].data, array_b[i].data, CV_COMP_CORREL);
            break;
        }
        correlations.at<float>(i) = r;
    }
}

void HistogramCalculationsCPU::calcCorrelations( vector<HistogramCPU> &array_a, vector<HistogramCPU> &array_b, Mat &correlations, int method )
{
    // Allocate
    if( correlations.empty() )
        correlations = Mat( 1, array_a.size(), CV_32FC1 );

    for( int i=0; i<array_a.size(); i++ ){
        double r = compareHist(array_a[i].data, array_b[i].data, method);
        if(method == CV_COMP_BHATTACHARYYA)
            r = 1-r;
        correlations.at<float>(i) = r;
    }
}

void HistogramCalculationsCPU::calcCorrelations_2( vector<HistogramCPU> &array_a, vector<HistogramCPU> &array_b, Mat &correlations )
{
    // Allocate
    if( correlations.empty() )
        correlations = Mat( 1, array_a.size(), CV_32FC1 );

    for( int i=0; i<array_a.size(); i++ )
        correlations.at<float>(i) = compareHist_2(array_a[i].data, array_b[i].data);
}

void HistogramCalculationsCPU::calcCorrelations_L1L2( vector<HistogramCPU> &array_a, vector<HistogramCPU> &array_b, Mat &correlations, bool L2_mode )
{
    // Allocate
    if( correlations.empty() )
        correlations = Mat( 1, array_a.size(), CV_32FC1 );

    for( int i=0; i<array_a.size(); i++ )
        correlations.at<float>(i) = compareHist_L1L2(array_a[i].data, array_b[i].data, L2_mode);
}

void HistogramCalculationsCPU::featureHistogram( const MetacloudConstPtr &metacloud, HistogramCPU &histogram, const Mat &mask, int norm )
{
    // OpneCV setup
    vector<int> lut = histogram.lut;
    vector<Mat> arrays;
    vector<int> sizes;
    vector<float> ranges;
    for( int i=0; i<lut.size(); i++ ){
        arrays.push_back( Mat(*metacloud->features[lut[i]]) );
        sizes.push_back( HIST_RES );
        ranges.push_back(-1.0);
        ranges.push_back(1.0);
    }
    int cv_norm;
    if( norm == HIST_NORM_L1 ) cv_norm = NORM_L1;
    else if( norm == HIST_NORM_L2 ) cv_norm = NORM_L2;

    // Calculate the histogram
    calcHist( arrays, vector<int>(), mask, histogram.data, sizes, ranges );
    normalize( histogram.data, histogram.data, 1, 0, cv_norm );
}

void HistogramCalculationsCPU::incrementBin(Mat& data, int idx){
    int lenght = data.cols;
    if(idx >= lenght){
        int new_lenght = idx+1;
        Mat new_data(1, new_lenght, data.type());
        new_data.setTo(0);
        Rect roi = Rect(0,0,lenght,1);
        Mat data_roi = new_data(roi);
        data.copyTo(data_roi);
        data = new_data;
    }
    data.at<float>(0,idx)++;
}

double HistogramCalculationsCPU::compareHist_2(Mat a, Mat b){
    Mat a_gauss(a.rows, a.cols, CV_32FC1);
    Mat b_gauss(b.rows, b.cols, CV_32FC1);

    const Size ksize(3, 3);
    const double sigma = 0.7;

    GaussianBlur(a, a_gauss, ksize, sigma, sigma);
    GaussianBlur(b, b_gauss, ksize, sigma, sigma);

    const Scalar C(0.1);
    a_gauss += C;
    b_gauss += C;

    Mat p_min = min(a_gauss, b_gauss);
    Mat p_max = max(a_gauss, b_gauss);
    Mat p;
    divide(p_min, p_max, p);

    Scalar s_avg = mean(p);
    double p_avg = s_avg.val[0];

    double pearson = compareHist(a, b, CV_COMP_CORREL);

    const float high_thresh = 0.7;
    const float low_thresh = 0.4;
    Mat ht, lt;
    threshold(p, ht, high_thresh, 1.0f, CV_THRESH_BINARY_INV);
    threshold(p, lt, low_thresh, 1.0f, CV_THRESH_BINARY_INV);
    float ln = countNonZero(lt);
    float hn = countNonZero(ht);
    float max_n = p.cols*p.rows;
    double r_outliers = hn/max_n;
    double r_huge_outliers = ln/max_n;
    if(pearson < 0) pearson = 0;

    double r = sqrt(pearson) * (1-r_outliers) * pow(1-r_huge_outliers, 2);
    return r;
}

double HistogramCalculationsCPU::compareHist_L1L2(Mat a, Mat b, bool L2_mode){
    Mat d;
    absdiff(a, b, d);
    Mat d2;
    if(L2_mode) multiply(d, d, d2);
    else d.copyTo(d2);
    Scalar s = sum(d2);
    double l_dist = s.val[0];
    if(L2_mode)
        l_dist = sqrt(l_dist);
    double r = 1 - l_dist;
    return r;
}

float HistogramCalculationsCPU::getObjectCorrelation(Mat &corr, Mat &weights)
{
    double ret = 0;
    double norm = 0;
    for(int i=0; i<corr.cols; i++){
        float c = corr.at<float>(i);
        float w = weights.at<float>(i);
        ret += c * w;
        norm += w;
    }
    ret /= norm;
    return ret;
}

vector<HistogramCPU> HistogramCalculationsCPU::calcAllHistograms( const MetacloudConstPtr &metacloud, const IndicesConstPtr &indices, int instance )
{
    // Output vector
    vector<HistogramCPU> ftr_hists;

    // 1D histograms lookup table
    for( int i=0; i<metacloud->numFeatures(); i++ )
        ftr_hists.push_back( HistogramCPU(i,instance) );

    // 2D histograms lookup table
    ftr_hists.push_back( HistogramCPU(0,1,instance) ); // inclination-convexity
    ftr_hists.push_back( HistogramCPU(0,2,instance) ); // inclination-anisotropy
    ftr_hists.push_back( HistogramCPU(1,2,instance) ); // convexity-anisotropy
    ftr_hists.push_back( HistogramCPU(3,4,instance) ); // h-s

    // Calculate histograms
    calcNormHistograms( metacloud, indices, ftr_hists, HIST_NORM_L2 );

    // Global cluster histogram features (such as D2-D4)
    for(size_t i=0; i<5; i++)
        ftr_hists.push_back( HistogramCPU(metacloud->numFeatures()+i, instance) );
    HistogramCalculationsCPU::calcNormD2D3D4Histograms( metacloud, indices, ftr_hists, HIST_NORM_L2 );

    return ftr_hists;
}
