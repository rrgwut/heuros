#include<histograms_manager.h>

using namespace cv;
inline void coutMat( Mat mat )
{
    for( int y=0; y<mat.rows; y++ ){
        for( int x=0; x<mat.cols; x++ ){
            cout << setprecision(4);
            cout << fixed << mat.at<float>(y,x) << "\t";
        }
        cout << endl;
    }
}

inline void printMat( Mat mat, ofstream &file )
{
    for( int y=0; y<mat.rows; y++ ){
        for( int x=0; x<mat.cols; x++ ){
            file << setprecision(4);
            file << fixed << mat.at<float>(y,x) << "\t";
        }
        file << endl;
    }
}

inline Mat nanProofReduceAVG( const Mat &input )
{
    Mat output = Mat( input.rows, 1, CV_32FC1 );
    output.setTo(0);
    for( int y=0; y<input.rows; y++ ){
        int count = 0;
        for( int x=0; x<input.cols; x++ ){
            float in_val = input.at<float>(y,x);
            if( NAN_CHECK(in_val) ) continue;
            output.at<float>(y) += in_val;
            count ++;
        }
        if( count == 0 )
            output.at<float>(y) = F_NAN;
        else
            output.at<float>(y) /= count;
    }
    return output;
}

inline float sgn( float x )
{
    if( x<0 ) return -1;
    else if( x==0 ) return 0;
    else return 1;
}

inline float meanStdDevFewEls( Mat A, Scalar &mean, Scalar &stddev )
{
    mean[0] = 0;
    stddev[0] = 0;
    for( int i=0; i<A.total(); i++ )
        mean[0] += A.at<float>(i);
    mean[0] /= A.total();
    for( int i=0; i<A.total(); i++ )
        stddev[0] += (A.at<float>(i) - mean[0])*(A.at<float>(i) - mean[0]);
    stddev[0] = sqrt( stddev[0] / (A.total()-1) );
}

HistogramsManager::HistogramsManager()
{
    // Variables initialization
    grouping_mode = GROUPING_CLASS_ALL;

    cudaEventCreate( &start_all );
    cudaEventCreate( &start_gpu );
    cudaEventCreate( &start_cpu );
    cudaEventCreate( &stop_all );
}

void HistogramsManager::clear()
{
    histograms.clear();
    class_names.clear();
    base_feature_names.clear();
    hist_feature_names.clear();
}

void HistogramsManager::getOrCreateClass(int &class_idx, int &instance )
{
    // Read prefix
    ros::NodeHandle n;
    string prefix;
    n.param( "heuros/prefix", prefix, string("") );

    // Get the first prefix element
    vector<string> elems;
    boost::split( elems, prefix, boost::is_any_of("_") );
    prefix = elems[0];
    instance = -1;
    if( elems.size() > 1 )
        instance = atoi(elems[1].c_str());

    // Check if exists
    class_idx = -1;
    for( int i=0; i<class_names.size(); i++ ){
        if( prefix == class_names[i] )
            class_idx = i;
    }

    // Append new class if doesn't exist
    if( class_idx < 0 ){
        class_idx = class_names.size();
        class_names.push_back( prefix );
        histograms.push_back( vector<vector<HistogramCPU> >() );
    }
}

void HistogramsManager::setHistFeaturesNames()
{
    hist_feature_names.clear();
    int num_hist_features = histograms[0][0].size();
    for( int f=0; f<num_hist_features; f++ ){
        vector<int> lut = histograms[0][0][f].lut;
        if( lut.size() == 1 ){
            hist_feature_names.push_back(base_feature_names[lut[0]]);
        }else if( lut.size() == 2 ){
            string ftr_name = base_feature_names[lut[0]] + " - " + base_feature_names[lut[1]];
            hist_feature_names.push_back(ftr_name);
        }
    }
}

void HistogramsManager::calcHistogramsForSegments(MetacloudPtr metacloud){
    timerStart( start_all );
    clear();
    //MetacloudGPUPtr metacloud_gpu = ADDRESS_2_METACLOUD(metacloud->metacloud_gpu);
    int num_segments = metacloud->segmented_points.size();
    //metacloud->segment_histograms.resize(num_segments);
    for(size_t i=0; i<num_segments; i++){
        processCluster(metacloud, metacloud->segmented_points[i]);
        //metacloud->segment_histograms[i] = histograms[0][i];
    }
    cout << "Histograms created on CPU in: " << fixed <<
            timerStop( start_all, stop_all ) << " ms." << endl;
}

void HistogramsManager::processCluster( MetacloudPtr metacloud, IndicesConstPtr indices )
{
    //timerStart( start_all );

    // Get class idx
    int class_idx, instance;
    getOrCreateClass( class_idx, instance );
    //timerStop( start_all, start_cpu );

    // Call the calculation method
    vector<HistogramCPU> ftr_hists = HistogramCalculationsCPU::calcAllHistograms( metacloud, indices, instance );

    // Appned to the main array
    histograms[class_idx].push_back( ftr_hists );

    //cout << "Histograms created on CPU in: " << fixed << timerStop( start_cpu, stop_all ) << " ms." << endl;

    //============= FEATURES NAMES =============

    // Setup features names (only if this is the first histogram)
    if( histograms.size() == 1 && histograms[0].size() == 1 ){

        base_feature_names = metacloud->features_names;

        base_feature_names.push_back("D2");
        base_feature_names.push_back("D3");
        base_feature_names.push_back("D4");
        base_feature_names.push_back("D3_mod");
        base_feature_names.push_back("D4_mod");

        setHistFeaturesNames();
    }
}

void HistogramsManager::instancesToClasses( vector<vector<vector<HistogramCPU> > > &new_histograms, vector<string> &new_class_names )
{
    for( int c=0; c<histograms.size(); c++ ){
        int class_first_inst_idx = new_histograms.size();
        vector<int> found_instances;
        for( int h=0; h<histograms[c].size(); h++ ){
            int instance = histograms[c][h][0].instance;
            int instance_idx = -1;
            for( int i=0; i<found_instances.size(); i++ )
                if( instance == found_instances[i] )
                    instance_idx = i;
            if( instance_idx == -1 ){ // First time encountered instance
                stringstream ss;
                ss << class_names[c] << "_" << instance;
                found_instances.push_back( instance );
                new_class_names.push_back( ss.str() );
                vector<vector<HistogramCPU> > new_class;
                new_class.push_back( histograms[c][h] );
                new_histograms.push_back( new_class );
            }else{ // Existent instance
                new_histograms[class_first_inst_idx+instance_idx].push_back( histograms[c][h] );
            }
        }
    }
}

void HistogramsManager::processAllHistograms( string file_name, int corr_mode )
{
    timerStart( start_all );

    vector<vector<vector<HistogramCPU> > > local_hists;
    vector<string> local_class_names;

    if( grouping_mode == GROUPING_INSTANCE ){
        instancesToClasses( local_hists, local_class_names );
    }else{
        local_hists = histograms;
        local_class_names = class_names;
    }

    int num_classes = local_class_names.size();
    int num_hist_features = local_hists[0][0].size();
    int num_clusters = 0;
    for(int c=0; c<num_classes; c++)
        num_clusters += local_hists[c].size();
    vector<vector<float> > raw_corrs(num_hist_features);
    vector<vector<vector<float> > > raw_intern_corrs(num_hist_features);
    for( int f=0; f<num_hist_features; f++ )
        raw_intern_corrs[f] = vector<vector<float> >(num_classes);

    //============= SIMILARITY =============

    // Feature correlations of every class pair
    mean_corrs.resize( num_hist_features );
    all_corrs.resize( num_hist_features );

    for( int i=0; i<num_hist_features; i++ ){
        mean_corrs[i] = Mat( num_classes, num_classes, CV_32FC1 );
        mean_corrs[i].setTo(0);
    }

    // For every object class pair
    for( int cy=0; cy<num_classes; cy++ ){
        for( int cx=cy; cx<num_classes; cx++ ){

            Mat corr_sums = Mat( 1, num_hist_features, CV_32FC1 );
            corr_sums.setTo(0);
            int counted_elems = 0;

            vector<string> elems;
            boost::split( elems, local_class_names[cy], boost::is_any_of("_") );
            string class_y = elems[0];
            boost::split( elems, local_class_names[cx], boost::is_any_of("_") );
            string class_x = elems[0];

            // For every cluster pair (one from cy, one from cx)
            for( int hy=0; hy<local_hists[cy].size(); hy++){
                for( int hx=0; hx<local_hists[cx].size(); hx++){

                    if( cy==cx && hy==hx ) continue; // No self-correlation

                    if( grouping_mode == GROUPING_CLASS_NOREP && local_hists[cx][hx][0].instance == local_hists[cy][hy][0].instance ) continue;

                    if( grouping_mode == GROUPING_INSTANCE && class_y != class_x ) continue;

                    // Calculate correlations
                    Mat ftr_corrs;
                    if(corr_mode == USE_PEARSON_CORR){
                        HistogramCalculationsCPU::calcCorrelations( local_hists[cy][hy], local_hists[cx][hx], ftr_corrs );
                    }
                    else if(corr_mode == USE_CUSTOM_CORR){
                        HistogramCalculationsCPU::calcCorrelations_2( local_hists[cy][hy], local_hists[cx][hx], ftr_corrs );
                    }
                    else if( corr_mode == USE_BHATTACHARYYA ){
                        HistogramCalculationsCPU::calcCorrelations( local_hists[cy][hy], local_hists[cx][hx], ftr_corrs, CV_COMP_BHATTACHARYYA );
                    }
                    else if(corr_mode == USE_L2_DIST){
                        HistogramCalculationsCPU::calcCorrelations_L1L2( local_hists[cy][hy], local_hists[cx][hx], ftr_corrs, true );
                    }
                    else if(corr_mode == USE_L1_DIST){
                        HistogramCalculationsCPU::calcCorrelations_L1L2( local_hists[cy][hy], local_hists[cx][hx], ftr_corrs, false );
                    }

                    corr_sums += ftr_corrs;
                    counted_elems++;

                    for( int f=0; f<num_hist_features; f++ ){
                        // All correlations
                        raw_corrs[f].push_back(ftr_corrs.at<float>(f));
                        // Internal class correlations
                        if( cx==cy ) raw_intern_corrs[f][cy].push_back(ftr_corrs.at<float>(f));
                    }
                }
            }

            for( int f=0; f<num_hist_features; f++ ){
                // Add to the correspondent means position
                mean_corrs[f].at<float>(cy,cx) = corr_sums.at<float>(f) / counted_elems;
            }
        }
    }
    for( int i=0; i<num_hist_features; i++ ){
        all_corrs[i] = Mat(raw_corrs[i], true);
    }

    // Complete matrix symmetrically
    Mat mat_T;
    for( int f=0; f<num_hist_features; f++ ){
        Mat mean_corrs_D = mean_corrs[f].mul( Mat::eye(num_classes, num_classes, CV_32FC1) );
        transpose( mean_corrs[f], mat_T );
        mean_corrs[f] = mean_corrs[f] + mat_T - mean_corrs_D;
    }

    //============= FEATURE-CLASS UNIQUENESS =============

    Mat uniqueness = Mat( num_hist_features, num_classes, CV_32FC1 );
    for( int f=0; f<num_hist_features; f++ ){
        // Calc stddevs
        Mat stddevs = Mat( num_classes, 1, CV_32FC1 );
        for( int c=0; c<num_classes; c++ ){
            Scalar mean, stddev;
            Mat similarities_mat = Mat(raw_intern_corrs[f][c]);
            meanStdDevFewEls( similarities_mat, mean, stddev );
            stddevs.at<float>(c) = stddev[0];
        }
        // Sum rows without diagonal
        Mat corrs_no_diag = mean_corrs[f].clone();
        for( int i=0; i<corrs_no_diag.rows; i++ )
            corrs_no_diag.at<float>(i,i) = F_NAN;
        Mat mean_interclass = nanProofReduceAVG( corrs_no_diag );
        // (internacl corr - mean interclass corr) / stddev
        Mat ftr_uniq_T;
        divide( mean_corrs[f].diag()-mean_interclass, stddevs, ftr_uniq_T );
        transpose( ftr_uniq_T, uniqueness.row(f) );
    }

    //============= FEATURE UNIQUENESS SCORE =============

    const float B = 0.5; const float A=1/B;
    Mat uniqueness_trans( uniqueness.rows, uniqueness.cols, CV_32FC1 );
    for(int i=0; i<uniqueness.rows*uniqueness.cols; i++ ){
        float uniq = uniqueness.at<float>(i);
        uniqueness_trans.at<float>(i) = A*atan(B*uniq);
        //uniqueness_trans.at<float>(i) = sgn(uniq) * sqrt( fabs(uniq) );
    }
    Mat uniqueness_score = nanProofReduceAVG( uniqueness_trans );

    //Mat uniqueness_score = nanProofReduceAVG( uniqueness );

    //============= INTERNAL MEAN SIMILARITY =============

    Mat mean_corrs_diags = Mat( num_hist_features, num_classes, CV_32FC1 );
    for( int f=0; f<num_hist_features; f++ )
        transpose( mean_corrs[f].diag(), mean_corrs_diags.row(f) );
    Mat intern_corr = nanProofReduceAVG( mean_corrs_diags );

    //============= SAVE STATISTICS =============

    if( file_name == "" )
        file_name = "default.res";
    string path = ros::package::getPath("heuros_core") + "/../data/histograms/" + file_name;
    ofstream file( path.c_str() );

    // Class names
    file << "Class names:" << endl;
    for( int i=0; i<local_class_names.size(); i++ )
        file << local_class_names[i] << " ";
    file << endl << endl;

    // Histogram features names
    file << "Histogram features names: " << endl << endl;
    for( int f=0; f<num_hist_features; f++ ){
        file << hist_feature_names[f] << endl;
    }
    file << endl;

    // Uniqueness score
    file << "Feature uniqueness score:" << endl;
    printMat( uniqueness_score, file );
    file << endl;

    // Mean internal correlations
    file << "Mean internal similarity:" << endl;
    printMat( intern_corr, file );
    file << endl;

    // Feature-class uniqueness
    file << "Feature-class uniqueness:" << endl;
    printMat( uniqueness, file );
    file << endl;

    // Similarity
    file << "Mean interclass correlations: " << endl << endl;
    for( int f=0; f<num_hist_features; f++ ){
        file << hist_feature_names[f] << endl;
        printMat( mean_corrs[f], file );
        file << endl;
    }
    file.close();

    cout << "Histograms processed on CPU in: " << fixed << timerStop( start_all, stop_all ) << " ms." << endl;
}

void HistogramsManager::processHistograms(vector<vector<vector<HistogramCPU> > > &hists,
                                          vector<string> &class_names,
                                          vector<int> &corr_modes,
                                          Mat &uniqueness,
                                          Mat &uniqueness_score
                                          ){
    int num_classes = class_names.size();
    int num_hist_features = hists[0][0].size();
    int num_clusters = 0;
    for(int c=0; c<num_classes; c++)
        num_clusters += hists[c].size();
    vector<vector<float> > raw_corrs(num_hist_features);
    vector<vector<vector<float> > > raw_intern_corrs(num_hist_features);
    for( int f=0; f<num_hist_features; f++ )
        raw_intern_corrs[f] = vector<vector<float> >(num_classes);

    //============= SIMILARITY =============

    // Feature correlations of every class pair
    vector<Mat> mean_corrs( num_hist_features );
    vector<Mat> all_corrs( num_hist_features );

    for( int i=0; i<num_hist_features; i++ ){
        mean_corrs[i] = Mat( num_classes, num_classes, CV_32FC1 );
        mean_corrs[i].setTo(0);
    }

    // For every object class pair
    for( int cy=0; cy<num_classes; cy++ ){
        for( int cx=cy; cx<num_classes; cx++ ){

            Mat corr_sums = Mat( 1, num_hist_features, CV_32FC1 );
            corr_sums.setTo(0);
            int counted_elems = 0;

            // For every cluster pair (one from cy, one from cx)
            for( int hy=0; hy<hists[cy].size(); hy++){
                for( int hx=0; hx<hists[cx].size(); hx++){

                    if( cy==cx && hy==hx ) continue; // No self-correlation

                    // Calculate correlations
                    Mat ftr_corrs;
                    HistogramCalculationsCPU::calcCorrelations( hists[cy][hy], hists[cx][hx], ftr_corrs, corr_modes );

                    corr_sums += ftr_corrs;
                    counted_elems++;

                    for( int f=0; f<num_hist_features; f++ ){
                        // All correlations
                        raw_corrs[f].push_back(ftr_corrs.at<float>(f));
                        // Internal class correlations
                        if( cx==cy ) raw_intern_corrs[f][cy].push_back(ftr_corrs.at<float>(f));
                    }
                }
            }

            for( int f=0; f<num_hist_features; f++ ){
                // Add to the correspondent means position
                mean_corrs[f].at<float>(cy,cx) = corr_sums.at<float>(f) / counted_elems;
            }
        }
    }
    for( int i=0; i<num_hist_features; i++ ){
        all_corrs[i] = Mat(raw_corrs[i], true);
    }

    // Complete matrix symmetrically
    Mat mat_T;
    for( int f=0; f<num_hist_features; f++ ){
        Mat mean_corrs_D = mean_corrs[f].mul( Mat::eye(num_classes, num_classes, CV_32FC1) );
        transpose( mean_corrs[f], mat_T );
        mean_corrs[f] = mean_corrs[f] + mat_T - mean_corrs_D;
    }

    //============= FEATURE-CLASS UNIQUENESS =============

    uniqueness = Mat( num_hist_features, num_classes, CV_32FC1 );
    for( int f=0; f<num_hist_features; f++ ){
        // Calc stddevs
        Mat stddevs = Mat( num_classes, 1, CV_32FC1 );
        for( int c=0; c<num_classes; c++ ){
            Scalar mean, stddev;
            Mat similarities_mat = Mat(raw_intern_corrs[f][c]);
            meanStdDevFewEls( similarities_mat, mean, stddev );
            stddevs.at<float>(c) = stddev[0];
        }
        // Sum rows without diagonal
        Mat corrs_no_diag = mean_corrs[f].clone();
        for( int i=0; i<corrs_no_diag.rows; i++ )
            corrs_no_diag.at<float>(i,i) = F_NAN;
        Mat mean_interclass = nanProofReduceAVG( corrs_no_diag );
        // (internacl corr - mean interclass corr) / stddev
        Mat ftr_uniq_T;
        divide( mean_corrs[f].diag()-mean_interclass, stddevs, ftr_uniq_T );
        transpose( ftr_uniq_T, uniqueness.row(f) );
    }

    const float B = 0.5; const float A=1/B;
    Mat uniqueness_trans( uniqueness.rows, uniqueness.cols, CV_32FC1 );
    for(int i=0; i<uniqueness.rows*uniqueness.cols; i++ ){
        float uniq = uniqueness.at<float>(i);
        uniqueness_trans.at<float>(i) = A*atan(B*uniq);
        //uniqueness_trans.at<float>(i) = sgn(uniq) * sqrt( fabs(uniq) );
    }
    uniqueness_score = nanProofReduceAVG( uniqueness_trans );
}

void HistogramsManager::saveHistograms( string file_name )
{
    if( file_name == "" )
        file_name = "default.his";
    string path = ros::package::getPath("heuros_core") + "/../data/histograms/" + file_name;
    ofstream file( path.c_str() );

    // Nubmber of classes
    file << "num_classes: " << class_names.size() << endl << endl;

    // Class names
    file << "class_names:" << endl;
    for( int i=0; i<class_names.size(); i++ )
        file << class_names[i] << " ";
    file << endl << endl;

    // Nubmber of base features
    file << "num_base_features: " << base_feature_names.size() << endl << endl;

    // Base features names
    file << "base_features_names:" << endl;
    for( int i=0; i<base_feature_names.size(); i++ )
        file << base_feature_names[i] << endl;
    file << endl;

    // Number of histogram features
    file << "num_hist_features: " << histograms[0][0].size() << endl << endl;

    // LUT
    file << "lut:" << endl;
    for( int f=0; f<histograms[0][0].size(); f++ ){
        vector<int> lut = histograms[0][0][f].lut;
        for( int d=0; d<lut.size(); d++ )
            file << lut[d] << " ";
        file << endl;
    }
    file << endl;

    // Clusters
    file << "class_clusters:" << endl;
    for( int c=0; c<class_names.size(); c++ )
        file << histograms[c].size() << " ";
    file << endl << endl;

    // Data
    file << "Data:" << endl;
    for( int c=0; c<class_names.size(); c++ ){
        for( int h=0; h<histograms[c].size(); h++ ){
            file << histograms[c][h][0].instance << " ";
            for( int f=0; f<histograms[c][h].size(); f++ ){
                Mat data = histograms[c][h][f].data;
                for( int i=0; i<data.total(); i++ )
                    file << data.at<float>(i) << " ";
            }
            file << endl;
        }
        file << endl;
    }
    file.close();

    // Close file
    file.close();
}

void HistogramsManager::loadHistograms( string file_name )
{
    clear();

    if( file_name == "" )
        file_name = "default.his";
    string path = ros::package::getPath("heuros_core") + "/../data/histograms/" + file_name;
    ifstream file( path.c_str() );
    string word, line;
    int buff;

    // Number of classes
    int num_classes;
    file >> word >> num_classes;

    // Class names
    file >> word;
    class_names.resize( num_classes );
    for( int i=0; i<num_classes; i++ )
        file >> class_names[i];

    // Nubmber of base features
    int num_base_features;
    file >> word >> num_base_features;

    // Base features names
    file >> word;
    base_feature_names.resize( num_base_features );
    getline( file, line );
    for( int i=0; i<num_base_features; i++ )
        getline( file, base_feature_names[i] );

    // Histogram features
    int num_hist_features;
    file >> word >> num_hist_features;

    // LUT
    file >> word;
    getline( file, line );
    vector<vector<int> > lut;
    for( int f=0; f<num_hist_features; f++ ){
        getline( file, line );
        istringstream iss( line, istringstream::in );
        vector<int> ftr_lut;
        while( iss >> buff )
            ftr_lut.push_back(buff);
        lut.push_back( ftr_lut );
    }

    // Clusters
    file >> word;
    histograms.resize( num_classes );
    for( int c=0; c<num_classes; c++ ){
        file >> buff;
        histograms[c].resize(buff);
    }

    // Data
    file >> word;
    for( int c=0; c<class_names.size(); c++ ){
        for( int h=0; h<histograms[c].size(); h++ ){
            int instance;
            file >> instance;
            for( int f=0; f<num_hist_features; f++ ){
                HistogramCPU hist( lut[f], instance );
                int bins_y = hist.numDimensions() == 1 ? 1 : HIST_RES;
                hist.data = Mat( bins_y, HIST_RES, CV_32FC1 );
                for( int i=0; i<hist.data.total(); i++ )
                    file >> hist.data.at<float>(i);
                histograms[c][h].push_back(hist);
            }
        }
    }

    // Histogram features names
    setHistFeaturesNames();
}

void HistogramsManager::calculateUniquenessInPairs(string file_name){
    if( file_name == "" )
        file_name = "default.m";
    string path = ros::package::getPath("heuros_core") + "/../data/histograms/" + file_name;
    ofstream file( path.c_str() );

    vector<Mat> uniq_corrs = all_corrs;

    int num_hist_features = uniq_corrs.size();
    Mat pairs_corr(num_hist_features, num_hist_features, CV_32FC1);
    for(int y=0; y<num_hist_features; y++){
        for(int x=0; x<num_hist_features; x++){
            float cr = compareHist(uniq_corrs[y], uniq_corrs[x], CV_COMP_CORREL);
            pairs_corr.at<float>(y, x) = cr;
        }
    }

    for( int f=0; f<num_hist_features; f++ ){
        file << "h_"<<f<<" = [";
        for(int y=0; y<uniq_corrs[f].rows; y++){
            for(int x=0; x<uniq_corrs[f].cols; x++){
                float val = uniq_corrs[f].at<float>(y,x);
                //change two points to get nice plot
                if(x==0 && y<=1)
                    val = y;
                file << val << " ";
            }
        }
        file <<"];"<< endl;
    }
    file << endl << "tab = [";
    for( int f=0; f<num_hist_features; f++ ){
        file << "h_" << f <<"; ";
    }
    file << "];" << endl;
    file << "corr = [";
    for(int y=0; y<num_hist_features; y++){
        for(int x=0; x<num_hist_features; x++){
            float cr = pairs_corr.at<float>(y, x);
            file << cr << " ";
        }
        file <<"; ";
    }
    file << "]" << endl;
    stringstream ss;
    ss << "menu('Choose feature for axe', ";
    for( int f=0; f<num_hist_features; f++ ){
        ss << "'" << hist_feature_names[f] <<"', ";
    }
    ss << "'Quit');" << endl;

    file << "while(1)" << endl << "idx_x = " << ss.str() <<endl;
    file << "if(idx_x == " << num_hist_features+1 <<")" << endl;
    file << "break;" << endl << "endif" << endl;
    file << "idx_y = " << ss.str() <<endl;
    file << "if(idx_y == " << num_hist_features+1 <<")" << endl;
    file << "break;" << endl << "endif" << endl;
    file << "plot(tab(idx_x,:), tab(idx_y,:), '@');" << endl;
    //file << "axis([0,1,0,1], \"square\");" << endl;
    file << "cf = corr(idx_x, idx_y);" <<endl;
    file << "cs = num2str(cf);"<< endl <<
            "tt = ['Correlation = ', cs];" << endl
            << "title(tt);" << endl;
    file << "pause();" << endl << "endwhile" <<endl;

    file.close();
}
