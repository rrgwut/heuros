#include <QtGui>
#include <QMessageBox>
#include <iostream>
#include "main_window.hpp"

namespace heuros_gui {

using namespace Qt;

MainWindow::MainWindow( int argc, char** argv, InterfaceNode *_node , QWidget *parent )
	: QMainWindow(parent)
{
    node = _node;
    ui.setupUi(this);

    // Signals
    node->sig_done.connect(boost::bind( &MainWindow::doneCb, this, _1 ));

    // GUI components
    this->setFixedSize(this->size());
    updateLoadMenu(true);
    ui.btn_n->setStyleSheet("* { background-color: rgb(100,100,150) }");
    ui.btn_0->setStyleSheet("* { background-color: rgb(150,150,255) }");
    ui.btn_1->setStyleSheet("* { background-color: rgb(150,150,255) }");
    ui.btn_2->setStyleSheet("* { background-color: rgb(150,150,255) }");
    ui.btn_3->setStyleSheet("* { background-color: rgb(150,150,255) }");

    // Read the class names
    readClassNames();
    readFeatures();
    readScripts();
    readBaseFiles();
    readHSEModelsNames();
}

MainWindow::~MainWindow() {}

void MainWindow::updateLoadMenu( bool value )
{
    if( value == false ) return;

    string path_comp;
    if( ui.radio_test->isChecked() ){
        path_comp = "test";
    }else{
        path_comp = "train";
    }

    // Scenes
    string directory = "scenes/" + path_comp;
    vector<string> scenes = node->getStringList( directory );
    sort(scenes.begin(), scenes.end());
    ui.combo_scene->clear();
    ui.combo_scene->addItem( "Select scene" );
    for( int i=0; i<scenes.size(); i++ ){
        ui.combo_scene->addItem( QString::fromStdString(scenes[i]) );
    }

    // Clusters
    directory = "clusters/" + path_comp;
    vector<string> clusters = node->getStringList( directory );
    sort(clusters.begin(), clusters.end());
    ui.combo_cluster->clear();
    ui.combo_cluster->addItem( "Select cluster" );
    for( int i=0; i<clusters.size(); i++ ){
        ui.combo_cluster->addItem( QString::fromStdString(clusters[i]) );
    }
}

void MainWindow::sceneChanged( QString qstr )
{

    string path_comp;
    if( ui.radio_test->isChecked() ){
        path_comp = "test";
    }else{
        path_comp = "train";
    }

    string file_name = path_comp + "/" + qstr.toStdString();

    //if( ui.combo_det_mode->currentIndex() == 0 )
    //    node->loadScene( file_name );
    //else
    node->backproject( file_name );

    readClassNames();
}

void MainWindow::clusterChanged( QString qstr )
{
    string path_comp;
    if( ui.radio_test->isChecked() ){
        path_comp = "test";
    }else{
        path_comp = "train";
    }

    string file_name = path_comp + "/" + qstr.toStdString();
    node->loadCluster( file_name );
}

void MainWindow::readHSEModelsNames(){
    vector<string> labels = node->getHSEClassNames();
    ui.combo_model_name->clear();
    for(size_t i=0; i<labels.size(); i++){
        ui.combo_model_name->addItem(QString::fromStdString(labels[i]));
    }
}

void MainWindow::readBaseFiles(){
    vector<string> files = node->getStringList("hse");
    ui.combo_base_name->clear();
    for(size_t i=0; i<files.size(); i++){
        ui.combo_base_name->addItem(QString::fromStdString(files[i]));
    }
}

void MainWindow::readFeatures()
{
    vector<string> features = node->getFeaturesNames();
    //vector<string> features = node->getStringList( "features" );
    ui.combo_feature->clear();
    for( int i=0; i<features.size(); i++ ){
        ui.combo_feature->addItem( QString::fromStdString(features[i]) );
    }
    ui.combo_feature->addItem( "RGB colors" );
    ui.combo_feature->addItem( "Kinect Fusion" );
    ui.combo_feature->setCurrentIndex(0);
}

void MainWindow::readClassNames()
{
    vector<string> class_names = node->getClassNames( "props" );
    ui.combo_object->clear();
    for( int i=0; i<class_names.size(); i++ ){
        ui.combo_object->addItem( QString::fromStdString(class_names[i]) );
    }
}

void MainWindow::readScripts(){
    string directory = ros::package::getPath("heuros_interface") + "/../scripts";
    vector<string> file_list;
    if( boost::filesystem::exists( directory ) ){
        boost::filesystem::directory_iterator end ;
        for( boost::filesystem::directory_iterator iter(directory) ; iter != end ; ++iter )
            if ( iter->path().extension() == ".hes" ){
                file_list.push_back( iter->path().filename().string() );
            }
    }

    for(size_t i=0; i<file_list.size(); i++){
        ui.combo_script->addItem( QString::fromStdString(file_list[i]) );
    }
}

void MainWindow::representationChanged( int representation )
{
    node->setRepresentation( representation );
}

void MainWindow::on_add_model(){
    int idx = ui.combo_model_name->currentIndex();
    string model_name = ui.combo_model_name->currentText().toStdString();
    if(model_name.length() != 0){
        node->sendAddModelInstruction(model_name);
    }
    readHSEModelsNames();
    ui.combo_model_name->setCurrentIndex(idx);
}

void MainWindow::on_load_base(){
    string base_name = ui.combo_base_name->currentText().toStdString();
    if(base_name.length() != 0){
        node->sendLoadBaseInstruction(base_name);
    }
    readHSEModelsNames();
}

void MainWindow::on_save_base(){
    string base_name = ui.combo_base_name->currentText().toStdString();
    if(base_name.length() != 0){
        node->sendSaveBaseInstruction(base_name);
    }
}

void MainWindow::on_new_base(){
    node->sendNewBaseInstruction();
}

void MainWindow::display_props(){
    node->sendDisplayPropsInstruction();
}

void MainWindow::onButtonN()
{
    node->setViewport(-1);
}

void MainWindow::onButton0()
{
    node->setViewport(0);
}

void MainWindow::onButton1()
{
    node->setViewport(1);
}

void MainWindow::onButton2()
{
    node->setViewport(2);
}

void MainWindow::onButton3()
{
    node->setViewport(3);
}

void MainWindow::checkNormals( bool state )
{
    node->setNormals(state);
}

void MainWindow::spinPointSize( int size )
{
    node->setPointSize(size);
}

void MainWindow::featureChanged( int ftr_num )
{
    node->setFeature( ftr_num );
}

void MainWindow::prefixChanged( QString prefix )
{
    node->setPrefix( prefix.toStdString() );
}

void MainWindow::setSegmentation( bool value )
{
    if( value == false ) return;

    if( ui.radio_props->isChecked() ){
        node->setSegmentation("segment_nonflat");
    }else{
        node->setSegmentation("segment_flat");
    }
}

void MainWindow::onButtonSaveLabel()
{
    node->saveCluster();
}

void MainWindow::setDetections( QString combo_mode )
{
    string mode;
    if( combo_mode == "Backprojection" )
        mode = "backprojection";
    else if( combo_mode == "Grid filter" )
        mode = "grid";
    else if( combo_mode == "Backpr + Grid" )
        mode = "backprojection_grid";

    node->setDetections( mode );
}

void MainWindow::setObject( int object_id )
{
    node->setObject( object_id );
}

void MainWindow::doneCb( string msg )
{
    //if( msg == "visualization" )
    //    readFeatures();
}

void MainWindow::onPBExecuteScript(){
    QString script_name = ui.combo_script->currentText();
    node->startScript(script_name.toStdString());
}

void MainWindow::onPBShowScript(){
    QString script_name = ui.combo_script->currentText();
    string path = ros::package::getPath("heuros_interface") + "/../scripts/"+script_name.toStdString();
    stringstream ss;
    ss << "gedit " << path << " &";
    system( ss.str().c_str() );
}

void MainWindow::scriptChanged(QString qstr){} // Currently not used

void MainWindow::onBtnStart()
{
    node->kinfuStart();
}

void MainWindow::onBtnStop()
{
    node->kinfuStop();
}

void MainWindow::onBtnSaveScene()
{
    node->kinfuSaveScene( ui.edit_save_scene->text().toStdString() );
}

void MainWindow::periodChanged( double period )
{
    node->kinfuSetPeriod( period );
}

void MainWindow::checkW84Heuros( bool state )
{
    node->kinfuSetAutoLoop( !state );
}

void MainWindow::checkPubCPU( bool state )
{
    node->kinfuSetPubCPU( state );
}

} // namespace
