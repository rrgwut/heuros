#include <console_interface.h>

void ConsoleInterface::init( InterfaceNode *_node )
{
    node = _node;
    bool terminated = false;

    cout << "Heuros interface active. Waiting for instructions" << endl;
    cout << "heuros:> ";
    //cout.flush();

    while( !terminated ){

        bool show_prompt = true;
        string line, cmd;
        getline( cin, line );
        istringstream parse( line );

        parse >> cmd;

        // ================== GENERAL ==================

        if( cmd == "prefix" )
        {
            parse >> cmd;
            node->setPrefix( cmd );
        }
        else if( cmd == "s" )
        {
            node->saveCluster();
        }
        else if( cmd == "scene" )
        {
            parse >> cmd;
            if( /* inteligentne warunki */ !cmd.empty() )
                node->loadScene( cmd );
        }
        else if( cmd == "cluster" )
        {
            parse >> cmd;
            if( /* inteligentne warunki */ !cmd.empty() )
                node->loadCluster( cmd );
        }
        else if( cmd == "script" )
        {
            parse >> cmd;
            if( /* inteligentne warunki */ !cmd.empty() )
                node->startScript( cmd );
            show_prompt = false;
        }

        // ================== REGISTRATION ==================

        else if( cmd == "target" )
        {
            parse >> cmd;
            if( /* inteligentne warunki */ !cmd.empty() )
                node->registrationTarget( cmd );
        }

        else if( cmd == "source" )
        {
            parse >> cmd;
            if( /* inteligentne warunki */ !cmd.empty() )
                node->registrationSource( cmd );
        }
        else if( cmd == "align" )
        {
            parse >> cmd;
            string parameter = "";
            if( /* inteligentne warunki */ !cmd.empty() )
                parameter = cmd;
            node->registrationAlign( parameter );
        }

        // ================== MODELS ==================

        else if( cmd == "backpr_model" )
        {
            parse >> cmd;
            if( /* inteligentne warunki */ !cmd.empty() )
                node->backprModelAdd( cmd );
        }

        else if( cmd == "backpr_create" )
        {
            parse >> cmd;
            if( /* inteligentne warunki */ !cmd.empty() )
                node->backprCreateModels( cmd );
            else
                node->backprCreateModels();
        }

        else if( cmd == "backpr_load" )
        {
            parse >> cmd;
            if( /* inteligentne warunki */ !(cmd == "backpr_load") )
                node->backprLoadModels( cmd );
            else
                node->backprLoadModels();
        }

        else if( cmd == "backpr_reset" )
        {
            node->backprResetModels();
        }

        // ================== DETECTION ==================

        else if( cmd == "backproject" )
        {
            parse >> cmd;
            if( /* inteligentne warunki */ !cmd.empty() )
                node->backproject( cmd );
        }

        // ================== EXTRA ==================

        else if( cmd == "q" || cmd == "quit" || cmd == "exit" )
        {
            cout << "Console interface terminated" << endl;
            terminated = true;
            show_prompt = false;
        }
        else
        {
            cout << "Unknown command: " << cmd << endl;
        }

        if( show_prompt )
            cout << "heuros:> ";
    }
}
