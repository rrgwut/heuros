#include <interface_node.h>

// --------------------------
// ----- INITIALIZATION -----
// --------------------------

void InterfaceNode::init( int argc, char** argv )
{
    s_instruction = 0;
    s_subinstruction = 0;

    ros::init( argc, argv, "heuros_interface" );
    ros::NodeHandle n;

    // Subscribers
    sub_done = n.subscribe( "/heuros/done", 1, &InterfaceNode::doneCb , this );

    // Service clients
    cli_instruction = n.serviceClient<heuros_core::SimpleInstruction>( "/heuros/execute_instruction" );
    cli_get_string_list = n.serviceClient<heuros_core::GetStringList>( "/heuros/get_string_list" );
    cli_get_class_names = n.serviceClient<heuros_core::GetStringList>( "/heuros/get_class_names" );
    cli_get_features_names = n.serviceClient<heuros_core::GetStringList>( "/heuros/get_features_names" );
    cli_get_segmentation_names = n.serviceClient<heuros_core::GetStringList>( "/heuros/get_segmentation_names" );
    cli_io_instruction = n.serviceClient<heuros_core::SimpleInstruction>( "/heuros/io_instruction" );
    cli_seg_instruction = n.serviceClient<heuros_core::SimpleInstruction>( "/heuros/seg_instruction" );
    cli_his_instruction = n.serviceClient<heuros_core::SimpleInstruction>( "/heuros/his_instruction" );
    cli_reg_instruction = n.serviceClient<heuros_core::SimpleInstruction>( "/heuros/reg_instruction" );
    cli_mod_instruction = n.serviceClient<heuros_core::SimpleInstruction>( "/heuros/mod_instruction" );
    cli_det_instruction = n.serviceClient<heuros_core::SimpleInstruction>( "/heuros/det_instruction" );
    cli_vis_instruction = n.serviceClient<heuros_core::SimpleInstruction>( "/heuros/vis_instruction" );
    cli_kin_instruction = n.serviceClient<heuros_core::SimpleInstruction>( "/heuros/kin_instruction" );
    cli_hse_instruction = n.serviceClient<heuros_core::SimpleInstruction>( "/heuros/hse_instruction" );
    cli_hse_get_class_names = n.serviceClient<heuros_core::GetStringList>( "/heuros/hse/get_calss_names" );

    // Load models for detection
    backprLoadModels();

    ros::spin();
}

// ------------------------------------
// ----- HEUROS GLOBAL PARAMETERS -----
// ------------------------------------

void InterfaceNode::setPrefix( string prefix )
{
    cout << "Setting prefix: " << prefix << endl;
    ros::NodeHandle n;
    n.setParam( "heuros/prefix", prefix );
}

// ----------------------
// ----- LOAD SCENE -----
// ----------------------

void InterfaceNode::loadScene( string file_name, bool quiet )
{
    cout << "Sending load scene instruction: " << file_name << endl;

    heuros_core::SimpleInstruction srv;

    if( quiet ){
        srv.request.instruction = "reject_next_scene";
        cli_vis_instruction.call( srv );
    }

    srv.request.instruction = "load_scene";
    srv.request.parameter = file_name;
    cli_io_instruction.call( srv );
}

// ------------------------
// ----- LOAD CLUSTER -----
// ------------------------

string InterfaceNode::loadCluster( string file_name, bool quiet )
{
    cout << "Sending load cluster instruction: " << file_name << endl;

    heuros_core::SimpleInstruction srv;

    if( quiet ){
        srv.request.instruction = "reject_next_scene";
        cli_vis_instruction.call( srv );
        srv.request.instruction = "reject_next_cluster";
        cli_vis_instruction.call( srv );
    }

    srv.request.instruction = "load_cluster";
    srv.request.parameter = file_name;
    cli_io_instruction.call( srv );

    return srv.response.feedback;
}

// ------------------------
// ----- SAVE CLUSTER -----
// ------------------------

void InterfaceNode::saveCluster()
{
    cout << "Sending save cluster instruction" << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "save_cluster";
    cli_io_instruction.call( srv );
}

// ---------------------
// ----- CALLBACKS -----
// ---------------------

void InterfaceNode::doneCb( const std_msgs::StringConstPtr &msg )
{
    cout << "Received done message: " << msg->data << endl;
    if( msg->data != "visualization" )
        proceedScript();
    sig_done( msg->data );
}

// ------------------------
// ----- START SCRIPT -----
// ------------------------

void InterfaceNode::startScript( string file_name )
{
    cout << "Running script: " << file_name << endl;

    // Load the file
    string path = ros::package::getPath("heuros_interface") + "/../scripts/" + file_name;
    ifstream infile( path.c_str() );
    if( !infile.good() ){
        cout << "Error: File not found: " << file_name << endl;
        cout << "heuros:> ";
        cout.flush();
        return;
    }

    // Parse the script
    script.clear();
    string line, word;
    while( !infile.eof() ){
        getline( infile, line );

        // Ignore empty lines and comments
        if( line.size() < 2 ) continue;
        if( line[0] == '#' ) continue;

        // Detect and process script type
        istringstream parse_type( line );
        parse_type >> word;
        if( word == "Type:" || word == "type:" ){
            parse_type >> word;
            script_type = word;
            cout << "Script type: " << script_type << endl;
            continue;
        }

        // Parse line (a single instruction)
        vector<string> instruction;
        istringstream parse( line );
        while( parse >> word ){
            instruction.push_back( word );
        }
        script.push_back( instruction );
    }

    infile.close();
    s_instruction = 0;
    s_subinstruction = 0;
    proceedScript();
}

// --------------------------
// ----- PROCEED SCRIPT -----
// --------------------------

void InterfaceNode::proceedScript()
{
    // ================== SPECIAL CALLS AND VARS ==================

    static string models_name = "";
    static bool create_models = false;

    // Backprojection - create models at EOF
    if( create_models ){
        create_models = false;
        backprCreateModels( models_name );
        models_name = "";
    }


    // ================== END CONDITIONS A ==================

    if( s_instruction == script.size() ){
        script_type = "";
        if( script.size() == 0 )
            return;
    }

    // ================== HISTOGRAMS ==================

    if( script_type == "histograms" )
    {
        string parameter = "";
        if( script[s_instruction].size() > 1 )
            parameter = script[s_instruction][1];

        // Calculate histograms
        if( script[s_instruction][0] == "Save:" ){
            histogramsSave( parameter );
            s_instruction++; // Continue (not waiting for other nodes)
        }
        // Load
        else if( script[s_instruction][0] == "Load:" && script[s_instruction].size() > 1 ){
            histogramsLoad( parameter ); // Blocking callback
            s_instruction++; // Continue (not waiting for other nodes)
        }
        //Set correlation mode
        else if( script[s_instruction][0] == "SetCorrelationMode:" && script[s_instruction].size() > 1 ){
            histogramsSetCorr( parameter );
            s_instruction++; // Continue (not waiting for other nodes)
        }
        //Set grouping mode
        else if( script[s_instruction][0] == "SetGroupingMode:" && script[s_instruction].size() > 1 ){
            histogramsSetGrouping( parameter );
            s_instruction++; // Continue (not waiting for other nodes)
        }
        // Process
        else if( script[s_instruction][0] == "Process:" ){
            histogramsProcess( parameter );
            s_instruction++; // Continue (not waiting for other nodes)
        }
        // Uniqueness in pairs
        else if( script[s_instruction][0] == "UniquenessInPairs:" ){
            histogramsUniquenessIP( parameter );
            s_instruction++; // Continue (not waiting for other nodes)
        }
        // Add histograms
        else{
            // Add cluster files if "ALL" detected
            if( script[s_instruction][0] == "ALL" )
                scriptInsertAll("clusters/train");
            histogramsAdd( script[s_instruction][0] );
            s_instruction++;
        }
    }

    // ================== REGISTRATION ==================

    else if( script_type == "registration" )
    {
        // Process subinstructions
        if( s_subinstruction == 0 ){
            // Target
            registrationTarget( script[s_instruction][1] );
            s_subinstruction++;
        }else if( s_subinstruction == 1 ){
            // Source
            registrationSource( script[s_instruction][0] );
            s_subinstruction++;
        }
        else{
            // Check if a registration parameter is specified
            string parameter = "";
            if( script[s_instruction].size() > 2 ) parameter = script[s_instruction][2];
            // Align. Blocking callback, we don't wait
            registrationAlign( parameter );
            // Move to next instruction
            s_instruction++;
            s_subinstruction = 0;
            proceedScript();
        }
    }

    // ================== DETECTION ==================

    else if( script_type == "detection" )
    {
        // Switch modes
        static string mode = "";
        if( script[s_instruction][0] == "Train:" ){
            mode = "train";
            if( script[s_instruction].size() > 1 )
                models_name = script[s_instruction][1];
            else models_name = "";
            backprResetModels(); // Blocking callback.
            s_instruction++; // Continue (not waiting for other nodes)
        }else if( script[s_instruction][0] == "Load:" && script[s_instruction].size() > 1 ){
            mode = "load";
            backprLoadModels( script[s_instruction][1] );
            models_name = "";
            s_instruction++;
            return; // Wait for models
        }else if( script[s_instruction][0] == "Test:" ){
            string prev_mode = mode;
            mode = "test";
            s_instruction++;
            if( prev_mode == "train" ){ // If was in traning mode, create models
                backprCreateModels( models_name ); // Blocking callback
                models_name = "";
                return; // Wait for models
            }
        }

        // Train or test
        if( mode == "train" ){
            if( script[s_instruction][0] == "ALL" )
                scriptInsertAll("clusters/train");
            backprModelAdd( script[s_instruction][0] );
            // If the script ended - create models
            if( s_instruction == script.size()-1 ){
                create_models = true;
            }
        }else if( mode == "test" ){
            if( script[s_instruction][0] == "ALL" )
                scriptInsertAll("scenes/test");
            backproject( script[s_instruction][0] );
        }

        // Increment script counter
        s_instruction++;
    }

    // ================== UNKNOWN SCRIPT TYPE ==================

    else
    {
        cout << "Error: Unknown script type: " << script_type << endl;
        script_type = "";
        cout << "heuros:> ";
        cout.flush();
        s_instruction = script.size();
    }

    // ================== END CONDITIONS B ==================

    if( s_instruction == script.size() ){
        s_instruction = 0;
        s_subinstruction = 0;
        script.clear();
        script_type = "";
        cout << "Script finished." << endl;
        cout << "heuros:> ";
        cout.flush();
        return;
    }
}

// ---------------------
// ----- UTILITIES -----
// ---------------------

vector<string> InterfaceNode::getStringList( string query )
{
    cout << "Requested to get string list: " << query << endl;

    heuros_core::GetStringList srv;
    srv.request.query = query;
    cli_get_string_list.call( srv );
    return srv.response.string_list;
}

vector<string> InterfaceNode::getClassNames( string query )
{
    cout << "Requested to get class names: " << query << endl;

    heuros_core::GetStringList srv;
    srv.request.query = query;
    cli_get_class_names.call( srv );
    return srv.response.string_list;
}

vector<string> InterfaceNode::getHSEClassNames(){
    heuros_core::GetStringList srv;
    srv.request.query = "get_all_names";
    cli_hse_get_class_names.call(srv);
    return srv.response.string_list;
}

vector<string> InterfaceNode::getFeaturesNames()
{
    cout << "Requested to get features names " << endl;

    vector<string> names;

    heuros_core::GetStringList srv;
    srv.request.query = "features";
    cli_get_features_names.call( srv );
    names = srv.response.string_list;

    cout << "Requested to get segmentation names " << endl;

    srv.request.query = "features";
    cli_get_segmentation_names.call( srv );

    names.insert( names.end(),
                  srv.response.string_list.begin(),
                  srv.response.string_list.end() );

    return names;
}

void InterfaceNode::scriptInsertAll( string directory )
{
    cout << "Starting to process ALL files in " << directory << "..." << endl;

    // Get train / test from the file name
    vector<string> elems;
    boost::split( elems, directory, boost::is_any_of("/") );
    string path_comp = elems[1] + "/";

    // Get file list
    vector<string> file_names = getStringList( directory );

    // Increment the script counter
    s_instruction++;

    // Insert into the script
    for( int i=0; i<file_names.size(); i++ ){
        vector<string> insertion;
        insertion.push_back( path_comp + file_names[i] );
        script.insert( script.begin()+s_instruction+i, insertion );
    }
}

// ---------------------------
// ----------- HSE -----------
// ---------------------------

void InterfaceNode::sendAddModelInstruction(string model_name){
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "add_model_to_base";
    srv.request.parameter = model_name;
    cli_hse_instruction.call( srv );
}

void InterfaceNode::sendSaveBaseInstruction(string base_name){
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "save_base_to_file";
    srv.request.parameter = base_name;
    cli_hse_instruction.call( srv );
}

void InterfaceNode::sendLoadBaseInstruction(string base_name){
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "load_base_from_file";
    srv.request.parameter = base_name;
    cli_hse_instruction.call( srv );
}

void InterfaceNode::sendNewBaseInstruction(){
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "new_base";
    cli_hse_instruction.call( srv );
}

void InterfaceNode::sendDisplayPropsInstruction(){
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "display_props";
    cli_vis_instruction.call( srv );
}

// ------------------------
// ----- SEGMENTATION -----
// ------------------------

void InterfaceNode::setSegmentation( string segm_type )
{
    cout << "Requested to set segmenation type to: " << segm_type << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = segm_type;
    cli_seg_instruction.call( srv );
}

// ----------------------
// ----- HISTOGRAMS -----
// ----------------------

void InterfaceNode::histogramsAdd( string file_name )
{
    cout << "Requested to add a histogram: " << file_name << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "add_histogram";
    cli_his_instruction.call( srv );
    loadCluster( file_name );
}

void InterfaceNode::histogramsSave( string file_name )
{
    cout << "Requested to save histograms to: " << file_name << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "save";
    srv.request.parameter = file_name;
    cli_his_instruction.call( srv );
}

void InterfaceNode::histogramsLoad( string file_name )
{
    cout << "Requested to load histograms from: " << file_name << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "load";
    srv.request.parameter = file_name;
    cli_his_instruction.call( srv );
}

void InterfaceNode::histogramsSetCorr( string mode_name ){
    cout << "Requested to set correlation mode" << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "set_corr_mode";
    srv.request.parameter = mode_name;
    cli_his_instruction.call( srv );
}

void InterfaceNode::histogramsSetGrouping( string mode_name ){
    cout << "Requested to set grouping mode" << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "set_grouping_mode";
    srv.request.parameter = mode_name;
    cli_his_instruction.call( srv );
}

void InterfaceNode::histogramsProcess( string file_name )
{
    cout << "Requested process histograms " << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "process";
    srv.request.parameter = file_name;
    cli_his_instruction.call( srv );
}

void InterfaceNode::histogramsUniquenessIP(string file_name){
    cout << "Requested calculate uniqueness in pairs histograms " << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "uniqueness_in_pairs";
    srv.request.parameter = file_name;
    cli_his_instruction.call( srv );
}

// ------------------
// ----- MODELS -----
// ------------------

void InterfaceNode::backprModelAdd( string file_name )
{
    cout << "Requested to add a model cluster: " << file_name << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "add_cluster";
    cli_mod_instruction.call( srv );
    loadCluster( file_name );
}

void InterfaceNode::backprCreateModels( string file_name )
{
    cout << "Requested to create backprojection models: " << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "create_models";
    srv.request.parameter = file_name;
    cli_mod_instruction.call( srv );
}

void InterfaceNode::backprLoadModels( string file_name )
{
    cout << "Requested to load backprojection models: " << file_name << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "load_models";
    srv.request.parameter = file_name;
    cli_mod_instruction.call( srv );
}

void InterfaceNode::backprResetModels()
{
    cout << "Requested to reset backprojection models: " << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "reset_models";
    cli_mod_instruction.call( srv );
}

// --------------------------
// ----- BACKPROJECTION -----
// --------------------------

void InterfaceNode::backproject( string file_name )
{
    cout << "Requested to run backprojection on scene: " << file_name << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "detect";
    cli_det_instruction.call( srv );
    loadScene( file_name );
}

// ------------------------
// ----- REGISTRATION -----
// ------------------------

void InterfaceNode::registrationTarget( string file_name )
{
    cout << "Requested to set registration target: " << file_name << endl;

    heuros_core::SimpleInstruction srv;

    string ext = file_name.substr(file_name.find_last_of(".")+1);
    if( ext == "pcd" ){
        // Target scene
        srv.request.instruction = "target_scene";
        cli_reg_instruction.call( srv );
        loadScene( file_name );
        // Target scene
        srv.request.instruction = "target_scene_name";
        srv.request.parameter = file_name;
        cli_reg_instruction.call( srv );
    }else if( ext == "txt" ){
        // Target cluster
        srv.request.instruction = "target_cluster";
        cli_reg_instruction.call( srv );
        string target_name = loadCluster( file_name );
        // Target scene
        srv.request.instruction = "target_scene_name";
        srv.request.parameter = target_name;
        cli_reg_instruction.call( srv );
    }
}

void InterfaceNode::registrationSource( string file_name )
{
    cout << "Requested to set registration source: " << file_name << endl;

    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "source_cluster";
    cli_reg_instruction.call( srv );

    loadCluster( file_name, true );
}

void InterfaceNode::registrationAlign( string parameter )
{
    cout << "Requested to run registration algorithm" << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "align";
    srv.request.parameter = parameter;
    cli_reg_instruction.call( srv );
}

// ---------------------------
// ---------- KINFU ----------
// ---------------------------

void InterfaceNode::kinfuStart()
{
    cout << "Requested to run kinfu" << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "start";
    cli_kin_instruction.call( srv );
}

void InterfaceNode::kinfuStop()
{
    cout << "Requested to run kinfu" << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "stop";
    cli_kin_instruction.call( srv );
}

void InterfaceNode::kinfuSaveScene( string name )
{
    cout << "Requested to save the kinfu scene" << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "save";
    srv.request.parameter = name;
    cli_kin_instruction.call( srv );
}

void InterfaceNode::kinfuSetPeriod( float period )
{
    cout << "Requested set kinfu period to " << period << " s" <<  endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "set_period";
    stringstream ss;
    ss << period;
    srv.request.parameter = ss.str();
    cli_kin_instruction.call( srv );
}

void InterfaceNode::kinfuSetAutoLoop( bool state )
{
    cout << "Requested set kinfu auto-loop: " << state << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "set_auto";
    stringstream ss;
    ss << state;
    srv.request.parameter = ss.str();
    cli_kin_instruction.call( srv );
}

void InterfaceNode::kinfuSetPubCPU( bool state )
{
    cout << "Requested set kinfu CPU publication: " << state << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "set_cpu";
    stringstream ss;
    ss << state;
    srv.request.parameter = ss.str();
    cli_kin_instruction.call( srv );
}

// -------------------------
// ----- VISUALIZATION -----
// -------------------------

void InterfaceNode::setRepresentation( int representation )
{
    cout << "Requested to change feature representation to: " << representation << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "set_representation";
    stringstream ss;
    ss << representation;
    srv.request.parameter = ss.str();
    cli_vis_instruction.call( srv );
}

void InterfaceNode::setViewport( int viewport )
{
    cout << "Requested to change view port to: " << viewport << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "set_viewport";
    stringstream ss;
    ss << viewport;
    srv.request.parameter = ss.str();
    cli_vis_instruction.call( srv );
}

void InterfaceNode::setNormals( bool state )
{
    cout << "Requested to toggle normals: " << state << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "set_normals";
    stringstream ss;
    ss << state;
    srv.request.parameter = ss.str();
    cli_vis_instruction.call( srv );
}

void InterfaceNode::setPointSize( int size )
{
    cout << "Requested to set point size: " << size << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "set_point_size";
    stringstream ss;
    ss << size;
    srv.request.parameter = ss.str();
    cli_vis_instruction.call( srv );
}

void InterfaceNode::setFeature( int ftr_num )
{
    cout << "Requested to display feature: " << ftr_num << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "display_feature";
    stringstream ss;
    ss << ftr_num;
    srv.request.parameter = ss.str();
    cli_vis_instruction.call( srv );
}

void InterfaceNode::setDetections( string mode )
{
    cout << "Requested to set detections display mode: " << mode << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "set_det_display_mode";
    srv.request.parameter = mode;
    cli_vis_instruction.call( srv );
}

void InterfaceNode::setObject( int object_id )
{
    cout << "Requested to set visualization object to ID: " << object_id << endl;
    heuros_core::SimpleInstruction srv;
    srv.request.instruction = "set_object";
    stringstream ss;
    ss << object_id;
    srv.request.parameter = ss.str();
    cli_vis_instruction.call( srv );
}
