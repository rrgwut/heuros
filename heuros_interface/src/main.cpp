#include <iostream>
#include <boost/thread.hpp>
#include <QtGui>
#include <QApplication>

#include <console_interface.h>
#include <main_window.hpp>

using namespace std;

class QHackedApp : public QApplication
{
public:
    QHackedApp( int argc, char** argv ) : QApplication( argc, argv ) {}
    void hackedExec(){ exec(); }
};

int main( int argc, char** argv )
{
    // Create the interface node
    InterfaceNode interface_node;
    boost::thread node_thread( &InterfaceNode::init, &interface_node, argc, argv );

    // Start the console thread
    ConsoleInterface console;
    boost::thread console_thread( &ConsoleInterface::init, &console, &interface_node );
    //console_thread.join();

    // Start the GUI thread
    QApplication app(argc, argv);
    heuros_gui::MainWindow w( argc, argv, &interface_node );
    w.show();
    app.connect(&app, SIGNAL( lastWindowClosed()), &app, SLOT(quit()) );
    int result = app.exec();

	return 0;
}
