#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>
#include "ui_main_window.h"

#include <interface_node.h>

using namespace std;

namespace heuros_gui {

class MainWindow : public QMainWindow {
Q_OBJECT

public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    MainWindow( int argc, char** argv, InterfaceNode *_node, QWidget *parent = 0 );

    /** \brief Destructor. */
	~MainWindow();

    /** \brief Interface node pointer. */
    InterfaceNode *node;

    //================================================
    //================= PUBLIC SLOTS =================
    //================================================

public Q_SLOTS:

    /** \brief Load menu slots. */
    void updateLoadMenu( bool value );
    void sceneChanged( QString qstr );
    void clusterChanged( QString qstr );

    /** \brief Create labels slots. */
    void prefixChanged( QString prefix );
    void onButtonSaveLabel();
    void setSegmentation( bool value );

    /** \brief Visualization slots. */
    void readFeatures();
    void representationChanged( int representation );
    void onButtonN();
    void onButton0();
    void onButton1();
    void onButton2();
    void onButton3();
    void checkNormals( bool state );
    void spinPointSize( int size );
    void featureChanged( int ftr_num );

    /** \brief Detection slots. */
    void setDetections( QString mode );
    void setObject( int object_id );

    /** \brief Scripts slots. */
    void onPBShowScript();
    void onPBExecuteScript();
    void scriptChanged( QString qstr );

    /** \brief Kinect Fusion slots. */
    void onBtnStart();
    void onBtnStop();
    void onBtnSaveScene();
    void periodChanged( double period );
    void checkW84Heuros( bool state );
    void checkPubCPU( bool state );

    /** \brief Misc slots. */
    void doneCb( string msg );

    /** \brief Model base slots */
    void on_add_model();
    void on_save_base();
    void on_load_base();
    void on_new_base();
    void display_props();

    //=====================================================
    //================= PRIVATE VARIABLES =================
    //=====================================================

private:

    /** \brief GUI object. */
    Ui::MainWindowDesign ui;

    //===================================================
    //================= PRIVATE METHODS =================
    //===================================================

    /** \brief Get the known class names. */
    void readClassNames();

    /** \brief Get the available scripts. */
    void readScripts();

    /** \brief Get the avaliable base files */
    void readBaseFiles();

    /** \brief Get the current HSE model names */
    void readHSEModelsNames();
};

} // namespace

#endif // MAIN_WINDOW_H
