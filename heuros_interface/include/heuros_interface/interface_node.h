#ifndef INTERFACE_NODE_H
#define INTERFACE_NODE_H

#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/signals2.hpp>
#include <algorithm>

#include <ros/ros.h>
#include <ros/package.h>
#include <std_msgs/String.h>

#include <heuros_core/StampedAddress.h>
#include <heuros_core/SimpleInstruction.h>
#include <heuros_core/GetStringList.h>

using namespace std;

class InterfaceNode
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Process finished message subscriber. */
    ros::Subscriber sub_done;

    /** \brief Simple instruction client for the IO node. */
    ros::ServiceClient cli_instruction;

    /** \brief Simple instruction client for the IO node. */
    ros::ServiceClient cli_io_instruction;

    /** \brief Simple instruction client. */
    ros::ServiceClient cli_instruction_learning;

    /** \brief Get file list client. */
    ros::ServiceClient cli_get_string_list;

    /** \brief Get class names client. */
    ros::ServiceClient cli_get_class_names;

    /** \brief Get features names client. */
    ros::ServiceClient cli_get_features_names;

    /** \brief Get segmentation names client. */
    ros::ServiceClient cli_get_segmentation_names;

    /** \brief Simple instruction client for the segmentation node. */
    ros::ServiceClient cli_seg_instruction;

    /** \brief Simple instruction client for the histograms node. */
    ros::ServiceClient cli_his_instruction;

    /** \brief Simple instruction client for the registration node. */
    ros::ServiceClient cli_reg_instruction;

    /** \brief Simple instruction client for the models node. */
    ros::ServiceClient cli_mod_instruction;

    /** \brief Simple instruction client for the detection node. */
    ros::ServiceClient cli_det_instruction;

    /** \brief Simple instruction client for the visualization node. */
    ros::ServiceClient cli_vis_instruction;

    /** \brief Simple instruction client for the kinfu node. */
    ros::ServiceClient cli_kin_instruction;

    /** \brief Simple instruction client for the HSE node. */
    ros::ServiceClient cli_hse_instruction;

    /** \brief Get class names client for the HSE node. */
    ros::ServiceClient cli_hse_get_class_names;

    /** \brief Boost signal to inform the GUI about done messages. */
    boost::signals2::signal<void (string)> sig_done;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Initialization. */
    void init( int argc, char **argv );

    /** \brief Set heuros/prefix parameter. */
    void setPrefix( string prefix );

    /** \brief Load scene. */
    void loadScene( string file_name, bool quiet=false );

    /** \brief Load cluster. */
    string loadCluster( string file_name, bool quiet=false );

    /** \brief Run script. */
    void startScript( string file_name );

    /** \brief Save cluster. */
    void saveCluster();

    // ------------ IO ------------

    /** \brief Get files list. */
    vector<string> getStringList( string query );

    // ------------ VISUALIZATION ------------

    /** \brief Set the feature representation. */
    void setRepresentation( int representation );

    /** \brief Set active view port. */
    void setViewport( int viewport );

    /** \brief Set normals on / off. */
    void setNormals( bool state );

    /** \brief Set point size. */
    void setPointSize( int size );

    /** \brief Set the currently visualized feature. */
    void setFeature( int ftr_num );

    void setDetections( string mode );

    void setObject( int object_id );

    // ------------ SEGMENTATION ------------

    void setSegmentation( string segm_type );

    // ------------ HISTOGRAMS ------------

    /** \brief Send a new cluster to the histograms node. */
    void histogramsAdd( string file_name );

     /** \brief Save histograms to file. */
    void histogramsSave( string file_name );

    /** \brief Load histograms from file. */
    void histogramsLoad( string file_name );

    /** \brief Set histogram correlation mode. */
    void histogramsSetCorr( string mode_name );

    /** \brief Set histogram grouping mode. */
    void histogramsSetGrouping( string mode_name );

    /** \brief Send process instruction to the histograms node. */
    void histogramsProcess( string file_name );

    /** \brief Send calculate uniqueness in pairs instruction to the histograms node. */
    void histogramsUniquenessIP( string file_name );

    // ------------ MODELS ------------

    /** \brief Send a new cluster to the models node. */
    void backprModelAdd( string file_name );

    /** \brief Create backprojection models. */
    void backprCreateModels( string file_name="" );

    /** \brief Load backprojection models. */
    void backprLoadModels( string file_name="" );

    /** \brief Reset backprojection models. */
    void backprResetModels();

    /** \brief Get class names list. */
    vector<string> getClassNames( string query );

    /** \brief Get features names list. */
    vector<string> getFeaturesNames();

    // ------------ BACKPROJECTION ------------

    /** \brief Run backprojection. */
    void backproject( string file_name );

    // ------------ REGISTRATION ------------

    /** \brief Set the current scene or cluster as registration target. */
    void registrationTarget( string file_name );

    /** \brief Set the current cluster as registration source. */
    void registrationSource( string file_name );

    /** \brief Set the current alignment process parameter. */
    void registrationParameter( string parameter );

    /** \brief Align the current cluster against the target. */
    void registrationAlign( string parameter );

    // ------------ KINFU ------------

    /** \brief Run the kinfu algorithm. */
    void kinfuStart();

    /** \brief Stop the kinfu algorithm. */
    void kinfuStop();

    /** \brief Save the current kinfu scene. */
    void kinfuSaveScene( string name );

    /** \brief Set the cycle period of kinfu. */
    void kinfuSetPeriod( float period );

    /** \brief Auto-restart the kinfu loop after completion. */
    void kinfuSetAutoLoop( bool state );

    /** \brief Publish CPU clouds to ROS. */
    void kinfuSetPubCPU( bool state );

    // --------- HSE -------------

    /** \brief Send add model instruction to HSE */
    void sendAddModelInstruction( string model_name );

    /** \brief Send save base instruction to HSE */
    void sendSaveBaseInstruction( string base_name );

    /** \brief Send load base instruction to HSE */
    void sendLoadBaseInstruction( string base_name );

    /** \brief Send new base instruction to HSE */
    void sendNewBaseInstruction();

    /** \brief Send display props instruction to visualisation */
    void sendDisplayPropsInstruction();

    /** \brief Get list of HSE model base classes */
    vector<string> getHSEClassNames();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Current script step index. */
    int s_instruction;
    int s_subinstruction;

    /** \brief Type of currently executed script. */
    string script_type;

    /** \brief Vector of script lines organized as vectors of words. */
    vector<vector<string> > script;

 	//=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Processing done callback. */
    void doneCb( const std_msgs::StringConstPtr &msg );

    /** \brief Proceed with script after callback. */
    void proceedScript();

    /** \brief Insert all the files in a directory to the script. */
    void scriptInsertAll( string directory );

};

#endif
