#ifndef CONSOLE_INTERFACE_H
#define CONSOLE_INTERFACE_H

#include <iostream>
#include <interface_node.h>

class ConsoleInterface
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Interface node pointer. */
    InterfaceNode *node;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Initialization. */
    void init( InterfaceNode *_node );
};

#endif
