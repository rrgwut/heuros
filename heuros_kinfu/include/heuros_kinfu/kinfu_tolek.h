#ifndef KINFU_TOLEK_HPP
#define KINFU_TOLEK_HPP

/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2011, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 *  Author: Anatoly Baskeheev, Itseez Ltd, (myname.mysurname@mycompany.com)
 */


#define _CRT_SECURE_NO_DEPRECATE

#include <iostream>
#include <vector>

#include <image_transport/image_transport.h>
#include <image_transport/subscriber_filter.h>

#include <pcl/console/parse.h>

#include <boost/filesystem.hpp>

#include <pcl/gpu/kinfu/kinfu.h>
#include <pcl/gpu/kinfu/raycaster.h>
#include <pcl/gpu/kinfu/marching_cubes.h>
#include <pcl/gpu/containers/initialization.h>

#include <pcl/common/time.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/image_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/vtk_io.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/io/oni_grabber.h>
#include <pcl/io/pcd_grabber.h>
#include <pcl/exceptions.h>

#include "openni_capture.h"
#include <pcl/visualization/point_cloud_color_handlers.h>
#include "evaluation.h"

#include <pcl/common/angles.h>

#include "tsdf_volume.h"
#include "tsdf_volume.hpp"

/////

#include <ros/ros.h>
#include <std_msgs/Empty.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Imu.h>

#include <image_transport/image_transport.h>
#include <image_transport/subscriber_filter.h>
#include <tf/transform_broadcaster.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#ifdef HAVE_OPENCV  
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <pcl/gpu/utils/timers_opencv.hpp>
//#include "video_recorder.h"
typedef pcl::gpu::ScopeTimerCV ScopeTimeT;
#else
typedef pcl::ScopeTime ScopeTimeT;
#endif

#include <heuros_core/metacloud.h>
#include <heuros_core/StampedAddress.h>
#include "../src/internal.h"

using namespace std;
using namespace pcl;
using namespace pcl::gpu;
using namespace Eigen;
namespace pc = pcl::console;

namespace pcl
{
namespace gpu
{
void paint3DView (const KinfuTracker::View& rgb24, KinfuTracker::View& view, float colors_weight = 0.5f);
void mergePointNormal (const DeviceArray<PointXYZ>& cloud, const DeviceArray<Normal>& normals, DeviceArray<PointNormal>& output);
}

namespace visualization
{
//////////////////////////////////////////////////////////////////////////////////////
/** \brief RGB handler class for colors. Uses the data present in the "rgb" or "rgba"
      * fields from an additional cloud as the color at each point.
      * \author Anatoly Baksheev
      * \ingroup visualization
      */
template <typename PointT>
class PointCloudColorHandlerRGBCloud : public PointCloudColorHandler<PointT>
{
    using PointCloudColorHandler<PointT>::capable_;
    using PointCloudColorHandler<PointT>::cloud_;

    typedef typename PointCloudColorHandler<PointT>::PointCloud::ConstPtr PointCloudConstPtr;
    typedef typename pcl::PointCloud<RGB>::ConstPtr RgbCloudConstPtr;

public:
    typedef boost::shared_ptr<PointCloudColorHandlerRGBCloud<PointT> > Ptr;
    typedef boost::shared_ptr<const PointCloudColorHandlerRGBCloud<PointT> > ConstPtr;

    /** \brief Constructor. */
    PointCloudColorHandlerRGBCloud (const PointCloudConstPtr& cloud, const RgbCloudConstPtr& colors)
        : rgb_ (colors)
    {
        cloud_  = cloud;
        capable_ = true;
    }

    /** \brief Obtain the actual color for the input dataset as vtk scalars.
          * \param[out] scalars the output scalars containing the color for the dataset
          * \return true if the operation was successful (the handler is capable and
          * the input cloud was given as a valid pointer), false otherwise
          */
    virtual bool
    getColor (vtkSmartPointer<vtkDataArray> &scalars) const
    {
        if (!capable_ || !cloud_)
            return (false);

        if (!scalars)
            scalars = vtkSmartPointer<vtkUnsignedCharArray>::New ();
        scalars->SetNumberOfComponents (3);

        vtkIdType nr_points = vtkIdType (cloud_->points.size ());
        reinterpret_cast<vtkUnsignedCharArray*>(&(*scalars))->SetNumberOfTuples (nr_points);
        unsigned char* colors = reinterpret_cast<vtkUnsignedCharArray*>(&(*scalars))->GetPointer (0);

        // Color every point
        if (nr_points != int (rgb_->points.size ()))
            std::fill (colors, colors + nr_points * 3, static_cast<unsigned char> (0xFF));
        else
            for (vtkIdType cp = 0; cp < nr_points; ++cp)
            {
                int idx = cp * 3;
                colors[idx + 0] = rgb_->points[cp].r;
                colors[idx + 1] = rgb_->points[cp].g;
                colors[idx + 2] = rgb_->points[cp].b;
            }
        return (true);
    }

private:
    virtual std::string
    getFieldName () const { return ("additional rgb"); }
    virtual std::string
    getName () const { return ("PointCloudColorHandlerRGBCloud"); }

    RgbCloudConstPtr rgb_;
};
}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct SampledScopeTime : public StopWatch
{          
    enum { EACH = 33 };
    SampledScopeTime(int& time_ms) : time_ms_(time_ms) {}
    ~SampledScopeTime()
    {
        static int i_ = 0;
        time_ms_ += getTime ();
        if (i_ % EACH == 0 && i_)
        {
            cout << "Average frame time = " << time_ms_ / EACH << "ms ( " << 1000.f * EACH / time_ms_ << "fps )" << endl;
            time_ms_ = 0;
        }
        ++i_;
    }
private:    
    int& time_ms_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void
setViewerPose (visualization::PCLVisualizer& viewer, const Eigen::Affine3f& viewer_pose);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Eigen::Affine3f 
getViewerPose (visualization::PCLVisualizer& viewer);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename CloudT> void
writeCloudFile (const CloudT& cloud, string name);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename MergedT, typename PointT>
typename PointCloud<MergedT>::Ptr merge(const PointCloud<PointT>& points, const PointCloud<RGB>& colors)
{
    typename PointCloud<MergedT>::Ptr merged_ptr(new PointCloud<MergedT>());

    pcl::copyPointCloud (points, *merged_ptr);
    for (size_t i = 0; i < colors.size (); ++i)
        merged_ptr->points[i].rgba = colors.points[i].rgba;

    return merged_ptr;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct CurrentFrameCloudView
{
    CurrentFrameCloudView();

    void show (const KinfuTracker& kinfu);

    void setViewerPose (const Eigen::Affine3f& viewer_pose);

    PointCloud<PointXYZ>::Ptr cloud_ptr_;
    DeviceArray2D<PointXYZ> cloud_device_;
    visualization::PCLVisualizer cloud_viewer_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct ImageView
{
    ImageView(int viz);

    void showScene (KinfuTracker& kinfu, const sensor_msgs::ImageConstPtr& rgb, bool registration, Eigen::Affine3f* pose_ptr = 0);

    void showGeneratedDepth (KinfuTracker& kinfu, const Eigen::Affine3f& pose);

    void toggleImagePaint();

    int viz_;
    bool paint_image_;
    bool accumulate_views_;

    visualization::ImageViewer::Ptr viewerScene_;
    //visualization::ImageViewer::Ptr viewerDepth_;
    //visualization::ImageViewer viewerColor_;

    KinfuTracker::View view_device_;
    KinfuTracker::View colors_device_;
    vector<KinfuTracker::PixelRGB> view_host_;
    boost::shared_ptr<vector<KinfuTracker::PixelRGB> > view_ptr;

    RayCaster::Ptr raycaster_ptr_;

    KinfuTracker::DepthMap generated_depth_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct SceneCloudView
{
    enum { GPU_Connected6 = 0, CPU_Connected6 = 1, CPU_Connected26 = 2 };

    SceneCloudView(int viz);

    void show (KinfuTracker& kinfu, bool integrate_colors);

    void toggleCube(const Eigen::Vector3f& size);

    void toggleExtractionMode ();

    void toggleNormals ();

    void clearClouds (bool print_message = false);

    int viz_;
    int extraction_mode_;
    bool compute_normals_;
    bool valid_combined_;
    bool cube_added_;

    Eigen::Affine3f viewer_pose_;

    visualization::PCLVisualizer::Ptr cloud_viewer_;

    PointCloud<PointXYZ>::Ptr cloud_ptr_;
    PointCloud<Normal>::Ptr normals_ptr_;

    DeviceArray<PointXYZ> cloud_buffer_device_;
    DeviceArray<Normal> normals_device_;

    PointCloud<PointNormal>::Ptr combined_ptr_;
    DeviceArray<PointNormal> combined_device_;

    DeviceArray<RGB> point_colors_device_;
    PointCloud<RGB>::Ptr point_colors_ptr_;

    MarchingCubes::Ptr marching_cubes_;
    DeviceArray<PointXYZ> triangles_buffer_device_;

    boost::shared_ptr<pcl::PolygonMesh> mesh_ptr_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef boost::shared_ptr<sensor_msgs::Image> ImagePtr;

struct KinFuApp
{
    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image,
            sensor_msgs::CameraInfo, sensor_msgs::Image> DRGBSyncPolicy;
    typedef message_filters::Synchronizer<DRGBSyncPolicy> DRGBSync;
    typedef message_filters::TimeSynchronizer<sensor_msgs::Image,
            sensor_msgs::CameraInfo> DepthSync;

    enum { PCD_BIN = 1, PCD_ASCII = 2, PLY = 3, MESH_PLY = 7, MESH_VTK = 8 };

    KinFuApp(float vsz, int icp, int viz);

    ~KinFuApp();

    void setDepthIntrinsics(std::vector<float> depth_intrinsics);

    void toggleColorIntegration();

    void enableTruncationScaling();

    void imuCallback(const sensor_msgs::ImuConstPtr& input);

    void execute(const sensor_msgs::ImageConstPtr& depth,
                const sensor_msgs::CameraInfoConstPtr& cameraInfo,
                const sensor_msgs::ImageConstPtr& rgb =
                        sensor_msgs::ImageConstPtr());

    void writeCloud (string name) const;

    void reset();

    //=======================================
    /* Publishers */
    /** \brief Standard ROS cloud publisher. */
    ros::Publisher pub_kinfu_view;
    //tf::TransformBroadcaster pub_odom_;
    //image_transport::Publisher pub_scene_;
    //ros::Publisher pub_kinfu_reset_;

    /* Subscribers */
    ros::Subscriber sub_imu;
    boost::shared_ptr<image_transport::SubscriberFilter> sub_rgb_;
    boost::shared_ptr<image_transport::SubscriberFilter> sub_depth_;
    boost::shared_ptr<message_filters::Subscriber<sensor_msgs::CameraInfo> > sub_info_;
    boost::shared_ptr<DRGBSync> texture_sync_;
    boost::shared_ptr<DepthSync> depth_only_sync_;
    //=======================================
    // To publish
    ImagePtr image_msg_ptr;

    bool stopped;

    bool exit_;
    bool scan_;
    bool scan_volume_;

    bool independent_camera_;

    bool registration_;
    bool integrate_colors_;
    float focal_length_;

    KinfuTracker kinfu_;

    SceneCloudView scene_cloud_view_;
    ImageView image_view_;
    boost::shared_ptr<CurrentFrameCloudView> current_frame_cloud_view_;

    KinfuTracker::DepthMap depth_device_;

    pcl::TSDFVolume<float, short> tsdf_volume_;
    pcl::PointCloud<pcl::PointXYZI>::Ptr tsdf_cloud_ptr_;

    boost::mutex data_ready_mutex_;
    boost::condition_variable data_ready_cond_;

    std::vector<KinfuTracker::PixelRGB> source_image_data_;
    std::vector<unsigned short> source_depth_data_;
    PtrStepSz<const unsigned short> depth_;
    PtrStepSz<const KinfuTracker::PixelRGB> rgb24_;

    int time_ms_;
    int icp_, viz_;

    // Accelerometer HACK //
    Quaternionf starting_orientation, current_orientation;
    bool accel_running;
    int frame_limit;
    int frame_count;
};

#endif
