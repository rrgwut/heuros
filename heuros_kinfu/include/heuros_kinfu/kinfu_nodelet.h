#ifndef KINFU_NODELET_H
#define KINFU_NODELET_H

#include <iostream>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/PointCloud2.h>

#include <heuros_core/metacloud.h>
#include <heuros_core/StampedAddress.h>
#include <heuros_core/SimpleInstruction.h>

using namespace std;

namespace heuros_kinfu
{

class KinfuNodelet : public nodelet::Nodelet
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief On nodelet initialization. */
    virtual void onInit();

    ~KinfuNodelet();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Simple instruction service. */
    ros::ServiceServer srv_instruction;

    /** \brief The main Kinect Fusion object (void* to not include it here). */
    void *kinfu_app;

    /** \brief Output CPU point cloud. */
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr point_cloud;

    /** \brief Kinfu reset timer. */
    ros::Timer timer;

    /** \brief Auto-restart kinfu loop flag. */
    bool auto_loop;

    /** \brief CPU point cloud publication flag */
    bool publish_cpu;

    /** \brief Internal (nodelet) cloud publisher. */
    ros::Publisher pub_cloud_2_heuros;

    /** \brief Internal (nodelet) TSDF publisher. */
    ros::Publisher pub_tsdf_2_heuros;

    /** \brief Standard ROS cloud publisher. */
    ros::Publisher pub_cloud_2_ros;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Simple instructions service function. */
    bool instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp );

    /** \brief Timer callback. */
    void timerCallback(const ros::TimerEvent &event );

    /** \brief Start creating kinfu scene. */
    void start();

    /** \brief Stop creating kinfu scene. */
    void stop();

    /** \brief Save the current scene. */
    void save( string name );
};

}

#endif
