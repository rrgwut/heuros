//#include <kinfu_tolek.hpp>

// ----------------------------------------
// ---------- INSTRUCTION SERVER ----------
// ----------------------------------------
/*
bool instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp )
{
    // ================== ADD CLUSTER ==================

    if( req.instruction == "reset" )
    {
        //...
    }

    // ==================

    return true;
}
*/
// --------------------------
// ---------- MAIN ----------
// --------------------------
/*
int main (int argc, char* argv[])
{
    // Setup ROS
    ros::init(argc,argv,"heuros_kinfu");
    ros::NodeHandle nh("~");
    if ( ! ros::master::check() ){
        ROS_ERROR("NO ROS MASTER RUNNING!");
        return false;
    }

    // Starting arguments
    if (pc::find_switch (argc, argv, "--help") || pc::find_switch (argc, argv, "-h"))
        return print_cli_help ();
    float volume_size = 3.f;
    int icp = 1, visualization = 1;
    pc::parse_argument (argc, argv, "-volume_size", volume_size);
    pc::parse_argument (argc, argv, "--icp", icp);
    pc::parse_argument (argc, argv, "--viz", visualization);

    // Initialize kinfu
    KinFuApp app (volume_size, icp, visualization);

    // This might help - left just in case
    //
    /*if (pc::find_switch (argc, argv, "--scale-truncation") || pc::find_switch (argc, argv, "-st"))
        app.enableTruncationScaling();
    std::vector<float> depth_intrinsics;
    if (pc::parse_x_arguments (argc, argv, "--depth-intrinsics", depth_intrinsics) > 0)
    {
        if ((depth_intrinsics.size() == 4) || (depth_intrinsics.size() == 2))
        {
            app.setDepthIntrinsics(depth_intrinsics);
        }
        else
        {
            pc::print_error("Depth intrinsics must be given on the form fx,fy[,cx,cy].\n");
            return -1;
        }
    }*/
/*
    ros::Rate loop_rate(40);
    while (nh.ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
*/
