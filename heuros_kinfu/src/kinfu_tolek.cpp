#include <kinfu_tolek.h>
#include <heuros_core/private_thief.hpp>

void setViewerPose (visualization::PCLVisualizer& viewer, const Eigen::Affine3f& viewer_pose)
{
    Eigen::Vector3f pos_vector = viewer_pose * Eigen::Vector3f (0, 0, 0);
    Eigen::Vector3f look_at_vector = viewer_pose.rotation () * Eigen::Vector3f (0, 0, 1) + pos_vector;
    Eigen::Vector3f up_vector = viewer_pose.rotation () * Eigen::Vector3f (0, -1, 0);
    viewer.setCameraPosition (pos_vector[0], pos_vector[1], pos_vector[2],
            look_at_vector[0], look_at_vector[1], look_at_vector[2],
            up_vector[0], up_vector[1], up_vector[2]);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Eigen::Affine3f
getViewerPose (visualization::PCLVisualizer& viewer)
{
    Eigen::Affine3f pose = viewer.getViewerPose();
    Eigen::Matrix3f rotation = pose.linear();

    Matrix3f axis_reorder;
    axis_reorder << 0,  0,  1,
            -1,  0,  0,
            0, -1,  0;

    rotation = rotation * axis_reorder;
    pose.linear() = rotation;
    return pose;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename CloudPtr> void
writeCloudFile ( const CloudPtr& cloud_prt, string name )
{
    cout << "Saving point cloud to " << name << " (binary)... " << flush;
    pcl::io::savePCDFile (name, *cloud_prt, true);
    cout << "Done!" << endl;
}

template void writeCloudFile( const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr& cloud_prt, string name );

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CurrentFrameCloudView::CurrentFrameCloudView() : cloud_device_ (480, 640), cloud_viewer_ ("Frame Cloud Viewer")
{
    cloud_ptr_ = PointCloud<PointXYZ>::Ptr (new PointCloud<PointXYZ>);

    cloud_viewer_.setBackgroundColor (0, 0, 0.15);
    cloud_viewer_.setPointCloudRenderingProperties (visualization::PCL_VISUALIZER_POINT_SIZE, 1);
    cloud_viewer_.addCoordinateSystem (1.0);
    cloud_viewer_.initCameraParameters ();
    cloud_viewer_.setPosition (0, 500);
    cloud_viewer_.setSize (640, 480);
    cloud_viewer_.setCameraClipDistances (0.01, 10.01);
}

void
CurrentFrameCloudView::show (const KinfuTracker& kinfu)
{
    kinfu.getLastFrameCloud (cloud_device_);

    int c;
    cloud_device_.download (cloud_ptr_->points, c);
    cloud_ptr_->width = cloud_device_.cols ();
    cloud_ptr_->height = cloud_device_.rows ();
    cloud_ptr_->is_dense = false;

    cloud_viewer_.removeAllPointClouds ();
    cloud_viewer_.addPointCloud<PointXYZ>(cloud_ptr_);
    cloud_viewer_.spinOnce ();
}

void
CurrentFrameCloudView::setViewerPose (const Eigen::Affine3f& viewer_pose) {
    ::setViewerPose (cloud_viewer_, viewer_pose);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ImageView::ImageView(int viz) : viz_(viz), paint_image_ (false), accumulate_views_ (false)
{
    if (viz_)
    {
        //viewerScene_ = pcl::visualization::ImageViewer::Ptr(new pcl::visualization::ImageViewer);
        //viewerDepth_ = pcl::visualization::ImageViewer::Ptr(new pcl::visualization::ImageViewer);

        //viewerScene_->setWindowTitle ("View3D from ray tracing");
        //viewerScene_->setPosition (0, 0);
        //viewerDepth_->setWindowTitle ("Kinect Depth stream");
        //viewerDepth_->setPosition (640, 0);
        //viewerColor_.setWindowTitle ("Kinect RGB stream");
    }
}

void
ImageView::showScene (KinfuTracker& kinfu, const sensor_msgs::ImageConstPtr& rgb, bool registration, Eigen::Affine3f* pose_ptr)
{
    if (pose_ptr)
    {
        raycaster_ptr_->run(kinfu.volume(), *pose_ptr);
        raycaster_ptr_->generateSceneView(view_device_);
    }
    else
        kinfu.getImage (view_device_);

    if (paint_image_ && registration && !pose_ptr)
    {
        colors_device_.upload (&(rgb->data[0]), rgb->step, rgb->height, rgb->width);
        paint3DView (colors_device_, view_device_);
    }


    int cols;
    view_device_.download (view_host_, cols);

    //if (viz_)
    //    viewerScene_->showRGBImage (reinterpret_cast<unsigned char*> (&(*view_host_)[0]), view_device_.cols (), view_device_.rows ());

    //viewerColor_.showRGBImage ((unsigned char*)&rgb24.data, rgb24.cols, rgb24.rows);
}

/*void
showDepth (const PtrStepSz<const unsigned short>& depth) // DEPRECATED - BOGDAN
{
    if (viz_)
        viewerDepth_->showShortImage (depth.data, depth.cols, depth.rows, 0, 5000, true);
}*/

void
ImageView::showGeneratedDepth (KinfuTracker& kinfu, const Eigen::Affine3f& pose)
{
    raycaster_ptr_->run(kinfu.volume(), pose);
    raycaster_ptr_->generateDepthImage(generated_depth_);

    int c;
    vector<unsigned short> data;
    generated_depth_.download(data, c);

    // if (viz_)
    //     viewerDepth_->showShortImage (&data[0], generated_depth_.cols(), generated_depth_.rows(), 0, 5000, true);
}

void
ImageView::toggleImagePaint()
{
    paint_image_ = !paint_image_;
    cout << "Paint image: " << (paint_image_ ? "On   (requires registration mode)" : "Off") << endl;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SceneCloudView::SceneCloudView(int viz) : viz_(viz), extraction_mode_ (GPU_Connected6), compute_normals_ (false), valid_combined_ (false), cube_added_(false)
{
    cloud_ptr_ = PointCloud<PointXYZ>::Ptr (new PointCloud<PointXYZ>);
    normals_ptr_ = PointCloud<Normal>::Ptr (new PointCloud<Normal>);
    combined_ptr_ = PointCloud<PointNormal>::Ptr (new PointCloud<PointNormal>);
    point_colors_ptr_ = PointCloud<RGB>::Ptr (new PointCloud<RGB>);

    if (viz_)
    {
        cloud_viewer_ = pcl::visualization::PCLVisualizer::Ptr( new pcl::visualization::PCLVisualizer("Scene Cloud Viewer") );

        cloud_viewer_->setBackgroundColor (0, 0, 0);
        cloud_viewer_->addCoordinateSystem (1.0);
        cloud_viewer_->initCameraParameters ();
        cloud_viewer_->setPosition (0, 500);
        cloud_viewer_->setSize (640, 480);
        cloud_viewer_->setCameraClipDistances (0.01, 10.01);

        cloud_viewer_->addText ("H: print help", 2, 15, 20, 34, 135, 246);
    }
}

void
SceneCloudView::show (KinfuTracker& kinfu, bool integrate_colors)
{
    viewer_pose_ = kinfu.getCameraPose();

    ScopeTimeT time ("PointCloud Extraction");
    cout << "\nGetting cloud... " << flush;

    valid_combined_ = false;

    if (extraction_mode_ != GPU_Connected6)     // So use CPU
    {
        kinfu.volume().fetchCloudHost (*cloud_ptr_, extraction_mode_ == CPU_Connected26);
    }
    else
    {
        DeviceArray<PointXYZ> extracted = kinfu.volume().fetchCloud (cloud_buffer_device_);

        if (compute_normals_)
        {
            if( extracted.size() > 0 )
                kinfu.volume().fetchNormals (extracted, normals_device_);
            else
                normals_device_.create(0);

            pcl::gpu::mergePointNormal (extracted, normals_device_, combined_device_);
            combined_device_.download (combined_ptr_->points);
            combined_ptr_->width = (int)combined_ptr_->points.size ();
            combined_ptr_->height = 1;

            valid_combined_ = true;
        }
        else
        {
            extracted.download (cloud_ptr_->points);
            cloud_ptr_->width = (int)cloud_ptr_->points.size ();
            cloud_ptr_->height = 1;
        }

        if (integrate_colors)
        {
            if( extracted.size() > 0 )
                kinfu.colorVolume().fetchColors(extracted, point_colors_device_);
            else
                point_colors_device_.create(0);

            point_colors_device_.download(point_colors_ptr_->points);
            point_colors_ptr_->width = (int)point_colors_ptr_->points.size ();
            point_colors_ptr_->height = 1;
        }
        else
            point_colors_ptr_->points.clear();
    }
    size_t points_size = valid_combined_ ? combined_ptr_->points.size () : cloud_ptr_->points.size ();
    cout << "Done.  Cloud size: " << points_size / 1000 << "K" << endl;

    if (viz_)
    {
        cloud_viewer_->removeAllPointClouds ();
        if (valid_combined_)
        {
            visualization::PointCloudColorHandlerRGBCloud<PointNormal> rgb(combined_ptr_, point_colors_ptr_);
            cloud_viewer_->addPointCloud<PointNormal> (combined_ptr_, rgb, "Cloud");
            cloud_viewer_->addPointCloudNormals<PointNormal>(combined_ptr_, 50);
        }
        else
        {
            visualization::PointCloudColorHandlerRGBCloud<PointXYZ> rgb(cloud_ptr_, point_colors_ptr_);
            cloud_viewer_->addPointCloud<PointXYZ> (cloud_ptr_, rgb);
        }
    }
}

void
SceneCloudView::toggleCube(const Eigen::Vector3f& size)
{
    if (!viz_)
        return;

    if (cube_added_)
        cloud_viewer_->removeShape("cube");
    else
        cloud_viewer_->addCube(size*0.5, Eigen::Quaternionf::Identity(), size(0), size(1), size(2));

    cube_added_ = !cube_added_;
}

void
SceneCloudView::toggleExtractionMode ()
{
    extraction_mode_ = (extraction_mode_ + 1) % 3;

    switch (extraction_mode_)
    {
    case 0: cout << "Cloud exctraction mode: GPU, Connected-6" << endl; break;
    case 1: cout << "Cloud exctraction mode: CPU, Connected-6    (requires a lot of memory)" << endl; break;
    case 2: cout << "Cloud exctraction mode: CPU, Connected-26   (requires a lot of memory)" << endl; break;
    }
    ;
}

void
SceneCloudView::toggleNormals ()
{
    compute_normals_ = !compute_normals_;
    cout << "Compute normals: " << (compute_normals_ ? "On" : "Off") << endl;
}

void
SceneCloudView::clearClouds (bool print_message)
{
    if (!viz_)
        return;

    cloud_viewer_->removeAllPointClouds ();
    cloud_ptr_->points.clear ();
    normals_ptr_->points.clear ();
    if (print_message)
        cout << "Clouds/Meshes were cleared" << endl;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

KinFuApp::KinFuApp(float vsz, int icp, int viz) : exit_ (false), scan_ (false), scan_volume_ (false), independent_camera_ (false),
    registration_ (true), integrate_colors_ (true), focal_length_(-1.f), scene_cloud_view_(0), image_view_(viz), time_ms_(0),
    icp_(icp), viz_(viz), accel_running(false), stopped(true)
{
    //Init Kinfu Tracker
    Eigen::Vector3f volume_size = Vector3f::Constant (vsz/*meters*/);
    kinfu_.volume().setSize (volume_size);

    Eigen::Matrix3f R = Eigen::Matrix3f::Identity ();   // * AngleAxisf( pcl::deg2rad(-30.f), Vector3f::UnitX());
    Eigen::Vector3f t = volume_size * 0.5f - Vector3f (0, 0, volume_size (2) / 2 * 1.2f);

    Eigen::Affine3f pose = Eigen::Translation3f (t) * Eigen::AngleAxisf (R);

    kinfu_.setInitalCameraPose (pose);
    kinfu_.volume().setTsdfTruncDist (0.030f/*meters*/);
    kinfu_.setIcpCorespFilteringParams (0.1f/*meters*/, sin ( pcl::deg2rad(20.f) ));
    //kinfu_.setDepthTruncationForICP(5.f/*meters*/);
    kinfu_.setCameraMovementThreshold(0.001f);

    frame_limit = -1;
    frame_count = 0;

    if (!icp)
        kinfu_.disableIcp();


    //Init KinfuApp
    tsdf_cloud_ptr_ = pcl::PointCloud<pcl::PointXYZI>::Ptr (new pcl::PointCloud<pcl::PointXYZI>);
    image_view_.raycaster_ptr_ = RayCaster::Ptr( new RayCaster(kinfu_.rows (), kinfu_.cols ()) );

    if (viz_)
    {
        //scene_cloud_view_.cloud_viewer_->registerKeyboardCallback (keyboard_callback, (void*)this);
        //image_view_.viewerScene_->registerKeyboardCallback (keyboard_callback, (void*)this);
        //image_view_.viewerDepth_->registerKeyboardCallback (keyboard_callback, (void*)this);
        //scene_cloud_view_.toggleCube(volume_size);
    }

    //=================================================
    //====================== ROS ======================
    //=================================================

    ros::NodeHandle nh;
    image_transport::ImageTransport it(nh);
    pub_kinfu_view = nh.advertise<sensor_msgs::Image>( "/heuros/kin_view", 1, false );
    //pub_kinfu_reset_ = nh.advertise<std_msgs::Empty>("/kinfu_reset", 2);
    //pub_scene_ = it.advertise("/camera/kinfuLS/depth", 10);

    sub_imu = nh.subscribe("/imu", 1, &KinFuApp::imuCallback, this);

    sub_depth_ = boost::shared_ptr<image_transport::SubscriberFilter>(
                new image_transport::SubscriberFilter(it, "/camera/depth_registered/image_raw", 2));
    sub_info_ = boost::shared_ptr<message_filters::Subscriber<sensor_msgs::CameraInfo> >(
                new message_filters::Subscriber<sensor_msgs::CameraInfo>(nh, "/camera/depth_registered/camera_info", 2));
    sub_rgb_ = boost::shared_ptr<image_transport::SubscriberFilter>(
                new image_transport::SubscriberFilter(it, "/camera/rgb/image_color", 2));

    //the depth and the rgb cameras are not hardware synchronized
    //hence the depth and rgb images normally do not have the EXACT timestamp
    //so use approximate time policy for synchronization
    texture_sync_ = boost::shared_ptr<DRGBSync>( new DRGBSync(DRGBSyncPolicy(300), *sub_depth_, *sub_info_, *sub_rgb_));
    texture_sync_->registerCallback( boost::bind(&KinFuApp::execute, this, _1, _2, _3));
    toggleColorIntegration();
    scene_cloud_view_.toggleNormals();
    ROS_INFO("Running KinFu with texture extraction");

    //reconf_server_ = boost::make_shared<dynamic_reconfigure::Server<kinfu::kinfu_Config> >(nh); reconf_server_->setCallback(
    //                boost::bind(&KinFuLSApp::reconf_callback, this, _1, _2));

    //=================================================
    //=================================================
    //=================================================
}

KinFuApp::~KinFuApp()
{
    //...
}

void
KinFuApp::setDepthIntrinsics(std::vector<float> depth_intrinsics)
{
    float fx = depth_intrinsics[0];
    float fy = depth_intrinsics[1];

    if (depth_intrinsics.size() == 4)
    {
        float cx = depth_intrinsics[2];
        float cy = depth_intrinsics[3];
        kinfu_.setDepthIntrinsics(fx, fy, cx, cy);
        cout << "Depth intrinsics changed to fx="<< fx << " fy=" << fy << " cx=" << cx << " cy=" << cy << endl;
    }
    else {
        kinfu_.setDepthIntrinsics(fx, fy);
        cout << "Depth intrinsics changed to fx="<< fx << " fy=" << fy << endl;
    }
}

void
KinFuApp::toggleColorIntegration()
{
    if(registration_)
    {
        const int max_color_integration_weight = 2;
        kinfu_.initColorIntegration(max_color_integration_weight);
        integrate_colors_ = true;
    }
    cout << "Color integration: " << (integrate_colors_ ? "On" : "Off ( requires registration mode )") << endl;
}

void
KinFuApp::enableTruncationScaling()
{
    kinfu_.volume().setTsdfTruncDist (kinfu_.volume().getSize()(0) / 100.0f);
}

void KinFuApp::imuCallback(const sensor_msgs::ImuConstPtr& input)
{
    accel_running = true;

    // Transform infput to pointcloud frame (flip x and y)
    // Get gravity from accel (flip x, y, z)
    Vector3f gravity = Vector3f( input->linear_acceleration.x,
                                 input->linear_acceleration.y,
                                 -input->linear_acceleration.z );

    Vector3f down_dir = gravity.normalized();

    // Axis = cross prod of (0,1,0) and gravity
    // This is fine as the axis remains the same after CS rotation
    Vector3f camera_y = Vector3f(0,1,0);
    Vector3f axis = camera_y.cross(down_dir).normalized();

    // Angle acoss of the vectors dot prod
    // We could take asin from cross prod. but would fail for > PI/2
    float angle = acos( camera_y.dot(down_dir) );

    // Sensor orientation quaternion
    current_orientation = Quaternionf( AngleAxisf(angle, axis) );
}

void KinFuApp::execute(const sensor_msgs::ImageConstPtr& depth,
            const sensor_msgs::CameraInfoConstPtr& cameraInfo,
            const sensor_msgs::ImageConstPtr& rgb)
{
    if( !accel_running ){
        ROS_ERROR( "NO ACCELEROMETER DATA" );
        return;
    }

    // Check if kinfu is enabled
    if( stopped ) return;

    if( frame_limit > 0 ){
        if( frame_count >= frame_limit )
            return;
        frame_count++;
    }

    bool has_image = false;
    bool has_data = true;

    // Channels are swapped for some reason. Convert RGB to BGR
    sensor_msgs::ImagePtr bgr( new sensor_msgs::Image );
    cv_bridge::CvImagePtr cv_image_ptr = cv_bridge::toCvCopy(*rgb, sensor_msgs::image_encodings::RGB8);
    cv_image_ptr->toImageMsg(*bgr);

    if (has_data)
    {
        depth_device_.upload (&(depth->data[0]), depth->step, depth->height,depth->width);
        if (integrate_colors_){

            image_view_.colors_device_.upload (&(bgr->data[0]), rgb->step, rgb->height,rgb->width);
        }

        {
            SampledScopeTime fps(time_ms_);

            //run kinfu algorithm
            if (integrate_colors_)
                has_image = kinfu_ (depth_device_, image_view_.colors_device_);
            else
                has_image = kinfu_ (depth_device_);
        }

        //image_view_.showDepth (*depth);
        //image_view_.showGeneratedDepth(kinfu_, kinfu_.getCameraPose());
    }

    if (scan_)
    {
        scan_ = false;
        scene_cloud_view_.show (kinfu_, integrate_colors_);

        if (scan_volume_)
        {
            cout << "Downloading TSDF volume from device ... " << flush;
            kinfu_.volume().downloadTsdfAndWeighs (tsdf_volume_.volumeWriteable (), tsdf_volume_.weightsWriteable ());
            tsdf_volume_.setHeader (Eigen::Vector3i (pcl::device::VOLUME_X, pcl::device::VOLUME_Y, pcl::device::VOLUME_Z), kinfu_.volume().getSize ());
            cout << "done [" << tsdf_volume_.size () << " voxels]" << endl << endl;

            cout << "Converting volume to TSDF cloud ... " << flush;
            tsdf_volume_.convertToTsdfCloud (tsdf_cloud_ptr_);
            cout << "done [" << tsdf_cloud_ptr_->size () << " points]" << endl << endl;
        }
        else
            cout << "[!] tsdf volume download is disabled" << endl << endl;
    }

    if (has_image)
    {
        //Eigen::Affine3f viewer_pose = getViewerPose(*scene_cloud_view_.cloud_viewer_);
        // THIS MUST REMAIN HERE
        image_view_.showScene (kinfu_, bgr, registration_, 0);

        // Publish kinfu view
        image_msg_ptr = ImagePtr( new sensor_msgs::Image );
        image_msg_ptr->width = image_view_.view_device_.cols();
        image_msg_ptr->height = image_view_.view_device_.rows();
        image_msg_ptr->encoding = "rgb8";
        image_msg_ptr->step = 3*image_view_.view_device_.cols();
        int n_bytes = image_view_.view_host_.size()*sizeof(image_view_.view_host_[0]);
        image_msg_ptr->data.resize( n_bytes );
        memcpy( image_msg_ptr->data.data(), image_view_.view_host_.data(), n_bytes );
        pub_kinfu_view.publish( image_msg_ptr );
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
KinFuApp::writeCloud (string name) const
{
    const SceneCloudView& view = scene_cloud_view_;

    if (view.valid_combined_){
        // Accelerometer HACK //
        PointCloud<PointXYZRGBNormal>::Ptr oriented_cloud =  merge<PointXYZRGBNormal>(*view.combined_ptr_, *view.point_colors_ptr_);
        oriented_cloud->sensor_orientation_ = starting_orientation;
        writeCloudFile( oriented_cloud, name );
    }else{
        cout << "ERROR: INVALID CLOUD!" << endl;
    }
}

//===================================
//========== PRIVATE THIEF ==========
//===================================

struct KinfuTrackerReset { typedef void(KinfuTracker::*type)(); };
template class rob<KinfuTrackerReset, &KinfuTracker::reset>;

//===================================
//===================================
//===================================

// Reset kinfu
void KinFuApp::reset()
{
    starting_orientation = current_orientation;
    //kinfu_.reset();
    (kinfu_.*result<KinfuTrackerReset>::ptr)();
    //scene_cloud_view_.clearClouds (true);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
void
KinFuApp::keyboard_callback (const visualization::KeyboardEvent &e, void *cookie)
{


        case (int)'t': case (int)'T': app->scan_ = true; break;
        case (int)'1': case (int)'2': case (int)'3': app->writeCloud (key - (int)'0'); break;

        case (int)'x': case (int)'X':
            app->scan_volume_ = !app->scan_volume_;
            cout << endl << "Volume scan: " << (app->scan_volume_ ? "enabled" : "disabled") << endl << endl;
            break;
        case (int)'v': case (int)'V':
            cout << "Saving TSDF volume to tsdf_volume.dat ... " << flush;
            app->tsdf_volume_.save ("tsdf_volume.dat", true);
            cout << "done [" << app->tsdf_volume_.size () << " voxels]" << endl;
            cout << "Saving TSDF volume cloud to tsdf_cloud.pcd ... " << flush;
            pcl::io::savePCDFile<pcl::PointXYZI> ("tsdf_cloud.pcd", *app->tsdf_cloud_ptr_, true);
            cout << "done [" << app->tsdf_cloud_ptr_->size () << " points]" << endl;
            break;

        default:
            break;
}
*/
