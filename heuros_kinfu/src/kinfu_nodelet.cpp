///////////////////////////////////+++++++++++++++++++++......
//
// TODO:
//
// Connect kinfu output to Heuros and test
// pcd and tsdf to heuros_data
// Vanilla PCL
// Kinfu optional visualization (optional)
// Output to RVIZ - kinect vs kinfu (optional)
//
///////////////////////////////////+++++++++++++++++++++......

#include <kinfu_nodelet.h>
#include <kinfu_tolek.h>
// this should really be in the implementation (.cpp file)
#include <pluginlib/class_list_macros.h>
#include <pcl/ros/conversions.h>

namespace heuros_kinfu
{

void KinfuNodelet::onInit()
{
    cout << "Starting Heuros Kinfu..." << endl;

    auto_loop = true;
    publish_cpu = false;

    // Setup ROS
    ros::NodeHandle n;
    if ( ! ros::master::check() ){
        ROS_ERROR("NO ROS MASTER RUNNING!");
        return;
    }

    // Set up services
    srv_instruction = n.advertiseService( "/heuros/kin_instruction", &KinfuNodelet::instructionSrv, this );

    // Set up publishers
    pub_cloud_2_heuros = n.advertise<heuros_core::StampedAddress>( "/heuros/kin_cloud", 1, false );
    pub_tsdf_2_heuros = n.advertise<heuros_core::StampedAddress>( "/heuros/kin_tsdf", 1, false );
    pub_cloud_2_ros = n.advertise<sensor_msgs::PointCloud2>( "/kinfu_pointcloud", 1, false );

    // Kinfu starting arguments
    double volume_size = 3.0;
    int icp = 1, visualization = 1;
    n.param( "heuros/volume_size", volume_size, double(3.0) );
    //pc::parse_argument (argc, argv, "--icp", icp);
    //pc::parse_argument (argc, argv, "--viz", visualization);

    // Initialize kinfu
    kinfu_app = (void*)(new KinFuApp (volume_size, icp, visualization));

    timer = n.createTimer( ros::Duration(5.0), &KinfuNodelet::timerCallback, this );
    timer.stop();
}

KinfuNodelet::~KinfuNodelet()
{
    KinFuApp *kinfu_app_ = (KinFuApp*) kinfu_app;
    delete kinfu_app_;
}

void KinfuNodelet::start()
{
    cout << "Kinfu: starting new cycle" << endl;
    KinFuApp *kinfu_app_ = (KinFuApp*) kinfu_app;
    kinfu_app_->frame_count = 0;
    kinfu_app_->reset();
    kinfu_app_->stopped = false;
    timer.start();
}

void KinfuNodelet::stop()
{
    cout << "Kinfu: stop" << endl;
    KinFuApp *kinfu_app_ = (KinFuApp*) kinfu_app;
    timer.stop();
    kinfu_app_->stopped = true;
}

void KinfuNodelet::timerCallback( const ros::TimerEvent &event )
{
    cout << "Kinfu: publishing scene" << endl;

    // Publish GPU
    //...

    // Publish CPU
    KinFuApp *kinfu_app_ = (KinFuApp*) kinfu_app;
    SceneCloudView& view = kinfu_app_->scene_cloud_view_;
    view.show (kinfu_app_->kinfu_, true);
    point_cloud =  merge<PointXYZRGBNormal>(*view.combined_ptr_, *view.point_colors_ptr_);
    point_cloud->sensor_orientation_ = kinfu_app_->starting_orientation;

    // Publish to Heuros
    heuros_core::StampedAddressPtr heuros_msg( new heuros_core::StampedAddress );
    heuros_msg->address = PTR_2_ADDRESS(point_cloud);
    pub_cloud_2_heuros.publish( heuros_msg );

    // Publish to ROS
    sensor_msgs::PointCloud2Ptr ros_msg( new sensor_msgs::PointCloud2 );
    //pcl::toROSMsg( *oriented_cloud, *ros_msg );
    // ... publish

    if( auto_loop )
        start();
    else
        stop();
}

void KinfuNodelet::save( string name )
{
    string pcd_name = name + ".pcd";
    KinFuApp *kinfu_app_ = (KinFuApp*) kinfu_app;
    kinfu_app_->scene_cloud_view_.show (kinfu_app_->kinfu_, true);
    kinfu_app_->writeCloud( pcd_name );
    /*
    if (false)
    {
        cout << "Downloading TSDF volume from device ... " << flush;
        kinfu_.volume().downloadTsdfAndWeighs (tsdf_volume_.volumeWriteable (), tsdf_volume_.weightsWriteable ());
        tsdf_volume_.setHeader (Eigen::Vector3i (pcl::device::VOLUME_X, pcl::device::VOLUME_Y, pcl::device::VOLUME_Z), kinfu_.volume().getSize ());
        cout << "done [" << tsdf_volume_.size () << " voxels]" << endl << endl;

        cout << "Converting volume to TSDF cloud ... " << flush;
        tsdf_volume_.convertToTsdfCloud (tsdf_cloud_ptr_);
        cout << "done [" << tsdf_cloud_ptr_->size () << " points]" << endl << endl;
    }
    */
}

bool KinfuNodelet::instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp )
{
    // ================== START ==================

    if( req.instruction == "start" )
    {
        cout << "Kinfu: Received start instruction" << endl;
        start();
    }

    // ================== STOP ==================

    if( req.instruction == "stop" )
    {
        cout << "Kinfu: Received stop instruction" << endl;
        stop();
    }

    // ================== SAVE ==================

    if( req.instruction == "save" )
    {
        cout << "Kinfu: Received save instruction" << endl;
        save( req.parameter );
    }

    // ================== SET PERIOD ==================

    if( req.instruction == "set_period" )
    {
        cout << "Kinfu: Received set time period instruction" << endl;
        KinFuApp *kinfu_app_ = (KinFuApp*) kinfu_app;
        float period = atof( req.parameter.c_str() );
        if( period > 0 ){
            timer.setPeriod( ros::Duration(period) );
            kinfu_app_->frame_limit = -1;
        }
        else{
            timer.setPeriod( ros::Duration(0.5) );
            kinfu_app_->frame_limit = -period;
        }
    }

    // ================== SET AUTO ==================

    if( req.instruction == "set_auto" )
    {
        cout << "Kinfu: Received set set auto loop instruction" << endl;
        auto_loop = atoi( req.parameter.c_str() );
    }

    // ================== SET CPU ==================

    if( req.instruction == "set_cpu" )
    {
        cout << "Kinfu: Received set cpu publication instruction" << endl;
        publish_cpu = atoi( req.parameter.c_str() );
    }

    // ==================

    return true;
}

}

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(heuros_kinfu::KinfuNodelet, nodelet::Nodelet)
