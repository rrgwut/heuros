#ifndef DETECTIONS_H
#define DETECTIONS_H

#include<iostream>
#include<heuros_core/backprojection_models.h>

class Detections
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief File name. Left empty for for online metaclouds. */
    string file_name;

    /** \brief Object class names. */
    vector<string> class_names;

    /** \brief Constructor. */
    Detections( int n_objects );

    /** \brief Pointer to the input point cloud (on device).*/
    DPointCloudPtr cloud;

    /** \brief Bit-level object detections on each point. */
    NinjaArrayPtr bit_objects;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Get the number points. */
    int size() const;

    /** \brief Return the number of known objects. */
    int numObjects() const;

    /** \brief Return the number of ninja sets (32-object packs). */
    int numNinjaSets() const;

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Number of known objects. */
    int num_objects;
};

typedef boost::shared_ptr<Detections> DetectionsPtr;
typedef boost::shared_ptr<const Detections> DetectionsConstPtr;

#endif
