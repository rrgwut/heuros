#ifndef HISTOGRAMS_GPU
#define HISTOGRAMS_GPU

#include <iostream>
#include <cmath>
#include <heuros_core/features_gpu.h>

using namespace std;

#define HIST_RES 20
#define HIST_2D_STEP (HIST_RES*HIST_RES)

template<class T>
class HistogramsGPU : public FeaturesGPU<T>
{
public:

    //====================================================
    //================= TYPE DEFINITIONS =================
    //====================================================

    typedef boost::shared_ptr<HistogramsGPU> Ptr;

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    int num_dimensions;

    FeaturesGPU<int>::Ptr features_lut;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    HistogramsGPU();

    HistogramsGPU( int num_histograms, int _resolution, int _num_dimensions );

    void create( int num_histograms, int resolution, int _num_dimensions );

    int resolution() const;

    int numBins() const;
};

#endif
