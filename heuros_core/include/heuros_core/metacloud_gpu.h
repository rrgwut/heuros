#ifndef METACLOUD_GPU_H
#define METACLOUD_GPU_H

#include <iostream>
#include <heuros_core/impl/features_gpu.hpp>
#include <heuros_core/impl/histograms_gpu.hpp>

using namespace std;
using namespace pcl::gpu;

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif

#define ADDRESS_2_METACLOUD_GPU(a) *((MetacloudGPUPtr*)a)
// Set to 1 when using very high resolution (>512)
#define ULTRA_HIGH_RES 0
// Max number of point neighbors
#define MAX_NEIGHBORS (150*(1+ULTRA_HIGH_RES))
// Size of the neighborhoods
#define NBR_R_1 0.01
#define NBR_R_2 0.02

typedef boost::shared_ptr<vector<float> > FeaturePtr;
typedef boost::shared_ptr<const vector<float> > FeatureConstPtr;
typedef boost::shared_ptr<vector<int> > IndicesPtr;
typedef boost::shared_ptr<const vector<int> > IndicesConstPtr;

typedef float4 DPoint; // XYZ + RGB
typedef float4 DNormal;

typedef DeviceArray<DPoint> DPointCloud;
typedef DeviceArray<DNormal> DNormals;
typedef DeviceArray<float> DFeature;

typedef boost::shared_ptr<DPointCloud> DPointCloudPtr;
typedef boost::shared_ptr<DNormals> DNormalsPtr;
typedef boost::shared_ptr<DFeature> DFeaturePtr;
typedef boost::shared_ptr<const DPointCloud> DPointCloudConstPtr;
typedef boost::shared_ptr<const DNormals> DNormalsConstPtr;

typedef boost::shared_ptr<vector<DeviceArrayIntPtr> > SegmentedPointsPtr;

class MetacloudGPU
{
public:

    //==================================================
    //================= PUBLIC STRUCTURES ==============
    //==================================================

    /** \brief Structure holding individual segment features. */
    struct SegmentedFeature
    {
        string name;
        vector<float> values;
    };
    typedef boost::shared_ptr<SegmentedFeature> SegmentedFeaturePtr;

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Pointer to the input point cloud (on device).*/
    DPointCloudPtr cloud;

    /** \brief Pointer to the input normals (on device). */
    DNormalsPtr normals;

    /** \brief Pointer to the input RGB colors (on device). */
    DFeaturePtr colors;

    /** \brief Vector point features names. */
    vector<string> features_names;

    /** \brief Vector of known features names. */
    vector<string> segment_sets_names;

    /** \brief Structure holding GPU point features. */
    FeaturesGPU<float>::Ptr features;

    /** \brief Vector of segment features. */
    vector<vector<SegmentedFeaturePtr> > segmented_features;

    /** \brief Each point's segment indices. */
    FeaturesGPU<int>::Ptr segment_indices;

    /** \brief Indices of each point's neighbors (on device). */
    NeighborIndices neighbors_indices;

    /** \brief Farther away neighbor indices. */
    NeighborIndices neighbors_indices_2;

    /** \brief Average number of point neighbors. */
    float avg_neighbors;

    /** \brief Average number of farther away neighbors. */
    float avg_neighbors_2;

    /** \brief 3x3 rotation matrix. */
    DFeaturePtr sensor_orientation;

    /** \brief List of segemnts*/
    SegmentedPointsPtr segmented_points;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    MetacloudGPU();

    /** \brief Destructor. */
    ~MetacloudGPU();

    /** \brief Get the number of features. */
    size_t numFeatures() const;

    /** \brief Get the number of segment sets. */
    size_t numSegmentSets() const;

    /** \brief Get the number points. */
    size_t size() const;

    /** \brief Release device memory. */
    void release();
};

typedef boost::shared_ptr<MetacloudGPU> MetacloudGPUPtr;
typedef boost::shared_ptr<const MetacloudGPU> MetacloudGPUConstPtr;

#endif
