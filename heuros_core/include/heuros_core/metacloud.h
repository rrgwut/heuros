#ifndef METACLOUD_H
#define METACLOUD_H

#include <iostream>
#include <Eigen/Geometry>
#include <ros/ros.h>
#include <ros/package.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <opencv2/opencv.hpp>

#include <heuros_core/histograms_cpu.h>
#include <heuros_core/prop.h>

using namespace std;

#define ADDRESS_2_METACLOUD(a) *((MetacloudPtr*)a)
#define ADDRESS_2_INDICES(a) *((IndicesPtr*)a)
#define ADDRESS_2_BACKPROJECTION_MODELS(a) *((BackprojectionModelsPtr*)a)
#define ADDRESS_2_DETECTIONS(a) *((DetectionsPtr*)a)
#define ADDRESS_2_POINTCLOUD(a) *((pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr*)a)
#define PTR_2_ADDRESS(p) (long int)&p

/** \brief Feature and cluster type definitions */
// @{
typedef boost::shared_ptr<vector<float> > FeaturePtr;
typedef boost::shared_ptr<const vector<float> > FeatureConstPtr;
typedef boost::shared_ptr<vector<int> > IndicesPtr;
typedef boost::shared_ptr<const vector<int> > IndicesConstPtr;

// @}

union _float4
{
    float data[4];
    struct
    {
        float x, y, z, w;
    };
};

class Metacloud
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Convenience formats of the input cloud. */
    // @{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz;
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud_xyzrgbn;
    vector<float> vec_rgb;
    vector<_float4> vec_normals;
    // @}

    /** \brief Vector of known features names. */
    vector<string> features_names;

    /** \brief Vector of known features names. */
    vector<string> segment_sets_names;

    /** \brief Vector of pointers to known features. */
    vector<FeaturePtr> features;

    /** \brief Vector of each point's segment indices. */
    vector<IndicesPtr> segment_indices;

    /** \brief Vector of point's indices in each segment. */
    vector<IndicesPtr> segmented_points;

    /** \brief Vector of detected props */
    vector<Prop> props;

    /** \brief File name. Left empty for for online metaclouds. */
    string file_name;

    /** \brief Address to the GPU metacloud shared pointer object. */
    long int metacloud_gpu;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    Metacloud();

    /** \brief Get the number of features. */
    size_t numFeatures() const;

    /** \brief Get the number of segment sets. */
    size_t numSegmentSets() const;

    /** \brief Get the number points. */
    size_t size() const;

    /** \brief Get cloud origin. */
    Eigen::Vector4f getOrigin();

    /** \brief Get cloud orientation. */
    Eigen::Quaternionf getOrientation();
};

typedef boost::shared_ptr<Metacloud> MetacloudPtr;
typedef boost::shared_ptr<const Metacloud> MetacloudConstPtr;

#endif
