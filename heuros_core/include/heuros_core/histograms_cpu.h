#ifndef HISTOGRAMS_CPU
#define HISTOGRAMS_CPU

#include <opencv2/opencv.hpp>

using namespace std;

class HistogramCPU
{
public:

    //==================================================
    //================= PUBLIC VARIABLES =================
    //==================================================

    /** \brief Basic features lookup table. */
    vector<int> lut;

    /** \brief Data as OpenCV Mat. */
    cv::Mat data;

    /** \brief Index of the instance. */
    int instance;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    HistogramCPU(){}

    /** \brief Constructor. */
    HistogramCPU( vector<int> _lut, int _instance )
    {
        lut = _lut;
        instance = _instance;
    }

    /** \brief Constructor. */
    HistogramCPU( int ftr_a, int _instance )
    {
        lut.push_back(ftr_a);
        instance = _instance;
    }

    /** \brief Constructor. */
    HistogramCPU( int ftr_a, int ftr_b, int _instance )
    {
        lut.push_back(ftr_a);
        lut.push_back(ftr_b);
        instance = _instance;
    }

    /** \brief Get number of histogram dimensions. */
    int numDimensions(){
        return lut.size();
    }
};
#endif
