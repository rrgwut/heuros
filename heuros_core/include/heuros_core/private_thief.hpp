#ifndef PRIVATE_THIEF_HPP
#define PRIVATE_THIEF_HPP

#include <iostream>
using namespace std;

//===================================
// EVIL HACK TO ACCESS PRIVATE METHOD
//===================================
template<typename Tag>
struct result {
  /* export it ... */
  typedef typename Tag::type type;
  static type ptr;
};

template<typename Tag>
typename result<Tag>::type result<Tag>::ptr;

template<typename Tag, typename Tag::type p>
struct rob : result<Tag> {
  /* fill it ... */
  struct filler {
    filler() { result<Tag>::ptr = p; }
  };
  static filler filler_obj;
};

template<typename Tag, typename Tag::type p>
typename rob<Tag, p>::filler rob<Tag, p>::filler_obj;

/************** USAGE ***************

class MyTestClass
{
public:
    void publicFun()
    {
        cout << "Public function" << endl;
    }
private:
    void privateFun1()
    {
        cout << "Private function 1" << endl;
    }

    void privateFun2( int value )
    {
        cout << "Private function 2. Value: " << value << endl;
    }
};

//===================================

struct TestPrivateFun1 { typedef void(MyTestClass::*type)(); };
template class rob<TestPrivateFun1, &MyTestClass::privateFun1>;

struct TestPrivateFun2 { typedef void(MyTestClass::*type)(int); };
template class rob<TestPrivateFun2, &MyTestClass::privateFun2>;

//===================================

int main( int argc, char** argv )
{
    MyTestClass test_ob;
    (test_ob.*result<TestPrivateFun1>::ptr)();
    (test_ob.*result<TestPrivateFun2>::ptr)(15);

    return 0;
}

*************************************/

#endif
