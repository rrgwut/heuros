#ifndef TIMERS_GPU_H
#define TIMERS_GPU_H

#include <iostream>
#include <cuda_runtime.h>

using namespace std;

/** \brief Start GPU timer. */
void timerStart( cudaEvent_t _start );

/** \brief Stop GPU timer and return elapsed time. */
float timerStop( cudaEvent_t _start, cudaEvent_t _stop );

#endif
