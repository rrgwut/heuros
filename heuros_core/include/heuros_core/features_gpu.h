#ifndef FEATURES_GPU_H
#define FEATURES_GPU_H

#include <iostream>
#include <boost/shared_ptr.hpp>
#include <cuda_runtime.h>
#include <pcl/gpu/containers/device_array.h>
#include <pcl/gpu/octree/device_format.hpp>
#include <pcl/gpu/utils/safe_call.hpp>

using namespace std;

// HEUROS PARAMETERS
#define F_PI 3.1415926535f
#define F_PI_2 1.5707963267f
#define F_2_PI 6.283185307f
#define F_NAN (0.0f/0.0f)
#define NAN_CHECK(a) (a==a?false:true)

using namespace std;
using namespace pcl::gpu;

typedef boost::shared_ptr<DeviceArray<int> > DeviceArrayIntPtr;

template<class T>
class FeaturesGPU
{
public:

    //====================================================
    //================= TYPE DEFINITIONS =================
    //====================================================

    typedef boost::shared_ptr<FeaturesGPU> Ptr;
    typedef boost::shared_ptr<vector<T> > FeaturePtr;
    typedef DeviceArray<T> DFeature;
    typedef boost::shared_ptr<DFeature> DFeaturePtr;

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Number of points (step between features). */
    int step;

    /** \brief Number of features. */
    int num_features;

    /** \brief Templated array holding data. */
    DFeaturePtr array;

    /** \brief Counter used for accumulative creation. */
    int init_counter;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    FeaturesGPU();

    /** \brief Constructor with initialization. */
    FeaturesGPU( int _num_features, int size );

    /** \brief Destructor. */
    ~FeaturesGPU();

    /** \brief Initialize the data structures. */
    void create( int _num_features, int size );

    /** \brief Release device memory. */
    void release();

    /** \brief Download a selected feature to CPU. */
    void downloadFeature(FeaturePtr &host_feature, int ftr_idx );

    /** \brief Get the device pointer to the selected feature. */
    T* getFeaturePtr( int ftr_idx );

    /** \brief Get the device pointer to data. */
    T* data() const;

    /** \brief Counter function for accumulative initialization. */
    int countUp();

    /** \brief Get the number of points. */
    int numPoints() const;
};

#endif
