#ifndef PROP_H
#define PROP_H

#include <pcl/point_types.h>

using namespace std;

class Prop
{
public:

    //==================================================
    //================= PUBLIC VARIABLES =================
    //==================================================

    /** \brief Props' center */
    pcl::PointXYZ center;

    /** \brief Props' class name */
    string object_class;

    /** \brief Segment index */
    int segment_index;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    Prop( pcl::PointXYZ _center, string _object_class, int _segemnt_index )
    {
        center = _center;
        object_class = _object_class;
        segment_index = _segemnt_index;
    }
};
#endif
