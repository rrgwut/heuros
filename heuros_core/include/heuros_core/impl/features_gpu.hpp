#include <heuros_core/features_gpu.h>

template <class T>
FeaturesGPU<T>::FeaturesGPU()
{
    init_counter = 0;
    array = DFeaturePtr( new DFeature );
}

template <class T>
FeaturesGPU<T>::FeaturesGPU( int _num_features, int size )
{
    init_counter = 0;
    array = DFeaturePtr( new DFeature );
    create( _num_features, size );
}

template <class T>
FeaturesGPU<T>::~FeaturesGPU()
{
    release();
}

template <class T>
void FeaturesGPU<T>::create( int _num_features, int size )
{
    num_features = _num_features;
    step = size;

    array->create( num_features * size );
}

template <class T>
void FeaturesGPU<T>::release()
{
    array->release();
}

template <class T>
void FeaturesGPU<T>::downloadFeature( FeaturePtr &host_feature, int ftr_idx )
{
    host_feature = FeaturePtr( new vector<T>(step) );
    cudaSafeCall( cudaMemcpy(host_feature->data(), getFeaturePtr(ftr_idx), step*sizeof(float), cudaMemcpyDeviceToHost) );
    cudaSafeCall( cudaDeviceSynchronize() );
}

template <class T>
T* FeaturesGPU<T>::getFeaturePtr( int ftr_idx )
{
    return array->ptr() + ftr_idx*step;
}

template <class T>
T* FeaturesGPU<T>::data() const
{
    return array->ptr();
}

template <class T>
int FeaturesGPU<T>::countUp()
{
    return init_counter++;
}

template <class T>
int FeaturesGPU<T>::numPoints() const
{
    return array->size()/num_features;
}
