#include <heuros_core/histograms_gpu.h>

template <class T>
HistogramsGPU<T>::HistogramsGPU(){}

template <class T>
HistogramsGPU<T>::HistogramsGPU( int num_histograms, int resolution, int _num_dimensions ) : FeaturesGPU<T>( num_histograms, pow(resolution,_num_dimensions) )
{
    num_dimensions = _num_dimensions;
    features_lut = FeaturesGPU<int>::Ptr( new FeaturesGPU<int>( num_histograms, num_dimensions ) );
}

template <class T>
void HistogramsGPU<T>::create( int num_histograms, int resolution, int _num_dimensions )
{
    FeaturesGPU<T>::create( num_histograms, pow(resolution,_num_dimensions) );
    num_dimensions = _num_dimensions;
    features_lut = FeaturesGPU<int>::Ptr( new FeaturesGPU<int>( num_histograms, num_dimensions ) );
}

template <class T>
int HistogramsGPU<T>::resolution() const
{
    return pow( FeaturesGPU<T>::numPoints(), 1/num_dimensions );
}

template <class T>
int HistogramsGPU<T>::numBins() const
{
    return FeaturesGPU<T>::numPoints();
}
