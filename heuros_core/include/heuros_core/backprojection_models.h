#ifndef BACKPROJECTION_MODELS_H
#define BACKPROJECTION_MODELS_H

#include<iostream>
#include<heuros_core/metacloud_gpu.h>
//#include<heuros_core/object_model.h>

#define HIST_BINS 20
#define HIST_STEP (HIST_BINS*HIST_BINS)

typedef DeviceArray<unsigned int> NinjaArray;
typedef boost::shared_ptr<NinjaArray> NinjaArrayPtr;

typedef FeaturesGPU<int>::Ptr HistogramsPtr;

class BackprojectionModels
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Vector of the object class names. */
    vector<string> class_names;

    /** \brief Bit-level object detections tests for each histogram bin. */
    NinjaArrayPtr objects_test; // N_obj(32) * N_ftr * HIST_SIZE

    /** \brief Bit-level histograms relevance of each object. */
    NinjaArrayPtr histograms_relevance; // N_obj(32) * N_ftr

    /** \brief Histogram-feature lookup table. */
    FeaturesGPU<int>::Ptr histograms_lut;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    BackprojectionModels( int n_histograms, int n_objects );

    /** \brief Return the number of known objects. */
    int numObjects() const;

    /** \brief Return the number of 2D histograms. */
    int numHistograms() const;

    /** \brief Return the number of ninja sets (32-object packs). */
    int numNinjaSets() const;

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Number of known objects. */
    int num_objects;

    /** \brief Number of 2D histograms. */
    int num_histograms;
};

typedef boost::shared_ptr<BackprojectionModels> BackprojectionModelsPtr;
typedef boost::shared_ptr<const BackprojectionModels> BackprojectionModelsConstPtr;

#endif
