#include <detections.h>

Detections::Detections( int n_objects )
{
    num_objects = n_objects;
}

int Detections::size() const
{
    return cloud->size();
}

int Detections::numObjects() const
{
    return num_objects;
}

int Detections::numNinjaSets() const
{
    return num_objects / 32 + 1;
}


