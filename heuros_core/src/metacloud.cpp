#include <metacloud.h>

Metacloud::Metacloud()
{
    // automatic initialization of all pcl pointclouds
    cloud_xyz = pcl::PointCloud<pcl::PointXYZ>::Ptr( new pcl::PointCloud<pcl::PointXYZ> );
    cloud_xyzrgbn = pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr( new pcl::PointCloud<pcl::PointXYZRGBNormal> );
}

size_t Metacloud::numFeatures() const
{
    return features.size();
}

size_t Metacloud::numSegmentSets() const
{
    return segment_indices.size();
}

size_t Metacloud::size() const
{
    return cloud_xyzrgbn->size();
}

Eigen::Vector4f Metacloud::getOrigin()
{
    return cloud_xyzrgbn->sensor_origin_;
}

Eigen::Quaternionf Metacloud::getOrientation()
{
    return cloud_xyzrgbn->sensor_orientation_;
}
