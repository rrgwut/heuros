#include <backprojection_models.h>

BackprojectionModels::BackprojectionModels( int n_histograms , int n_objects )
{
    num_histograms = n_histograms;
    num_objects = n_objects;

    objects_test = NinjaArrayPtr( new NinjaArray );
    objects_test->create( HIST_STEP * num_histograms * numNinjaSets() );
    cudaMemset( objects_test->ptr(), ~0, objects_test->sizeBytes() );

    histograms_relevance = NinjaArrayPtr( new NinjaArray );
    histograms_relevance->create( num_histograms * numNinjaSets() );
    cudaMemset( histograms_relevance->ptr(), 0, histograms_relevance->sizeBytes() );

    histograms_lut = HistogramsPtr( new FeaturesGPU<int> );
    histograms_lut->create( num_histograms, 2 );
}

int BackprojectionModels::numObjects() const
{
    return num_objects;
}

int BackprojectionModels::numHistograms() const
{
    return num_histograms;
}

int BackprojectionModels::numNinjaSets() const
{
    return num_objects / 32 + 1;
}
