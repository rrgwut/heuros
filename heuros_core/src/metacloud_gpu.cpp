#include <metacloud_gpu.h>

MetacloudGPU::MetacloudGPU()
{
    cloud = DPointCloudPtr( new DPointCloud );
    normals = DNormalsPtr( new DNormals );
    colors = DFeaturePtr( new DFeature );
    sensor_orientation = DFeaturePtr( new DFeature );
    features = FeaturesGPU<float>::Ptr( new FeaturesGPU<float> );
    segment_indices = FeaturesGPU<int>::Ptr( new FeaturesGPU<int> );
    segmented_points = SegmentedPointsPtr( new vector<DeviceArrayIntPtr> );
    avg_neighbors = -1;
}

size_t MetacloudGPU::numFeatures() const
{
    return features->num_features;
}

size_t MetacloudGPU::numSegmentSets() const
{
    return segment_indices->num_features;
}

size_t MetacloudGPU::size() const
{
    return cloud->size();
}

MetacloudGPU::~MetacloudGPU()
{
    release();
}

void MetacloudGPU::release()
{
    // release main point clouds
    cloud->release();
    normals->release();
    colors->release();

    // release neighbors
    neighbors_indices.data.release();
    neighbors_indices.sizes.release();
    neighbors_indices_2.data.release();
    neighbors_indices_2.sizes.release();

    // release features
    features->release();
}
