#include <features_manager.h>

// PARAMETERS
// Threshold of avg. neighbors to calculate second neighborhood
#define WIDE_NBRS_THRESH 50

FeaturesManager::FeaturesManager()
{
    cudaEventCreate( &start_all );
    cudaEventCreate( &stop_all );
    cudaEventCreate( &stop_upload );
    cudaEventCreate( &stop_octree );
    cudaEventCreate( &stop_features );

    // Get the feature names vector
    ftr_theta.appendNames( features_names );
    ftr_convexity_anisotropy.appendNames( features_names );
    ftr_hs.appendNames( features_names );
}

FeaturesManager::~FeaturesManager(){
    release();
}

void FeaturesManager::setCloud( MetacloudPtr &_metacloud )
{
    metacloud = _metacloud;
    cout << "Cloud points: " << metacloud->size() << endl;

    // Create the device arrays and upload data
    metacloud_gpu = MetacloudGPUPtr( new MetacloudGPU );

    metacloud_gpu->cloud->upload( (DPoint*)(metacloud->cloud_xyz->points.data()), metacloud->size() );
    metacloud_gpu->normals->upload( (DNormal*)(metacloud->vec_normals.data()), metacloud->size() );
    metacloud_gpu->colors->upload( metacloud->vec_rgb.data(), metacloud->size() );

    cout << "GPU cloud points: " << metacloud_gpu->size() << endl;

    // Get sensor rotation matrix
    Eigen::Matrix3f rotation_matrix = metacloud->getOrientation().toRotationMatrix();
    metacloud_gpu->sensor_orientation->upload( rotation_matrix.data(), 9 );

    cout << "Cloud uploaded to GPU in: " << fixed << timerStop( start_all, stop_upload ) << " ms." << endl;
}

void FeaturesManager::calculateNeighborhoods()
{
    // Create structures
    octree = pcl::gpu::Octree::Ptr( new pcl::gpu::Octree );
    pcl::gpu::Octree::PointCloud octree_cloud;
    octree_cloud.upload( metacloud->cloud_xyz->points );
    octree->setCloud( octree_cloud );
    octree->build();

    // Perform search 1
    metacloud_gpu->neighbors_indices.create( octree_cloud.size(), MAX_NEIGHBORS );
    octree->radiusSearch( octree_cloud, NBR_R_1, MAX_NEIGHBORS, metacloud_gpu->neighbors_indices ); // for 1 cm radius up to 30 neighbors
    metacloud_gpu->avg_neighbors = calcAvgNeighbors( metacloud_gpu->neighbors_indices );
    cout << "Calculated r=" << NBR_R_1<< " neighborhoods. Avg nbrs: " << metacloud_gpu->avg_neighbors << endl;

    // Perform second search
    if( metacloud_gpu->avg_neighbors < WIDE_NBRS_THRESH ){    
        metacloud_gpu->neighbors_indices_2.create( octree_cloud.size(), MAX_NEIGHBORS );
        octree->radiusSearch( octree_cloud, NBR_R_2, MAX_NEIGHBORS, metacloud_gpu->neighbors_indices_2 );
        metacloud_gpu->avg_neighbors_2 = calcAvgNeighbors( metacloud_gpu->neighbors_indices_2 );
        cout << "Calculated r=" << NBR_R_2 << " neighborhoods. Avg nbrs: " << metacloud_gpu->avg_neighbors_2 << endl;
    }else{
        metacloud_gpu->neighbors_indices_2 = metacloud_gpu->neighbors_indices;
    }

    /*/ Download search results to host (only needed for debugging)
    vector<int> host_sizes( metacloud_gpu->neighbors_indices.sizes.size() );
    metacloud_gpu->neighbors_indices.sizes.download( host_sizes.data() );
    vector<int> host_sizes_2( metacloud_gpu->neighbors_indices_2.sizes.size() );
    metacloud_gpu->neighbors_indices_2.sizes.download( host_sizes_2.data() );
    float avg_size = 0;
    float avg_size_2 = 0;
    for( int i=0; i<host_sizes.size(); i+=100 ){
        avg_size += host_sizes[i];
        avg_size_2 += host_sizes_2[i];
    }
    cout << "AVG NBRHOOD 1 SIZE: " << avg_size / (host_sizes.size()/100) << endl; 
    cout << "AVG NBRHOOD 2 SIZE: " << avg_size_2 / (host_sizes_2.size()/100) << endl; 
    */
}

float FeaturesManager::calcAvgNeighbors( NeighborIndices neighbors ) const
{
    int q_step = 10;

    vector<int> sizes;
    neighbors.sizes.download(sizes);

    int sum = 0;
    for( int i=0; i<metacloud->size(); i+= q_step ){
        sum += sizes[i];
    }
    return float(sum)/(metacloud->size()/q_step);
}

void FeaturesManager::release()
{
    // Release gpu metacloud
    metacloud_gpu->release();

    // Release octree search - related stuff (produces error)
    //octree->clear();
    //octree_cloud.release();
}

void FeaturesManager::downloadFeatures()
{
    // Simple features
    for( int i=0; i<metacloud_gpu->numFeatures(); i++ ){
        FeaturePtr new_host_ftr( new vector<float>( metacloud_gpu->size() ) );
        metacloud_gpu->features->downloadFeature( new_host_ftr, i );
        metacloud->features.push_back( new_host_ftr );
    }

    /*
    // Flat features (append for visualization)
    for( int i=0; i<metacloud_gpu->numFlatFeatures(); i++ ){
        FeaturePtr new_host_ftr( new vector<float>( metacloud_gpu->size() ) );
        metacloud_gpu->flat_features->downloadFeature( new_host_ftr, i );
        metacloud->features.push_back( new_host_ftr );
    }
    */

    // Copy names
    metacloud->features_names = metacloud_gpu->features_names;
}

void FeaturesManager::setActiveFeatures()
{

}

void FeaturesManager::calculateFeatures()
{
    assert( metacloud_gpu->cloud );

    // General initial calculations
    calculateNeighborhoods();

    metacloud_gpu->avg_neighbors = calcAvgNeighbors( metacloud_gpu->neighbors_indices );
    metacloud_gpu->avg_neighbors_2 = calcAvgNeighbors( metacloud_gpu->neighbors_indices_2 );

    cout << "Neighborhoods calculated in: " << fixed << timerStop( stop_upload, stop_octree ) << " ms." << endl;

    // :::::::::::::::::::::::::
    // ::: EACH FEATURE HERE :::
    // :::::::::::::::::::::::::

    // Initialize local features
    const int num_features = 5;
    metacloud_gpu->features->create( num_features, metacloud->size());

    // Perform calculations for each feature class
    ftr_theta.process( metacloud_gpu );
    ftr_convexity_anisotropy.process( metacloud_gpu );
    ftr_hs.process( metacloud_gpu );

    metacloud_gpu->features_names = features_names;
    /*
    // Initialize flat area features
    const int num_flat_features = 4;
    metacloud_gpu->flat_features->create( num_flat_features, metacloud->size());

    // Calculate the flat area features
    flat_features.process( metacloud_gpu );
    flatDimensionsCPU();
    flat_features.calculate2();
    */

    // Download features to host
    downloadFeatures();

    // Attach metacloud_gpu to metacloud
    metacloud->metacloud_gpu = PTR_2_ADDRESS( metacloud_gpu );

    cout << "All features calculated and downloaded to CPU in: " << fixed << timerStop( stop_octree, stop_features ) << " ms." << endl;

    // release GPU memory (IMPORTANT) | moved to destructor
    //
    //release();
}

void FeaturesManager::calculateFeatures(MetacloudPtr &_metacloud )
{
    timerStart( start_all );
    setCloud( _metacloud );
    calculateFeatures();
    cout << "Total processing and memory operations time: " << fixed << timerStop( start_all, stop_all ) << " ms." << endl;
}

void FeaturesManager::timerStart( cudaEvent_t _start )
{
    cudaEventRecord( _start, 0 );
}

float FeaturesManager::timerStop( cudaEvent_t _start, cudaEvent_t _stop )
{
    float time;
    cudaEventRecord( _stop, 0 );
    cudaEventSynchronize( _stop );
    cudaEventElapsedTime( &time, _start, _stop );
    return time;
}
