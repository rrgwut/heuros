#include <ftr_convexity_anisotropy.h>

__device__ inline void rotateVector( const float mat[9], const float4 &vec, float4 &result )
{
    result.x = mat[0]*vec.x + mat[1]*vec.y + mat[2]*vec.z;
    result.y = mat[3]*vec.x + mat[4]*vec.y + mat[5]*vec.z;
    result.z = mat[6]*vec.x + mat[7]*vec.y + mat[8]*vec.z; // LATER TRY IF THIS GIVES US ANYTHING
}

__device__ inline void calcNormalRotation( const DNormal &normal, float rot_matrix[9] )
{
    float S1, C1, S2, C2;
    C2 = normal.z;
    if( C2>1 ) C2 = 1;
    else if( C2<-1 ) C2 = -1;
    S2 = sqrt( 1 - C2*C2 );
    if( S2 == 0 ){
        S1 = 0;
        C1 = 1;
    }else{
        C1 = normal.x / S2;
        S1 = normal.y / S2;
    }

    rot_matrix[0] = C1*C2;
    rot_matrix[1] = S1*C2;
    rot_matrix[2] = -S2;
    rot_matrix[3] = -S1;
    rot_matrix[4] = C1;
    rot_matrix[5] = 0;
    rot_matrix[6] = normal.x;
    rot_matrix[7] = normal.y;
    rot_matrix[8] = C2;
}

__device__ float processProjectedNeighbor( const DPoint &point,
                                           const DNormal &normal )
{
    // check if projections are of non-zero length
    if( ( normal.x == 0 && normal.y == 0 ) ||
        ( point.x == 0 && point.y == 0 ) ) return 0;

    ///////float n_abs = sqrt( normal.x*normal.x + normal.y*normal.y );
    float cos_pn = ( point.x*normal.x + point.y*normal.y ) /
                   sqrt( ( point.x*point.x + point.y*point.y ) *
                         ( normal.x*normal.x + normal.y*normal.y ) );

    // if the vector goes against the plane normal, return either -1 or 1
    if( normal.z < 0 && cos_pn < 0 )
        return -1;
    else if( normal.z < 0 && cos_pn > 0 )
        return 1;
    // else return cos_pn * asin( n_abs ) / F_PI_2
    // the same as cos_pn * acos(normal.z)/F_PI_2
    else
        return cos_pn * acos(normal.z)/F_PI_2;
}

__global__ void calculateDevice( const DPoint *cloud_begin,
                                 const DNormal *normals_begin,
                                 const int max_neighbors,
                                 const int *neighbors_sizes_begin,
                                 const int *neighbors_indices_begin,
                                 float* feature1_begin,
                                 float* feature2_begin,
                                 int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){

        // Convenience variables
        const DPoint point = *(cloud_begin + idx);
        const DNormal normal = *(normals_begin + idx);
        const int neighbors_size = *(neighbors_sizes_begin + idx);
        float &feature1 = *(feature1_begin + idx);
        float &feature2 = *(feature2_begin + idx);

        // Calculations

        // Find rotation matrix so that R*normal = (0,0,1)
        float rot_2_local[9];
        calcNormalRotation( normal, rot_2_local );

        // Chceck if neighborhood is big enough
        if( neighbors_size < 5 ){
            feature1 = F_NAN;
            feature2 = F_NAN;
            return;
        }

        // Go over the neighbors
        //float neighbors_height = 0;
        int n_included = 0;
        float sum_convexity = 0;
        float max_convexity = -1;
        float min_convexity = 1;
        for( int i=0; i<neighbors_size; i++ ){

            // Get and check neighbor index
            int nbr_idx = neighbors_indices_begin[ idx*max_neighbors+i ];
            if( nbr_idx == idx ) continue; // no auto-connection

            // Project neighbor point on a plane perpendicular to the point's normal
            DPoint neighbor_unrotated_point, neighbor_point;
            neighbor_unrotated_point.x = cloud_begin[ nbr_idx ].x - point.x;
            neighbor_unrotated_point.y = cloud_begin[ nbr_idx ].y - point.y;
            neighbor_unrotated_point.z = cloud_begin[ nbr_idx ].z - point.z;
            rotateVector( rot_2_local, neighbor_unrotated_point, neighbor_point );

            //neighbors_height += neighbor_point.z;

            // Tuning (yes, square distance and it would be faster)
            float neighbor_dist = sqrt( neighbor_unrotated_point.x*neighbor_unrotated_point.x + neighbor_unrotated_point.y*neighbor_unrotated_point.y + neighbor_unrotated_point.z*neighbor_unrotated_point.z );
            if( neighbor_dist < 0.008 ) continue;
            n_included++;

            // Project) normal
            DNormal neighbor_normal;
            rotateVector( rot_2_local, normals_begin[ nbr_idx ], neighbor_normal );

            // Let cosA be the cos of the angle between the neighbor's point and normal projections
            // Calculate neighbor convexity as: cosA * acos(neighbor_normal.z)/F_PI_2
            float neighbor_convexity = processProjectedNeighbor( neighbor_point, neighbor_normal );

            sum_convexity += neighbor_convexity;

            if( neighbor_convexity > max_convexity )
                max_convexity = neighbor_convexity;
            if( neighbor_convexity < min_convexity )
                min_convexity = neighbor_convexity;
        }

        // Mean convexity for all the neighborhood (comment continue and uncomment sum_convexity)
        //feature1 = 2 * sum_convexity / neighbors_size;

        // Mean convexity for a radius range of neighbors (a 'pseudosphere') (uncomment sum_convexity)
        feature1 = (sum_convexity>0?1:-1) * sqrt( abs( sum_convexity / n_included ) );

        // Scaled mean hight of a radius range of neighbors (a 'pseudosphere') (uncomment neighbors_height)
        //feature1 = - 500 * neighbors_height / neighbors_size;

        max_convexity = (max_convexity>0?1:-1) * sqrt( abs( max_convexity ) );
        min_convexity = (min_convexity>0?1:-1) * sqrt( abs( min_convexity ) );
        // Anisotropy for a radius range of neighbors (a 'pseudosphere')
        feature2 = 2*sqrt(( max_convexity - min_convexity )/2) - 1;
        //feature2 = ( max_convexity - min_convexity )/2 - 1;

        // Check range
        if( feature1 > 1.0 ) feature1 = 1.0;
        else if( feature1 < -1.0 ) feature1 = -1.0;
        if( feature2 > 1.0 ) feature2 = 1.0;
        else if( feature2 < -1.0 ) feature2 = -1.0;
    }
}

void FtrConvexityAnisotropy::appendNames( vector<string> &names )
{
   names.push_back("Convexity"); 
   names.push_back("Anisotropy of convexity"); 
}

void FtrConvexityAnisotropy::calculate()
{
    // Allocate n features in metacloud_gpu and create vector of corresponding indices
    vector<int> ftr_indices = allocFeatures(2);

    // Set up kernel params
    int N = metacloud_gpu->cloud->size();
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Call kernel and wait for it to finish
    timerStart();
    calculateDevice <<< nBlocks, blockSize >>> ( metacloud_gpu->cloud->ptr(),
                                                 metacloud_gpu->normals->ptr(),
                                                 metacloud_gpu->neighbors_indices.max_elems,
                                                 metacloud_gpu->neighbors_indices.sizes.ptr(),
                                                 metacloud_gpu->neighbors_indices.data.ptr(),
                                                 metacloud_gpu->features->getFeaturePtr(ftr_indices[0]),
                                                 metacloud_gpu->features->getFeaturePtr(ftr_indices[1]),
                                                 N );

    cudaSafeCall(cudaGetLastError());
    cout << "Convexity & anisotropy calculated in " << fixed << timerStop() << " ms." << endl;
    cudaSafeCall(cudaDeviceSynchronize());
}
