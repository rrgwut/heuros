#include <ftr_theta.h>

__device__ inline void rotateVector( const float mat[9], const float4 &vec, float4 &result )
{
    result.x = mat[0]*vec.x + mat[1]*vec.y + mat[2]*vec.z;
    result.y = mat[3]*vec.x + mat[4]*vec.y + mat[5]*vec.z;
    result.z = mat[6]*vec.x + mat[7]*vec.y + mat[8]*vec.z;
}

__global__ void calculateDevice( const DNormal *normals_begin,
                                 float* feature_begin,
                                 float* sensor_orientation,
                                 int N )
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<N){

        // Convenience variables
        const DNormal normal = *(normals_begin + idx);
        float &feature = *(feature_begin + idx);

        // Calculations
        DNormal rotated_normal;
        rotateVector( sensor_orientation, normal, rotated_normal );
        feature = acosf( rotated_normal.y ) / F_PI_2 - 1;
        //feature = acosf( normal.y ) / F_PI_2 - 1;

        // Check range
        if( feature > 1.0 ) feature = 1.0;
        else if( feature < -1.0 ) feature = -1.0;
    }
}

void FtrTheta::appendNames( vector<string> &names )
{
   names.push_back("Inclination"); 
}

void FtrTheta::calculate()
{
    // Allocate n features in metacloud_gpu and create vector of corresponding indices
    vector<int> ftr_indices = allocFeatures(1);

    // Set up kernel params
    int N = metacloud_gpu->cloud->size();
    int blockSize = 512;
    int nBlocks = N/blockSize + (N%blockSize == 0?0:1);

    // Call kernel and wait for it to finish
    timerStart();
    calculateDevice <<< nBlocks, blockSize >>> ( metacloud_gpu->normals->ptr(),
                                                 metacloud_gpu->features->getFeaturePtr(ftr_indices[0]),
                                                 metacloud_gpu->sensor_orientation->ptr(),
                                                 N );
    cudaSafeCall(cudaGetLastError());
    cout << "Theta calculated in " << fixed << timerStop() << " ms." << endl;
    cudaSafeCall(cudaDeviceSynchronize());
}
