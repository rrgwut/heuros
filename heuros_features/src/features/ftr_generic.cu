#include <ftr_generic.h>

FtrGeneric::FtrGeneric()
{
    cudaEventCreate( &start );
    cudaEventCreate( &stop );
}

vector<int> FtrGeneric::allocFeatures( int num_features )
{
    vector<int> ftr_indices;
    for( int i=0; i<num_features; i++ ){
        ftr_indices.push_back( metacloud_gpu->features->countUp() );
    }
    return ftr_indices;
}

void FtrGeneric::process( MetacloudGPUPtr _metacloud_gpu )
{
    metacloud_gpu = _metacloud_gpu;

    // Perform the actual GPU calculations
    calculate();
}

void FtrGeneric::timerStart()
{
    cudaEventRecord( start, 0 );
}

float FtrGeneric::timerStop()
{
    float time;
    cudaEventRecord( stop, 0 );
    cudaEventSynchronize( stop );
    cudaEventElapsedTime( &time, start, stop );
    return time;
}
