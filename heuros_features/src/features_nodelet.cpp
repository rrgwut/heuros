#include <features_nodelet.h>
// this should really be in the implementation (.cpp file)
#include <pluginlib/class_list_macros.h>

namespace heuros_features
{

void FeaturesNodelet::onInit()
{
    cout << endl << "Starting Heuros Features..." << endl;

    // Get input file name parameter
    ros::NodeHandle n;

    // Subscribers
    sub_metacloud = n.subscribe( "/heuros/input_metacloud", 1, &FeaturesNodelet::metacloudCb , this );

    // Publishers
    pub_metacloud = n.advertise<heuros_core::StampedAddress>( "/heuros/ftr_metacloud", 1, true );

    // Set up services
    srv_get_string_list = n.advertiseService("/heuros/get_features_names", &FeaturesNodelet::getStringListSrv, this);
}

void FeaturesNodelet::metacloudCb( const heuros_core::StampedAddressConstPtr &msg )
{
    cout << "Features: Input cloud received" << endl;

    // Calculate features
    features_manager.calculateFeatures( ADDRESS_2_METACLOUD(msg->address) );

    // Publish results
    cout << "Publishing features..." << endl;
    heuros_core::StampedAddressPtr pub_msg( new heuros_core::StampedAddress );
    pub_msg->address = PTR_2_ADDRESS( features_manager.metacloud );
    pub_metacloud.publish( pub_msg );
}

bool FeaturesNodelet::getStringListSrv( heuros_core::GetStringListRequest &req, heuros_core::GetStringListResponse &resp )
{
    cout << "Features: Received get features names request" << endl;

    if( req.query == "features" ){
        resp.string_list = features_manager.features_names;
    }

    return true;
}

}

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(heuros_features::FeaturesNodelet, nodelet::Nodelet)
