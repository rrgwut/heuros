#ifndef FTR_THETA_H
#define FTR_THETA_H

#include <ftr_generic.h>

using namespace std;

class FtrTheta : public FtrGeneric
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** Append the features names */
    void appendNames( vector<string> &names ); 

    /** \brief Main method to calculate the feature values */
    void calculate();
};

#endif
