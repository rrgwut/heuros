#ifndef CONVEXITY_ANISOTROPY_H
#define CONVEXITY_ANISOTROPY_H

#include <ftr_generic.h>

using namespace std;

class FtrConvexityAnisotropy : public FtrGeneric
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** Append the features names */
    void appendNames( vector<string> &names ); 

    /** \brief Main method to calculate the feature values */
    void calculate();
};

#endif
