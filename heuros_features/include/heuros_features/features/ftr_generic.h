// PLEASE NOTE:
/*//-------------------------------------------------------
  ---------------------------------------------------------

    Features should be normalized to the range [-1, 1]


    Every new feature should be included in the code
    of the following files:

        features_manager.h

        features_manager.cpp

        CMakeLists.txt

  ---------------------------------------------------------
*///-------------------------------------------------------


#ifndef FTR_GENERIC_H
#define FTR_GENERIC_H

#include <heuros_core/metacloud_gpu.h>

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif

using namespace std;
using namespace pcl::gpu;

class FtrGeneric
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    FtrGeneric();

    /** \brief Main method to calculate the feature values (needs implementation). */
    virtual void calculate()=0;

    /** \brief Process the point cloud (calculate and do stuff). */
    virtual void process( MetacloudGPUPtr _metacloud_gpu );

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief GPU metacloud */
    MetacloudGPUPtr metacloud_gpu;

    /** \brief GPU timer events. */
    cudaEvent_t start, stop;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Start GPU timer. */
    void timerStart();

    /** \brief Stop GPU timer and return elapsed time. */
    float timerStop();

    /** \brief Allocate the features on GPU metacloud and return corresponding indices. */
    virtual vector<int> allocFeatures( int num_features );
};

#endif
