#ifndef FEATURES_NODELET_H
#define FEATURES_NODELET_H

#include <iostream>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <std_msgs/String.h>

#include <features_manager.h>
#include <heuros_core/metacloud.h>
#include <heuros_core/StampedAddress.h>
#include <heuros_core/GetStringList.h>

using namespace std;

namespace heuros_features
{

class FeaturesNodelet : public nodelet::Nodelet
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief On nodelet initialization */
    virtual void onInit();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Features manager object. */
    FeaturesManager features_manager;

    /** \brief Input metacloud subscriber. */
    ros::Subscriber sub_metacloud;

    /** \brief Metacloud publisher. */
    ros::Publisher pub_metacloud;

    /** \brief Get string list service. */
    ros::ServiceServer srv_get_string_list;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Input metacloud received callback. */
    void metacloudCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Get features list service function. */
    bool getStringListSrv( heuros_core::GetStringListRequest &req, heuros_core::GetStringListResponse &resp );
};

}

#endif
