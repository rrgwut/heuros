#ifndef FEATURES_MANAGER_H
#define FEATURES_MANAGER_H

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/gpu/octree/octree.hpp>

#include <heuros_core/metacloud.h>
#include <ftr_theta.h>
#include <ftr_convexity_anisotropy.h>
#include <ftr_hs.h>
//#include <flat_features.h>

using namespace std;

class FeaturesManager
{
public:

    //====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Metacloud shared pointer. */
    MetacloudPtr metacloud;

    /** \brief GPU metacloud */
    MetacloudGPUPtr metacloud_gpu;

    // :::::::::::::::::::::::::
    // ::: EACH FEATURE HERE :::
    // :::::::::::::::::::::::::

    /** \brief Theta inclination angle. */
    FtrTheta ftr_theta;

    /** \brief Convexity and anisotropy of convexity. */
    FtrConvexityAnisotropy ftr_convexity_anisotropy;

    /** \brief Area of a flat surface. */
    //FlatFeatures flat_features;

    /** \brief Color hue and saturation. */
    FtrHS ftr_hs;

    /** \brief GPU timer events. */
    cudaEvent_t start_all, stop_all, stop_upload, stop_octree, stop_features;

    /** \brief Vector of feature names. */
    vector<string> features_names;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief Constructor. */
    FeaturesManager();

    /** \brief Destructor. */
    ~FeaturesManager();

    /** \brief Set the input point cloud and upload it to GPU. */
    void setCloud( MetacloudPtr &_metacloud );

    /** \brief Set a mask to calculate only some features. */
    void setActiveFeatures();

    /** \brief Trigger all the active feature calculations. */
    void calculateFeatures();

    /** \brief Set input point cloud and trigger all the active feature calculations. */
    void calculateFeatures( MetacloudPtr &_metacloud );

    /** \brief Release all used GPU memory. */
    void release();

    /** \brief Download features to host. */
    void downloadFeatures();

    /** \brief Start GPU timer. */
    void timerStart( cudaEvent_t _start );

    /** \brief Stop GPU timer and return elapsed time. */
    float timerStop( cudaEvent_t _start, cudaEvent_t _stop );

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Octree for neighbors search. */
    pcl::gpu::Octree::Ptr octree;

    /** \brief GPU XYZ point cloud for octree search. */
    //pcl::gpu::Octree::PointCloud octree_cloud;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Calculate the neighborhoods for each point. */
    void calculateNeighborhoods();

    /** \brief Find the voxel size of the original TSDF. */
    float calcAvgNeighbors( NeighborIndices neighbors ) const;
};

#endif
