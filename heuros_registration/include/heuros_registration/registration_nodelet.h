#ifndef REGISTRATION_NODELET_H
#define REGISTRATION_NODELET_H

#include <iostream>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <std_msgs/String.h>

#include <heuros_core/StampedAddress.h>
#include <heuros_core/SimpleInstruction.h>
#include <aligner.h>

using namespace std;

namespace heuros_registration
{

class RegistrationNodelet : public nodelet::Nodelet
{
public:

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

    /** \brief On nodelet initialization. */
    virtual void onInit();

protected:

    //=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Metacloud shared pointer. Needed here. */
    MetacloudConstPtr current_metacloud;

    /** \brief Metacloud with alignment results (for visualization). */
    MetacloudPtr result_metacloud;

    /** \brief Aligner utility object. */
    Aligner aligner;

    /** \brief Metacloud subscriber. */
    ros::Subscriber sub_metacloud;

    /** \brief Cluster subscriber. */
    ros::Subscriber sub_cluster;

    /** \brief Metacloud publisher (for visualization). */
    ros::Publisher pub_metacloud;

    /** \brief Processing done publisher. */
    ros::Publisher pub_done;

    /** \brief Simple instruction service. */
    ros::ServiceServer srv_instruction;

    /** \brief Processing done message. */
    std_msgs::String::Ptr done_msg;

    /** \brief Holds last received instruction. */
    string instruction;

    /** \brief Variables that control whether to accept new scenes and clusters. */
    bool accept_scene, accept_cluster;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Metacloud received callback. */
    void metacloudCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Cluster indices received callback. */
    void clusterCb( const heuros_core::StampedAddressConstPtr &msg );

    /** \brief Simple instructions service function. */
    bool instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp );
};

}

#endif
