#ifndef CORRESPONDENCE_REJECTION_UNKNOWN_AREAS_H_
#define CORRESPONDENCE_REJECTION_UNKNOWN_AREAS_H_

#include <pcl/registration/correspondence_rejection.h>
#include <pcl/point_cloud.h>
#include "tsdf_converter.h"
#include "my_octree.hpp"

namespace pcl
{
  namespace registration
  {
    /** \brief CorrespondenceRejectorUnknownAreas implements a correspondence rejection method
     *
     *  If you use this in academic work, please cite:
     *  ARTYKÓŁ KKR
     *
     *  \author Daiel Koguciuk
     *  \ingroup heuros_registration
    */
    template <typename SourceT, typename TargetT>
    class PCL_EXPORTS CorrespondenceRejectorUnknownAreas: public CorrespondenceRejector
    {
      using CorrespondenceRejector::input_correspondences_;
      using CorrespondenceRejector::rejection_name_;
      using CorrespondenceRejector::getClassName;

      public:
        typedef boost::shared_ptr<CorrespondenceRejectorUnknownAreas> Ptr;
        typedef boost::shared_ptr<const CorrespondenceRejectorUnknownAreas> ConstPtr;
        
        typedef pcl::PointCloud<TargetT> PointCloudTarget;
        typedef typename PointCloudTarget::Ptr PointCloudTargetPtr;
        typedef typename PointCloudTarget::ConstPtr PointCloudTargetConstPtr;

        /** \brief Empty constructor */
        CorrespondenceRejectorUnknownAreas () : iterations_ (10000)
        { rejection_name_ = "CorrespondenceRejectorUnknownAreas"; }

        /** \brief Get a list of valid correspondences after rejection from the original set of correspondences.
          * \param[in] original_correspondences the set of initial correspondences given
          * \param[out] remaining_correspondences the resultant filtered set of remaining correspondences
          */
        void 
        getRemainingCorrespondences (const pcl::Correspondences& original_correspondences, 
                                     pcl::Correspondences& remaining_correspondences);
        
        /** \brief See if this rejector requires source points */
        bool
        requiresSourcePoints () const
        { return (false); }

        /** \brief See if this rejector requires a target cloud */
        bool
        requiresTargetPoints () const
        { return (true); }

        /** \brief Provide a target point cloud dataset (must contain XYZ data!), used to compute the correspondence distance.
          * \param[in] target a cloud containing XYZ data
          */
        inline void
        setInputTarget (const PointCloudTargetConstPtr &target)
        { target_ = target; }

        /** \brief Function for building octree from TSDF file. */
        bool
        setTargetTSDF (std::string fileName)
        {
            //Open file
            TSDFConverter file;
            if (!file.load(fileName)) return false;

            //Convert to pcd cloud
            pcl::PointCloud<pcl::PointXYZI>::Ptr tsdf_cloud;
            tsdf_cloud = pcl::PointCloud<pcl::PointXYZI>::Ptr (new pcl::PointCloud<pcl::PointXYZI>);
            file.convertTSDFToPCDCloud(tsdf_cloud);

            //Build octree
            int voxelsNumber = (int)file.getVoxelNumber()[0];
            float voxelSize  = ((float)file.getVoxelSize()[0])/voxelsNumber;

            if (octree.build(tsdf_cloud,voxelsNumber,voxelSize)) return false;

            //return
            return true;
        }

        /** \brief Ask point cloud about number of visible points. */
        int
        getPointVisibility(const pcl::PointCloud<pcl::PointXYZRGBNormal> cloud)
        {
            int number;
            octree.getPointsVisibility(cloud,&number);
            return number;
        }

        /** \brief Method for setting the target cloud */
        void
        setTargetPoints (pcl::PCLPointCloud2::ConstPtr cloud2)
        { 
          PointCloudTargetPtr cloud (new PointCloudTarget);
          fromPCLPointCloud2 (*cloud2, *cloud);
          setInputTarget (cloud);
        }

        /** \brief Set the number of iterations
          * \param iterations number of iterations
          */
        inline void 
        setIterations (int iterations)
        { iterations_ = iterations; }
        
        /** \brief Get the number of iterations
          * \return number of iterations
          */
        inline int 
        getIterations ()
        { return (iterations_); }

      protected:
        /** \brief Apply the rejection algorithm.
          * \param[out] correspondences the set of resultant correspondences.
          */
        inline void 
        applyRejection (pcl::Correspondences &correspondences)
        { getRemainingCorrespondences (*input_correspondences_, correspondences); }
        
        /** \brief The input point cloud dataset target */
        PointCloudTargetConstPtr target_;
        
        /** \brief Number of iterations to run */
        int iterations_;

        /** \brief Octree object. */
        My_Octree octree;
    };
  }
}

#include <impl/correspondence_rejection_unknown_areas.hpp>

#endif    // CORRESPONDENCE_REJECTION_UNKNOWN_AREAS_H_
