#ifndef MY_OCTREE
#define MY_OCTREE

#include <cuda.h>
#include <cuda_runtime.h>                   //Runtime
#include <cuda_runtime_api.h>               //Header for __global__
#include <device_launch_parameters.h>       //Header for threadInd.x

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <iostream>

class My_Octree
{
public:

    /** @brief  Structure describing single octree node stored at GPU. */
    struct d_OctreeNode
    {
        /**
         *  @brief  First index of node childern. Point (0,0,0) was taken
         *          at top-front-left corner of cube and every axis is
         *          situated along cube. Children are numbered as listed:
         *          0   -   x<  y<  z<
         *          1   -   x>  y<  z<
         *          2   -   x<  y>  z<
         *          3   -   x>  y>  z<
         *          4   -   x<  y<  z>
         *          5   -   x>  y<  z>
         *          6   -   x<  y>  z>
         *          7   -   x>  y>  z>
         *
         */
        int children_index;

        /** @brief  Value of middle index in x asix of node. */
        short int x_half;

        /** @brief  Value of middle index in y asix of node. */
        short int y_half;

        /** @brief  Value of middle index in z asix of node. */
        short int z_half;

        /**
         *  \brief      Self value of node as follows:
         *              0   -   Node don't have value yet.
         *              1   -   Node is in unknown area.
         *              2   -   Node is in border area.
         *              3   -   Node is in empty area.
         *              4   -   Node is divided.
        */
        char value;
    };

    /** @brief Default constructor. There is checking CUDA-compatible and compute capability inside. */
    My_Octree()
    {
        //Help variables ========================================================================
        int device_count;
        int device = 0;
        cudaDeviceProp device_prop;

        //Device count ==========================================================================
        cudaGetDeviceCount(&device_count);
        if (device_count==0)
        {
            std::cout << "ERROR! No devices supporting CUDA.\n";
            exit(EXIT_FAILURE);
        }

        //Device properties =====================================================================
        cudaSetDevice(device);
        if(cudaGetDeviceProperties(&device_prop,device)==0) compute_capability = device_prop.major*10 + device_prop.minor;
        else compute_capability=0;

        //Flag ==================================================================================
        builded = false;
    }

    /** @brief Destructor of class. If octree was build - dealocate it. */
    ~My_Octree()
    {
        if (builded) cudaFree(d_octree_nodes_data);
    }

    /**
     *  @brief      Build octree at GPU from TSDF cloud stored in pcl point cloud.
     *  @param[in]  _cloug_ptr      Pointer to pcl cloud.
     *  @param[in]  cloud_one_dim   Size of one dimension of TSDF cloud.
     *  @param[in]  voxel_size      Size of single voxel in meters.
     *  @return     Error number:
                    0   -   succed
                    1   -   cloud too big.
     */
    int build(const pcl::PointCloud<pcl::PointXYZI>::ConstPtr &_cloud_ptr, int cloud_one_dim, float voxel_size);

    /**
     *  @brief      Ask octree about visibility of point cloud in parameter. Visibility is checked in 3x3x3 cube
     *              around asked point to prevent errors coused by border blur.
     *  @param[in]  cloud_ptr               Pointer to pcl point cloud to ask about.
     *  @param[out] visible_points_number   Number of points in empty (visible) area.
     */
    int getPointsVisibility(const pcl::PointCloud<pcl::PointXYZRGBNormal> &cloud, int* visible_points_number);

private:

    /** @brief Nodes array. */
    d_OctreeNode *d_octree_nodes_data;

    /** @brief Ocree buil flag. */
    bool builded;

    /** @brief GPU compute capability stored as integer number (10*major + minor). */
    int compute_capability;

    /** @brief Single voxel size in meters. */
    float voxelSize;
};

#endif //MY_OCTREE
