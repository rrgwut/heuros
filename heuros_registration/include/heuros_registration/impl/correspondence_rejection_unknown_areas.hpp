/*
 * Software License Agreement (BSD License)
 *
 *  Point Cloud Library (PCL) - www.pointclouds.org
 *  Copyright (c) 2010-2011, Willow Garage, Inc.
 *  Copyright (c) 2012-, Open Perception, Inc.
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef CORRESPONDENCE_REJECTION_UNKNOWN_AREAS_HPP_
#define CORRESPONDENCE_REJECTION_UNKNOWN_AREAS_HPP_


///////////////////////////////////////////////////////////////////////////////////////////
template <typename SourceT, typename TargetT> void 
pcl::registration::CorrespondenceRejectorUnknownAreas<SourceT, TargetT>::getRemainingCorrespondences (
    const pcl::Correspondences& original_correspondences, 
    pcl::Correspondences& remaining_correspondences)
{
  // This is reset after all the checks below
  remaining_correspondences = original_correspondences;
/*
  if (!target_)
  {
    PCL_ERROR ("[pcl::registration::%s::getRemainingCorrespondences] No target was input! Returning all input correspondences.\n",
               getClassName ().c_str ());
    return;
  }
  
  // Check cardinality
  if (cardinality_ < 2)
  {
    PCL_ERROR ("[pcl::registration::%s::getRemainingCorrespondences] Polygon cardinality too low!. Returning all input correspondences.\n",
               getClassName ().c_str() );
    return;
  }
  
  // Number of input correspondences
  const int nr_correspondences = static_cast<int> (original_correspondences.size ());

  // Not enough correspondences for polygonal rejections
  if (cardinality_ >= nr_correspondences)
  {
    PCL_ERROR ("[pcl::registration::%s::getRemainingCorrespondences] Number of correspondences smaller than polygon cardinality! Returning all input correspondences.\n",
               getClassName ().c_str() );
    return;
  }

  // Initialization of result
  remaining_correspondences.clear ();
  remaining_correspondences.reserve (nr_correspondences);
  
  // Number of times a correspondence is sampled and number of times it was accepted
  std::vector<int> num_samples (nr_correspondences, 0);
  std::vector<int> num_accepted (nr_correspondences, 0);
  
  // Main loop
  for (int i = 0; i < iterations_; ++i)
  {
    // Sample cardinality_ correspondences without replacement
    const std::vector<int> idx = getUniqueRandomIndices (nr_correspondences, cardinality_);
    
    // Verify the polygon similarity
    if (thresholdPolygon (original_correspondences, idx))
    {
      // Increment sample counter and accept counter
      for (int j = 0; j < cardinality_; ++j)
      {
        ++num_samples[ idx[j] ];
        ++num_accepted[ idx[j] ];
      }
    }
    else
    {
      // Not accepted, only increment sample counter
      for (int j = 0; j < cardinality_; ++j)
        ++num_samples[ idx[j] ];
    }
  }
  
  // Now calculate the acceptance rate of each correspondence
  std::vector<float> accept_rate (nr_correspondences, 0.0f);
  for (int i = 0; i < nr_correspondences; ++i)
  {
    const int numsi = num_samples[i];
    if (numsi == 0)
      accept_rate[i] = 0.0f;
    else
      accept_rate[i] = static_cast<float> (num_accepted[i]) / static_cast<float> (numsi);
  }
  
  // Compute a histogram in range [0,1] for acceptance rates
  const int hist_size = nr_correspondences / 2; // TODO: Optimize this
  const std::vector<int> histogram = computeHistogram (accept_rate, 0.0f, 1.0f, hist_size);
  
  // Find the cut point between outliers and inliers using Otsu's thresholding method
  const int cut_idx = findThresholdOtsu (histogram);
  const float cut = static_cast<float> (cut_idx) / static_cast<float> (hist_size);
  
  // Threshold
  for (int i = 0; i < nr_correspondences; ++i)
    if (accept_rate[i] > cut)
      remaining_correspondences.push_back (original_correspondences[i]);
*/
}

#endif    // CORRESPONDENCE_REJECTION_UNKNOWN_AREAS_HPP_
