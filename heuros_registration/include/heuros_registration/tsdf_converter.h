#ifndef TSDF_CONVERTER
#define TSDF_CONVERTER

//PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/console/print.h>
#include <pcl/io/pcd_io.h>

//Boost
#include <boost/shared_ptr.hpp>

//Files
#include <fstream>

class TSDFConverter
{
public:
    /** @brief Boost class pointer. */
    typedef boost::shared_ptr<TSDFConverter> Ptr;

    /** @brief Boost class constant pointer. */
    typedef boost::shared_ptr<const TSDFConverter> ConstPtr;

    /** @brief File header structure. */
    struct Header
    {
        /** @brief 3D eigen vector of voxel number in point cloud. */
        Eigen::Vector3i resolution;

        /** @brief 3D eigen vector of point cloud size in meters. */
        Eigen::Vector3f volume_size;

        /** @brief Volume element size in bits. */
        int volume_element_size;

        /** @brief Weights element size in bits. */
        int weights_element_size;

        /** @brief Empty constructor of structure Header. */
        Header ()
            : resolution (0,0,0),
              volume_size (0,0,0),
              volume_element_size (sizeof(float)),
              weights_element_size (sizeof(short))
        {}

        /**
         *  @brief Reaload constructor initializing resolution value and volume size.
         *  @param[in]  _resolution     Reference value of TSDF cloud resolution.
         *  @param[in]  _volume_size    Reference value of TSDF cloud volume size.
        */
        Header (const Eigen::Vector3i &_resolution, const Eigen::Vector3f &_volume_size)
            : resolution (_resolution),
              volume_size (_volume_size),
              volume_element_size (sizeof(float)),
              weights_element_size (sizeof(short))
        {}

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    };

    /** @brief Empty constructor. */
    TSDFConverter ()
        : volume_ (new std::vector<float>),
          weights_ (new std::vector<short>)
    {}

    /**
     *  @brief      Constructor, which loads TSDF cloud from file.
     *  @param[in]  filename    Path to TSDF data file.
     *  @param[in]  binary      TSDF data file flag - if true the file is in binary format,
     *                          if false the file is in text format.
    */
    TSDFConverter (const std::string &filename, bool binary = true)
        : volume_ (new std::vector<float>),
          weights_ (new std::vector<short>)
    {
      if (!load (filename),binary) std::cout << "TSDF file data load error!" << std::endl;
    }

    /**
     *  @brief      Get total voxel number in cloud.
     *  @return     Total voxel number in cloud.
    */
    inline size_t
    getTotalVoxelNumber() const { return header_.resolution[0] * header_.resolution[1] * header_.resolution[2]; }

    /**
     *  @brief      Get voxel number in each axis.
     *  @return     Voxel number in each axis stored in Eigen vector.
    */
    inline Eigen::Vector3i
    getVoxelNumber() const { return header_.resolution; }

    /** @brief      Get cloud size in meters in each axis.
     *  @return     Cloud size in meters in each axis stored in Eigen vector.
    */
    inline Eigen::Vector3f
    getVoxelSize() const { return header_.volume_size; }

    /**
     *  @brief      Load TSDF data from file.
     *  @param[in]  filename    Path to TSDF data file.
     *  @param[in]  binary      TSDF data file flag - if true the file is in binary format,
     *                          if false the file is in text format.
     *  @return     Operation success flag.
    */
    bool
    load(const std::string &filename, bool binary = true);

    /**
     *  @brief      Save TSDF cloud to file.
     *  @param[in]  filename    Path to TSDF data file.
     *  @param[in]  binary      TSDF data file flag - if true the file is in binary format,
     *                          if false the file is in text format.
     *  @param[in]  divisor     Divisor for cloud size downsampling
     *  @return     Operation success flag.
    */
    bool
    save(const std::string &filename = "tsdf_volume.dat", bool binary = true, int divisor = 1) const;

    /**
     *  @brief      Save TSDF cloud to file.
     *  @param[in]  filename    Path to TSDF data file.
     *  @param[in]  binary      TSDF data file flag - if true the file is in binary format,
     *                          if false the file is in text format.
     *  @param[in]  color       Point cloud color flag - if true the file is in XYZRBG format,
     *                          if false the file is in XYZI format.
     *  @param[in]  divisor     Divisor for cloud size downsampling
     *  @return     Operation success flag.
    */
    bool
    saveTSDFInPCD(const std::string &filename = "tsdf_in_pcd.dat", bool binary = true, bool color = true, int divisor = 1) const;

    /**
     *  @brief      Convert TSDF cloud to PCD format.
     *  @param[out] cloud       PCL cloud pointer.
     *  @param[in]  divisor     Divisor for cloud size downsampling
     *  @return     Operation success flag.
    */
    bool
    convertTSDFToPCDCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, int divisor = 1) const;

    /**
     *  @brief      Convert TSDF cloud to PCD format.
     *  @param[out] cloud       PCL cloud pointer.
     *  @param[in]  divisor     Divisor for cloud size downsampling
     *  @return     Operation success flag.
    */
    bool
    convertTSDFToPCDCloud(pcl::PointCloud<pcl::PointXYZI>::Ptr cloud, int divisor = 1) const;

    /** @brief File load information. */
    bool
    isLoaded() { return loaded; }

private:
    /** @brief Boost pointer to volume vector typedef. */
    typedef boost::shared_ptr<std::vector<float> > VolumePtr;

    /** @brief Boost pointer to weights vector typedef. */
    typedef boost::shared_ptr<std::vector<short> > WeightsPtr;

    /** @brief Header variable. */
    Header header_;

    /** Volume variable. */
    VolumePtr volume_;

    /** Wieghts variable. */
    WeightsPtr weights_;

    /** File load flag. */
    bool loaded;

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};
#endif //TSDF_CONVERTER
