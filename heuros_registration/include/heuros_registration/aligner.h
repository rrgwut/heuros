#ifndef ALIGNER_H
#define ALIGNER_H

#include <iostream>
#include <cuda_runtime.h>
#include <fstream>
#include <ros/ros.h>
#include <ros/package.h>

#include <pcl/features/fpfh_omp.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/registration/icp.h>
//#include <pcl/registration/sample_consensus_prerejective.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <heuros_core/metacloud.h>

#include <sample_consensus_prerejective_unknown_areas.h>



using namespace std;

class Aligner
{
public:

	//====================================================
    //================= PUBLIC VARIABLES =================
    //====================================================

    /** \brief Target scene file name. */
    string target_scene_name;

    /** \brief Source metacloud shared pointer. */
    MetacloudConstPtr source_metacloud;

    /** \brief Target metacloud shared pointer. */
    MetacloudConstPtr target_metacloud;

    /** \brief Source cloud indices. */
    IndicesConstPtr source_indices;

    /** \brief Target cloud indices. */
    IndicesConstPtr target_indices;

    /** \brief Object alignment result (for visualization). */
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr object_aligned;

    //==================================================
    //================= PUBLIC METHODS =================
    //==================================================

	/** \brief Constructor. */
    Aligner();

    /** \brief Main function - calls the actual alginment algorithm. */
    void align();

    /** \brief Change prerejection method:
     *          0 - no prerejection;
     *          1 - prerejection poly
     *          2 - prerejection unknown areas
     *          3 - prerejection poly + unknown areas (default).
    */
    void setPrerejection( int prerejection );

    /** \brief Get actual prerejection method. */
    int getPrerejection();

protected:
	
	//=======================================================
    //================= PROTECTED VARIABLES =================
    //=======================================================

    /** \brief Point type. */
    typedef pcl::PointXYZRGBNormal PointNT;

    /** \brief Point cloud type. */
    typedef pcl::PointCloud<PointNT> PointCloudT;

    /** \brief Feature type. */
    typedef pcl::FPFHSignature33 FeatureT;

    /** \brief Feature estimation type. */
    typedef pcl::FPFHEstimationOMP<PointNT,PointNT,FeatureT> FeatureEstimationT;

    /** \brief Feature cloud type. */
    typedef pcl::PointCloud<FeatureT> FeatureCloudT;

    /** \brief Handler to color point cloud. */
    typedef pcl::visualization::PointCloudColorHandlerCustom<PointNT> ColorHandlerT;

    /** \brief GPU timer events. */
    cudaEvent_t start_all, stop_mem, stop_downsample, stop_features, stop_align;

    //=====================================================
    //================= PROTECTED METHODS =================
    //=====================================================

    /** \brief Start GPU timer. */
    void timerStart( cudaEvent_t _start );

    /** \brief Stop GPU timer and return elapsed time. */
    float timerStop( cudaEvent_t _start, cudaEvent_t _stop );

    /** \brief RANSAC aligning class. */
    pcl::SampleConsensusPrerejectiveUnknownAreas<PointNT,PointNT,FeatureT> ransac;
};

#endif
