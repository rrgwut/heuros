#include <registration_nodelet.h>
// this should really be in the implementation (.cpp file)
#include <pluginlib/class_list_macros.h>

namespace heuros_registration
{

void RegistrationNodelet::onInit()
{
    cout << "Starting Heuros Registration..." << endl;

    accept_scene = false;
    accept_cluster = false;
    ros::NodeHandle n;

    // Set up publishers
    pub_metacloud = n.advertise<heuros_core::StampedAddress>( "/heuros/reg_metacloud", 1, false );
    pub_done = n.advertise<std_msgs::String>( "/heuros/done", 1 );
    done_msg = std_msgs::String::Ptr( new std_msgs::String );
    done_msg->data = "registration";

    // Set up subscribers
    sub_metacloud = n.subscribe( "/heuros/seg_metacloud", 1, &RegistrationNodelet::metacloudCb , this );
    sub_cluster = n.subscribe( "/heuros/cluster", 1, &RegistrationNodelet::clusterCb , this );

    // Set up services
    srv_instruction = n.advertiseService( "/heuros/reg_instruction", &RegistrationNodelet::instructionSrv, this );
}

void RegistrationNodelet::metacloudCb( const heuros_core::StampedAddressConstPtr &msg )
{
    // Keep in case IO sends only the cluster without reloading the metacloud
    current_metacloud = ADDRESS_2_METACLOUD(msg->address);

    // Accept only if instructed
    if( !accept_scene ) return;
    accept_scene = false;

    cout << "Registration: Metacloud received" << endl;

    if( instruction == "target_scene" ){
        aligner.target_metacloud = current_metacloud;
        aligner.target_indices = IndicesPtr();
    }

    // Publish done message
    pub_done.publish( done_msg );
}

void RegistrationNodelet::clusterCb( const heuros_core::StampedAddressConstPtr &msg )
{
    // Accept only if instructed
    if( !accept_cluster ) return;
    accept_cluster = false;

    cout << "Registration: Cluster indices received" << endl;

    if( instruction == "target_cluster" ){
        aligner.target_metacloud = current_metacloud;
        aligner.target_indices = ADDRESS_2_INDICES(msg->address);
    }

    if( instruction == "source_cluster" ){
        aligner.source_metacloud = current_metacloud;
        aligner.source_indices = ADDRESS_2_INDICES(msg->address);
    }

    // Publish done message
    pub_done.publish( done_msg );
}

// ------------------------------
// ----- INSTRUCTION SERVER -----
// ------------------------------

bool RegistrationNodelet::instructionSrv( heuros_core::SimpleInstructionRequest &req, heuros_core::SimpleInstructionResponse &resp )
{
    // ================== TARGET SCENE ==================

    if( req.instruction == "target_scene" )
    {
        cout << "Registration: Received target scene instruction" << endl;
        accept_scene = true;
        instruction = req.instruction;
    }

    // ================== TARGET CLUSTER ==================

    else if( req.instruction == "target_cluster" )
    {
        cout << "Registration: Received target cluster instruction" << endl;
        accept_scene = true;
        accept_cluster = true;
        instruction = req.instruction;
    }

    // ================== SOURCE CLUSTER ==================

    else if( req.instruction == "source_cluster" )
    {
        cout << "Registration: Received source cluster instruction" << endl;
        accept_scene = true;
        accept_cluster = true;
        instruction = req.instruction;
    }

    // ================== TARGET SCENE NAME ==================

    else if( req.instruction == "target_scene_name" )
    {
        cout << "Registration: Received target scene name" << endl;
        aligner.target_scene_name = req.parameter;
        // Don't overwrite instruction here
    }

    // ================== ALIGN ==================

    else if( req.instruction == "align" )
    {
        cout << "Registration: Received align instruction" << endl;

        int prerejection = (req.parameter.size()==0) ? 3 : (req.parameter.at(req.parameter.size()-1)-48);
        aligner.setPrerejection( prerejection );
        aligner.align();

        result_metacloud = MetacloudPtr( new Metacloud );
        result_metacloud->cloud_xyzrgbn = aligner.object_aligned;
        heuros_core::StampedAddressPtr cloud_msg( new heuros_core::StampedAddress );
        cloud_msg->address = PTR_2_ADDRESS( result_metacloud );
        pub_metacloud.publish( cloud_msg );

        instruction = req.instruction;
    }

    // ==================

    //std_msgs::String::Ptr done_msg( new std_msgs::String );
    //done_msg->data = "registration";
    //pub_done.publish( done_msg );

    return true;
}

}

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(heuros_registration::RegistrationNodelet, nodelet::Nodelet)
