//block Eigen headers
#define EIGEN_PACKET_MATH_SSE_H
#define EIGEN_MATH_FUNCTIONS_SSE_H
#define EIGEN_COMPLEX_SSE_H
#define PCL_COMMON_POINT_TESTS_H_
#define EIGEN_GEOMETRY_SSE_H

#include <my_octree.hpp>

/**
 *  @brief      Convert float vector from TSDF cloud to char vector used in octree building.
 *  @param[in]  cloud               Float vector from TSDF cloud.
 *  @param[out] converted_cloud     Char vector valued as in My_Octree::value.
 *  @param[out] number              Border points number in cloud.
 *  @attention  Kernel jest zrobiony bardzo nieefektywnie - w przypadku optymalizacji należy przerobić zliczanie
 *              punktów granicznych na operację REDUCE, ponieważ teraz jest to załatwione operacją atomic.
 */
__global__ void gpu_convert_cloud(float* cloud, char* converted_cloud, int* number)
{
    //Definicja indeksu
    int index_x = (blockIdx.x % gridDim.y) * blockDim.x + threadIdx.x;
    int index_y = blockIdx.y * blockDim.y + threadIdx.y;
    int index_z = (blockIdx.x / gridDim.y) * blockDim.z + threadIdx.z;
    int index   = index_z*gridDim.y*blockDim.x*gridDim.y*blockDim.y + index_y*gridDim.y*blockDim.x + index_x;

    if (cloud[index]>0)         converted_cloud[index]=3;       //WATOSC ZNANA
    else if (cloud[index]==0)   converted_cloud[index]=1;       //WATOSC NIEZNANA
    else
    {
        converted_cloud[index]=2;                               //WATOSC GRANICZNA
        atomicAdd(number,1);
    }
}

/**
 *  @brief      Get indicies of border points in converted vector.
 *  @param[in]  converted_cloud     Char vector valued as in My_Octree::value.
 *  @param[out] pointx              Vector of x coordinate of border points.
 *  @param[out] pointy              Vector of y coordinate of border points.
 *  @param[out] pointz              Vector of z coordinate of border points.
 *  @param[out] first_free_index    First free index of coordinate vector of border points.
 *  @attention  Kernel jest zrobiony bardzo nieefektywnie - w przypadku optymalizacji należy przerobić zliczanie
 *              punktów granicznych na operację SCAN, ponieważ teraz jest to załatwione operacją atomic.
 */
__global__ void gpu_get_border_voxels(char* converted_cloud, int* pointx, int* pointy, int* pointz, int* first_free_index)
{
    //Definicja indeksu
    int index_x = (blockIdx.x % gridDim.y) * blockDim.x + threadIdx.x;
    int index_y = blockIdx.y * blockDim.y + threadIdx.y;
    int index_z = (blockIdx.x / gridDim.y) * blockDim.z + threadIdx.z;
    int index   = index_z*gridDim.y*blockDim.x*gridDim.y*blockDim.y + index_y*gridDim.y*blockDim.x + index_x;

    if (converted_cloud[index]==2)
    {
        int actual_index = atomicAdd(first_free_index,1);
        pointx[actual_index] = index_x;
        pointy[actual_index] = index_y;
        pointz[actual_index] = index_z;
    }
}

/**
 *  @brief      Build octree kernel.
 *  @param[out] octree_nodes_data       Nodes array.
 *  @param[in]  converted_cloud         Char vector valued as in My_Octree::value.
 *  @param[out] pointx                  Vector of x coordinate of border points.
 *  @param[out] pointy                  Vector of y coordinate of border points.
 *  @param[out] pointz                  Vector of z coordinate of border points.
 *  @param[in]  points_number           Border points numer.
 *  @param[out] actual_first_index      First free index of nodes array.
 *  @param[in]  max_depth               Maximum depth of tree.
 *  @param[in]  one_dim_size            One dimension size of cloud.
 *  @param[out] flag                    Help variable vector used to prevent from double parent node writing.
 *  @param[in]  level                   Actual octree level.
 */
__global__ void gpu_build(My_Octree::d_OctreeNode* octree_nodes_data, int* pointx, int* pointy, int* pointz, int* points_number, int* actual_first_index, int one_dim_size, int* flag, int level)
{
    //Aktualnie budowany poziom
    int cloud_one_dim_size  = one_dim_size;
    int border_point_index  = blockIdx.x*blockDim.x + threadIdx.x;

    //Jezeli nie wyszliśmy za zakres
    if (border_point_index<(*points_number))
    {
        //Indeks aktualnego punktu granicznego
        int index_x     = pointx[border_point_index];
        int index_y     = pointy[border_point_index];
        int index_z     = pointz[border_point_index];

        //Indeks aktualnej flagi
        int actual_node_width    = cloud_one_dim_size/(1<<(level));
        int flag_index  = (index_z/actual_node_width)*(cloud_one_dim_size/actual_node_width)*(cloud_one_dim_size/actual_node_width)
                        + (index_y/actual_node_width)*(cloud_one_dim_size/actual_node_width)
                        + (index_x/actual_node_width);

        //Operacja CAS na fladze
        int old_flag_value = atomicCAS(&flag[flag_index],0,1);

        //Jezeli to zwycięski thread
        if (old_flag_value==0)
        {
            //Zmienne pomocnicze
            int actual_node_index=0;
            int actual_node_xy=0;
            int parent_x_half=cloud_one_dim_size;
            int parent_y_half=cloud_one_dim_size;
            int parent_z_half=cloud_one_dim_size;

            //Znajdujemy index rodzica
            for (int i=0; i<level; i++)
            {
                parent_x_half = octree_nodes_data[actual_node_index].x_half;
                parent_y_half = octree_nodes_data[actual_node_index].y_half;
                parent_z_half = octree_nodes_data[actual_node_index].z_half;
                actual_node_xy = (index_z/parent_z_half)*4 + (index_y/parent_y_half)*2 + (index_x/parent_x_half);
                actual_node_index = octree_nodes_data[actual_node_index].children_index + actual_node_xy;
            }

            //Inkrementujemy adres o jeden
            int first_child_index = atomicAdd(actual_first_index,8);

            //Zmienne pomicnicze dla rodzica
            int xy_factor = actual_node_width/2;
            int x_factor = ((actual_node_xy%2)) ? 1 : -1;
            int y_factor = (((actual_node_xy%4)/2)) ? 1 : -1;
            int z_factor = ((actual_node_xy/4)) ? 1 : -1;

            //Przypisujemy wartości rodzica
            octree_nodes_data[actual_node_index].value=4;
            octree_nodes_data[actual_node_index].children_index=first_child_index;
            octree_nodes_data[actual_node_index].x_half=parent_x_half + x_factor*xy_factor;
            octree_nodes_data[actual_node_index].y_half=parent_y_half + y_factor*xy_factor;
            octree_nodes_data[actual_node_index].z_half=parent_z_half + z_factor*xy_factor;

        }
    }
}

/**
 *  @brief      Assign builded octree with values from converted cloud vector.
 *  @param[out] octree_nodes_data       Nodes array.
 *  @param[in]  converted_cloud         Char vector valued as in My_Octree::value.
 *  @param[out] cloud_points_number     Points number in cloud.
 */
__global__ void gpu_assign(My_Octree::d_OctreeNode* octree_nodes_data, char* converted_cloud, int cloud_points_number)
{
    //Definicja indeksu
    int index_x = (blockIdx.x % gridDim.y) * blockDim.x + threadIdx.x;
    int index_y = blockIdx.y * blockDim.y + threadIdx.y;
    int index_z = (blockIdx.x / gridDim.y) * blockDim.z + threadIdx.z;
    int index   = index_z*gridDim.y*blockDim.x*gridDim.y*blockDim.y + index_y*gridDim.y*blockDim.x + index_x;

    //Znajdujemy index rodzica
    int node_index=0;
    while (octree_nodes_data[node_index].value==4)
    {
        int help_index = (index_z/octree_nodes_data[node_index].z_half)*4 + (index_y/octree_nodes_data[node_index].y_half)*2 + (index_x/octree_nodes_data[node_index].x_half);
        node_index = octree_nodes_data[node_index].children_index + help_index;
    }

    //Przypisywanie wartości
    octree_nodes_data[node_index].value= (index < cloud_points_number) ? converted_cloud[index] : 1;
}

/**
 *  @brief      Ask octree about visibility of specific points.
 *  @param[in]  octree_node_data    Nodes array.
 *  @param[in]  pointx              Vector of x coordinate of border points to ask.
 *  @param[in]  pointy              Vector of y coordinate of border points to ask.
 *  @param[in]  pointz              Vector of z coordinate of border points to ask.
 *  @param[in]  points_number       Total points to ask number.
 *  @param[out] visibility          Vector of asked points state.
 */
__global__ void gpu_getPointsVisibilityVoxelCoordinates(My_Octree::d_OctreeNode* octree_nodes_data, int* pointx, int* pointy, int* pointz, int* points_number, bool* visibility)
{
    //Zdobywamy współrzędne punktu
    int index   = blockIdx.x*blockDim.x + threadIdx.x;
    int index_x = pointx[index];
    int index_y = pointy[index];
    int index_z = pointz[index];

    //Jezeli sa jeszcze punkty do spytania
    if (index<*points_number)
    {
        //Znajdujemy index rodzica
        int node_index=0;
        while (octree_nodes_data[node_index].value==4)
        {
            int help_index = (index_z/octree_nodes_data[node_index].z_half)*4 + (index_y/octree_nodes_data[node_index].y_half)*2 + (index_x/octree_nodes_data[node_index].x_half);
            node_index = octree_nodes_data[node_index].children_index + help_index;
        }

        //Przypisywanie wartości
        if (octree_nodes_data[node_index].value==3) visibility[index]=true;
    }
}

int My_Octree::build(const pcl::PointCloud<pcl::PointXYZI>::ConstPtr &cloud_ptr, int cloud_one_dim, float voxel_size)
{
    //===========================================================================================
    //KROK 0 - SPRAWDZANIE ROZMIARU CHMURY ======================================================
    //===========================================================================================

    if (compute_capability>=20 && cloud_one_dim>1024) return 1;
    if (compute_capability<20 && cloud_one_dim>512) return 1;

    //===========================================================================================
    //KROK 1 - DEKLARACJA ZMIENNYCH POMOCNICZYCH ================================================
    //===========================================================================================

    int D;                                                                      //Maksymalna głębokość drzewa
    int N;                                                                      //Ilość node'ów granicznych
    int cloud_points_number = cloud_one_dim*cloud_one_dim*cloud_one_dim;        //Całkowia ilość punktów chmury
    voxelSize = voxel_size;

    //===========================================================================================
    //KROK 2 - PRZERZUCAMY CHMURĘ NA GPU ========================================================
    //===========================================================================================

    float* cloud = new float[cloud_points_number];
    //Iteracja po wszystkich punktach chmury=============================================
    for( int i=0; i<cloud_points_number; i++ )
    {
        int z_index = cloud_ptr->at(i).z * cloud_one_dim * cloud_one_dim;
        int y_index = cloud_ptr->at(i).y * cloud_one_dim;
        int x_index = cloud_ptr->at(i).x;
        cloud[z_index + y_index + x_index] = cloud_ptr->at(i).intensity;
    }

    //===========================================================================================
    //KROK 2 - PRZERZUCAMY CHMURĘ NA GPU ========================================================
    //===========================================================================================

    //Deklaracja i alokowanie pamięci na chmurę na GPU
    float *d_cloud;
    cudaMalloc((void**)&d_cloud,cloud_points_number*sizeof(float));

    //Kopiowanie pamięci z chmury CPU na GPU
    cudaMemcpy((void*)d_cloud, (void*)cloud, cloud_points_number*sizeof(float), cudaMemcpyHostToDevice);

    //Zwalnianie pamięci
    delete cloud;

    //===========================================================================================
    //KROK 3 - KONWERTUJEMY CHMURE TSDF =========================================================
    //===========================================================================================

    //Deklaracja i alokowanie pamięci na przekonwertowaną chmurę na GPU
    char *d_converted_cloud;
    cudaMalloc((void**)&d_converted_cloud,cloud_points_number*sizeof(char));
    cudaMemset((void*)d_converted_cloud,0,cloud_points_number*sizeof(char));

    //Obliczenie ilości bloków
    int convert_thread_count = 8;
    int convert_block_count = 1;
    if (cloud_one_dim>=convert_thread_count) convert_block_count = (cloud_one_dim/convert_thread_count);
    else convert_thread_count = cloud_one_dim;
    dim3 convert_block_grid(convert_block_count*convert_block_count,convert_block_count,1);
    dim3 convert_thread_grid(convert_thread_count,convert_thread_count,convert_thread_count);

    int* d_border_voxels_number;
    cudaMalloc((void**)&d_border_voxels_number,sizeof(int));
    cudaMemset((void*)d_border_voxels_number,0,sizeof(int));

    //Kernel konwertujący chmurę
    gpu_convert_cloud<<<convert_block_grid,convert_thread_grid>>>(d_cloud,d_converted_cloud,d_border_voxels_number); //BARDZO NIEEKONOMICZNIE! PRZEROBIĆ Z WYKORZYSTANIEM OPERACJI REDUCE!

    //Kopiowanie pamieci
    cudaMemcpy((void*)&N,(void*)d_border_voxels_number,sizeof(int),cudaMemcpyDeviceToHost);

    //Zwalnianie zasobów
    cudaFree(d_border_voxels_number);
    cudaFree(d_cloud);

    //===========================================================================================
    //KROK 4 - OBLICZANIE GŁĘBOKOŚCI DRZEWA =====================================================
    //===========================================================================================

    int cloud_one_dim_extended = 1;
    while (cloud_one_dim_extended<cloud_one_dim) cloud_one_dim_extended<<=1;
    D = (int) (log(cloud_one_dim_extended)/log(2));

    //===========================================================================================
    //KROK 6 - KONWERSJA CHMURY TYLKO NA PUNKTY GRANICZNE =======================================
    //===========================================================================================

    //Deklaracja wektorów punktów granicznych
    int* d_pointx;
    cudaMalloc((void**)&d_pointx,N*sizeof(int));
    cudaMemset((void*)d_pointx,0,N*sizeof(int));

    int* d_pointy;
    cudaMalloc((void**)&d_pointy,N*sizeof(int));
    cudaMemset((void*)d_pointy,0,N*sizeof(int));

    int* d_pointz;
    cudaMalloc((void**)&d_pointz,N*sizeof(int));
    cudaMemset((void*)d_pointz,0,N*sizeof(int));

    int* d_first_free_points_index;
    cudaMalloc((void**)&d_first_free_points_index,sizeof(int));
    cudaMemset((void*)d_first_free_points_index,0,sizeof(int));

    //Kernel
    gpu_get_border_voxels<<<convert_block_grid,convert_thread_grid>>>(d_converted_cloud,d_pointx,d_pointy,d_pointz,d_first_free_points_index);

    //===========================================================================================
    //KROK 7 - PRZYGOTOWANIE BUDOWANIA ==========================================================
    //===========================================================================================

    //Adres
    int first_free_node_index=1;
    int* d_first_free_node_index;
    cudaMalloc((void**)&d_first_free_node_index,sizeof(int));
    cudaMemcpy((void*)d_first_free_node_index,(void*)&first_free_node_index,sizeof(int),cudaMemcpyHostToDevice);

    //Alokowanie danych nodów
    cudaMalloc((void**)&d_octree_nodes_data,(N*8*D+1)*sizeof(d_OctreeNode));
    cudaMemset((void*)d_octree_nodes_data,0,(N*8*D+1)*sizeof(d_OctreeNode));

    //Ilość flag
    int flag_number = (1<<(3*(D-1)));
    int* d_flag;
    cudaMalloc((void**)&d_flag,(flag_number)*sizeof(int));
    cudaMemset((void*)d_flag,0,(flag_number)*sizeof(int));

    //===========================================================================================
    //KROK 8 - BUDOWANIE ========================================================================
    //===========================================================================================

    //Obliczenie ilości bloków
    int thread_count2 = (compute_capability>=20) ? 1024 : 512;
    int block_count2 = 1;
    if (N>=thread_count2) block_count2 = (N/thread_count2)+((N%thread_count2));
    else thread_count2 = N;
    dim3 block_grid2(block_count2,1,1);
    dim3 thread_grid2(thread_count2,1,1);

    //Kernele
    for (int level=0; level<D; level++)
    {
        gpu_build<<<block_grid2,thread_grid2>>>(d_octree_nodes_data,d_pointx,d_pointy,d_pointz,d_first_free_points_index,d_first_free_node_index,cloud_one_dim_extended, d_flag,level);
        cudaMemset((void*)d_flag,0,flag_number*sizeof(int));
    }
    gpu_assign<<<convert_block_grid,convert_thread_grid>>>(d_octree_nodes_data,d_converted_cloud,cloud_points_number);

    //Zwalnianie zasobów
    cudaFree(d_pointx);
    cudaFree(d_pointy);
    cudaFree(d_converted_cloud);
    cudaFree(d_flag);

    //===========================================================================================
    //KROK 9 - RESIZE ===========================================================================
    //===========================================================================================

    //Pobranie aktualnego pierwszego wolnego indeksu
    cudaMemcpy((void*)&first_free_node_index,(void*)d_first_free_node_index,sizeof(int),cudaMemcpyDeviceToHost);

    //Help variable
    d_OctreeNode *d_help;
    cudaMalloc((void**)&d_help,(first_free_node_index)*sizeof(d_OctreeNode));

    //Kopiowanie w prawo
    cudaMemcpy((void*)d_help,(void*)d_octree_nodes_data,first_free_node_index*sizeof(My_Octree::d_OctreeNode),cudaMemcpyDeviceToDevice);

    //Realokowanie
    cudaFree((void*)d_octree_nodes_data);
    cudaMalloc((void**)&d_octree_nodes_data,(first_free_node_index)*sizeof(d_OctreeNode));

    //Kopiowanie w lewo
    cudaMemcpy((void*)d_octree_nodes_data,(void*)d_help,first_free_node_index*sizeof(My_Octree::d_OctreeNode),cudaMemcpyDeviceToDevice);

    //Zwalnianie zasobów
    cudaFree((void*)d_help);

    //===========================================================================================
    //KROK 10 - ZWALNIANIE ZASOBÓW ==============================================================
    //===========================================================================================
    builded=true;
    cudaFree(d_first_free_node_index);

    return 0;
}
int My_Octree::getPointsVisibility(const pcl::PointCloud<pcl::PointXYZRGBNormal> &cloud, int* visible_points_number)
{
    int points_number = cloud.size()*27;
    int* h_pointx=new int[points_number];
    int* h_pointy=new int[points_number];
    int* h_pointz=new int[points_number];

    for (int i=0;i<cloud.size();i++)
    {
        pcl::PointXYZRGBNormal point;
        point = cloud.at(i);
        int X = (point.x-voxelSize/2)/voxelSize-1;
        int Y = (point.y-voxelSize/2)/voxelSize-1;
        int Z = (point.z-voxelSize/2)/voxelSize-1;

        for (int z=0; z<3; z++)
            for (int y=0; y<3; y++)
                for (int x=0; x<3; x++)
                {
                    h_pointx[i*27 + z*9 + y*3 + x] = X + x;
                    h_pointy[i*27 + z*9 + y*3 + x] = Y + y;
                    h_pointz[i*27 + z*9 + y*3 + x] = Z + z;
                }
    }

    //===========================================================================================
    //KROK 1 - DEKLARACJA PAMIĘCI GPU ===========================================================
    //===========================================================================================

    int* d_pointx;
    cudaMalloc((void**)&d_pointx,points_number*sizeof(int));
    cudaMemcpy((void*)d_pointx,(void*)h_pointx,points_number*sizeof(int),cudaMemcpyHostToDevice);

    int* d_pointy;
    cudaMalloc((void**)&d_pointy,points_number*sizeof(int));
    cudaMemcpy((void*)d_pointy,(void*)h_pointy,points_number*sizeof(int),cudaMemcpyHostToDevice);

    int* d_pointz;
    cudaMalloc((void**)&d_pointz,points_number*sizeof(int));
    cudaMemcpy((void*)d_pointz,(void*)h_pointz,points_number*sizeof(int),cudaMemcpyHostToDevice);

    int* d_points_number;
    cudaMalloc((void**)&d_points_number,sizeof(int));
    cudaMemcpy((void*)d_points_number,(void*)&points_number,sizeof(int),cudaMemcpyHostToDevice);

    bool* d_visibility;
    cudaMalloc((void**)&d_visibility,points_number*sizeof(bool));
    cudaMemset((void*)d_visibility,0,points_number*sizeof(bool));

    delete h_pointx;
    delete h_pointy;
    delete h_pointz;

    //===========================================================================================
    //KROK 2 - PRZYGOTOWYWANIE KERNELA ==========================================================
    //===========================================================================================

    //Obliczenie ilości bloków
    int thread_count = (compute_capability>=20) ? 1024 : 256;
    int block_count = 1;
    if (points_number>=thread_count) block_count = (points_number/thread_count);
    else thread_count = points_number;
    dim3 block_grid(block_count,1,1);
    dim3 thread_grid(thread_count,1,1);

    //===========================================================================================
    //KROK 3 - KERNEL ===========================================================================
    //===========================================================================================

    gpu_getPointsVisibilityVoxelCoordinates<<<block_grid,thread_grid>>>(d_octree_nodes_data,d_pointx,d_pointy,d_pointz,d_points_number,d_visibility);

    //===========================================================================================
    //KROK 3 - ZLICZANIE ========================================================================
    //===========================================================================================

    bool* h_visibility = new bool[points_number];
    cudaMemcpy((void*)h_visibility,(void*)d_visibility,points_number*sizeof(bool),cudaMemcpyDeviceToHost);

    int count=0;
    for (int i=0; i<cloud.size(); i++)
    {
        int suma = 0;
        for (int j=0; j<27; j++) suma += (h_visibility[i*27 + j]) ? 1 : 0;
        if (suma==27) count++;
    }
    *visible_points_number = count;

    //===========================================================================================
    //KROK 3 - DEALOKOWANIE =====================================================================
    //===========================================================================================
    cudaFree(d_pointx);
    cudaFree(d_pointy);
    cudaFree(d_pointz);
    cudaFree(d_points_number);
    cudaFree(d_visibility);

    return 0;
}
