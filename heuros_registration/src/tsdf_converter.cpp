#include <tsdf_converter.h>

bool TSDFConverter::load(const std::string &filename, bool binary)
{
    //Load flag
    loaded = false;

    //File
    std::ifstream file (filename.c_str());

    //If open
    if (file.is_open())
    {
        //If binary
        if (binary)
        {
            //Read header
            file.read ((char*) &header_, sizeof (Header));

            //If volume element is not float
            if (header_.volume_element_size != sizeof(float))
            {
                pcl::console::print_error ("[TSDFVolume::load] Error: Given volume element size (%d) doesn't fit data (%d)", sizeof(float), header_.volume_element_size);
                return false;
            }

            //If weight element is not short
            if ( header_.weights_element_size != sizeof(short))
            {
                pcl::console::print_error ("[TSDFVolume::load] Error: Given weights element size (%d) doesn't fit data (%d)", sizeof(short), header_.weights_element_size);
                return false;
            }

            //Read data
            int num_elements = this->getTotalVoxelNumber();
            volume_->resize (num_elements);
            weights_->resize (num_elements);
            file.read ((char*) &(*volume_)[0], num_elements * sizeof(float));
            file.read ((char*) &(*weights_)[0], num_elements * sizeof(short));
        }

        //If not binary - error
        else pcl::console::print_error ("[TSDFVolume::load] Error: ASCII loading not implemented.\n");

        //Close file
        file.close();
    }
    //If file not open
    else
    {
        pcl::console::print_error ("[TSDFVolume::load] Error: Cloudn't read file %s.\n", filename.c_str());
        return false;
    }

    //Return
    loaded=true;
    return true;
}
bool TSDFConverter::save(const std::string &filename, bool binary, int divisor) const
{
    //File variable
    std::ofstream file (filename.c_str(), binary ? std::ios_base::binary : std::ios_base::out);

    //If file is open
    if (file.is_open())
    {
        //If file is in binary format
        if (binary)
        {
            //Write header
            file.write ((char*) &header_, sizeof (Header));

            //Write data
            file.write ((char*) &(volume_->at(0)), volume_->size()*sizeof(float));
            file.write ((char*) &(weights_->at(0)), weights_->size()*sizeof(short));
        }
        else
        {
            //Write header information
            file << header_.resolution(0) << " " << header_.resolution(1) << " " << header_.resolution(2) << std::endl;
            file << header_.volume_size(0) << " " << header_.volume_size(1) << " " << header_.volume_size(2) << std::endl;
            file << sizeof (float) << " " << sizeof(short) << std::endl;

            //Write data
            for (typename std::vector<float>::const_iterator iter = volume_->begin(); iter < volume_->end();)
            {
                if (*iter<0) file << *iter << std::endl;
                iter+=divisor;
            }
        }

        //Close file
        file.close();
    }
    //If is not open
    else
    {
      pcl::console::print_error ("[saveTsdfVolume] Error: Couldn't open file %s.\n", filename.c_str());
      return false;
    }

    //Return
    return true;
}
bool TSDFConverter::saveTSDFInPCD(const std::string &filename, bool color, bool binary, int divisor) const
{
    //Open file
    std::ofstream file (filename.c_str(), binary ? std::ios_base::binary : std::ios_base::out);

    //Create rgb point cloud
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_rgb;
    cloud_rgb = pcl::PointCloud<pcl::PointXYZRGB>::Ptr (new pcl::PointCloud<pcl::PointXYZRGB>);

    //Create intensity point cloud
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud_i;
    cloud_i = pcl::PointCloud<pcl::PointXYZI>::Ptr (new pcl::PointCloud<pcl::PointXYZI>);

    //Convert point cloud
    if (color) convertTSDFToPCDCloud(cloud_rgb,divisor);
    else convertTSDFToPCDCloud(cloud_i,divisor);

    //Open file
    if (file.is_open())
    {
        //Save file in binary format
        if (binary)
        {
            if (color) pcl::io::savePCDFileBinary(filename, *cloud_rgb);
            else pcl::io::savePCDFileBinary(filename, *cloud_i);
            if (color) pcl::io::savePCDFileBinary(filename, *cloud_rgb);
            else pcl::io::savePCDFileBinary(filename, *cloud_i);
        }
        //Save file in text format
        else
        {
            if (color) pcl::io::savePCDFileASCII(filename, *cloud_rgb);
            else pcl::io::savePCDFileASCII(filename, *cloud_i);
        }

        //Close file
        file.close();
    }
    //If file is not open
    else
    {
        pcl::console::print_error ("[saveTsdfVolume] Error: Couldn't open file %s.\n", filename.c_str());
        return false;
    }

    //Return
    return true;
}
bool TSDFConverter::convertTSDFToPCDCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, int divisor) const
{
    //Set point clud parameters
    const int cloud_size = this->getTotalVoxelNumber() / (divisor*divisor*divisor);
    cloud->clear();
    cloud->reserve (std::min (cloud_size/10, 500000));

    //Get voxel number
    int sx = header_.resolution(0);
    int sy = header_.resolution(1);
    int sz = header_.resolution(2);

    //Help
    int volume_idx = 0;

    //Loop after every point in point cloud
    for (int z = 0; z < sz; z+=divisor)
        for (int y = 0; y < sy; y+=divisor)
            for (int x = 0; x < sx; x+=divisor)
            {
                //Count voxel global position
                volume_idx = sx*sy*z + sx*y + x;

                //Add voxel to point cloud
                pcl::PointXYZRGB point_rgb;
                point_rgb.x = x; point_rgb.y = y; point_rgb.z = z;
                point_rgb.r = volume_->at(volume_idx);
                point_rgb.g = 0;
                point_rgb.b = 0;
                cloud->push_back (point_rgb);
            }

    //Return
    return true;
}
bool TSDFConverter::convertTSDFToPCDCloud(pcl::PointCloud<pcl::PointXYZI>::Ptr cloud, int divisor) const
{
    //Set point cloud parameters
    const int cloud_size = this->getTotalVoxelNumber() / (divisor*divisor*divisor);
    cloud->clear();
    cloud->reserve (std::min (cloud_size/10, 500000));

    //Get voxel number
    int sx = header_.resolution(0);
    int sy = header_.resolution(1);
    int sz = header_.resolution(2);

    //Help
    int volume_idx = 0;

    //Loop after every point in point cloud
    for (int z = 0; z < sz; z+=divisor)
        for (int y = 0; y < sy; y+=divisor)
            for (int x = 0; x < sx; x+=divisor)
            {
                //Count voxel position
                volume_idx = sx*sy*z + sx*y + x;

                //Add voxel to point cloud
                pcl::PointXYZI point_i;
                point_i.x = x; point_i.y = y; point_i.z = z;
                point_i.intensity = volume_->at(volume_idx);
                cloud->push_back (point_i);
            }

    //Return
    return true;
}
