#include <aligner.h>
#include <pcl/point_types.h>

#include "tsdf_converter.h"
#include "my_octree.hpp"


bool fileExists( string file_name )
{
    ifstream infile( file_name.c_str() );
    return infile.good();
}

Aligner::Aligner()
{
    cudaEventCreate( &start_all );
    cudaEventCreate( &stop_mem );
    cudaEventCreate( &stop_downsample );
    cudaEventCreate( &stop_features );
    cudaEventCreate( &stop_align );
}

void Aligner::setPrerejection( int prerejection )
{
    ransac.setPrerejection( prerejection );
}

int Aligner::getPrerejection()
{
    return ransac.getPrerejection();
}

void Aligner::align()
{
    cout << "Performing alignment..." << endl;

    if( !target_metacloud ){ cout << "Registration error: Target empty!" << endl; return; }
    if( !source_metacloud ){ cout << "Registration error: Source empty!" << endl; return; }

    timerStart( start_all );

    // Point clouds
    PointCloudT::Ptr object (new PointCloudT);
    object_aligned = PointCloudT::Ptr(new PointCloudT);
    PointCloudT::Ptr scene (new PointCloudT);
    FeatureCloudT::Ptr object_features (new FeatureCloudT);
    FeatureCloudT::Ptr scene_features (new FeatureCloudT);

    // Fill data
    if( !target_indices ){
        scene = target_metacloud->cloud_xyzrgbn;
    }else{
        for( int i=0; i<target_indices->size(); i++ )
            scene->push_back( target_metacloud->cloud_xyzrgbn->at( target_indices->at(i) ) );
    }
    for( int i=0; i<source_indices->size(); i++ )
        object->push_back( source_metacloud->cloud_xyzrgbn->at( source_indices->at(i) ) );

    cout << "Clouds created in : " << fixed << timerStop( start_all, stop_mem ) << " ms." << endl;

    // Downsample
    pcl::console::print_highlight ("Downsampling...\n");
    pcl::VoxelGrid<PointNT> grid;
    const float leaf = 0.005f;
    grid.setLeafSize (leaf, leaf, leaf);
    grid.setInputCloud (object);
    grid.filter (*object);
    grid.setInputCloud (scene);
    grid.filter (*scene);

    cout << "Clouds downsampled in : " << fixed << timerStop( stop_mem, stop_downsample ) << " ms." << endl;

    // Estimate features
    pcl::console::print_highlight ("Estimating features...\n");
    FeatureEstimationT fest;
    fest.setRadiusSearch (0.025);
    fest.setInputCloud (object);
    fest.setInputNormals (object);
    fest.compute (*object_features);
    fest.setInputCloud (scene);
    fest.setInputNormals (scene);
    fest.compute (*scene_features);

    cout << "FPFH featurs calculated in : " << fixed << timerStop( stop_downsample, stop_features ) << " ms." << endl;


    // Set TSDF path
    string tsdf_name = "";
    for( int i=0; true; i++ ){
        char ch = target_scene_name[i];
        if( ch == '.' ) break;
        tsdf_name.push_back( ch );
    }
    tsdf_name = tsdf_name + ".dat";
    string tsdf_path = ros::package::getPath("heuros_registration") + "/../data/scenes/" + tsdf_name;

    /*
    //Open file
    My_Octree octree;
    TSDFConverter file;
    if (!file.load(tsdf_path))
    {
        cout << "Can't open file!" << endl;
        return ;
    }

    //Convert to pcd cloud
    pcl::PointCloud<pcl::PointXYZI>::Ptr tsdf_cloud;
    tsdf_cloud = pcl::PointCloud<pcl::PointXYZI>::Ptr (new pcl::PointCloud<pcl::PointXYZI>);
    file.convertTSDFToPCDCloud(tsdf_cloud);

    //Build octree
    int voxelsNumber = (int)file.getVoxelNumber()[0];
    float voxelSize  = ((float)file.getVoxelSize()[0])/voxelsNumber;

    if (octree.build(tsdf_cloud,voxelsNumber,voxelSize))
    {
        cout << "Octree build failed!!" << endl;
        return ;
    } else cout << "Octree build succed!!" << endl;
*/

    // Perform alignment
    pcl::console::print_highlight ("Starting alignment...\n");
    ransac.setInputSource (object);
    ransac.setSourceFeatures (object_features);
    ransac.setInputTarget (scene);
    ransac.setTargetFeatures (scene_features);
    ransac.setNumberOfSamples (3);                          // Number of points to sample for generating/prerejecting a pose
    ransac.setCorrespondenceRandomness (2);                 // Number of nearest features to use
    ransac.setSimilarityThreshold (0.6f);                   // Polygonal edge length similarity threshold
    ransac.setMaxCorrespondenceDistance (1.5f * leaf);      // Set inlier threshold
    //ransac.setPrerejection(3);                              // Set both prerejectors (already done in constructor)

    ransac.setMaximumIterations(10000);                     // Set iterations number
    ransac.setInlierFraction (0.30f);                       // Set required inlier fraction

    //Ransac!
    if (ransac.getPrerejection()<2)
    {
        ransac.align (*object_aligned);
        pcl::console::print_highlight ("Prerejection ratio = checked count / prerejection count / hypotesis count = %i / %i / %i . Percentage = %f \n", ransac.getHypotesisCount() - ransac.getPrerejectionsCount() ,ransac.getPrerejectionsCount(), ransac.getHypotesisCount(), (float)100*ransac.getPrerejectionsCount() / ransac.getHypotesisCount());
    } else if (ransac.setTargetTSDF(tsdf_path))
    {
        ransac.align (*object_aligned);
        pcl::console::print_highlight ("Prerejection ratio = checked count / prerejection count / hypotesis count = %i / %i / %i . Percentage = %f \n", ransac.getHypotesisCount() - ransac.getPrerejectionsCount() ,ransac.getPrerejectionsCount(), ransac.getHypotesisCount(), (float)100*ransac.getPrerejectionsCount() / ransac.getHypotesisCount());
    } else
    {
        cout << "Error: The TSDF file does not exist.";
        return;
    }

    float align_time = timerStop( stop_features, stop_align );

    //////////////////////////
    ////// SAVE RESULTS //////
    //////////////////////////

    // Compose file name
    int file_no = 1;
    string path;
    do{
        stringstream ss; ss << file_no++ << ".mat";
        path = ros::package::getPath("heuros_registration") + "/../data/registration/" + ss.str();
    }while( fileExists(path) );

    // Create and write histogram file
    ofstream file( path.c_str() );
    file << "# name: alignment_success" << endl;
    file << "# type: scalar" << endl;

    if (ransac.hasConverged ())
    {
        // Print results
        //Eigen::Matrix4f transformation = align.getFinalTransformation ();
        //pcl::console::print_info ("    | %6.3f %6.3f %6.3f | \n", transformation (0,0), transformation (0,1), transformation (0,2));
        //pcl::console::print_info ("R = | %6.3f %6.3f %6.3f | \n", transformation (1,0), transformation (1,1), transformation (1,2));
        //pcl::console::print_info ("    | %6.3f %6.3f %6.3f | \n", transformation (2,0), transformation (2,1), transformation (2,2));
        //pcl::console::print_info ("\n");
        //pcl::console::print_info ("t = < %0.3f, %0.3f, %0.3f >\n", transformation (0,3), transformation (1,3), transformation (2,3));
        //pcl::console::print_info ("\n");

        float inliers = float( ransac.getInliers().size() ) / object->size();

        file << 1 << endl << endl;

        file << "# name: num_hypotheses" << endl;
        file << "# type: scalar" << endl;
        file << ransac.getHypotesisCount() << endl << endl;

        file << "# name: num_tested" << endl;
        file << "# type: scalar" << endl;
        file << ransac.getHypotesisCount() - ransac.getPrerejectionsCount() << endl << endl;

        file << "# name: time" << endl;
        file << "# type: scalar" << endl;
        file << align_time << endl << endl;

        file << "# name: inliers" << endl;
        file << "# type: scalar" << endl;
        file <<  inliers << endl << endl;

        file << "# name: fitness_score" << endl;
        file << "# type: scalar" << endl;
        file <<  ransac.getFitnessScore() << endl << endl << endl << endl;

        cout << "ALIGNMENT DONE." << endl;
        cout << "TOTAL HYPOTHESES: " << ransac.getHypotesisCount() << endl;
        cout << "TESTED HYPOTHESES: " << ransac.getHypotesisCount() - ransac.getPrerejectionsCount() << endl;
        cout << "TIME: " << fixed << align_time << " ms." << endl;
        cout << "INLIERS: " << ransac.getInliers ().size () << " / " << object->size () << endl;
        cout << "FITNESS SCORE: " << ransac.getFitnessScore() << endl << endl;

        // Show alignment
        //pcl::visualization::PCLVisualizer visu("Alignment");
        //visu.addPointCloud (scene, ColorHandlerT (scene, 0.0, 255.0, 0.0), "scene");
        //visu.addPointCloud (object_aligned, ColorHandlerT (object_aligned, 0.0, 0.0, 255.0), "object_aligned");
        //visu.spin ();
    }
    else
    {
        file << 0 << endl;
        cout << "Alignment failed!" << endl;
    }

    file.close();
}

void Aligner::timerStart( cudaEvent_t _start )
{
    cudaEventRecord( _start, 0 );
}

float Aligner::timerStop( cudaEvent_t _start, cudaEvent_t _stop )
{
    float time;
    cudaEventRecord( _stop, 0 );
    cudaEventSynchronize( _stop );
    cudaEventElapsedTime( &time, _start, _stop );
    return time;
}
